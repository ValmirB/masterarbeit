function EE_Rot = f_EE_Rot(t, x_, varargin)
% f_EE_Rot - definition of time-depending user-defined variable EE_Rot

global sys;


% constant user-defined variables
EE_Rot_d = sys.parameters.data.EE_Rot_d;
EE_Rot = zeros(1,1);

EE_Rot(1) = EE_Rot_d;

% END OF FILE

