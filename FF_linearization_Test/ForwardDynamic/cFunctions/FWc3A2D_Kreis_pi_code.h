#ifndef FWc3A2D_Kreis_pi_ind_H
#define FWc3A2D_Kreis_pi_ind_H

#include "FWc3A2D_Kreis_paraStruct.h"

/* System dynamics */
void FWc3A2D_Kreis_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr);

/* Auxiliary dynamics */
void FWc3A2D_Kreis_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

/* Equations of motion */
void FWc3A2D_Kreis_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

#endif
