#ifndef FWc3A2D_Kreis_Flexor_H
#define FWc3A2D_Kreis_Flexor_H
#define FWC3A2D_KREIS_SYS_DOF (4)
#define FWC3A2D_KREIS_AUX_DOF (0)
#define FWC3A2D_KREIS_LOOPS_EXIST (1)
#define FWC3A2D_KREIS_NUM_LOOPS (2)
#define FWC3A2D_KREIS_NUM_POSCONSTRAINTS (2)
#define FWC3A2D_KREIS_NUM_VELCONSTRAINTS (0)
#define FWC3A2D_KREIS_NUM_INDEPENDENT_GC (2)
#define FWC3A2D_KREIS_NUM_DEPENDENT_GC (2)
#define FWC3A2D_KREIS_NUM_INDEPENDENT_GV (2)
#define FWC3A2D_KREIS_NUM_DEPENDENT_GV (2)
#define FWC3A2D_KREIS_NUM_FELEM (2)

#define FWC3A2D_KREIS_NUM_INPUT_GROUPS (2)
#define FWC3A2D_KREIS_NUM_INPUTS_CONTROL (9)
#define FWC3A2D_KREIS_NUM_INPUTS_CONTACTFORCE (2)
#define FWC3A2D_KREIS_NUM_INPUTS {9, 2}
#define FWC3A2D_KREIS_NUM_ALL_INPUTS (11)
#define FWC3A2D_KREIS_NUM_OUTPUT_GROUPS (3)
#define FWC3A2D_KREIS_NUM_OUTPUTS_EXACTOUT (6)
#define FWC3A2D_KREIS_NUM_OUTPUTS_APPROXOUT (6)
#define FWC3A2D_KREIS_NUM_OUTPUTS_CURVATURE (4)
#define FWC3A2D_KREIS_NUM_OUTPUTS {6, 6, 4}
#define FWC3A2D_KREIS_NUM_ALL_OUTPUTS (16)
#endif
