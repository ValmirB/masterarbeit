#include <math.h>
#include "FWc3A2D_Kreis_paraStruct.h"
#include "FWc3A2D_Kreis_pi_code.h"
#include "FWc3A2D_Kreis_pd_matlab.h"
#include "FWc3A2D_Kreis_userDefined.h"
#include "neweul.h"

void FWc3A2D_Kreis_educatedGuess(double t, double *y_, void *dataPtr);

void FWc3A2D_Kreis_educatedGuess(double t, double *y_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
