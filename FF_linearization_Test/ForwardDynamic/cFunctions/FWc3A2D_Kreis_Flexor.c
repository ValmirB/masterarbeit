#define S_FUNCTION_NAME FWc3A2D_Kreis_Flexor
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include "FWc3A2D_Kreis_paraStruct.h"
#include "FWc3A2D_Kreis_pi_code.h"
#include "FWc3A2D_Kreis_Flexor.h"
#include "FWc3A2D_Kreis_userDefined.h"
#include "FWc3A2D_Kreis_constraintEquations.h"
#ifdef  MATLAB_MEX_FILE
#include "FWc3A2D_Kreis_pd_matlab.h"
#endif
#include "neweul.h"

#define IS_PARAM_DOUBLE(pVal) (mxIsNumeric(pVal) && !mxIsLogical(pVal) && !mxIsEmpty(pVal) && !mxIsSparse(pVal) && !mxIsComplex(pVal) && mxIsDouble(pVal))

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)

static void mdlCheckParameters(SimStruct *S) {
	if (!mxIsEmpty(ssGetSFcnParam(S,0))){
		if (!mxIsStruct(ssGetSFcnParam(S,0))) {
			ssSetErrorStatus(S,"First parameter to S-function must be sys.parameters.data");
			return;
		}
	}
	if (mxGetNumberOfElements(ssGetSFcnParam(S,1)) != FWC3A2D_KREIS_SYS_DOF) {
		ssSetErrorStatus(S,"Second parameter to S-function must be 4x1 vector containing the initial values of the generalized coordinates");
		return;
	}
	if (mxGetNumberOfElements(ssGetSFcnParam(S,2)) != FWC3A2D_KREIS_SYS_DOF) {
		ssSetErrorStatus(S,"Third parameter to S-function must be 4x1 vector containing the initial values of the generalized velocities");
		return;
	}
}
#endif

/* INITIALIZE SIZES */

#define MDL_INITIALIZE_SIZES

static void mdlInitializeSizes(SimStruct *S) {
	int j_ = 0;
#if FWC3A2D_KREIS_NUM_INPUT_GROUPS > 0
	int inputGroups[] = FWC3A2D_KREIS_NUM_INPUTS;
#endif
	int outputGroups[] = FWC3A2D_KREIS_NUM_OUTPUTS;
	ssSetNumSFcnParams(S, 3); /* 1 S-function parameter */
#if defined(MATLAB_MEX_FILE)
	if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
		mdlCheckParameters(S);
		if (ssGetErrorStatus(S) != NULL) {
			return;
		}
	} else {
		return; /* Parameter mismatch will be reported by Simulink */
	}
#endif

	ssSetNumContStates(S, 2*FWC3A2D_KREIS_SYS_DOF); /* Number of continous states */
	ssSetNumDiscStates(S, 0); /* Number of discrete states */

	if (!ssSetNumInputPorts(S, FWC3A2D_KREIS_NUM_INPUT_GROUPS)) return; /* Number of S-function inputs */
#if FWC3A2D_KREIS_NUM_INPUT_GROUPS > 0
	for (j_=0; j_< FWC3A2D_KREIS_NUM_INPUT_GROUPS; j_++){
		ssSetInputPortWidth(S, j_, inputGroups[j_]);
		ssSetInputPortDirectFeedThrough(S, j_, 0);
	}
#endif

	if (!ssSetNumOutputPorts(S, FWC3A2D_KREIS_NUM_OUTPUT_GROUPS+FWC3A2D_KREIS_LOOPS_EXIST+1)) return; /* Number of S-function inputs */
	ssSetOutputPortWidth(S, 0, 2*FWC3A2D_KREIS_SYS_DOF);
	for (j_=0; j_< FWC3A2D_KREIS_NUM_OUTPUT_GROUPS; j_++){
		ssSetOutputPortWidth(S, j_+1, outputGroups[j_]);
	}

	ssSetOutputPortWidth(S, 1+FWC3A2D_KREIS_NUM_OUTPUT_GROUPS, FWC3A2D_KREIS_NUM_LOOPS);

	ssSetNumSampleTimes(S,0);
	ssSetNumRWork(S, 0);
	ssSetNumIWork(S, 0);
	ssSetNumPWork(S, 2);
	ssSetNumModes(S, 0);
	ssSetNumNonsampledZCs(S, 0);

	ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

/* INITIALIZE SAMPLE TIME */

#define MDL_INITIALIZE_SAMPLE_TIMES
static void mdlInitializeSampleTimes(SimStruct *S) {
	ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
	ssSetOffsetTime(S, 0, 0.0);
}

/* ENABLE */

#define MDL_ENABLE

static void mdlEnable(SimStruct *S) {
	UNUSED_ARG(S);
}

/* INITIALIZE CONDITIONS */

#define MDL_INITIALIZE_CONDITIONS

static void mdlInitializeConditions(SimStruct *S) {

	int i_ = 0;

	double t                   = ssGetT(S);
	double *x_                 = ssGetContStates(S);

#ifdef  MATLAB_MEX_FILE
	const mxArray *parameters  = ssGetSFcnParam(S, 0);
#endif
	const mxArray *y_init_pt   = ssGetSFcnParam(S, 1);
	const mxArray *Dy_init_pt  = ssGetSFcnParam(S, 2);

	double *y_init             = mxGetPr(y_init_pt);
	double *Dy_init            = mxGetPr(Dy_init_pt);

	void *dataPtr              = NULL;


	double *u_                 = calloc(11, sizeof(double));

	/* Check dimensions*/
	if ((mxGetM(y_init_pt)!=FWC3A2D_KREIS_SYS_DOF) || (mxGetN(y_init_pt)!=1))
		printf("The initial vector of the generalized coordinates has to be a 4x1 vector!");

	if ((mxGetM(Dy_init_pt)!=FWC3A2D_KREIS_SYS_DOF) || (mxGetN(Dy_init_pt)!=1))
		printf("The initial vector of the generalized velocities has to be a 4x1 vector!");

	dataPtr = FWc3A2D_Kreis_initializeSystemStruct();

	/* Read out initial conditions */
	for (i_=0; i_< FWC3A2D_KREIS_SYS_DOF; i_++) {
		x_[i_] = y_init[i_];
		x_[i_+FWC3A2D_KREIS_SYS_DOF] = Dy_init[i_];
	}

#ifdef  MATLAB_MEX_FILE
	if (!mxIsEmpty(parameters))
		FWc3A2D_Kreis_getSystemStruct(dataPtr, (const void *) parameters);
#endif

	/* Store pointers in simulink system structure */
	ssSetPWorkValue(S, 0, dataPtr);
	ssSetPWorkValue(S, 1, u_);
}

/* OUTPUTS */

static void mdlOutputs(SimStruct *S, int tid) {

	int i_         = 0;
	double t       = ssGetT(S);
	double *x_     = ssGetContStates(S);
	double *u_     = ssGetPWorkValue(S,1);
	void *dataPtr  = ssGetPWorkValue(S,0);
	double *states = ssGetOutputPortRealSignal(S,0);
	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;
	double *y_exactOut = ssGetOutputPortRealSignal(S, 1);
	double *y_approxOut = ssGetOutputPortRealSignal(S, 2);
	double *y_Curvature = ssGetOutputPortRealSignal(S, 3);
	double *y_lagrange = ssGetOutputPortRealSignal(S, 4);

	for (i_ = 0; i_<(2*FWC3A2D_KREIS_SYS_DOF); i_++)
		states[i_] = x_[i_];

	FWc3A2D_Kreis_f_exactOut_outputs(t, x_, u_, y_exactOut, dataPtr);

	FWc3A2D_Kreis_f_approxOut_outputs(t, x_, u_, y_approxOut, dataPtr);

	FWc3A2D_Kreis_f_Curvature_outputs(t, x_, u_, y_Curvature, dataPtr);

	/* Lagrange multipliers */;
	FWc3A2D_Kreis_lagrangeMultipliers(t, x_, u_, dataPtr);
	memcpy(y_lagrange, data->con->lambda_, FWC3A2D_KREIS_NUM_LOOPS*sizeof(double));

}

/* DERIVATIVES */

#define MDL_DERIVATIVES

static void mdlDerivatives(SimStruct *S) {

	double t     = ssGetT(S);
	double *x_   = ssGetContStates(S);
	double *f    = ssGetdX(S);

	int i_=0;

	void *dataPtr = ssGetPWorkValue(S,0);
	double *u_    = ssGetPWorkValue(S,1);

	/* Inputs */
	InputRealPtrsType u_control = ssGetInputPortRealSignalPtrs(S, 0);
	InputRealPtrsType u_contactforce = ssGetInputPortRealSignalPtrs(S, 1);

	/* Combine all inputs */
	u_[0] = *u_control[0];
	u_[1] = *u_control[1];
	u_[2] = *u_control[2];
	u_[3] = *u_control[3];
	u_[4] = *u_control[4];
	u_[5] = *u_control[5];
	u_[6] = *u_control[6];
	u_[7] = *u_control[7];
	u_[8] = *u_control[8];
	u_[9] = *u_contactforce[0];
	u_[10] = *u_contactforce[1];

	clearVector(FWC3A2D_KREIS_SYS_DOF,&(f[FWC3A2D_KREIS_SYS_DOF]));

	/* Compute system dynamics */
	FWc3A2D_Kreis_equations_of_motion(t, x_, u_, f, dataPtr);

}

/* TERMINATE */

#define MDL_TERMINATE

static void mdlTerminate(SimStruct *S) {

	/* Pointer casting */
	void *dataPtr = ssGetPWorkValue(S,0);
	double *u_    = ssGetPWorkValue(S,1);

	/* Free System Struct */
	FWc3A2D_Kreis_freeStructure(dataPtr);
	free(u_);

}

#ifdef  MATLAB_MEX_FILE
#include "simulink.c"
#else
#include "cg_sfun.h"
#endif

