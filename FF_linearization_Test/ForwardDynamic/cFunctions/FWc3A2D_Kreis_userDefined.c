#include "FWc3A2D_Kreis_userDefined.h"
#include <stdio.h>
#include <math.h>
#include "neweul.h"
#include <stdlib.h>

double FWc3A2D_Kreis_f_p_a(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double p_a_d = data->p_a_d;

	myRes_ = p_a_d;

	return myRes_;
}

double FWc3A2D_Kreis_f_Dp_a(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_D2p_a(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_p_b(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double p_b_d = data->p_b_d;

	myRes_ = p_b_d;

	return myRes_;
}

double FWc3A2D_Kreis_f_Dp_b(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_D2p_b(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_r_gamma3(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double r_gamma3_d = data->r_gamma3_d;

	myRes_ = r_gamma3_d;

	return myRes_;
}

double FWc3A2D_Kreis_f_Dr_gamma3(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_D2r_gamma3(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_EE_force_x(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double EE_force_x_d = data->EE_force_x_d;

	myRes_ = EE_force_x_d;

	return myRes_;
}

double FWc3A2D_Kreis_f_DEE_force_x(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_D2EE_force_x(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_EE_force_y(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double EE_force_y_d = data->EE_force_y_d;

	myRes_ = EE_force_y_d;

	return myRes_;
}

double FWc3A2D_Kreis_f_DEE_force_y(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double FWc3A2D_Kreis_f_D2EE_force_y(double t, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}




double FWc3A2D_Kreis_f_DelBo_EA1_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA2_1_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[6];

	myRes_ = -0.078781818435274722500771815703047*DEA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA2_1_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[6];

	myRes_ = -0.000000000000078774485292178524877876637249719*DEA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA2_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[6];

	myRes_ = 0.12720812859558897067735472319328*DEA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA2_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA2_3_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[6];

	myRes_ = 0.13074928527605483852269685485226*DEA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA2_3_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[6];

	myRes_ = -0.00000000000075504122110177805935058228092758*DEA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA2_3_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[6];

	myRes_ = 0.05177806232203822911497326231256*DEA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA3_2_rot_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[7];

	myRes_ = 0.000002211520596332927660824640286874*DEA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA3_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[7];

	myRes_ = 0.41814120987041747401491420532693*DEA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA3_2_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[7];

	myRes_ = -0.0000000000079072648755528962870184095894835*DEA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_DelBo_EA3_2_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[7];

	myRes_ = 0.11086564964710307612527628862154*DEA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA1_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0.42;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA2_1_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[2];

	myRes_ = -0.078781818435274722500771815703047*EA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA2_1_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[2];

	myRes_ = -0.000000000000078774485292178524877876637249719*EA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[2];

	myRes_ = 0.12720812859558897067735472319328*EA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA2_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0.6;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA2_3_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[2];

	myRes_ = 0.13074928527605483852269685485226*EA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA2_3_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[2];

	myRes_ = 1.0 - 0.00000000000075504122110177805935058228092758*EA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA2_3_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[2];

	myRes_ = 0.05177806232203822911497326231256*EA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA3_2_rot_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[3];

	myRes_ = 0.000002211520596332927660824640286874*EA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA3_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[3];

	myRes_ = 0.41814120987041747401491420532693*EA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA3_2_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[3];

	myRes_ = 0.4643 - 0.0000000000079072648755528962870184095894835*EA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_f_elBo_EA3_2_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[3];

	myRes_ = 0.11086564964710307612527628862154*EA3_q001;

	return myRes_;
}

double forces_EE_fx(double t, double *x_, double *u_, double *f_EE_fx, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	/* system inputs */
	double EE_force_x = u_[9];

	/* Vectorial force law */
	f_EE_fx[0] = - 
	1.0*EE_force_x;

	return euclideanNorm_vec(6, f_EE_fx);
}

double forces_EE_fy(double t, double *x_, double *u_, double *f_EE_fy, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	/* system inputs */
	double EE_force_y = u_[10];

	/* Vectorial force law */
	f_EE_fy[1] = - 
	1.0*EE_force_y;

	return euclideanNorm_vec(6, f_EE_fy);
}


void FWc3A2D_Kreis_f_control_inputs(double t, double *x_, double *u_, void *dataPtr){

	u_[0] = FWc3A2D_Kreis_f_p_a(t, dataPtr);
	u_[1] = FWc3A2D_Kreis_f_Dp_a(t, dataPtr);
	u_[2] = FWc3A2D_Kreis_f_D2p_a(t, dataPtr);
	u_[3] = FWc3A2D_Kreis_f_p_b(t, dataPtr);
	u_[4] = FWc3A2D_Kreis_f_Dp_b(t, dataPtr);
	u_[5] = FWc3A2D_Kreis_f_D2p_b(t, dataPtr);
	u_[6] = FWc3A2D_Kreis_f_r_gamma3(t, dataPtr);
	u_[7] = FWc3A2D_Kreis_f_Dr_gamma3(t, dataPtr);
	u_[8] = FWc3A2D_Kreis_f_D2r_gamma3(t, dataPtr);
}


void FWc3A2D_Kreis_f_contactforce_inputs(double t, double *x_, double *u_, void *dataPtr){

	u_[0] = FWc3A2D_Kreis_f_EE_force_x(t, dataPtr);
	u_[1] = FWc3A2D_Kreis_f_EE_force_y(t, dataPtr);
}


double FWc3A2D_Kreis_output_Pos_x(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];



	/* constant user-defined variables */ 

	double d_axes = data->d_axes;


	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = FWc3A2D_Kreis_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = FWc3A2D_Kreis_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = FWc3A2D_Kreis_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = FWc3A2D_Kreis_f_elBo_EA3_2_y_(x_);

	double p_b = u_[3];
	double r_gamma3 = u_[6];

	myRes_ = d_axes + p_b + elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*elBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Pos_y(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];



	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = FWc3A2D_Kreis_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = FWc3A2D_Kreis_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = FWc3A2D_Kreis_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = FWc3A2D_Kreis_f_elBo_EA3_2_y_(x_);

	double r_gamma3 = u_[6];

	myRes_ = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + elBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Rot_angle_theta(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double EA2_q001 = x_[2];
	double EA3_q001 = x_[3];


	double r_gamma3 = u_[6];

	myRes_ = 0.0035411566804658678453421316589811*EA2_q001 + 0.41814120987041747401491420532693*EA3_q001 + r_beta2 + r_gamma3;

	return myRes_;
}


double FWc3A2D_Kreis_output_Vel_x(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double Dr_beta2 = x_[5];



	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = FWc3A2D_Kreis_f_DelBo_EA2_2_x_(x_);
	double DelBo_EA2_3_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_3_rot_z_(x_);
	double DelBo_EA2_3_x_ = FWc3A2D_Kreis_f_DelBo_EA2_3_x_(x_);
	double DelBo_EA2_3_y_ = FWc3A2D_Kreis_f_DelBo_EA2_3_y_(x_);
	double DelBo_EA3_2_x_ = FWc3A2D_Kreis_f_DelBo_EA3_2_x_(x_);
	double DelBo_EA3_2_y_ = FWc3A2D_Kreis_f_DelBo_EA3_2_y_(x_);
	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = FWc3A2D_Kreis_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = FWc3A2D_Kreis_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = FWc3A2D_Kreis_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = FWc3A2D_Kreis_f_elBo_EA3_2_y_(x_);

	double Dp_b = u_[4];
	double r_gamma3 = u_[6];
	double Dr_gamma3 = u_[7];

	myRes_ = Dp_b - 1.0*(elBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + elBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3))*(DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2 + Dr_gamma3) - (DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2)*(1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)) + DelBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*DelBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Vel_y(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double Dr_beta2 = x_[5];



	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = FWc3A2D_Kreis_f_DelBo_EA2_2_x_(x_);
	double DelBo_EA2_3_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_3_rot_z_(x_);
	double DelBo_EA2_3_x_ = FWc3A2D_Kreis_f_DelBo_EA2_3_x_(x_);
	double DelBo_EA2_3_y_ = FWc3A2D_Kreis_f_DelBo_EA2_3_y_(x_);
	double DelBo_EA3_2_x_ = FWc3A2D_Kreis_f_DelBo_EA3_2_x_(x_);
	double DelBo_EA3_2_y_ = FWc3A2D_Kreis_f_DelBo_EA3_2_y_(x_);
	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = FWc3A2D_Kreis_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = FWc3A2D_Kreis_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = FWc3A2D_Kreis_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = FWc3A2D_Kreis_f_elBo_EA3_2_y_(x_);

	double r_gamma3 = u_[6];
	double Dr_gamma3 = u_[7];

	myRes_ = DelBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*(elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + DelBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + (elBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*elBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3))*(DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2 + Dr_gamma3) + DelBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*DelBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Rot_velocity(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double Dr_beta2 = x_[5];



	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_3_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_3_rot_z_(x_);
	double DelBo_EA3_2_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA3_2_rot_z_(x_);

	double Dr_gamma3 = u_[7];

	myRes_ = DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + DelBo_EA3_2_rot_z_ + Dr_beta2 + Dr_gamma3;

	return myRes_;
}


double FWc3A2D_Kreis_output_Pos_x_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double EA2_q001 = x_[2];
	double EA3_q001 = x_[3];



	/* constant user-defined variables */ 

	double d_axes = data->d_axes;
	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);

	double p_b = u_[3];
	double r_gamma3 = u_[6];

	myRes_ = d_axes + p_b + l3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Pos_y_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double EA2_q001 = x_[2];
	double EA3_q001 = x_[3];



	/* constant user-defined variables */ 

	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);

	double r_gamma3 = u_[6];

	myRes_ = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + EA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Rot_angle_rlc_theta(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double EA2_q001 = x_[2];
	double EA3_q001 = x_[3];



	/* constant user-defined variables */ 

	double psi2_1 = data->psi2_1;
	double psi3_1 = data->psi3_1;
	double v2_1 = data->v2_1;
	double v3_1 = data->v3_1;


	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);

	double r_gamma3 = u_[6];

	myRes_ = r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1;

	return myRes_;
}


double FWc3A2D_Kreis_output_Vel_x_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double EA2_q001 = x_[2];
	double EA3_q001 = x_[3];
	double Dr_beta2 = x_[5];
	double DEA2_q001 = x_[6];
	double DEA3_q001 = x_[7];



	/* constant user-defined variables */ 

	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = FWc3A2D_Kreis_f_DelBo_EA2_2_x_(x_);
	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);

	double Dp_b = u_[4];
	double r_gamma3 = u_[6];
	double Dr_gamma3 = u_[7];

	myRes_ = Dp_b - 1.0*(l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*(l3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + EA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1))*(Dr_beta2 - 1.0*DelBo_EA2_2_rot_z_ + Dr_gamma3 + DEA2_q001*psi2_1*v2_1) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*DEA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + DEA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Vel_y_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[1];
	double EA2_q001 = x_[2];
	double EA3_q001 = x_[3];
	double Dr_beta2 = x_[5];
	double DEA2_q001 = x_[6];
	double DEA3_q001 = x_[7];



	/* constant user-defined variables */ 

	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = FWc3A2D_Kreis_f_DelBo_EA2_2_x_(x_);
	double elBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FWc3A2D_Kreis_f_elBo_EA2_2_x_(x_);

	double r_gamma3 = u_[6];
	double Dr_gamma3 = u_[7];

	myRes_ = (l3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*EA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1))*(Dr_beta2 - 1.0*DelBo_EA2_2_rot_z_ + Dr_gamma3 + DEA2_q001*psi2_1*v2_1) - 1.0*(l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DEA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + DEA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	return myRes_;
}


double FWc3A2D_Kreis_output_Rot_vel_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double Dr_beta2 = x_[5];
	double DEA2_q001 = x_[6];
	double DEA3_q001 = x_[7];



	/* constant user-defined variables */ 

	double psi2_1 = data->psi2_1;
	double psi3_1 = data->psi3_1;
	double v2_1 = data->v2_1;
	double v3_1 = data->v3_1;


	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = FWc3A2D_Kreis_f_DelBo_EA2_2_rot_z_(x_);

	double Dr_gamma3 = u_[7];

	myRes_ = Dr_beta2 - 1.0*DelBo_EA2_2_rot_z_ + Dr_gamma3 + DEA2_q001*psi2_1*v2_1 + DEA3_q001*psi3_1*v3_1;

	return myRes_;
}


double FWc3A2D_Kreis_output_Curvature_1_EA2(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[2];


	(void) u_;

	myRes_ = 0.67665331847908083418019486998674*EA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_output_Curvature_1_EA3(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[3];


	(void) u_;

	myRes_ = 2.1640712562231518845123900973704*EA3_q001;

	return myRes_;
}


double FWc3A2D_Kreis_output_DCurvature_1_EA2(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[6];


	(void) u_;

	myRes_ = 0.67665331847908083418019486998674*DEA2_q001;

	return myRes_;
}


double FWc3A2D_Kreis_output_DCurvature_1_EA3(double t, double *x_, double *u_, void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[7];


	(void) u_;

	myRes_ = 2.1640712562231518845123900973704*DEA3_q001;

	return myRes_;
}


void FWc3A2D_Kreis_f_exactOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr){

	z[0] = FWc3A2D_Kreis_output_Pos_x(t, x_, u_, dataPtr);
	z[1] = FWc3A2D_Kreis_output_Pos_y(t, x_, u_, dataPtr);
	z[2] = FWc3A2D_Kreis_output_Rot_angle_theta(t, x_, u_, dataPtr);
	z[3] = FWc3A2D_Kreis_output_Vel_x(t, x_, u_, dataPtr);
	z[4] = FWc3A2D_Kreis_output_Vel_y(t, x_, u_, dataPtr);
	z[5] = FWc3A2D_Kreis_output_Rot_velocity(t, x_, u_, dataPtr);

}


void FWc3A2D_Kreis_f_approxOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr){

	z[0] = FWc3A2D_Kreis_output_Pos_x_rlc(t, x_, u_, dataPtr);
	z[1] = FWc3A2D_Kreis_output_Pos_y_rlc(t, x_, u_, dataPtr);
	z[2] = FWc3A2D_Kreis_output_Rot_angle_rlc_theta(t, x_, u_, dataPtr);
	z[3] = FWc3A2D_Kreis_output_Vel_x_rlc(t, x_, u_, dataPtr);
	z[4] = FWc3A2D_Kreis_output_Vel_y_rlc(t, x_, u_, dataPtr);
	z[5] = FWc3A2D_Kreis_output_Rot_vel_rlc(t, x_, u_, dataPtr);

}


void FWc3A2D_Kreis_f_Curvature_outputs(double t, double *x_, double *u_, double *z, void *dataPtr){

	z[0] = FWc3A2D_Kreis_output_Curvature_1_EA2(t, x_, u_, dataPtr);
	z[1] = FWc3A2D_Kreis_output_Curvature_1_EA3(t, x_, u_, dataPtr);
	z[2] = FWc3A2D_Kreis_output_DCurvature_1_EA2(t, x_, u_, dataPtr);
	z[3] = FWc3A2D_Kreis_output_DCurvature_1_EA3(t, x_, u_, dataPtr);

}

