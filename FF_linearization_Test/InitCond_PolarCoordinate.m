clear all
%%
currentFolder = pwd;
cd(currentFolder)
addpath(genpath([currentFolder '/SDOF_FilterValmir']))
addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))
addpath(genpath([currentFolder '/Trajectory']))
addpath(genpath([currentFolder '/neweulm2_20151213']))
addpath(genpath([currentFolder '/ForwardDynamic']))
% addpath(genpath([currentFolder '/redefined_observer']))

load('sys.mat')
load('sysFW.mat')
load('snvLambda.mat')


SampleTime = 1e-3;

% rigid
% w2_1 = 0;
% w3_1 = 0;
% v2_1 = 0;
% v3_1 = 0;


%Faktoren f�r relocated output
w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;

%model
sys.parameters.data.EE_x_d = 1;
sys.parameters.data.EE_y_d = 0.6;
sys.parameters.data.EE_Rot_d = 60*pi/180;

[y0, dy0] = calcInitConditions(0,[0,0,-0.7,0.7,0,0,0,zeros(1,7)]', zeros(1,9)'); %-1 und 1 sind initial guesses der Winkel, zeros(3,1) sind Geschwindigkeit

pos_EE = eqm_sysout_exactOut(0,[y0; dy0],[y0(1,1),0,0,y0(2,1),0,0,y0(3,1),0,0]');

x_EE0 = pos_EE(1,1);
y_EE0 = pos_EE(2,1);

% %error

psi2_1 = -0.0093 ; % Wert aus Sens
psi3_1 = -0.2091; % Wert aus Sens

a_error = 0;
b_error = 0;
alpha_error = 0;
beta_error = 0;
gam_error = 0 * pi/180; 
q1_error = 0 * pi/180 * 1/psi2_1; 
q2_error = 0 * pi/180 * 1/psi3_1;

y_error = [a_error; b_error; alpha_error; beta_error; gam_error; q1_error; q2_error];
y0_error = y0 + y_error;
sysFW.parameters.data.m_ee = 0;
sysFW.parameters.data.l3 = 0.4760; %0.4760
% Trajectory
t_end   = 4;
t_start = 1;
x_end   = 1.3;
y_end   = 0.4;

t = t_start:0.01:t_end;

%% Wand 

r_w1 = [sys.parameters.data.EE_x_d;...    %Startposition der Wand wird hier definiert. 
        sys.parameters.data.EE_y_d];      %Dieser muss nicht!!! mit der Starposition des EE 
                                          % �bereinstimmen 
r_w2 = r_w1 + [0.38; -0.38];

r0 = [1/2*(r_w1(1) + r_w2(1) + r_w2(2) - r_w1(2)); ...
            r_w1(1) + r_w2(2) - 1/2*(r_w1(1) + r_w2(1) + r_w2(2) - r_w1(2))]; %Neues KOS des Kreises

xO = r0(1);
yO = r0(2);

R = sqrt((r_w1(1)-xO)^2+ (r_w1(2)-yO)^2);

sigma0 = atan2(r_w1(2)-yO, r_w1(1)-xO);
sigma_end = sigma0 - 90*pi/180;
                                          
% run('traj_v_x.m')
% run('traj_v_y.m')
run('Dsigma_traj.m')
%%
% Reglerparameter

P_tilde = [0;0;0];
P = diag(P_tilde);

D_tilde = [0;0;0];
D = diag(D_tilde);

I_tilde = [0;0;0];
I = diag(I_tilde);
