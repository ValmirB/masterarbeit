#ifndef SCg3A2D_pd_matlab_H
#define SCg3A2D_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "SCg3A2D_paraStruct.h"

double SCg3A2D_getScalar(const void *context, const char *name, double defaultValue);

int SCg3A2D_getSystemStruct(void *dataPtr, const void *context);

void SCg3A2D_educatedGuess(double t, double *y_, void *dataPtr);
void SCg3A2D_educatedGuess(double t, double *y_, void *dataPtr);

#endif
