#ifndef SCg3A2D_linearizeMBS_H
#define SCg3A2D_linearizeMBS_H
#include "SCg3A2D_paraStruct.h"

/* Function prototypes */
void SCg3A2D_createSystemMatrix(double t_WP, double *x_WP, double *u_WP,
    double **A, int dim, void *dataPtr);

void SCg3A2D_createInputMatrix(double t_WP, double *x_WP, double *u_WP,
    double **B, int dim, int numIn, void *dataPtr);

void SCg3A2D_createOutputMatrix(double t_WP, double *x_WP,
    double *u_WP, double **C,
    int dim, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));

void SCg3A2D_createFeedthroughMatrix(double t_WP, double *x_WP,
    double *u_WP, double **D,
    int numIn, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));
#endif
