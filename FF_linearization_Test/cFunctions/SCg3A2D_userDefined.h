#ifndef SCg3A2D_userDefined_H
#define SCg3A2D_userDefined_H

#include "SCg3A2D_paraStruct.h"
/* Time dependent values */
double SCg3A2D_f_EE_x(double t, void *dataPtr);
double SCg3A2D_f_DEE_x(double t, void *dataPtr);
double SCg3A2D_f_D2EE_x(double t, void *dataPtr);
double SCg3A2D_f_EE_y(double t, void *dataPtr);
double SCg3A2D_f_DEE_y(double t, void *dataPtr);
double SCg3A2D_f_D2EE_y(double t, void *dataPtr);
double SCg3A2D_f_EE_Rot(double t, void *dataPtr);
double SCg3A2D_f_DEE_Rot(double t, void *dataPtr);
double SCg3A2D_f_D2EE_Rot(double t, void *dataPtr);

/* User-defined signals */

/* State dependent values */

/* Abbreviations */

double SCg3A2D_f_DelBo_EA1_2_x_(double *x_);

double SCg3A2D_f_DelBo_EA2_1_rot_z_(double *x_);

double SCg3A2D_f_DelBo_EA2_1_x_(double *x_);

double SCg3A2D_f_DelBo_EA2_2_rot_z_(double *x_);

double SCg3A2D_f_DelBo_EA2_2_x_(double *x_);

double SCg3A2D_f_DelBo_EA2_3_rot_z_(double *x_);

double SCg3A2D_f_DelBo_EA2_3_x_(double *x_);

double SCg3A2D_f_DelBo_EA2_3_y_(double *x_);

double SCg3A2D_f_DelBo_EA3_2_rot_x_(double *x_);

double SCg3A2D_f_DelBo_EA3_2_rot_z_(double *x_);

double SCg3A2D_f_DelBo_EA3_2_x_(double *x_);

double SCg3A2D_f_DelBo_EA3_2_y_(double *x_);

double SCg3A2D_f_elBo_EA1_2_x_(double *x_);

double SCg3A2D_f_elBo_EA2_1_rot_z_(double *x_);

double SCg3A2D_f_elBo_EA2_1_x_(double *x_);

double SCg3A2D_f_elBo_EA2_2_rot_z_(double *x_);

double SCg3A2D_f_elBo_EA2_2_x_(double *x_);

double SCg3A2D_f_elBo_EA2_3_rot_z_(double *x_);

double SCg3A2D_f_elBo_EA2_3_x_(double *x_);

double SCg3A2D_f_elBo_EA2_3_y_(double *x_);

double SCg3A2D_f_elBo_EA3_2_rot_x_(double *x_);

double SCg3A2D_f_elBo_EA3_2_rot_z_(double *x_);

double SCg3A2D_f_elBo_EA3_2_x_(double *x_);

double SCg3A2D_f_elBo_EA3_2_y_(double *x_);

/* Input groups */

void SCg3A2D_f_endeffector_acceleration_inputs(double t, double *x_, double *u_, void *dataPtr);

/* System outputs */

double SCg3A2D_output_des_x(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_des_y(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_des_Dx(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_des_Dy(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Pos_x(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Pos_y(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Rot_angle_theta(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Vel_x(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Vel_y(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Rot_velocity(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Pos_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Pos_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Rot_angle_rlc_theta(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Vel_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Vel_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Rot_vel_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Curvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_Curvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_DCurvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double SCg3A2D_output_DCurvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

/* Output groups */

void SCg3A2D_f_desiredOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void SCg3A2D_f_exactOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void SCg3A2D_f_approxOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void SCg3A2D_f_Curvature_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

#endif
