/* %%%%%%% DESCRIPTION OF SDOF_KALMAN.H %%%%%% */
/* This Header-File contains the preprocessor constants (#defines) and the 
 * function protypes of the ones used in the SDOF Linear Kalman Filter (KF) 
 * and SDOF Unscented Kalman Filter (UKF). Since the system is linear both
 * implementation create the exact same results. In the following paragraphs
 * a few constants (Captial Letters) will be explained. 
 *
 * The number of dialog parameters is defined by NPARAMS and should only be
 * modified if additional parameters are needed in the Kalman Filter, since
 * NPARAMS is used to double-check the dialog parameters.
 *
 * Via DOF, the total dimension of the state vector of the Kalman Filter 
 * is defined to which the computation time for each time step is directly 
 * related to. The states are: Coordinate, Veloctiy, Acceleration, Jerk.
 * To increase efficiency, DOF should only be increased if a higher filter
 * order is actually needed and if noise models do actually exists. 
 * For example: Although jerk noise models are available, there might be 
 * not useful for certain purposes (mechanical systems). 
 * If that's the case, set DOF down to 3 (increase computational efficiency).
 *
 * NTYPES defines the number of available noise models and is used to double
 * check the user dialog-parameters. If DOF is reduced, this automatically 
 * reduces the number of available noise models accordingly to prevent errors.
 *
 * For certain applications (e.g. tracking in 2-D, plane trajectories), it 
 * is useful to incorparate additional information as equality constraints 
 * to improve the accuracy of the estimates. Doing so, CONSTRAINTS must be
 * set to one (otherwise 0) and the number of constraint equations NUM_CON
 * must be specified (>0). The equality constraints are then considered as
 * perfect observations using the measurement augmentation approach. In case
 * of nonlinear constraints, the SDOF Unscented Kalman Filter must be used.
 *
 * By setting USEADES to one, an additional inport to the S-function block
 * of the Kalman Filter is provided, which can be used to supply the 
 * corresponding desired trajectory of the (velocity (type=6)/acceleration 
 * (type=12)/ jerk (type=18)) signal for the corresponding mean-adaptive 
 * singer model. For all other noise models, it is not used. It has been 
 * shown that only the mean-adaptive singer acceleration model (also 
 * referenced as the "current" model) might have an improving impact on the 
 * plain zero-mean Singer Model.
 * 
 * With OUTPUTSNV, the format of the S-function output can be defined. If it
 * equals to zero, only one outport is created where the estimated and 
 * corrected states of all input signals exit the S-function. If OUTPUTSNV
 * equals one, two outports are provided, one for the coordinates and the 
 * 2nd one for the velocity. All the other states are not outputted. 
 *
 * Activating STEADYSTATE can help to dramatically reduce the computational
 * effort of the UKF. It STEADYSTATE is equal to zero, the UKF procedure
 * remains unchanges and the Kalman Gains are computed on every time step.
 * If STEADYSTATE == 1, the Kalman Gain K_k is constant (steady-state) and the 
 * entire Kalman Gain matrix, must be provided as the last dialog parameter.
 * Then, at a time step, the only step which have to be carried out are:
 *      - 1x System Dynamics for time update xhat(-) = f(t,xhat(+),u)
 *      - 1x System Output with xhat(-) --> yhat = h(t,xhat(-)
 *      - 1x System Constraints with xhat(-) -->  dhat = h(t,xhat(-))
 *      - Data Assimilation step with Kalman Update: xhat(+) = xhat(+) = K_k*(y-yhat)   
 * Since, the Kalman Gain Matrix K_k is available columnswise at Outport 2,
 * one can simply check whether such an simplification is appropriate. Note,
 * that for a konstant Kalman Gain, the filter initialization time is in-
 * creased dramitically. That means, that it is required to wait longer before
 * the states are useable for feedback control or so.
 *
 * The very small constant DELTA is used for all covariance matrices to
 * prevent singularities. For instance, if no noise model is defined, DELTA 
 * will be placed on all diagonal entries of Q. It is also used as the 
 * "constraint noise" to improve numerial stability in the MAKF.
 *
 * The parameter CONSTANTQ can aid to further reduce computation time of the
 * Kalman Filter. If it is set, the user verifies that the ModelCovariance 
 * Q_K is constant all the time. Hence it will be calculated only once in
 * mdlInitializeConditions and stored in a variable. If CONSTANTQ is set
 * to zero, then the C-Function sdofModelCovariance is called ad Q_k computed
 * on every major time step of the simulation.
 *
 * If RVONTHEFLY = 0, RV is constant and the square matrix for the noise 
 * variance will be taken from the Dialog Parameters of the S-function. If 
 * RVONTHEFLY = 1, Rv will be estimated at each time step via a windowing 
 * technique. The size of the window can be set by NWIN. Rv of the dialog
 * parameters is then only used during initialization phase until the time
 * step counter exceeds NWIN.
 *
 * Also an outlier rejction is implemented, which is only activated if the
 * value of the kill switch OUTLIER_REJECTION is greater than zero. The 
 * detection of an potential outlier is done by comparing the innovation, 
 * which is the difference between the input signal y to the a priori estimate 
 * of the coordinate xhat(-), to some threshold value, the outlier criteria:
 *              outliercrit = FACTOR_STDDEV * sigma_V
 * where FACTOR_STDDEV is a positive multiplier, which equals 3.0 for a 
 * Gaussian Normal Distribution (can be modified below!!). Sigma_V is the 
 * square root of diagonal entries of either the sensor noise variance
 * RK (CONSTRAINT_EQUATION==1) or the output covariance Pyy 
 * (CONSTRAINT_EQUATION==2), which is defined as Pyy = C*Pxx(-)*C + Rk.
 *
 * The action which is carried out after an outlier has been detected can be
 * selected by RJCTTYPE (Dialog Parameter), which can be individually be
 * defined for each input signal seperately.
 *      0           none. The outlier criteria is ignored completely.
 *      1           Saturate the innovation to the value of the outlier criteria
 *      2           Reset innovation to zero completely.
 * 
 * It the above mentioned threshold value is violated, one of the following
 * action will be executed, which can be selected for each input signal 
 * seperately via the dialog Parameter RJCTTYPE.
 *
 * If S-Function is used in variable step simulations or with not equidistant
 * distributed time stamp values in postprocessing, this must be explicitly
 * specified by activating VARIABLESTEP. Also the expected update period
 * DELT must be defined, which is used at the very first time step. After
 * that tstep is the time difference between two major time steps.
 */

/*%%%%%%% CONSTANTS %%%%%%%%*/
#define NPARAMS (6)             /* Number of S-function dialog parameters */
#define DOF (3)                 /* States for each input */
#define NTYPES (18)             /* Number of available process noise models */
#define CONSTRAINTS (0)         /* Switch whether constraints are present in SDOF system */
#define NUM_CON (0)             /* Number of Constraint Equations */
#define STEADYSTATE (0)         /* Flag if KF is in steady state (constant Kalman Gains) */
#define USEADES (0)             /* Flag whether desired acceleration of trajectory is used */
#define OUTPUTSNV (0)           /* Flag whether all systemstates (0) or just x and Dx (1) are outputted */
#define DELTA (1.0e-11)         /* Variable to ensure positive definiteness of covariances */
#define CONSTANTQ (1)           /* Flag whether Additive Model Covariance is constant all the time */
#define RVONTHEFLY (0)          /* Specifies whether Adaptive Noise Variance is used */ 
#define NWIN (80)               /* Window size to compute Rv on the fly (Adaptive Noise Variance) */
#define OUTLIER_REJECTION (1)   /* Type of outlier rejection: (0) Disabled, (1) R_k, (2) P_yy */
#define FACTOR_STDDEV (3.0)     /* Factor for Standard Deviation to compute Outlier Criteria - Default: 3*sigma */
#define VARIABLESTEP (0)        /* Flag to specify if a fixedstep or variablestep solver is used */
#define DELT (1e-3)             /* Expected update period for variablestep [s] (for first time step) */

/* Parameters only utilized in Unscented Kalman Filter */
#define SPHERICAL (1)           /* Switch whether to use Spherical or normal sigma points */
#define ALPHA (0.5)             /* Scaling Factor for the spread of the sigma points */
#define WEIGHT0 (0.5)           /* Weight of 0th Spherical Sigma Point */
#define CAPAMEAN (0)            /* Flag whether mean acceleration is calculated for every sigma Point */
/* The flag SPHERICAL specifies wheather normal (2*dim+1) or spherical
 * sigma points (dim+2) are used. Spherical sigma points can be used to
 * reduce the computational effort of the UKF as a trade for slightly worse
 * results for highly nonlinear system dynamics (and large step sizes).
 *
 * The parameter ALPHA is a scaling factor between 1e-4 and < 1, which
 * defines the spread of the Sigma Points to reduce or varying the effect
 * of the higher-order nonlinearities of the statistic distribution. For
 * linear system dynamics, it will have no effect on the results since, the 
 * propagation of the statistics can exactly be described by the mean and
 * the error covariance (first two statistical moments). More tuning 
 * parameters (KAPPA, BETA) for the normal sigma points can be found in 
 * the C-Function UKFSigmaPoints.
 *
 * For the spherical sigma point the weight of the zeroth Spherical Sigma
 * Point (which is equal to xhat(+) of the last time step) can be defined
 * by WEIGHT0. Its range goes from 0 to 1. For spherical sigma points, the
 * parameter ALPHA is also used as a scaling factor.
 *
 * CAPAMEAN is like USEADES only relevant for the mean-adaptive Singer
 * noise models (type = 6,12,18) and specifies whether the adaptive-mean 
 * is calculated for each sigma point sepeately or not. If it is set, a 
 * weighted mean with the exact same weights of the sigma points will be 
 * computed which is then used for the Adaptive Model Variance algorithm
 * described in [Zhou84]. Otherwise for all sigma points the same adaptive
 * mean will be utilized, leading to the same results as the linear KF.
 */
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

#ifndef PI
#define PI (3.141592653589793)
#endif
/* Limit different types of noise models according to number of states for each input */
/* Example: If Jerk models are not useful, reducing DOF to 3 will reduce computational effort significantly */
#if ((DOF == 2) && (NTYPES != 6))
#undef NTYPES
#define NTYPES (6)
#elif ((DOF == 3) && (NTYPES != 12))
#undef NTYPES
#define NTYPES (12)
#elif ((DOF >= 4) && (NTYPES != 18))
#undef NTYPES
#define NTYPES (18)
#endif
/* If a variable-step solver is used, Q is never constant */
#if ((VARIABLESTEP == 1) && (CONSTANTQ == 1))
#undef CONSTANTQ
#define CONSTANTQ (0)
#endif
#if ((STEADYSTATE == 1)  && (OUTLIER_REJECTION == 2))
#undef OUTLIER_REJECTION /* In Steady-State only R_k is available */
#define OUTLIER_REJECTION (1)
#endif

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
    
/*%%%%%%% PROTOTYPES %%%%%%%%*/

void checkParams(SimStruct *S, int numout, int num_ma);

void sdofSteadyStateK_k(SimStruct *S, int numout, int dim, double **K_k, double **dlgParam);

void inixhatPhat(SimStruct *S, double tstep, int dim, double *xhat, int dimP, double **errorUpdate, double **dlgParam);

void sdofDynamics(SimStruct *S, double tstep, int dim, double **Phi, double *x, double *xhat, double *abar, double **dlgParam);

void sdofOutput(SimStruct *S, double tstep, int numout, int dim, double **C, double *x, double *y, double **dlgParam);

void sdofConstraints(SimStruct *S, double tstep, int ncon, int dim, double **D, double *x, double *d, double **dlgParam);

void sdofModelCov(SimStruct *S, double tstep, int q, double *x, double **Q_k, double *abar, double **dlgParam);

void sdofMeasurementCov(SimStruct *S, double tstep, int r, double *x, double **R_k, double *Rvotf, double **dlgParam);

void sdofOutlierRejection(SimStruct *S, double tstep, int dim, int numout, double *x, double *y, double **R_k, double *inno, double **dlgParam);

void UKFSigmaPs(int dim, int exdim, double alpha, double W0, double *gamma, double *Wm, double *Wc);

void UKFpartI(int dim, int exdim, double *gamma, double *xhat, double *x, double **P);

void UKFpartII(int dim, int exdim, double *xhat, double *x, double *Wm);

void UKFpartIII(int dim, int exdim, double *x, double *xhat, double *vector, double **P, double *Wc, double **matrix);

void UKFpartIV(int n, int m, int exdim, double *x, double *xhat, double *y, double *yhat, double *vector1, double *vector2, double **Pxy, double **matrix, double *Wc);

void UKFpartV(int dim, int exdim, double *xhat, double *x, double *Wm);

void UKFpartVI(int dim, int exdim, double *x, double *xhat, double *vector, double **P, double *Wc, double **matrix);
