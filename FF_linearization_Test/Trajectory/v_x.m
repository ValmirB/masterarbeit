% Velocity in x direction

% constant user-defined variables
traj_end = t_end;
traj_start = t_start;
traj_x_end = x_end;
traj_x_start = x_EE0;

startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = traj_x_start;
endValue(1) = traj_x_end;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = [-2.1875; 0; 6.5625; 0; -6.5625; 0; 2.1875];

Dtcp_traj1 = (0.5*(endValue-startValue)*(2/(endTime-startTime))*polyval(transitionPoly_,max(min(scaledTime_,1),-1))).';

t_ = 0:0.1:length(Dtcp_traj1-1)*0.1;
v_x = [t_ Dtcp_traj1];

% END OF FILE