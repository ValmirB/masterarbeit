function sens_L = sensitivity_L(y0)

a = y0(1);
b = y0(2);
gamma = y0(5);
d = 0.610;

syms l1 l2 l3 l4 

beta = - 2*atan((1000*(12*a - 5*(-((10000*a^2 + 10000*b^2 + 12200*b + 3397)*(10000*a^2 + 10000*b^2 + 12200*b - ...
6683))/25000000)^(1/2)))/(10000*a^2 + 10000*b^2 + 24200*b + 12877));

r_EE   = [d + b + l3*cos(beta) + l4*cos(beta+gamma); l3*sin(beta) + l4*sin(beta+gamma); beta+gamma];

sens_L_ = jacobian(r_EE, [l1; l2; l3; l4]);

f = matlabFunction(sens_L_);

sens_L = f(0.42,0.6,0.4,0.4643);
end