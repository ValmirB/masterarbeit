function sens = sensitivity_L_(L_,y0)

[sens,~,~] = finiteDifferences(@(L)getEE(L,y0),[L_(1); L_(2); L_(3); L_(4)],1e-11);

end