function c = positionConstraints_new(L,y0, phi)


l1 = L(1);
l2 = L(2);

a = y0(1);
b = y0(2);
alpha = phi(1);
beta = phi(2);

d = 0.610;

c = zeros(2,1);

c(1) = l1*cos(-alpha) + l2*cos(beta) - d - b;
c(2) = -l1*sin(-alpha) + a + l2*sin(beta);

end