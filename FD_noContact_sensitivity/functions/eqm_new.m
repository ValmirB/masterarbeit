function out = eqm_new(xi)


[y0, dy0] = calcInitConditions(0,[xi(1),xi(2),-0.7,0.7,xi(3),xi(4),xi(5),zeros(1,7)]'); %-1 und 1 sind initial guesses der Winkel, zeros(3,1) sind Geschwindigkeit

out = eqm_sysout_exactOut(0, [y0, dy0], 0);

end