function sens = sensitivity(y0)

sens_ = finiteDifferences(@(xi)eqm_new(xi),[y0(1:2); y0(5:7)],1e-8);

sens = sens_(1:3,:);
end