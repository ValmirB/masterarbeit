function beta = solve_beta(L,y0)

l1_ = L(1);
l2_ = L(2);

options = optimset('Display','off');

z = fsolve(@(phi)positionConstraints_new([l1_; l2_],y0,phi),[-0.7,0.7], options);

beta = z(2);


end