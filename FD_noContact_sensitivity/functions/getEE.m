function r_EE = getEE(L,y0)

l3 = L(3);
l4 = L(4);

a = y0(1);
b = y0(2);
gamma = y0(5);

d = 0.61;

beta = solve_beta(L,y0);

r_EE   = [d + b + l3*cos(beta) + l4*cos(beta+gamma); l3*sin(beta) + l4*sin(beta+gamma); beta+gamma];

end