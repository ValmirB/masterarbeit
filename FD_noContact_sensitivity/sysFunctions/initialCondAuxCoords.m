function z_ = initialCondAuxCoords(t, x_, u_, varargin)


global sys

if ~isempty(varargin)&&isstruct(varargin{1})
	kin = varargin{1};
else
	kin = evalAbsoluteKinematics(t, x_, u_, 1);
end

success_ = false(0,1);


% Return values

z_ = zeros(0, 1);


end

