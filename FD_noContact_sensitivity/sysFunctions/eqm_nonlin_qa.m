function result_ = eqm_nonlin_qa(t, x_, u_, varargin)
% EQM_NONLIN_QA - Vector of generalized applied forces of the system Flexor
% For each rigid body there are 3 translations and 3 rotations.
% For flexible bodies, the elastic deformations are appended.
% The vector q_ contains also the elastic forces.
% The vector q_ = Jg * (qa + h_e) is in the space of generalized coordinates
global sys;

% generalized coordinates, velocities and auxiliary coordinates

EA2_q001 = x_(6);
EA3_q001 = x_(7);
% system inputs
u1 = u_(1);
u2 = u_(2);
u3 = u_(3);

% constant user-defined variables
K_L = sys.parameters.data.K_L;
K_R = sys.parameters.data.K_R;
g = sys.parameters.data.g;
m_C1 = sys.parameters.data.m_C1;
m_C2 = sys.parameters.data.m_C2;
m_ee = sys.parameters.data.m_ee;
m_mb = sys.parameters.data.m_mb;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

elBo_EA3_2_rot_x_ =  SID_EA3_.frame.node(2).orientation(1) + SID_EA3_.frame.node(2).psi(1,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_rot_z_ =  SID_EA3_.frame.node(2).orientation(3) + SID_EA3_.frame.node(2).psi(3,:) * x_(sys.model.body.EA3.data.edof.idx);


% Force vector

qa_ = cell(7,1);


qa_{1} = zeros(6,1);

qa_{1}(2) = K_L*u1;
qa_{1}(3) = - ...
	g*m_C1;

qa_{2} = zeros(6,1);

qa_{2}(1) = K_L*u2;
qa_{2}(3) = - ...
	g*m_C2;

qa_{3} = zeros(6,1);

qa_{3}(3) = - ...
	(1079*g)/500;
qa_{3}(5) = (20813*g)/50000;

qa_{4} = zeros(7,1);

qa_{4}(3) = - ...
	(3142485719189569*g)/562949953421312;
qa_{4}(4) = - ...
	(7407986008894379*EA2_q001*g)/72057594037927936;
qa_{4}(5) = - ...
	(g*(4709707183750797*EA2_q001 - ...
	 8804736243303947221300412416))/2475880078570760549798248448;
qa_{4}(6) = - ...
	K_R*u3;
qa_{4}(7) = - ...
	(2355369729792797*K_R*u3)/18014398509481984;

qa_{5} = zeros(7,1);

qa_{5}(3) = - ...
	(61051*g)/31250;
qa_{5}(4) = - ...
	(5954682815617887*EA3_q001*g)/72057594037927936;
qa_{5}(5) = - ...
	(g*(5153537714989119*EA3_q001 - ...
	 284882298419310817479491584))/618970019642690137449562112;
qa_{5}(6) = K_R*u3;

qa_{6} = zeros(6,1);

qa_{6}(1) = - ...
	g*m_ee*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
qa_{6}(2) = - ...
	g*m_ee*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_);
qa_{6}(3) = - ...
	g*m_ee*cos(elBo_EA3_2_rot_x_);

qa_{7} = zeros(6,1);

qa_{7}(3) = - ...
	g*m_mb;
result_ = vertcat(qa_{:});


% END OF FILE

