function u_control = control_inputs(t, x_)
% control_inputs - Wrapper function for control inputs
% These inputs are stored under sys.in.control
% --------------------- Automatically generated file! ---------------------

u_control = zeros(3,1);

u_control(1) = f_u1(t, x_);
u_control(2) = f_u2(t, x_);
u_control(3) = f_u3(t, x_);

% END OF FILE

