function res_ = f_Du2(t, x_, u_, varargin)
% f_Du2 - definition of state-depending user-defined variable Du2
%
% If you want to use this parameter as a state-dependent parameter in the
% definition of a coordinate system, please also define the derivatives
% with respect to the local coordinate directions x1 (Cases II and III), in
% which the parameter is used. This is necessary to calculate the jacobian
% matrices: Jt = dr/dy = dr/dx1 * dx1/dy
%
% This is not necessary if this parameter is to be a parameter of a force
% element. In both cases if you want to linearize the equations of motion,
% Case IV is necessary, where you can return a symbolic expression for this
% parameter.
%
% Positions, velocities, ... of coordinate systems can be calculated by
% calling the functions in 'sysFunctions', e.g. body1_r

global sys;

res_ = 0; % Default return value
calcDerivatives_ = false;

if(length(varargin)>1 && ischar(varargin{1}) && strcmp(varargin{1},'derivative'))
	% Case II: Call was f_...(t, x_,'derivatives', ...)
	% Only necessary if this parameter is used to define a coordinate system
	calcDerivatives_ = true;
	component_ = varargin{2}; % Define axis direction
elseif(nargin==1 && ischar(t) && strcmp(t,'linearization'))
	% Case III: Call was f_...('linearization')
	% Return symbolic expression to calculate the linearization
	% Necessary for all parameters appearing in the equations of motion to linearize!
	res_ = sym('0');
	return;
end

% If you have any parameters to define, do that here
% Definitions start #######################################################

% Definitions stop ########################################################
% Function definitions
if(~calcDerivatives_)
	% Regular call, calculate position
	% Please enter your function here
	% Function start ######################################################
	res_ = zeros(1,1);

	% Function stop  ######################################################
else
	% Please define here the derivatives with respect to the
	% cartesian coordinates of the local coordinate system as numerical
	% values
	% Derivatives start ###################################################
	if(component_ == 1) % x-direction
		
	elseif(component_ == 2) % y-direction
		
	elseif(component_ == 3) % z-direction
		
	end
	% Derivatives stop  ###################################################
end

% END OF FILE

