#ifndef FD3A2D_userDefined_H
#define FD3A2D_userDefined_H

#include "FD3A2D_paraStruct.h"
/* Time dependent values */

/* User-defined signals */

/* State dependent values */

double FD3A2D_f_u1(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_Du1(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_D2u1(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_u2(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_Du2(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_D2u2(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_u3(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_Du3(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_f_D2u3(double t, double *x_, double *u_, void *dataPtr);

/* Abbreviations */

double FD3A2D_f_DelBo_EA1_2_x_(double *x_);

double FD3A2D_f_DelBo_EA2_1_rot_z_(double *x_);

double FD3A2D_f_DelBo_EA2_1_x_(double *x_);

double FD3A2D_f_DelBo_EA2_2_rot_z_(double *x_);

double FD3A2D_f_DelBo_EA2_2_x_(double *x_);

double FD3A2D_f_DelBo_EA2_3_rot_z_(double *x_);

double FD3A2D_f_DelBo_EA2_3_x_(double *x_);

double FD3A2D_f_DelBo_EA2_3_y_(double *x_);

double FD3A2D_f_DelBo_EA3_2_rot_x_(double *x_);

double FD3A2D_f_DelBo_EA3_2_rot_z_(double *x_);

double FD3A2D_f_DelBo_EA3_2_x_(double *x_);

double FD3A2D_f_DelBo_EA3_2_y_(double *x_);

double FD3A2D_f_elBo_EA1_2_x_(double *x_);

double FD3A2D_f_elBo_EA2_1_rot_z_(double *x_);

double FD3A2D_f_elBo_EA2_1_x_(double *x_);

double FD3A2D_f_elBo_EA2_2_rot_z_(double *x_);

double FD3A2D_f_elBo_EA2_2_x_(double *x_);

double FD3A2D_f_elBo_EA2_3_rot_z_(double *x_);

double FD3A2D_f_elBo_EA2_3_x_(double *x_);

double FD3A2D_f_elBo_EA2_3_y_(double *x_);

double FD3A2D_f_elBo_EA3_2_rot_x_(double *x_);

double FD3A2D_f_elBo_EA3_2_rot_z_(double *x_);

double FD3A2D_f_elBo_EA3_2_x_(double *x_);

double FD3A2D_f_elBo_EA3_2_y_(double *x_);

/* Force elements */
double forces_LDD1(double t, double *x_, double *u_, double *f_LDD1, void *dataPtr);
double forces_LDD2(double t, double *x_, double *u_, double *f_LDD2, void *dataPtr);
double forces_RDD3(double t, double *x_, double *u_, double *f_RDD3, void *dataPtr);
/* Input groups */

void FD3A2D_f_control_inputs(double t, double *x_, double *u_, void *dataPtr);

/* System outputs */

double FD3A2D_output_Pos_x(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Pos_y(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Rot_angle_theta(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Vel_x(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Vel_y(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Rot_velocity(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Pos_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Pos_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Rot_angle_rlc_theta(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Vel_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Vel_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Rot_vel_rlc(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Curvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_Curvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_DCurvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double FD3A2D_output_DCurvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

/* Output groups */

void FD3A2D_f_exactOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void FD3A2D_f_approxOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void FD3A2D_f_Curvature_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

#endif
