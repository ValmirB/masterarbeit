/**
 * @file neweul.h
 * --------------------------------
 *
 * @brief Additional math operations
 *
 * @version 0.9
 * 
 * @date 2014-04-22
 *
 *
 * @em Neweul-M2 \n
 * Multibody systems based on symbolic Newton-Euler-Equations \n
 * Copyright (c) ITM University of Stuttgart
 * <a href="http://www.itm.uni-stuttgart.de">www.itm.uni-stuttgart.de</a>
 *
 */

#ifndef NEWEUL_H
#define NEWEUL_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#if defined(__linux__)
#include <time.h>
#endif

/* DEFINES */

#define EPS 2.22e-16

/* FUNCTION PROTOTYPES*/

/**
 * @brief Perform a Cholesky decomposition of the symmetric positive definite matrix A.
 * The decomposition is perfomed on the same memory. The lower triagle of A contains L after the function call.
 *
 * @sa cholDecomposition, cholDecomposition2dim 
 *
 * @param n Dimension of the matrix
 * @param A Pointer to the matrix A[n][n]
 * @return short int
 */
short cholesky(unsigned short n, double **A);

/**
 * @brief Perform a LDL^T decomposition of the symmetric positive definite matrix A.
 * The decomposition is perfomed on the same memory. The lower triagle of A contains L after the function call.
 *
 * @sa cholDecomposition, cholDecomposition2dim 
 *
 * @param n Dimension of the matrix
 * @param A Pointer to the matrix A[n][n]
 * @return short int
 */
short LDLT(unsigned short n, double **A);

/**
 * @brief Solve the LES A*x=b with a Cholesky decomposition. The right-hand side b is a vector.
 * The decomposition is perfomed on the same memory, so A contains L and b contains x after the function call.
 * 
 * @par Example
 * @code{.c}
 * 
 * double ** A = callocMatrix(2,2);
 * double * b = (double *) calloc(2, sizeof(double));
 * 
 * short res;
 * 
 * A[0][0] = 2.0;
 * A[0][1] = 1.0;
 * A[1][0] = 1.0;
 * A[1][1] = 4.0;
 * 
 * b[0] = 0.5;
 * b[1] = -1.0;
 * 
 * res = cholDecomposition(2, A, b);
 * 
 * freeMatrix(2,2,A);
 * free(b);
 * 
 * @endcode
 * 
 * @sa cholesky, LLTForBack
 * 
 * @param n Dimension of the matrix
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the vector b[n]
 * @return short int
 */
short cholDecomposition(unsigned short n, double **A, double *b);

/**
 * @brief Solve the LES A*X=B with a Cholesky decomposition. The right-hand side B is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @par Example
 * @code{.c}
 * 
 * double ** A = callocMatrix(2,2);
 * double ** B = callocMatrix(2,2);
 * 
 * short res;
 * 
 * A[0][0] = 2.0;
 * A[0][1] = 1.0;
 * A[1][0] = 1.0;
 * A[1][1] = 4.0;
 * 
 * B[0][0] = 0.5;
 * B[1][1] = -1.0;
 * 
 * res = cholDecomposition2dim(2, 2, A, B);
 * 
 * freeMatrix(2,2,A);
 * freeMatrix(2,2,B);
 * 
 * @endcode
 * 
 * @sa cholesky, LLTForBack2dim
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[n][nrhs]
 * @return short int
 */
short cholDecomposition2dim(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Solve the LES L*L^T*X=B. The matrix A is already Cholesky decomposed. The right-hand side b is a vector.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @sa cholesky, LLTForBack2dim
 * 
 * @param n Dimension of the matrix A
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the vector b[n]
 * @return short int
 */
void LLTForBack(unsigned short n, double **A, double *b);

/**
 * @brief Solve the LES L*L^T*X=B. The matrix A is already Cholesky decomposed. The right-hand side is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @sa cholesky, LLTForBack
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[n][nrhs]
 * @return short int
 */
void LLTForBack2dim(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Solve the LES L*D*L^T*X=B. The matrix A is already LDL^T decomposed. The right-hand side is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @sa cholesky, LLTForBack
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[n][nrhs]
 * @return short int
 */
short LDLTDecomposition2dim(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Solve the LES L*D*L^T*x=b. The matrix A is already LDL^T decomposed. The right-hand side is a vector.
 * The decomposition is perfomed on the same memory, therefore A is over-written and b contains x after the function call.
 * 
 * @sa cholesky, LLTForBack
 * 
 * @param n Dimension of the matrix A
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the matrix b[n]
 * @return short int
 */
short LDLTDecomposition(unsigned short n, double **A, double *b);

/**
 * @brief Solve the LES L*D*L^T*X=B. The matrix A is already LDL^T decomposed. The right-hand side is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @sa cholesky, LLTForBack
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[n][nrhs]
 * @return short int
 */
void LDLTForBack2dim(unsigned short n, unsigned short nrhs,  double **A, double **B);

/**
 * @brief Solve the LES L*D*L^T*x=b. The matrix A is already LDL^T decomposed. The right-hand side is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and b contains x after the function call.
 * 
 * @sa cholesky, LLTForBack
 * 
 * @param n Dimension of the matrix A
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the matrix b[n]
 * @return short int
 */
void LDLTForBack(unsigned short n, double **A, double *b);

/**
 * @brief Solve the LES A*X=B with a QR decomposition. The right-hand side is a vector.
 * The decomposition is perfomed on the same memory, therefore A is over-written and b contains x after the function call.
 * 
 * @sa householderQR, MatrixVectorMultiplication, backSub
 * 
 * @param n Dimension of the matrix A
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the vector b[n]
 * @return short int
 */
short solveLES(unsigned short n, double **A, double *b);

/**
 * @brief Solve the LES A*X=B with a QR decomposition. The right-hand side is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @sa householderQR, MatrixMultiplication, backSub2dim
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[n][nrhs]
 * @return short int
 */
short solveLES2dim(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Forward substitution for a lower triangular matrix stored in A and a vector-valued right-hand side b.
 * 
 * @sa forwardSub2dim, forwardSub2dimTransposed, forwardSubNT
 *
 * @param n Dimension of the matrix A
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the vector b[n]
 * @return short int
 **/
short forwardSub(unsigned short n, double **A, double *b);

/**
 * @brief Forward substitution for a lower triangular matrix stored in A. The right-hand side B is a matrix.
 *
 * @sa forwardSub, forwardSub2dimTransposed, forwardSubNT
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[n][nrhs]
 * @return short int
 **/
short forwardSub2dim(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Forward substitution for a lower triangular matrix stored in A. The right-hand side B is a transposed matrix.
 *
 * @sa forwardSub, forwardSub2dim, forwardSubNT
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[nrhs][n]
 * @return short int
 **/
short forwardSub2dimTransposed(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Forward substitution for a lower triangular matrix stored in the transposed matrix A and a vector-valued right-hand side b.
 *
 *  @sa forwardSub, forwardSub2dim, forwardSub2dimTransposed
 * 
 * @param nt Char ('N' or 'T') defining whether the matrix A should be transposed or not
 * @param n Dimension of the matrix A
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the vector b[n]
 * @return short int
 **/
short forwardSubNT(char nt, unsigned short n,  double **A, double *b);

/**
 * @brief Backward substitution for a upper triangular matrix stored in A. The right-hand side b is a vector.
 *
 * @sa forwardSub, forwardSub2dim, forwardSub2dimTransposed, forwardSubNT, backSub2dim, backSub2dimTransposed
 * 
 * @param n Dimension of the matrix A
 * @param A Pointer to the matrix A[n][n]
 * @param b Pointer to the vector b[n]
 * @return short int
 **/
short backSub(unsigned short n, double **A, double *b);

/**
 * @brief Backward substitution for a upper triangular matrix stored in A. The right-hand side B is a matrix.
 *
 * @sa forwardSub, forwardSub2dim, forwardSub2dimTransposed, forwardSubNT, backSub, backSub2dimTransposed
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[n][nrhs]
 * @return short int
 **/
short backSub2dim(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Backward substitution for a upper triangular matrix stored in A. The right-hand side B is a transposed matrix.
 *
 * @sa forwardSub, forwardSub2dim, forwardSub2dimTransposed, forwardSubNT, backSub, backSub2dim
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer to the matrix A[n][n]
 * @param B Pointer to the matrix B[nrhs][n]
 * @return short int
 **/
short backSub2dimTransposed(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Solve the LES A*X=B in case A contains a LU decomposition. The right-hand side is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @sa forwardSub, forwardSub2dim, forwardSub2dimTransposed, forwardSubNT, backSub2dim, backSub2dimTransposed
 * 
 * @sa GaussianElimination, InversionByLU
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param A Pointer the matrix A[n][n]
 * @param B Pointer the matrix B[n][nrhs]
 * @return short int
 */
short LUForBack(unsigned short n, unsigned short nrhs, double **A, double **B);

/**
 * @brief Solve the LES A*x=b in case A contains a LU decomposition. The right-hand side b is a vector.
 * The decomposition is perfomed on the same memory, therefore A is over-written and b contains x after the function call.
 * 
 * @sa GaussianElimination, InversionByLU1dim
 * 
 * @param n Dimension of the matrix A
 * @param A Pointer the matrix A[n][n]
 * @param b Pointer the vector b[n]
 * @return short int
 */
short LUForBack_1dim(unsigned short n, double **A, double *b);

/**
 * @brief Matrix multiplication according to
 * C = alpha*A^(*)*B^(*)+beta*C
 * C will over-written, A and B are unchanged
 * 
 * @sa MatrixMultiplicationMod, MatrixVectorMultiplication, MatrixVectorMultiplicationMod
 *  
 * @note In case both matrices are transposed a tremendous performance drop for large arrays will occur!
 * 
 * @param nt Char ('N' or 'T') defining whether the matrix A should be transposed
 * @param nt2 Char ('N' or 'T') defining whether the matrix B should be transposed
 * @param n First dimension of A and C
 * @param m Second dimension of A and first dimension of B
 * @param l Second dimension of B and C
 * @param alpha Factor which is multiplied with A*B
 * @param A Pointer to the matrix A[n][m]
 * @param B Pointer to the matrix B[m][l]
 * @param beta Factor which is multiplied with C
 * @param C Pointer to the matrix C[n][l]
 * @return void
 */
void MatrixMultiplication(char nt, char nt2, unsigned short n, unsigned short m, unsigned short l, double alpha, double **A,
		double **B, double beta, double **C);

/**
 * @brief Matrix multiplication according to
 * D = alpha*A^(*)*B^(*)+beta*C
 * D will over-written, A, B and C are unchanged
 * 
 * @sa MatrixMultiplication, MatrixVectorMultiplication, MatrixVectorMultiplicationMod
 * 
 * @note In case both matrices are transposed a tremendous performance drop for large arrays will occur!
 * 
 * @param nt Char ('N' or 'T') defining whether the matrix A should be transposed
 * @param nt2 Char ('N' or 'T') defining whether the matrix B should be transposed
 * @param n First dimension of A and C
 * @param m Second dimension of A and first dimension of B
 * @param l Second dimension of B and C
 * @param alpha Factor which is multiplied with A*B
 * @param A Pointer to the matrix A[n][m]
 * @param B Pointer to the matrix B[m][l]
 * @param beta Factor which is multiplied with C
 * @param C Pointer to the matrix C[n][l]
 * @param D Pointer to the matrix D[n][l]
 * @return void
 */
void MatrixMultiplicationMod(char nt, char nt2, unsigned short n, unsigned short m, unsigned short l, double alpha, double **A,
		double **B, double beta, double **C, double **D);

/**
 * @brief Matrix-vector multiplication according to
 * c = alpha*A^(*)*b+beta*c
 * c will over-written, A and b are unchanged
 * 
 * @sa MatrixVectorMultiplicationMod, MatrixMultiplication, MatrixMultiplicationMod 
 * 
 * @param nt Char ('N' or 'T') defining whether the matrix A should be transposed
 * @param n First dimension of A and c
 * @param m Dimension of b
 * @param alpha Factor which is multiplied with A*b
 * @param A Pointer to the matrix A[n][m]
 * @param b Pointer to the vector b[m]
 * @param beta Factor which is multiplied with c
 * @param c Pointer to the vector c[n]
 * @return void
 */
void MatrixVectorMultiplication(char nt, unsigned short n, unsigned short m, double alpha, double **A,
		double *b, double beta, double *c);

/**
 * @brief Matrix-vector multiplication according to
 * c = alpha*A^(*)*b+beta*c
 * d will over-written, A, b and c are unchanged
 * 
 * @sa MatrixVectorMultiplication, MatrixMultiplication, MatrixMultiplicationMod 
 * 
 * @param nt Char ('N' or 'T') defining whether the matrix A should be transposed
 * @param n First dimension of A and c
 * @param m Dimension of b
 * @param alpha Factor which is multiplied with A*b
 * @param A Pointer to the matrix A[n][m]
 * @param b Pointer to the vector b[m]
 * @param beta Factor which is multiplied with c
 * @param c Pointer to the vector c[n]
 * @param d Pointer to the vector d[n]
 * @return void
 */
void MatrixVectorMultiplicationMod(char nt, unsigned short n, unsigned short m, double alpha, double **A,
		double *b, double beta, double *c, double *d);

/**
 * @brief Vector cross-product according to
 * c = alpha*r_tilde*b + beta*c
 * c will over-written, r and b are unchanged
 * 
 * @sa MatrixCrossProduct
 * 
 * @param alpha Factor which is multiplied with r_tilde*b
 * @param r Pointer to the vector r[3]
 * @param b Pointer to the vector b[3]
 * @param beta Scalar which is multiplied with c
 * @param c Pointer to the vector c[3]
 * @return void
 */
void VectorCrossProduct(double alpha, double *r, double *b, double beta, double *c);

/**
 * @brief Vector cross-product for multiple right-hand sides according to
 * C = alpha*r_tilde*B + beta*C
 * C will over-written, r and B are unchanged
 * 
 * @sa VectorCrossProduct
 * 
 * @param n Number of right-hand sides
 * @param alpha Factor which is multiplied with r_tilde*B
 * @param r Pointer to the 3-dimensional vector r
 * @param B Pointer to the matrix B[3][n]
 * @param beta Factor which is multiplied with C
 * @param C Pointer to the matrix C[3][n]
 * @return void
 */
void MatrixCrossProduct(unsigned short n, double alpha, double *r, double **B, double beta, double **C);

/**
 * @brief Vector transformation, e.g. rotation according to
 * b = S*b
 * b will be updated, S remains unchanged
 * 
 * @param nt Char ('N' or 'T') defining whether the matrix S should be transposed
 * @param S Pointer to the matrix S[3][3]
 * @param b Pointer to the vector b[3]
 * @return void
 */
void transformVector(char nt, double **S, double *b);

/**
 * @brief Matrix transformation, e.g. rotation according to
 * B = S*B
 * B will be updated, S remains unchanged
 * 
 * @param nt Char ('N' or 'T') defining whether the matrix S should be transposed
 * @param n Number of right-hand sides
 * @param S Pointer to the matrix S[3][3]
 * @param B Pointer to the matrix B[3][n]
 * @return void
 */
void transformMatrix(char nt, unsigned short n, double **S, double **B);

/**
 * @brief LU decomposition based on Gaussian elimination with pivoting 
 * according to
 * A = L*U
 * 
 * @sa InversionByLU, InversionByLU1dim
 * 
 * @param n Dimension of the matrix A
 * @param z Pointer to the permuation vector z[n] (unsigned short)
 * @param A Pointer the matrix A[n][n]
 * @return short int
 */
short GaussianElimination(unsigned short n, unsigned short *z, double **A);

/**
 * @brief Solve the LES A*X=B with a LU decomposition. The right-hand side is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and B contains X after the function call.
 * 
 * @sa GaussianElimination, permuteMatrix, LUForBack
 * 
 * @param n Dimension of the matrix A
 * @param nrhs Number of right-hand sides
 * @param z Pointer to the permuation vector z[n] (unsigned short)
 * @param A Pointer the matrix A[n][n]
 * @param B Pointer the matrix B[n][nrhs]
 * @return short int
 */
short InversionByLU(unsigned short n, unsigned short nrhs, unsigned short *z, double **A, double **B);

/**
 * @brief Solve the LES A*x=b with a LU decomposition. The right-hand side b is a matrix.
 * The decomposition is perfomed on the same memory, therefore A is over-written and b contains x after the function call.
 * 
 * @sa GaussianElimination, permuteVector, LUForBack_1dim
 * 
 * @param n Dimension of the matrix A
 * @param z Pointer to the permuation vector z[n] (unsigned short)
 * @param A Pointer the matrix A[n][n]
 * @param b Pointer the vector b[n]
 * @return short int
 */
short InversionByLU1dim(unsigned short n, unsigned short *z, double **A, double *b);

/**
 * @brief Perform a matrix transponation on the same memory.
 *
 * @param n Dimension of the matrix A
 * @param A Pointer the matrix A[n][n]
 * @return void
 **/
void squareMatrixTranspose(unsigned short n, double **A);

/**
 * @brief Perform a column permutation of a matrix A on the same memory.
 *
 * @param n First dimension of the matrix A
 * @param m Second dimension of the matrix A
 * @param z Pointer the permutation vector z[n] (unsigned short)
 * @param A Pointer the matrix A[n][m]
 * @return void
 **/
void permuteMatrix(unsigned short n, unsigned short m, unsigned short *z, double **A);

/**
 * @brief Perform a column permutation of a matrix A on the same memory.
 *
 * @param n First dimension of the matrix a
 * @param z Pointer the permutation vector z[n] (unsigned short)
 * @param a Pointer the matrix a[n]
 * @return void
 **/
void permuteVector(unsigned short n, unsigned short *z, double *a);

/**
 * @brief Compute the infinity norm of a matrix A.
 *
 * @param n First dimension of the matrix A
 * @param m Second dimension of the matrix A
 * @param A Pointer the matrix A[n][m]
 * @return double
 **/
double infinityNorm(unsigned short n, unsigned short m, double **A);

/**
 * @brief Maximum value of [a b]
 *
 * @param a Scalar
 * @param b Scalar
 * @return double
 **/
double maxDouble(double a, double b);

/**
 * @brief Minimal value of [a b]
 *
 * @param a Scalar
 * @param b Scalar
 * @return double
 **/
double minDouble(double a, double b);

/**
 * @brief Get sign of the scalar a
 *
 * @param a Scalar
 * @return double
 **/
double signum(double a);

/**
 * @brief Overwrite all values of the matrix A with zeros.
 *
 * @param n First dimension of the matrix A
 * @param m Second dimension of the matrix A
 * @param A Pointer the matrix A[n][m]
 * @return void
 **/
void clearMatrix(unsigned short n, unsigned short m, double **A);

/**
 * @brief Overwrite all values of the vector x with zeros.
 *
 * @param n Dimension of the vector x
 * @param x Pointer the matrix x[n]
 * @return void
 **/
void clearVector(unsigned short n, double *x);

/**
 * @brief Explicit inversion of a 2x2 matrix A
 *
 * @param A Pointer the matrix A[2][2]
 * @return short int
 **/
short inversion2x2(double **A);

/**
 * @brief Compute the dyadic product of a vector with itself 
 *
 * @param dim Dimension of the vector
 * @param vector Pointer the vector[dim]
 * @param matrix Pointer the matrix[dim][dim]
 * @return void
 **/
void dyadicProduct(unsigned short dim, double *vector, double **matrix);

/**
 * @brief Compute the dyadic product of vector1 and vector2
 *
 * @param n Dimension of the vector1
 * @param m Dimension of the vector2
 * @param vector1 Pointer the vector1[n]
 * @param vector2 Pointer the vector2[m]
 * @param matrix Pointer the matrix[n][m]
 * @return void
 **/
void dyadicProduct2(unsigned short n, unsigned short m, double *vector1, double *vector2, double **matrix);

/**
 * @brief Perform a Householder reflection
 * 
 * @sa householderQR
 *
 * @param n Dimension of x and v
 * @param x IN: Pointer to the vector x[n]
 * @param v OUT: Pointer to the vector v[n]
 * @param beta OUT: Pointer to the scalar beta
 * @return void
 **/
void house(unsigned short n, const double *x, double *v, double *beta);

/**
 * @brief Perform a Householder Bidiagonalization
 * 
 * @note DO BE DONE!
 * 
 * @param m First dimension of A
 * @param n Second dimension of A
 * @param A IN: Pointer to the matrix A[m][n]
 * @param U OUT: Pointer to the matrix U[m][n]
 * @param V OUT: Pointer to the matrix V[m][n]
 * @return short int
 */
short householderBidiag(unsigned short m, unsigned short n, double **A, double **U, double **V);

/**
 * @brief QR decomposition of the matrix A using Householder reflections according to
 * A = Q*R.
 * Performs on the same memory, so A contains R after fucntion call.
 * 
 * @sa house, givensQR
 * 
 * @param m First dimension of the matrix A
 * @param n Second dimension of the matrix A
 * @param A IN/OUT: Pointer to the matrix A[m][n]
 * @param Q OUT: Pointer to the matrix Q[m][m] (allocated by user)
 * @return short int
 */
short householderQR(unsigned short m, unsigned short n, double **A, double **Q);

/**
 * @brief Perform a Givens rotation
 * 
 * @sa givensQR
 * 
 * @param a Scalar a
 * @param b Scalar b
 * @param c OUT: Pointer to the scalar c
 * @param s OUT: Pointer to the scalar s
 * @return void
 */
void givensrotation(double a, double b, double *c, double *s);

/**
 * @brief QR decomposition of the matrix A using Givens rotations according to
 * A = Q*R
 * Performs on the same memory, so A contains R after fucntion call.
 * 
 * @sa givensrotation, householderQR
 * 
 * @param m First dimension of the matrix A
 * @param n Second dimension of the matrix A
 * @param A IN/OUT: Pointer to the matrix A[m][n]
 * @param Q OUT: Pointer to the matrix Q[m][m] (allocated by user)
 * @return short int
 */
short givensQR(unsigned short m, unsigned short n, double **A, double **Q);

/**
 * @brief Allocate memory for a matrix.
 * 
 * @sa freeMatrix, callocTensor
 * 
 * @param m First dimension of the matrix
 * @param n Second dimension of the matrix
 * @return double**
 */
double** callocMatrix(unsigned short m, unsigned short n);

/**
 * @brief Create a random matrix 
 * 
 * @sa callocMatrix, freeMatrix
 * 
 * @param m First dimension of the matrix
 * @param n Second dimension of the matrix
 * @param minVal Smallest number in the random distribution
 * @param maxVal Largest number in the random distribution
 * @return double**
 */
double** createRandomMatrix(unsigned short m, unsigned short n, double minVal, double maxVal);

/**
 * @brief Allocate memory for a third-order tensor.
 * 
 * @sa callocMatrix, freeTensor
 * 
 * @param m First dimension of the matrix
 * @param n Second dimension of the matrix
 * @param l Third dimension of the matrix
 * @return double***
 */
double ***callocTensor( unsigned short m, unsigned short n, unsigned short l);

/**
 * @brief Free allocated memory of the matrix A.
 * 
 * @sa callocMatrix, freeTensor
 * 
 * @param m First dimension of the matrix A
 * @param n Second dimension of the matrix A
 * @param A Pointer to the matrix A[m][n]
 * @return void
 */
void freeMatrix(unsigned short m, unsigned short n, double **A);

void freeIntMatrix( unsigned short m, unsigned short n, int **A);

/**
 * @brief Free allocated memory of the tensor A.
 * 
 * @sa callocTensor, freeMatrix
 * 
 * @param m First dimension of the matrix A
 * @param n Second dimension of the matrix A
 * @param l Third dimension of the matrix A
 * @param A Pointer to the tensor A[m][n][l]
 * @return void
 */
void freeTensor( unsigned short m, unsigned short n, unsigned short l, double ***A);

/**
 * @brief Two-sided transformation of the matrix M with Jr and Jl according to
 * M := Jr*((transpose(Jl)*M*Jr)^(-1))*transposed(Jl)
 * 
 * @sa MatrixMultiplication, InversionByLU, callocMatrix, freeMatrix
 * 
 * @param oldDim Dimension of the input matrix M
 * @param newDim Dimension of the subspace
 * @param Jr Pointer to the matrix A[oldDim][newDim]
 * @param Jl Pointer to the matrix A[oldDim][newDim]
 * @param M Pointer to the matrix A[oldDim][oldDim]
 * @return short int
 */
short reduceMassMatrix(unsigned short oldDim, unsigned short newDim, double **Jr, double **Jl, double **M);


/**
 * @brief ...
 * 
 * @param ext ...
 * @param stateSpace ...
 * @param subSpace ...
 * @param Jr ...
 * @param Jl ...
 * @param M ...
 * @param f ...
 * @param gamma ...
 * @return short int
 */
short projectSystemDynamics(unsigned short ext, unsigned short stateSpace, unsigned short subSpace, double **Jr,  double **Jl, double **M, double *f, double *gamma);

void getProjectionMatrix(unsigned m, unsigned n, double **C_, double **Q_, double **R_);

void posUpdate(double *r, double **S, double *v, double *y);

/**
 * @brief Perform Horner scheme to evaluate a polynominal
 * 
 * @param n Number of coefficients
 * @param a Pointer to a vector containing the coefficients a[n]
 * @param x Argument
 * @return double
 */
double hornerScheme(unsigned short n, double *a, double x);

/**
 * @brief Computes the taxicab norm of the vector vec
 * 
 * @param dim Dimension of the vector
 * @param vec Pointer to the vector vec[dim]
 * @return double
 */
double taxicabNorm_vec(unsigned short dim, double *vec);

/**
 * @brief Computes the maximum norm of the vector vec
 * 
 * @param dim Dimension of the vector
 * @param vec Pointer to the vector vec[dim]
 * @return double
 */
double maximumNorm_vec(unsigned short dim, double *vec);

/**
 * @brief Computes the Euclidean norm of the vector vec
 * 
 * @param dim Dimension of the vector
 * @param vec Pointer to the vector vec[dim]
 * @return double
 */
double euclideanNorm_vec(unsigned short dim, double *vec);

/**
 * @brief Extract the quaternions of the rotaion matrix S
 * 
 * @param S IN: Pointer to the matrix S[3][3]
 * @param w OUT: Pointer to the scalar w
 * @param x OUT: Pointer to the scalar x
 * @param y OUT: Pointer to the scalar y
 * @param z OUT: Pointer to the scalar z
 * @return void
 */
void rotmat2quat(double **S, double *w, double *x, double *y, double *z);

/**
 * @brief Compute the rotation matrix S for the quaternions w,x,y,z
 * 
 * @param S OUT: Pointer to the roation matrix S[3][3]
 * @param w IN: Pointer to the scalar w
 * @param x IN: Pointer to the scalar x
 * @param y IN: Pointer to the scalar y
 * @param z IN: Pointer to the scalar z
 * @return void
 */
void quat2rotmat(double **S, double *w, double *x, double *y, double *z);

/**
 * @brief Extract the Kardan angles of the rotation matrix S
 * 
 * @param S IN: Pointer to the matrix S[3][3]
 * @param phi OUT: Pointer to the Kardan angles phi[3]
 * @param type Rotation order as char
 * kardan angles "xyz"
 * euler angles  "zxz"
 * "xyx"
 * "xzx"
 * "xzy"
 * "yxy"
 * "yzy"
 * "yxz"
 * "yzx"
 * "zyz"
 * "zxy"
 * "zyx"
 * @return short int
 */
short getPhi(double **S, double *phi, const char *type);

/**
 * @brief Euler integration scheme
 * 
 * @param nx Dimension of state vector x_k
 * @param h Step
 * @param t_k Current time
 * @param x_k Current state vector x_k[n]
 * @param u_k Input vector
 * @param x_k1 Computed state vector at t=t_k+h x_k1[n]
 * @param data Pointer to user defined parameters
 * @param systemDynamics Function pointer for the system dynamics 
 * @return double t_k1
 */
double Euler(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *));

/**
 * @brief Second-order Heun integration scheme
 * 
 * @param nx Dimension of state vector x_k
 * @param h Step
 * @param t_k Current time
 * @param x_k Current state vector x_k[nx]
 * @param u_k Input vector
 * @param x_k1 Computed state vector at t=t_k+h x_k1[nx]
 * @param data Pointer to user defined parameters
 * @param systemDynamics Function pointer for the system dynamics 
 * @return double t_k1
 */
double Heun2(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *));

/**
 * @brief Third-order Heun integration scheme
 * 
 * @param nx Dimension of state vector x_k
 * @param h Step
 * @param t_k Current time
 * @param x_k Current state vector x_k[nx]
 * @param u_k Input vector
 * @param x_k1 Computed state vector at t=t_k+h x_k1[nx]
 * @param data Pointer to user defined parameters
 * @param systemDynamics Function pointer for the system dynamics 
 * @return double t_k1
 */
double Heun3(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *));

/**
 * @brief Second-order Runge-Kutta integration scheme
 * 
 * @param nx Dimension of state vector x_k
 * @param h Step
 * @param t_k Current time
 * @param x_k Current state vector x_k[nx]
 * @param u_k Input vector
 * @param x_k1 Computed state vector at t=t_k+h x_k1[nx]
 * @param data Pointer to user defined parameters
 * @param systemDynamics Function pointer for the system dynamics 
 * @return double t_k1
 */
double RK2(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *));

/**
 * @brief Third-order Runge-Kutta integration scheme
 * 
 * @param nx Dimension of state vector x_k
 * @param h Step
 * @param t_k Current time
 * @param x_k Current state vector x_k[nx]
 * @param u_k Input vector
 * @param x_k1 Computed state vector at t=t_k+h x_k1[nx]
 * @param data Pointer to user defined parameters
 * @param systemDynamics Function pointer for the system dynamics 
 * @return double t_k1
 */
double RK3(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *));

/**
 * @brief Fourth-order Runge-Kutta integration scheme
 * 
 * @param nx Dimension of state vector x_k
 * @param h Step
 * @param t_k Current time
 * @param x_k Current state vector x_k[nx]
 * @param u_k Input vector
 * @param x_k1 Computed state vector at t=t_k+h x_k1[nx]
 * @param data Pointer to user defined parameters
 * @param systemDynamics Function pointer for the system dynamics 
 * @return double t_k1
 */
double RK4(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *));


double getDirectionT(double *r, double *v);

double getDirectionR(double *d, double **S);

double scalarProduct(unsigned short n, double *a, double *b);

/**
 * @brief Print status message to log file "neweulm2Log.txt"
 * 
 * @param logMessage String containing the message
 * @return void
 */
void statusOutput(const char *logMessage);


/**
 * @brief Print out matrix
 * 
 * @param n First dimension of the matrix A
 * @param m Second dimension of the matrix A
 * @param A Pointer to the matrix A[n][m]
 * @param ident Identifier
 * @return void
 */
void printMatrix(unsigned short n, unsigned short m, double **A, const char *ident);

#endif
