#ifndef FD3A2D_pd_matlab_H
#define FD3A2D_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "FD3A2D_paraStruct.h"

double FD3A2D_getScalar(const void *context, const char *name, double defaultValue);

int FD3A2D_getSystemStruct(void *dataPtr, const void *context);

void FD3A2D_educatedGuess(double t, double *y_, void *dataPtr);
void FD3A2D_educatedGuess(double t, double *y_, void *dataPtr);

#endif
