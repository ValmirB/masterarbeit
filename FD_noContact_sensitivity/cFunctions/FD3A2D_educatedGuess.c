#include <math.h>
#include "FD3A2D_paraStruct.h"
#include "FD3A2D_pi_code.h"
#include "FD3A2D_pd_matlab.h"
#include "FD3A2D_userDefined.h"
#include "neweul.h"

void FD3A2D_educatedGuess(double t, double *y_, void *dataPtr);

void FD3A2D_educatedGuess(double t, double *y_, void *dataPtr){

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
