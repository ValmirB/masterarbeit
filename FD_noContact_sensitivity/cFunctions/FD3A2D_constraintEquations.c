#include "FD3A2D_userDefined.h"
#include <stdio.h>
#include <math.h>
#include "FD3A2D_pi_code.h"
#include <stdlib.h>
#include "neweul.h"
#include "FD3A2D_paraStruct.h"
#include <string.h>
#include "FD3A2D_Flexor.h"
#include "FD3A2D_constraintEquations.h"
#include "FD3A2D_constraintDerivatives.h"

void FD3A2D_constraintEquations(double t, double *x_, double *u_, void *dataPtr){
/* FD3A2D_CONSTRAINT_EQUATIONS - Assembly of the Jacobian matrix,         */
/* the velocity and accelerations due to the constraints.            */
/* Usage of the qr decomposition to remove the Lagrange multipliers. */

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;

	int i_ = 0;
	int j_ = 0;
	int k_ = 0;

	double **C_     = data->con->C_;
	double *Dc_     = data->con->Dc_;
	double *D2c_    = data->con->D2c_;
	double **G_     = data->con->G_;

	double **Jr_v  = data->con->Jr_v;
	double **Jr_a  = data->con->Jr_a;
	double **Jl_a  = data->con->Jl_a;
	double *theta_ = data->con->theta_;
	double *gamma_ = data->con->gamma_;

	double **Q_c_  = callocMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF);
	double **R_c_  = callocMatrix(FD3A2D_SYS_DOF,(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS));
	double **Q_acc = callocMatrix(FD3A2D_SYS_DOF,(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS));
	double **Q_vel = callocMatrix(FD3A2D_SYS_DOF,FD3A2D_NUM_POSCONSTRAINTS);
	double **R_acc = callocMatrix((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS));
	double **R_vel = callocMatrix((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),FD3A2D_NUM_POSCONSTRAINTS);
	double *temp_  = calloc((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),sizeof(double));
	unsigned short *z = calloc((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),sizeof(unsigned short));

	FD3A2D_constraint_derivatives(t, x_, u_, data);

	for(i_ = 0; i_ < (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS); i_++){
		for(j_ = 0; j_ < FD3A2D_SYS_DOF; j_++){
			R_c_[j_][i_] = C_[i_][j_];
		}
	}

	/* Perform a qr decomposition */
	householderQR(FD3A2D_SYS_DOF, (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), R_c_, Q_c_);

	/* Get subspace */
	for(i_ = 0; i_ < FD3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS); j_++){
			Q_acc[i_][j_] = Q_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < FD3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < FD3A2D_NUM_POSCONSTRAINTS; j_++){
			Q_vel[i_][j_] = Q_c_[i_][j_];
		}
	}

	/* Jacobian mapping independent coordinates to all genCoords: */
	for(i_ = 0; i_ < FD3A2D_SYS_DOF; i_++){
		for(j_ = (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS); j_ < FD3A2D_SYS_DOF; j_++){
			Jr_a[i_][j_-(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS)] = Q_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < FD3A2D_SYS_DOF; i_++){
		for(j_ = FD3A2D_NUM_POSCONSTRAINTS; j_ < FD3A2D_SYS_DOF; j_++){
			Jr_v[i_][j_-FD3A2D_NUM_POSCONSTRAINTS] = Q_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < FD3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < FD3A2D_NUM_INDEPENDENT_GV; j_++){
			Jl_a[j_][i_] = Jr_a[i_][j_];
		}
	}

	for(i_ = 0; i_ < (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS); i_++){
		for(j_ = 0; j_ < (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS); j_++){
			R_acc[j_][i_] = R_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < FD3A2D_NUM_POSCONSTRAINTS; i_++){
		for(j_ = 0; j_ < (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS); j_++){
			R_vel[j_][i_] = R_c_[i_][j_];
		}
	}

	/* Local velocities */
	memcpy(temp_, Dc_, (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS)*sizeof(double));
	InversionByLU1dim((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), z, R_vel, temp_);
	MatrixVectorMultiplication('N', FD3A2D_SYS_DOF, FD3A2D_NUM_POSCONSTRAINTS, -1.0, Q_vel, temp_, 0.0, theta_);

	/* Local accelarations */
	memcpy(temp_, D2c_, (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS)*sizeof(double));
	InversionByLU1dim((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), z, R_acc, temp_);
	MatrixVectorMultiplication('N', FD3A2D_SYS_DOF, (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), -1.0, Q_acc, temp_, 0.0, gamma_);

	free(z);
	free(temp_);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,Q_c_);
	freeMatrix(FD3A2D_SYS_DOF,(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),R_c_);
	freeMatrix(FD3A2D_SYS_DOF,(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),Q_acc);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_NUM_POSCONSTRAINTS,Q_vel);
	freeMatrix((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),R_acc);
	freeMatrix((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),FD3A2D_NUM_POSCONSTRAINTS,R_vel);

}

void FD3A2D_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr){

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;

	double *deltaY       = calloc(2,sizeof(double));
	double *Dy_ind       = calloc(5,sizeof(double));
	double **C_dep       = callocMatrix(2,2);
	double **C_ind       = callocMatrix(2,5);

	double normRes_      = 1.0;
	double myTol_        = 1e-8;

	unsigned short *z    = calloc(2,sizeof(unsigned short));
	int counter_         = 0;
	int numIter_         = 0;
	int maxIter_         = 15;
	int k_ = 0;

	/* y_ contains the educated guess */
	while ( (normRes_ > myTol_) && (numIter_ < maxIter_) ){

		/* get constraintEq */
		FD3A2D_positionConstraints(t, x_, u_, deltaY, C_dep, dataPtr);
		normRes_ = taxicabNorm_vec(2,deltaY);

		/* get deltaY */
		InversionByLU1dim(2, z, C_dep, deltaY);

		normRes_ = maxDouble(taxicabNorm_vec(2,deltaY),normRes_);

		for ( counter_ = 0 ; counter_ < 2 ; counter_++ ){
			x_[data->dependentIndices[counter_]] = x_[data->dependentIndices[counter_]] - deltaY[counter_];
		}

		numIter_ += 1;

	}

	for ( counter_ = 0 ; counter_ < 5 ; counter_++ ){
		Dy_ind[counter_] = x_[data->independentIndices[counter_] + FD3A2D_SYS_DOF];
	}

	for ( counter_ = 0 ; counter_ < 2 ; counter_++ ){
		deltaY[counter_] = 0.0;
	}

	FD3A2D_localVelocityConstraints( t, x_, u_, deltaY, C_ind, dataPtr);

	MatrixVectorMultiplication('N', 2, 5, -1.0,  C_ind, Dy_ind, -1.0, deltaY);

	permuteVector(2, z, deltaY);

	LUForBack_1dim(2, C_dep, deltaY);

	for ( counter_ = 0 ; counter_ < 2 ; counter_++ ){
		x_[data->dependentIndices[counter_] + FD3A2D_SYS_DOF] = deltaY[counter_];
	}

	/* Freeing allocated memory */
	free(deltaY);
	free(Dy_ind);
	free(z);
	freeMatrix(2,2,C_dep);
	freeMatrix(2,5,C_ind);

}

void FD3A2D_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr){

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;

	int i_ = 0;
	int j_ = 0;

	double **C_     = data->con->C_;
	double *Dc_     = data->con->Dc_;
	double *D2c_    = data->con->D2c_;
	double **G_     = data->con->G_;
	double *lambda_ = data->con->lambda_;

	double **M     = callocMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF);
	double **MTemp = callocMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF);
	double **mCxmC = callocMatrix((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS));
	double **GTemp = callocMatrix(FD3A2D_SYS_DOF,(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS));
	double *f      = calloc(FD3A2D_SYS_DOF,sizeof(double));
	unsigned short *z = calloc((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),sizeof(unsigned short));

	FD3A2D_constraint_derivatives(t, x_, u_, dataPtr);


	/* as isempty(G_): GTemp_ = transpose(C_) */
	for(i_ = 0; i_ < (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS); i_++){
		for(j_ = 0; j_ < FD3A2D_SYS_DOF; j_++){
			GTemp[j_][i_] = C_[i_][j_];
		}
	}

	/* Evaluate system dynamics */
	FD3A2D_system_dynamics(t, x_, u_, f, M, dataPtr);

	for(i_ = 0; i_ < FD3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < FD3A2D_SYS_DOF; j_++){
			MTemp[i_][j_] = M[i_][j_];
		}
	}

	/* f = M^(-1)*f */
	cholDecomposition(FD3A2D_SYS_DOF, M, f);

	/* lambda_ =  C*M^(-1)*f + D2c_ */
	MatrixVectorMultiplicationMod('N', (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), FD3A2D_SYS_DOF, 1.0, C_, f, 1.0, D2c_, lambda_);

	/* GTemp = M^(-1)*G */
	cholDecomposition2dim(FD3A2D_SYS_DOF, (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), MTemp, GTemp);

	/* mCxmC = -C*M^(-1)*G */
	MatrixMultiplication('N', 'N', (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), FD3A2D_SYS_DOF, (FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), -1.0, C_, GTemp, 0.0, mCxmC);

	/* lambda_ = mCxmC^(-1) * lambda_ ; please note that cholesky is not possible here */
	InversionByLU1dim((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS), z, mCxmC, lambda_);

	free(z);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,M);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,MTemp);
	freeMatrix((FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),mCxmC);
	freeMatrix(FD3A2D_SYS_DOF,(FD3A2D_NUM_POSCONSTRAINTS+FD3A2D_NUM_VELCONSTRAINTS),GTemp);
	free(f);

}

void FD3A2D_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr){

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double d_axes = data->d_axes;


	/* Automatically introduced abbreviations */

	double elBo_EA1_2_x_ = FD3A2D_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = FD3A2D_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FD3A2D_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FD3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FD3A2D_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double p_a = x_[0];
	double p_b = x_[1];
	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];



	/* Dependent constraint matrix C */
	C_dep[0][0] = elBo_EA1_2_x_*sin(r_alpha1);
	C_dep[1][0] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	C_dep[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_dep[1][1] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));

	/* Constraint equations on position level */
	con_eq[0] = d_axes + p_b - 1.0*elBo_EA1_2_x_*cos(r_alpha1) + elBo_EA2_1_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	con_eq[1] = - 1.0*p_a - 1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_1_x_ - 1.0*elBo_EA2_2_x_) - 1.0*elBo_EA1_2_x_*sin(r_alpha1);

}

void FD3A2D_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr){

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;


	/* Automatically introduced abbreviations */

	double elBo_EA2_1_rot_z_ = FD3A2D_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FD3A2D_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FD3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FD3A2D_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];


	/* Independent constraint matrix C */
	C_ind[0][0] = 0.0;
	C_ind[1][0] = - 1.0*pow(cos(r_alpha1),2.0)- 1.0*pow(sin(r_alpha1),2.0);
	C_ind[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_ind[1][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_ind[0][2] = 0.0;
	C_ind[1][2] = 0.0;
	C_ind[0][3] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));
	C_ind[1][3] = cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_ind[0][4] = 0.0;
	C_ind[1][4] = 0.0;

	/* Local constraint equations on velocity level */
	Dc_[0] = 0.0;
	Dc_[1] = 0.0;

}

void FD3A2D_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr){

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;


	/* Automatically introduced abbreviations */

	double elBo_EA1_2_x_ = FD3A2D_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = FD3A2D_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FD3A2D_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FD3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FD3A2D_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];

	/* Help vars */
	double **GT_ind      = callocMatrix(2,5);
	double **GT_dep      = callocMatrix(2,2);
	double **C_dep       = callocMatrix(2,2);
	double **C_ind       = callocMatrix(2,5);
	unsigned short *z    = calloc(2,sizeof(unsigned short));
	int i_               = 0;
	int j_               = 0;

	double **C_    = data->con->C_;

	FD3A2D_constraint_derivatives(t, x_, u_, dataPtr);

	/* Independent constraint matrix C (sign changed) */
	C_ind[0][0] = -C_[0][0];
	C_ind[0][1] = -C_[0][1];
	C_ind[0][2] = -C_[0][4];
	C_ind[0][3] = -C_[0][5];
	C_ind[0][4] = -C_[0][6];
	C_ind[1][0] = -C_[1][0];
	C_ind[1][1] = -C_[1][1];
	C_ind[1][2] = -C_[1][4];
	C_ind[1][3] = -C_[1][5];
	C_ind[1][4] = -C_[1][6];

	/* Dependent constraint matrix C */
	C_dep[0][0] = C_[0][2];
	C_dep[0][1] = C_[0][3];
	C_dep[1][0] = C_[1][2];
	C_dep[1][1] = C_[1][3];

	/* Transposed independent input matrix G (sign changed)*/
	GT_ind[1][0] = pow(cos(r_alpha1),2.0)+pow( sin(r_alpha1),2.0);
	GT_ind[0][1] = - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_ind[1][1] = cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_ind[0][3] = cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) + sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));
	GT_ind[1][3] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));

	/* Transposed dependent input matrix G */
	GT_dep[0][0] = elBo_EA1_2_x_*sin(r_alpha1);
	GT_dep[1][0] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	GT_dep[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	GT_dep[1][1] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));

	InversionByLU(2, 5, z, C_dep, C_ind);

	InversionByLU(2, 5, z, GT_dep, GT_ind);

	for ( i_ = 0 ; i_ < 5 ; i_++ ){
		Jr[data->independentIndices[i_]][i_]  =  1.0;
		Jl[data->independentIndices[i_]][i_]  =  1.0;
		for ( j_ = 0 ; j_ < 2 ; j_++ ){
			Jr[data->dependentIndices[j_]][i_]  =  C_ind[j_][i_];
			Jl[data->dependentIndices[j_]][i_]  =  GT_ind[j_][i_];
		}
	}

	freeMatrix(2,5,GT_ind);
	freeMatrix(2,2,GT_dep);
	freeMatrix(2,2,C_dep);
	freeMatrix(2,5,C_ind);
	free(z);

}
