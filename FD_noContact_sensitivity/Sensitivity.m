% Sensitivit�tsanalyse


%% init relocated
clear all

currentFolder = pwd;
cd(currentFolder)
% addpath(genpath([currentFolder '/ServoConstraints']))
addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))
addpath(genpath([currentFolder '/functions']))
load('sys.mat')

%Faktoren f�r redefined output
w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;

% phi2_1 = sys.parameters.data.phi2_1;
% phi3_1 = sys.parameters.data.phi3_1;
% psi2_1 = sys.parameters.data.psi2_1;
% psi3_1 = sys.parameters.data.psi3_1;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;

l1 = 0.42 ;
l2 = 0.6 ;
l3 = 0.4 ;
l4 = 0.4643 ;

L = [l1;l2;l3;l4];

sampleTime = 1e-3;

dy0 = zeros(7,1);

psi2_1 = 0.1308; % Wert aus Sens
psi3_1 = 0.4181; % Wert aus Sens

%% estimation

y_estimated = [1e-3; 1e-3; 0.3*pi/180; 0.5*pi/180*1/psi2_1; 0.3*pi/180*1/psi3_1];
L_estimated = [0.5e-3; 0.5e-3; 1e-3; 1e-3];

%% Case 1

y01  = [-0.1837; 0.0551; -0.7725; 0.9184; -0.3948; 0; 0];
pos_EEd1 = eqm_sysout_exactOut(0,[y01; dy0],zeros(5,1));

beta_rigid_d1 = y01(4);
beta_flex1 = 0.9031;

y_meas1 = [-0.183702; 0.055103; -0.3944];

% a_error1 = y01(1)-y_meas1(1);
% b_error1 = y01(2)-y_meas1(2);
% gam_error1 = y01(5)-y_meas1(3); 

a_error1 = 0;
b_error1 = 0;
gam_error1 = 0; 

%Beta 
% beta_rigid_d1 = solve_beta(L,y_meas1);

q1_error1 = (beta_flex1 - beta_rigid_d1) * 1/psi2_1; 
q2_error1 =  0 * 1/psi3_1; 

pos_EE_meas1 = [1.318; 0.543; 0.5084];
% pos_EE_meas = [1.318; 0.542; 0.5084];


%% Sensitivity of Case 1

l1_error1 = 0;
l2_error1 = 0;
l3_error1 = 0;%1e-3;
l4_error1 = 0;%1e-3;

y_error1 = [a_error1; b_error1; gam_error1; q1_error1; q2_error1];
L_error1 = [l1_error1; l2_error1; l3_error1; l4_error1];

sens_y1 = sensitivity(y01);
sens_L1 = sensitivity_L_(L,y01);

sens_all1= [sens_y1, sens_L1];

sens_n1 = [sens_y1(:,1)*y_estimated(1), sens_y1(:,2)*y_estimated(2), sens_y1(:,3)*y_estimated(3), sens_y1(:,4)*y_estimated(4), sens_y1(:,5)*y_estimated(5)];
sens_Ln1 = [sens_L1(:,1)*L_estimated(1), sens_L1(:,2)*L_estimated(2), sens_L1(:,3)*L_estimated(3), sens_L1(:,4)*L_estimated(4)];

sense_normed1 = 1000*[sens_n1 sens_Ln1];




%% Case 2

y02  = [0.2405; -0.0842; -1.6846; 0.2991; 0.2245; 0; 0];

pos_EEd2 = eqm_sysout_exactOut(0,[y02; dy0],zeros(5,1));

psi2_1 = 0.1308; % Wert aus Sens
psi3_1 = 0.4181; % Wert aus Sens


beta_rigid_d2 = y02(4);
beta_flex2 = 0.2887;

a_error2 = 0;
b_error2 = 0;
gam_error2 = 0 * pi/180; 
q1_error2 = (beta_flex2 - beta_rigid_d2)* 1/psi2_1; 
q2_error2 = 0 * 1/psi3_1;

pos_EE_meas2 = [1.313; 0.343; 0.5121];

%% Sensitivity of Case 2

l1_error2 = 0;
l2_error2 = 0;
l3_error2 = 0;%1e-3;
l4_error2 = 0;%1e-3;

y_error2 = [a_error2; b_error2; gam_error2; q1_error2; q2_error2];
L_error2 = [l1_error2; l2_error2; l3_error2; l4_error2];

sens_y2 = sensitivity(y02);
sens_L2 = sensitivity_L_(L,y02);

sens_all2= [sens_y2, sens_L2];

sens_n2 = [sens_y2(:,1)*y_estimated(1), sens_y2(:,2)*y_estimated(2), sens_y2(:,3)*y_estimated(3), sens_y2(:,4)*y_estimated(4), sens_y2(:,5)*y_estimated(5)];
sens_Ln2 = [sens_L2(:,1)*L_estimated(1), sens_L2(:,2)*L_estimated(2), sens_L2(:,3)*L_estimated(3), sens_L2(:,4)*L_estimated(4)];

sense_normed2 = 1000*[sens_n2 sens_Ln2];



%% Case 3

y03  = [0.1922; -0.1863; -1.9124; 0.3461; 1.2247; 0; 0];

pos_EEd3 = eqm_sysout_exactOut(0,[y03; dy0],zeros(5,1));

y_meas3 = [0.1922; -0.1863; 0; 19.4064*pi/180; 70.2*pi/180];

psi2_1 = 0.1308; % Wert aus Sens
psi3_1 = 0.4181; % Wert aus Sens


beta_rigid_d3 = y03(4);
beta_flex3 = 19.4064*pi/180;

a_error3 = 0;
b_error3 = 0;
gam_error3 = 0 * pi/180; 
q1_error3 = (beta_flex3 - beta_rigid_d3)* 1/psi2_1; 
q2_error3 = 0 * 1/psi3_1;

pos_EE_meas3 = [0.804; 0.593; (19.4064+70.2)*pi/180];

%% Sensitivity of Case 3

l1_error3 = 0;
l2_error3 = 0;
l3_error3 = 0;%1e-3;
l4_error3 = 0;%1e-3;

y_error3 = [a_error3; b_error3; gam_error3; q1_error3; q2_error3];
L_error3 = [l1_error3; l2_error3; l3_error3; l4_error3];

sens_y3 = sensitivity(y03);
sens_L3 = sensitivity_L_(L,y03);

sens_all3= [sens_y3, sens_L3];

sens_n3 = [sens_y3(:,1)*y_estimated(1), sens_y3(:,2)*y_estimated(2), sens_y3(:,3)*y_estimated(3), sens_y3(:,4)*y_estimated(4), sens_y3(:,5)*y_estimated(5)];
sens_Ln3 = [sens_L3(:,1)*L_estimated(1), sens_L3(:,2)*L_estimated(2), sens_L3(:,3)*L_estimated(3), sens_L3(:,4)*L_estimated(4)];

sense_normed3 = 1000*[sens_n3 sens_Ln3];

%% Case 4 

y04  = [0; 0; -1.1941; 0.7088; 0; 0; 0];

pos_EEd4 = eqm_sysout_exactOut(0,[y04; dy0],zeros(5,1));

y_meas4 = [0; 0; 0; 39.8845*pi/180; 0*pi/180];

beta_rigid_d4 = y04(4);
beta_flex4 = 39.8845*pi/180;

a_error4 = 0;
b_error4 = 0;
gam_error4 = 0 * pi/180; 
q1_error4 = (beta_flex4 - beta_rigid_d4)* 1/psi2_1;
q2_error4 = 0 * 1/psi3_1;


pos_EE_meas4 = [1.275; 0.553; 39.8845*pi/180];


%% Sensitivity of Case 4

% l1_error4 = 0.5e-3;
% l2_error4 = 0.5e-3;
% l3_error4 = 0.5e-3;%1e-3;
% l4_error4 = 0.5e-3;%1e-3;

l1_error4 = 0;
l2_error4 = 0;
l3_error4 = 0;
l4_error4 = 0;

y_error4 = [a_error4; b_error4; gam_error4; q1_error4; q2_error4];
L_error4 = [l1_error4; l2_error4; l3_error4; l4_error4];

sens_y4 = sensitivity(y04);
sens_L4 = sensitivity_L_(L,y04);

sens_all4= [sens_y4, sens_L4];

sens_n4 = [sens_y4(:,1)*y_estimated(1), sens_y4(:,2)*y_estimated(2), sens_y4(:,3)*y_estimated(3), sens_y4(:,4)*y_estimated(4), sens_y4(:,5)*y_estimated(5)];
sens_Ln4 = [sens_L4(:,1)*L_estimated(1), sens_L4(:,2)*L_estimated(2), sens_L4(:,3)*L_estimated(3), sens_L4(:,4)*L_estimated(4)];

sense_normed4 = 1000*[sens_n4 sens_Ln4];


%% Case 5  

% In diesem Case wird die Annahme getroffen, dass es keine Fehler in den
% linearmotren und rotationsmotor gibt. Aus dieser annahme l�sst sich der
% starre Winkel beta berechnen.

pos_EE_meas5 = [0.8; 0.6; (86.093-60.392)*pi/180];

y_meas5 = [-0.332412; -0.257136; 0; 1.5231; -60.392*pi/180; 0; 0];
%86.1204*pi/180 beta measured
pos_EEd5 = eqm_sysout_exactOut(0,[y_meas5; dy0],zeros(5,1));

y_meas5_tilde = [-0.332412; -0.257136; 0; (1.5231-1.2*pi/180); -60.392*pi/180; 0; 0];

pos_EEd5_tilde = eqm_sysout_exactOut(0,[y_meas5_tilde; dy0],zeros(5,1));

error_tilde = -pos_EEd5_tilde(1:3) + pos_EE_meas5;


beta_rigid_d5 = solve_beta(L,y_meas5); % theoretischer rigid durch die gemessenen aktuatoren

% beta_rigid_d5 = pos_EEd5(3) - y_meas5(5);

beta_flex5 = 86.1204*pi/180;

a_error5 = 0;
b_error5 = 0;
gam_error5 = 0 * pi/180; 
q1_error5 = (beta_flex5 - beta_rigid_d5)* 1/psi2_1;
q2_error5 = 0 * 1/psi3_1;

%% Sensitivity of Case 5

% l1_error4 = 0.5e-3;
% l2_error4 = 0.5e-3;
% l3_error4 = 0.5e-3;%1e-3;
% l4_error4 = 0.5e-3;%1e-3;

l1_error4 = 0;
l2_error4 = 0;
l3_error4 = 0;
l4_error4 = 0;

y_error5 = [a_error5; b_error5; gam_error5; q1_error5; q2_error5];
L_error4 = [l1_error4; l2_error4; l3_error4; l4_error4];

sens_y5 = sensitivity(y_meas5);
sens_L5 = sensitivity_L_(L,y_meas5);

sens_all5= [sens_y5, sens_L5];

sens_n5 = [sens_y5(:,1)*y_estimated(1), sens_y5(:,2)*y_estimated(2), sens_y5(:,3)*y_estimated(3), sens_y5(:,4)*y_estimated(4), sens_y5(:,5)*y_estimated(5)];
sens_Ln5 = [sens_L5(:,1)*L_estimated(1), sens_L5(:,2)*L_estimated(2), sens_L5(:,3)*L_estimated(3), sens_L5(:,4)*L_estimated(4)];

sense_normed5 = 1000*[sens_n5 sens_Ln5];


%% ERROR

pos_desired1 = pos_EEd1(1:3,:);
pos_error1 = pos_desired1 + [sens_y1, sens_L1] * [y_error1; L_error1];

sens_error1 = [sens_y1, sens_L1] * [y_error1; L_error1]; 
meas_error1 = pos_error1 - pos_EE_meas1;

position_error1 = - pos_desired1 + pos_EE_meas1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pos_desired2 = pos_EEd2(1:3,:);
pos_error2 = pos_desired2 + [sens_y2, sens_L2] * [y_error2; L_error2];

sens_error2 = [sens_y2, sens_L2] * [y_error2; L_error2]; 
meas_error2 = pos_error2 - pos_EE_meas2;

position_error2 = - pos_desired2 + pos_EE_meas2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pos_desired3 = pos_EEd3(1:3,:);
pos_error3 = pos_desired3 + [sens_y3, sens_L3] * [y_error3; L_error3];

sens_error3 = [sens_y3, sens_L3] * [y_error3; L_error3]; 
meas_error3 = pos_error3 - pos_EE_meas3;

position_error3 = - pos_desired3 + pos_EE_meas3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pos_desired4 = pos_EEd4(1:3,:);
pos_error4 = pos_desired4 + [sens_y4, sens_L4] * [y_error4; L_error4];

sens_error4 = [sens_y4, sens_L4] * [y_error4; L_error4]; 
meas_error4 = pos_error4 - pos_EE_meas4;

position_error4 = - pos_desired4 + pos_EE_meas4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


pos_desired5 = pos_EEd5(1:3,:);
pos_error5 = pos_desired5 + [sens_y5, sens_L5] * [y_error5; L_error4];

sens_error5 = [sens_y5, sens_L5] * [y_error5; L_error4]; 
meas_error5 = pos_error5 - pos_EE_meas5;

position_error5 = - pos_desired5 + pos_EE_meas5;


%% Plot

figure
bar([sens_error1(1), position_error1(1); sens_error1(2), position_error1(2); sens_error1(3), position_error1(3)])

figure
bar([sens_error2(1), position_error2(1); sens_error2(2), position_error2(2); sens_error2(3), position_error2(3)])

figure
bar([sens_error3(1), position_error3(1); sens_error3(2), position_error3(2); sens_error3(3), position_error3(3)])

figure
bar([sens_error4(1), position_error4(1); sens_error4(2), position_error4(2); sens_error4(3), position_error4(3)])

figure
bar([sens_error5(1), position_error5(1), error_tilde(1); sens_error5(2), position_error5(2) , error_tilde(2) ; sens_error5(3), position_error5(3) , error_tilde(3)]) %% Mit Vorsicht zu interpretieren da r�ckw�rts

beta_error = [beta_flex1 - beta_rigid_d1; beta_flex2 - beta_rigid_d2; beta_flex3 - beta_rigid_d3; beta_flex4 - beta_rigid_d4; beta_flex5 - beta_rigid_d5]*180/pi;
% bar([pos_error1(1) pos_desired1(1);pos_error1(2) pos_desired1(2);pos_error1(3) pos_desired1(3)])