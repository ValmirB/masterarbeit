%% Init Forward Model and Simulink

clear all 
clc
close all

%%

currentFolder = pwd;
cd(currentFolder)
addpath(genpath([currentFolder '/SDOF_FilterValmir']))
addpath(genpath([currentFolder '/Trajectory']))
addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))
addpath(genpath([currentFolder '/SC/cFunctions']))
addpath(genpath([currentFolder '/SC']))

load('init_redefined.mat')
load('sys.mat')
load('sysSC')

load('snvLambda.mat')


SampleTime = 1e-3;
sampleTime = 1e-3;


alpha =  pi/2;
Sw = [cos(alpha), -sin(alpha), 0; ...
     sin(alpha),  cos(alpha), 0; ...
     0,          0,           1];
 
Fd = 0;
 
F_contact = Fd; 
 
 
%% redefined output

w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;


sysSC.parameters.data.w2_1 = w2_1;
sysSC.parameters.data.w3_1 = w3_1;
sysSC.parameters.data.v2_1 = v2_1;
sysSC.parameters.data.v3_1 = v3_1;

dy0_red = zeros(7,1);

pos_redef_ = eqm_sysout_exactOut(0,[[y0_red(3:4);y0_red(6:7)]; zeros(4,1)],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');
pos_redef1 = eqm_sysout_approxOut(0,[[y0_red(3:4);y0_red(6:7)]; zeros(4,1)],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');

x_EE0 = pos_redef1(1,1);
y_EE0 = pos_redef1(2,1);


%% Wand 

% r_w1 = [pos_EE(1);...    %Startposition der Wand wird hier definiert. 
%         pos_EE(2)];      %Dieser muss nicht!!! mit der Starposition des EE 
                                          % übereinstimmen 
r_w1 = [pos_redef_(1,1);...            %Startposition der Wand wird hier definiert. 
        pos_redef_(2,1)];
    
r_w2 = r_w1 + [+0.38; -0.38];

r0 = [1/2*(r_w1(1) + r_w2(1) + r_w2(2) - r_w1(2)); ...
           r_w1(1) + r_w2(2) - 1/2*(r_w1(1) + r_w2(1) + r_w2(2) - r_w1(2))]; %Neues KOS des Kreises

xO = r0(1);
yO = r0(2);

R = sqrt((r_w1(1)-xO)^2+ (r_w1(2)-yO)^2)


%% exact output

% actuators
sys.parameters.data.p_a_d = y0_red(1,1);
sys.parameters.data.p_b_d = y0_red(2,1);
sys.parameters.data.r_gamma3_d = y0_red(5,1);
sys.parameters.data.z_0 = 0.17;

% sys.parameters.data.wl_xd = 1.31;
% sys.parameters.data.wl_yd = 0.55;


sys.parameters.data.Ox = xO;
sys.parameters.data.Oy = yO;

[y0, dy0] = calcInitConditions(0,[-1;1;0;0], zeros(4,1));   % initial guess necessary
y0 = staticEquilibrium(0,y0);

pos_EE = eqm_sysout_exactOut(0,[y0; dy0],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');
pos_redef = eqm_sysout_approxOut(0,[y0; dy0],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');

if (xO-pos_EE(1))^2+(yO-pos_EE(2))^2-0.38^2 < 1e-8
    
    display('Bindung eingehalten')
    
else
    
    display('Bindung verletzt')
    (xO-pos_EE(1))^2+(yO-pos_EE(2))^2-0.38^2
end


sigma0 = atan2(pos_redef1(2,1)-yO, pos_redef1(1,1)-xO);
% sigma0 = atan2(r_w1(2)-yO, r_w1(1)-xO);
sigma_end = sigma0 - 90*pi/180; %- 45*pi/180;

t_end   = 5;
t_start = 1;
% x_end   = x_EE0;
% y_end   = 0.30;

t = t_start:0.01:t_end;

run('Dsigma_traj.m')
% run('traj_v_x.m')
% run('traj_v_y.m')

%% Controller


% P_tilde = [0;3;3];
% P = diag(P_tilde);
% 
% D_tilde = [0;3;3];
% D = diag(D_tilde);

P = 0;
D = 0;
I = 0;

P_theta = 0;
D_theta = 0;


%%
% plot(simout.data(8:end))
% [y0_red(1,1); y0_red(2,1); y0(1:2,1);y0_red(5,1); y0(3:4,1)]