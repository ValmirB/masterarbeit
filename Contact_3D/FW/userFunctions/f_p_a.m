function p_a = f_p_a(t, x_, varargin)
% f_p_a - definition of time-depending user-defined variable p_a

global sys;


% constant user-defined variables
p_a_d = sys.parameters.data.p_a_d;
p_a = zeros(1,1);

p_a(1) = p_a_d;

% END OF FILE

