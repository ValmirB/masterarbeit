function r_gamma3 = f_r_gamma3(t, x_, varargin)
% f_r_gamma3 - definition of time-depending user-defined variable r_gamma3

global sys;


% constant user-defined variables
r_gamma3_d = sys.parameters.data.r_gamma3_d;
r_gamma3 = zeros(1,1);

r_gamma3(1) = r_gamma3_d;

% END OF FILE

