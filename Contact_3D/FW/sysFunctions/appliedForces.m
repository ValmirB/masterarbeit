function qa_ = appliedForces(t, x_, u_, varargin)
% qa_ = appliedForces(t, x_, u_, varargin)
% Applied forces qa_ in the system
% Vector of force as in the equations of motion before multiplication with J.'

global sys;

% generalized coordinates, velocities and auxiliary coordinates

EA2_q001 = x_(3);
EA3_q001 = x_(4);
% system inputs
r_gamma3 = u_(7);

% constant user-defined variables
g = sys.parameters.data.g;
m_C1 = sys.parameters.data.m_C1;
m_C2 = sys.parameters.data.m_C2;
m_ee = sys.parameters.data.m_ee;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

elBo_EA3_2_rot_x_ =  SID_EA3_.frame.node(2).orientation(1) + SID_EA3_.frame.node(2).psi(1,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_rot_z_ =  SID_EA3_.frame.node(2).orientation(3) + SID_EA3_.frame.node(2).psi(3,:) * x_(sys.model.body.EA3.data.edof.idx);

% Applied forces
qa_ = zeros(38,1);

qa_(3) = -1.0*g*m_C1;
qa_(9) = -1.0*g*m_C2;
qa_(15) = -2.158*g;
qa_(17) = 0.41626*g;
qa_(21) = -5.582176000000005799961400043685*g;
qa_(22) = -0.10280645791469449068511465839038*EA2_q001*g;
qa_(23) = -0.00000000000000000000000000040389678347315804437080502542479*g*(4709707183750797.0*EA2_q001 - 8804736243303947221300412416.0);
qa_(26) = -1.953632*g*sin(r_gamma3);
qa_(27) = -1.953632*g*cos(r_gamma3);
qa_(31) = g*(0.082637824577984173024525205164537*EA3_q001*sin(r_gamma3) + cos(r_gamma3)*(0.0000000000083259892263668554836784628430295*EA3_q001 - 0.46025217599999990181203202155302));
qa_(32) = -0.0000000000000000000000000048467614016778965324496603050974*g*(17050111967421348465082368.0*cos(r_gamma3) - 1717845904996373.0*sin(r_gamma3));
qa_(33) = -1.0*g*m_ee*(cos(elBo_EA3_2_rot_z_)*sin(r_gamma3) + cos(elBo_EA3_2_rot_x_)*cos(r_gamma3)*sin(elBo_EA3_2_rot_z_));
qa_(34) = g*m_ee*(sin(elBo_EA3_2_rot_z_)*sin(r_gamma3) - 1.0*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*cos(r_gamma3));
qa_(35) = g*m_ee*cos(r_gamma3)*sin(elBo_EA3_2_rot_x_);

% END OF FILE

