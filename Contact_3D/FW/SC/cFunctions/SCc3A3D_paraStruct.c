#include <string.h>
#include <stdlib.h>
#include "neweul.h"
#include "SCc3A3D_paraStruct.h"
#include "SCc3A3D_userDefined.h"
#include <stdio.h>

/* Initialize structure with system parameters */
void *SCc3A3D_initializeSystemStruct(void){

	struct SCc3A3D_paraStruct *data = calloc(1, sizeof(struct SCc3A3D_paraStruct));

	/* User defined constant parameters */
	data->D2EE_force_x_s = 0.000000e+00;
	data->D2EE_force_y_s = 0.000000e+00;
	data->D2EE_x_s = 0.000000e+00;
	data->D2EE_y_s = 0.000000e+00;
	data->D2EE_z_s = 0.000000e+00;
	data->DEE_force_x_s = 0.000000e+00;
	data->DEE_force_y_s = 0.000000e+00;
	data->DEE_x_s = 0.000000e+00;
	data->DEE_y_s = 0.000000e+00;
	data->DEE_z_s = 0.000000e+00;
	data->EE_force_x_d = 0.000000e+00;
	data->EE_force_x_s = 0.000000e+00;
	data->EE_force_y_d = 0.000000e+00;
	data->EE_force_y_s = 0.000000e+00;
	data->EE_x_d = 0.000000e+00;
	data->EE_x_s = 0.000000e+00;
	data->EE_y_d = 0.000000e+00;
	data->EE_y_s = 0.000000e+00;
	data->EE_z_d = 0.000000e+00;
	data->EE_z_s = 0.000000e+00;
	data->I_ee = 0.000000e+00;
	data->K_L = 8.000000e+01;
	data->K_R = 1.500000e+01;
	data->d_axes = 6.100000e-01;
	data->g = 9.810000e+00;
	data->l2 = 1.000000e+00;
	data->l3 = 4.643000e-01;
	data->m_C1 = 6.000000e+00;
	data->m_C2 = 6.600000e+00;
	data->m_ee = 0.000000e+00;
	data->phi2_1 = 5.177806e-02;
	data->phi3_1 = 1.108656e-01;
	data->psi2_1 = 1.307493e-01;
	data->v2_1 = 7.500000e-01;
	data->w2_1 = 1.000000e+00;
	data->w3_1 = 9.000000e-01;
	data->z_0 = 1.570000e-01;
	data->p_a_s = 0.000000e+00;
	data->Dp_a_s = 0.000000e+00;
	data->D2p_a_s = 0.000000e+00;
	data->p_b_s = 0.000000e+00;
	data->Dp_b_s = 0.000000e+00;
	data->D2p_b_s = 0.000000e+00;
	data->r_alpha1_s = 0.000000e+00;
	data->Dr_alpha1_s = 0.000000e+00;
	data->D2r_alpha1_s = 0.000000e+00;
	data->r_beta2_s = 0.000000e+00;
	data->Dr_beta2_s = 0.000000e+00;
	data->D2r_beta2_s = 0.000000e+00;
	data->r_gamma3_s = 0.000000e+00;
	data->Dr_gamma3_s = 0.000000e+00;
	data->D2r_gamma3_s = 0.000000e+00;
	data->EA2_q001_s = 0.000000e+00;
	data->DEA2_q001_s = 0.000000e+00;
	data->D2EA2_q001_s = 0.000000e+00;
	data->EA3_q001_s = 0.000000e+00;
	data->DEA3_q001_s = 0.000000e+00;
	data->D2EA3_q001_s = 0.000000e+00;

	/* Rudimental system information */
	data->sysDof = 7;
	data->numBodies = 6;
	data->externalForces = NULL;
	data->externalTorques = NULL;

	/* Independent generalized coordinates */
	data->independentIndices[0] = 5;
	data->independentIndices[1] = 6;

	/* Dependent generalized coordinates */
	data->dependentIndices[0] = 0;
	data->dependentIndices[1] = 1;
	data->dependentIndices[2] = 2;
	data->dependentIndices[3] = 3;
	data->dependentIndices[4] = 4;

	SCc3A3D_initializeConstraintStructure(data);

	return (void *)data;

}


void SCc3A3D_initializeConstraintStructure(struct SCc3A3D_paraStruct *data){

	data->con = malloc(sizeof(struct SCc3A3D_constraintStructure));

	data->con->C_      = callocMatrix((SCC3A3D_NUM_POSCONSTRAINTS+SCC3A3D_NUM_VELCONSTRAINTS),SCC3A3D_SYS_DOF);
	data->con->Dc_     = calloc((SCC3A3D_NUM_POSCONSTRAINTS+SCC3A3D_NUM_VELCONSTRAINTS),sizeof(double));
	data->con->D2c_    = calloc((SCC3A3D_NUM_POSCONSTRAINTS+SCC3A3D_NUM_VELCONSTRAINTS),sizeof(double));
	data->con->G_      = callocMatrix(SCC3A3D_SYS_DOF,(SCC3A3D_NUM_POSCONSTRAINTS+SCC3A3D_NUM_VELCONSTRAINTS));

	data->con->Jr_v    = callocMatrix(SCC3A3D_SYS_DOF,SCC3A3D_NUM_INDEPENDENT_GC);
	data->con->Jr_a    = callocMatrix(SCC3A3D_SYS_DOF,SCC3A3D_NUM_INDEPENDENT_GV);
	data->con->Jl_a    = callocMatrix(SCC3A3D_NUM_INDEPENDENT_GV,SCC3A3D_SYS_DOF);
	data->con->theta_  = calloc(SCC3A3D_SYS_DOF,sizeof(double));
	data->con->gamma_  = calloc(SCC3A3D_SYS_DOF,sizeof(double));

	data->con->lambda_ = calloc((SCC3A3D_NUM_POSCONSTRAINTS+SCC3A3D_NUM_VELCONSTRAINTS),sizeof(double));

}

void SCc3A3D_freeConstraintStructure(struct SCc3A3D_paraStruct *data) {

	freeMatrix((SCC3A3D_NUM_POSCONSTRAINTS+SCC3A3D_NUM_VELCONSTRAINTS),SCC3A3D_SYS_DOF,data->con->C_);
	free(data->con->Dc_);
	free(data->con->D2c_);
	freeMatrix(SCC3A3D_SYS_DOF,(SCC3A3D_NUM_POSCONSTRAINTS+SCC3A3D_NUM_VELCONSTRAINTS),data->con->G_);

	freeMatrix(SCC3A3D_SYS_DOF,SCC3A3D_NUM_INDEPENDENT_GC,data->con->Jr_v);
	freeMatrix(SCC3A3D_SYS_DOF,SCC3A3D_NUM_INDEPENDENT_GV,data->con->Jr_a);
	freeMatrix(SCC3A3D_NUM_INDEPENDENT_GV,SCC3A3D_SYS_DOF,data->con->Jl_a);
	free(data->con->theta_);
	free(data->con->gamma_);

	free(data->con->lambda_);

	free(data->con);

}

void SCc3A3D_freeStructure(void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	SCc3A3D_freeConstraintStructure(data);
	free(data);

}
