#include "SCc3A3D_pd_matlab.h"

double SCc3A3D_getScalar(const void *context, const char *name, double defaultValue){

	const mxArray *myField;

	const mxArray *passedStruct;

	passedStruct = (const mxArray *) context;

	if (passedStruct==NULL)
		return defaultValue;

	if (mxGetFieldNumber(passedStruct, name)!=-1)
		myField = mxGetField(passedStruct, 0, name);
	else
		return defaultValue;

	if ((mxIsEmpty(myField))||(myField==NULL))
		return defaultValue;
	else
		return mxGetScalar(myField);

}

int SCc3A3D_getSystemStruct(void *dataPtr, const void *context){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	data->D2EE_force_x_s = SCc3A3D_getScalar(context, "D2EE_force_x_s", 0.000000e+00);
	data->D2EE_force_y_s = SCc3A3D_getScalar(context, "D2EE_force_y_s", 0.000000e+00);
	data->D2EE_x_s = SCc3A3D_getScalar(context, "D2EE_x_s", 0.000000e+00);
	data->D2EE_y_s = SCc3A3D_getScalar(context, "D2EE_y_s", 0.000000e+00);
	data->D2EE_z_s = SCc3A3D_getScalar(context, "D2EE_z_s", 0.000000e+00);
	data->DEE_force_x_s = SCc3A3D_getScalar(context, "DEE_force_x_s", 0.000000e+00);
	data->DEE_force_y_s = SCc3A3D_getScalar(context, "DEE_force_y_s", 0.000000e+00);
	data->DEE_x_s = SCc3A3D_getScalar(context, "DEE_x_s", 0.000000e+00);
	data->DEE_y_s = SCc3A3D_getScalar(context, "DEE_y_s", 0.000000e+00);
	data->DEE_z_s = SCc3A3D_getScalar(context, "DEE_z_s", 0.000000e+00);
	data->EE_force_x_d = SCc3A3D_getScalar(context, "EE_force_x_d", 0.000000e+00);
	data->EE_force_x_s = SCc3A3D_getScalar(context, "EE_force_x_s", 0.000000e+00);
	data->EE_force_y_d = SCc3A3D_getScalar(context, "EE_force_y_d", 0.000000e+00);
	data->EE_force_y_s = SCc3A3D_getScalar(context, "EE_force_y_s", 0.000000e+00);
	data->EE_x_d = SCc3A3D_getScalar(context, "EE_x_d", 0.000000e+00);
	data->EE_x_s = SCc3A3D_getScalar(context, "EE_x_s", 0.000000e+00);
	data->EE_y_d = SCc3A3D_getScalar(context, "EE_y_d", 0.000000e+00);
	data->EE_y_s = SCc3A3D_getScalar(context, "EE_y_s", 0.000000e+00);
	data->EE_z_d = SCc3A3D_getScalar(context, "EE_z_d", 0.000000e+00);
	data->EE_z_s = SCc3A3D_getScalar(context, "EE_z_s", 0.000000e+00);
	data->I_ee = SCc3A3D_getScalar(context, "I_ee", 0.000000e+00);
	data->K_L = SCc3A3D_getScalar(context, "K_L", 8.000000e+01);
	data->K_R = SCc3A3D_getScalar(context, "K_R", 1.500000e+01);
	data->d_axes = SCc3A3D_getScalar(context, "d_axes", 6.100000e-01);
	data->g = SCc3A3D_getScalar(context, "g", 9.810000e+00);
	data->l2 = SCc3A3D_getScalar(context, "l2", 1.000000e+00);
	data->l3 = SCc3A3D_getScalar(context, "l3", 4.643000e-01);
	data->m_C1 = SCc3A3D_getScalar(context, "m_C1", 6.000000e+00);
	data->m_C2 = SCc3A3D_getScalar(context, "m_C2", 6.600000e+00);
	data->m_ee = SCc3A3D_getScalar(context, "m_ee", 0.000000e+00);
	data->phi2_1 = SCc3A3D_getScalar(context, "phi2_1", 5.177806e-02);
	data->phi3_1 = SCc3A3D_getScalar(context, "phi3_1", 1.108656e-01);
	data->psi2_1 = SCc3A3D_getScalar(context, "psi2_1", 1.307493e-01);
	data->v2_1 = SCc3A3D_getScalar(context, "v2_1", 7.500000e-01);
	data->w2_1 = SCc3A3D_getScalar(context, "w2_1", 1.000000e+00);
	data->w3_1 = SCc3A3D_getScalar(context, "w3_1", 9.000000e-01);
	data->z_0 = SCc3A3D_getScalar(context, "z_0", 1.570000e-01);
	data->p_a_s = SCc3A3D_getScalar(context, "p_a_s", 0.000000e+00);
	data->Dp_a_s = SCc3A3D_getScalar(context, "Dp_a_s", 0.000000e+00);
	data->D2p_a_s = SCc3A3D_getScalar(context, "D2p_a_s", 0.000000e+00);
	data->p_b_s = SCc3A3D_getScalar(context, "p_b_s", 0.000000e+00);
	data->Dp_b_s = SCc3A3D_getScalar(context, "Dp_b_s", 0.000000e+00);
	data->D2p_b_s = SCc3A3D_getScalar(context, "D2p_b_s", 0.000000e+00);
	data->r_alpha1_s = SCc3A3D_getScalar(context, "r_alpha1_s", 0.000000e+00);
	data->Dr_alpha1_s = SCc3A3D_getScalar(context, "Dr_alpha1_s", 0.000000e+00);
	data->D2r_alpha1_s = SCc3A3D_getScalar(context, "D2r_alpha1_s", 0.000000e+00);
	data->r_beta2_s = SCc3A3D_getScalar(context, "r_beta2_s", 0.000000e+00);
	data->Dr_beta2_s = SCc3A3D_getScalar(context, "Dr_beta2_s", 0.000000e+00);
	data->D2r_beta2_s = SCc3A3D_getScalar(context, "D2r_beta2_s", 0.000000e+00);
	data->r_gamma3_s = SCc3A3D_getScalar(context, "r_gamma3_s", 0.000000e+00);
	data->Dr_gamma3_s = SCc3A3D_getScalar(context, "Dr_gamma3_s", 0.000000e+00);
	data->D2r_gamma3_s = SCc3A3D_getScalar(context, "D2r_gamma3_s", 0.000000e+00);
	data->EA2_q001_s = SCc3A3D_getScalar(context, "EA2_q001_s", 0.000000e+00);
	data->DEA2_q001_s = SCc3A3D_getScalar(context, "DEA2_q001_s", 0.000000e+00);
	data->D2EA2_q001_s = SCc3A3D_getScalar(context, "D2EA2_q001_s", 0.000000e+00);
	data->EA3_q001_s = SCc3A3D_getScalar(context, "EA3_q001_s", 0.000000e+00);
	data->DEA3_q001_s = SCc3A3D_getScalar(context, "DEA3_q001_s", 0.000000e+00);
	data->D2EA3_q001_s = SCc3A3D_getScalar(context, "D2EA3_q001_s", 0.000000e+00);

	return 0;
}
