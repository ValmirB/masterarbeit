#include "SCc3A3D_userDefined.h"
#include <stdio.h>
#include <math.h>
#include "neweul.h"
#include <stdlib.h>

double SCc3A3D_f_EE_x(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double EE_x_d = data->EE_x_d;

	myRes_ = EE_x_d;

	return myRes_;
}

double SCc3A3D_f_DEE_x(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_D2EE_x(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_EE_y(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double EE_y_d = data->EE_y_d;

	myRes_ = EE_y_d;

	return myRes_;
}

double SCc3A3D_f_DEE_y(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_D2EE_y(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_EE_z(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double EE_z_d = data->EE_z_d;

	myRes_ = EE_z_d;

	return myRes_;
}

double SCc3A3D_f_DEE_z(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_D2EE_z(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_EE_force_x(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double EE_force_x_d = data->EE_force_x_d;

	myRes_ = EE_force_x_d;

	return myRes_;
}

double SCc3A3D_f_DEE_force_x(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_D2EE_force_x(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_EE_force_y(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;


	/* constant user-defined variables */ 

	double EE_force_y_d = data->EE_force_y_d;

	myRes_ = EE_force_y_d;

	return myRes_;
}

double SCc3A3D_f_DEE_force_y(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}

double SCc3A3D_f_D2EE_force_y(double t, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}




double SCc3A3D_f_DelBo_EA1_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA2_1_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[12];

	myRes_ = -0.078781818435274722500771815703047*DEA2_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA2_1_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[12];

	myRes_ = -0.000000000000078774485292178524877876637249719*DEA2_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA2_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[12];

	myRes_ = 0.12720812859558897067735472319328*DEA2_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA2_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA2_3_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[12];

	myRes_ = 0.13074928527605483852269685485226*DEA2_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA2_3_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[12];

	myRes_ = -0.00000000000075504122110177805935058228092758*DEA2_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA2_3_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[12];

	myRes_ = 0.05177806232203822911497326231256*DEA2_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA3_2_rot_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[13];

	myRes_ = 0.000002211520596332927660824640286874*DEA3_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA3_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[13];

	myRes_ = 0.41814120987041747401491420532693*DEA3_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA3_2_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[13];

	myRes_ = -0.0000000000079072648755528962870184095894835*DEA3_q001;

	return myRes_;
}


double SCc3A3D_f_DelBo_EA3_2_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[13];

	myRes_ = 0.11086564964710307612527628862154*DEA3_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA1_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0.42;

	return myRes_;
}


double SCc3A3D_f_elBo_EA2_1_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[5];

	myRes_ = -0.078781818435274722500771815703047*EA2_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA2_1_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[5];

	myRes_ = -0.000000000000078774485292178524877876637249719*EA2_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA2_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[5];

	myRes_ = 0.12720812859558897067735472319328*EA2_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA2_2_x_(double *x_){

	double myRes_ = 0.0;

	myRes_ = 0.6;

	return myRes_;
}


double SCc3A3D_f_elBo_EA2_3_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[5];

	myRes_ = 0.13074928527605483852269685485226*EA2_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA2_3_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[5];

	myRes_ = 1.0 - 0.00000000000075504122110177805935058228092758*EA2_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA2_3_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[5];

	myRes_ = 0.05177806232203822911497326231256*EA2_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA3_2_rot_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[6];

	myRes_ = 0.000002211520596332927660824640286874*EA3_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA3_2_rot_z_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[6];

	myRes_ = 0.41814120987041747401491420532693*EA3_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA3_2_x_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[6];

	myRes_ = 0.4643 - 0.0000000000079072648755528962870184095894835*EA3_q001;

	return myRes_;
}


double SCc3A3D_f_elBo_EA3_2_y_(double *x_){

	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[6];

	myRes_ = 0.11086564964710307612527628862154*EA3_q001;

	return myRes_;
}

double forces_EE_fx(double t, double *x_, double *u_, double *f_EE_fx, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	/* system inputs */
	double EE_force_x = u_[9];

	/* Vectorial force law */
	f_EE_fx[0] = - 
	1.0*EE_force_x;

	return euclideanNorm_vec(6, f_EE_fx);
}

double forces_EE_fy(double t, double *x_, double *u_, double *f_EE_fy, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;

	/* system inputs */
	double EE_force_y = u_[10];

	/* Vectorial force law */
	f_EE_fy[1] = - 
	1.0*EE_force_y;

	return euclideanNorm_vec(6, f_EE_fy);
}


void SCc3A3D_f_endeffector_acceleration_inputs(double t, double *x_, double *u_, void *dataPtr){

	u_[0] = SCc3A3D_f_EE_x(t, dataPtr);
	u_[1] = SCc3A3D_f_DEE_x(t, dataPtr);
	u_[2] = SCc3A3D_f_D2EE_x(t, dataPtr);
	u_[3] = SCc3A3D_f_EE_y(t, dataPtr);
	u_[4] = SCc3A3D_f_DEE_y(t, dataPtr);
	u_[5] = SCc3A3D_f_D2EE_y(t, dataPtr);
	u_[6] = SCc3A3D_f_EE_z(t, dataPtr);
	u_[7] = SCc3A3D_f_DEE_z(t, dataPtr);
	u_[8] = SCc3A3D_f_D2EE_z(t, dataPtr);
}


void SCc3A3D_f_contactforce_inputs(double t, double *x_, double *u_, void *dataPtr){

	u_[0] = SCc3A3D_f_EE_force_x(t, dataPtr);
	u_[1] = SCc3A3D_f_EE_force_y(t, dataPtr);
}


double SCc3A3D_output_des_x(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;


	double EE_x = u_[0];

	myRes_ = EE_x;

	return myRes_;
}


double SCc3A3D_output_des_y(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;


	double EE_y = u_[3];

	myRes_ = EE_y;

	return myRes_;
}


double SCc3A3D_output_des_Dx(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;


	double DEE_x = u_[1];

	myRes_ = DEE_x;

	return myRes_;
}


double SCc3A3D_output_des_Dy(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;


	double DEE_y = u_[4];

	myRes_ = DEE_y;

	return myRes_;
}


double SCc3A3D_output_Pos_x(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double p_b = x_[1];
	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];



	/* constant user-defined variables */ 

	double d_axes = data->d_axes;


	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = SCc3A3D_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = SCc3A3D_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = SCc3A3D_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = SCc3A3D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A3D_f_elBo_EA3_2_y_(x_);

	(void) u_;

	myRes_ = d_axes + p_b + elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Pos_y(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];



	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = SCc3A3D_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = SCc3A3D_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = SCc3A3D_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = SCc3A3D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A3D_f_elBo_EA3_2_y_(x_);

	(void) u_;

	myRes_ = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Pos_z(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_gamma3 = x_[4];



	/* constant user-defined variables */ 

	double z_0 = data->z_0;


	/* Automatically introduced abbreviations */

	double elBo_EA3_2_x_ = SCc3A3D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A3D_f_elBo_EA3_2_y_(x_);

	(void) u_;

	myRes_ = z_0 + elBo_EA3_2_y_*cos(r_gamma3) + elBo_EA3_2_x_*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Vel_x(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double Dp_b = x_[8];
	double Dr_beta2 = x_[10];
	double Dr_gamma3 = x_[11];



	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = SCc3A3D_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = SCc3A3D_f_DelBo_EA2_2_x_(x_);
	double DelBo_EA2_3_rot_z_ = SCc3A3D_f_DelBo_EA2_3_rot_z_(x_);
	double DelBo_EA2_3_x_ = SCc3A3D_f_DelBo_EA2_3_x_(x_);
	double DelBo_EA2_3_y_ = SCc3A3D_f_DelBo_EA2_3_y_(x_);
	double DelBo_EA3_2_x_ = SCc3A3D_f_DelBo_EA3_2_x_(x_);
	double DelBo_EA3_2_y_ = SCc3A3D_f_DelBo_EA3_2_y_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = SCc3A3D_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = SCc3A3D_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = SCc3A3D_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = SCc3A3D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A3D_f_elBo_EA3_2_y_(x_);

	(void) u_;

	myRes_ = Dp_b - (DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2)*(1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)) - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*DelBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_x_*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*sin(r_gamma3))*(DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2) - 1.0*Dr_gamma3*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_y_*cos(r_gamma3) + elBo_EA3_2_x_*sin(r_gamma3));

	return myRes_;
}


double SCc3A3D_output_Vel_y(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double Dr_beta2 = x_[10];
	double Dr_gamma3 = x_[11];



	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = SCc3A3D_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = SCc3A3D_f_DelBo_EA2_2_x_(x_);
	double DelBo_EA2_3_rot_z_ = SCc3A3D_f_DelBo_EA2_3_rot_z_(x_);
	double DelBo_EA2_3_x_ = SCc3A3D_f_DelBo_EA2_3_x_(x_);
	double DelBo_EA2_3_y_ = SCc3A3D_f_DelBo_EA2_3_y_(x_);
	double DelBo_EA3_2_x_ = SCc3A3D_f_DelBo_EA3_2_x_(x_);
	double DelBo_EA3_2_y_ = SCc3A3D_f_DelBo_EA3_2_y_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = SCc3A3D_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = SCc3A3D_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = SCc3A3D_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_x_ = SCc3A3D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A3D_f_elBo_EA3_2_y_(x_);

	(void) u_;

	myRes_ = DelBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*(elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*DelBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*DelBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_x_*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*sin(r_gamma3))*(DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2) - 1.0*Dr_gamma3*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_y_*cos(r_gamma3) + elBo_EA3_2_x_*sin(r_gamma3));

	return myRes_;
}


double SCc3A3D_output_Vel_z(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_gamma3 = x_[4];
	double Dr_gamma3 = x_[11];



	/* Automatically introduced abbreviations */

	double DelBo_EA3_2_x_ = SCc3A3D_f_DelBo_EA3_2_x_(x_);
	double DelBo_EA3_2_y_ = SCc3A3D_f_DelBo_EA3_2_y_(x_);
	double elBo_EA3_2_x_ = SCc3A3D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A3D_f_elBo_EA3_2_y_(x_);

	(void) u_;

	myRes_ = DelBo_EA3_2_y_*cos(r_gamma3) + DelBo_EA3_2_x_*sin(r_gamma3) + Dr_gamma3*elBo_EA3_2_x_*cos(r_gamma3) - 1.0*Dr_gamma3*elBo_EA3_2_y_*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Pos_x_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double p_b = x_[1];
	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double EA2_q001 = x_[5];
	double EA3_q001 = x_[6];



	/* constant user-defined variables */ 

	double d_axes = data->d_axes;
	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);

	(void) u_;

	myRes_ = d_axes + p_b + l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Pos_y_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double EA2_q001 = x_[5];
	double EA3_q001 = x_[6];



	/* constant user-defined variables */ 

	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);

	(void) u_;

	myRes_ = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) + EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Pos_z_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_gamma3 = x_[4];
	double EA3_q001 = x_[6];



	/* constant user-defined variables */ 

	double l3 = data->l3;
	double phi3_1 = data->phi3_1;
	double w3_1 = data->w3_1;
	double z_0 = data->z_0;

	(void) u_;

	myRes_ = z_0 + l3*sin(r_gamma3) + EA3_q001*phi3_1*w3_1*cos(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Vel_x_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double EA2_q001 = x_[5];
	double EA3_q001 = x_[6];
	double Dp_b = x_[8];
	double Dr_beta2 = x_[10];
	double Dr_gamma3 = x_[11];
	double DEA2_q001 = x_[12];
	double DEA3_q001 = x_[13];



	/* constant user-defined variables */ 

	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = SCc3A3D_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = SCc3A3D_f_DelBo_EA2_2_x_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);

	(void) u_;

	myRes_ = Dp_b - 1.0*(l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_gamma3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*sin(r_gamma3) + EA3_q001*phi3_1*w3_1*cos(r_gamma3)) - 1.0*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*cos(r_gamma3) - 1.0*EA3_q001*phi3_1*w3_1*sin(r_gamma3))*(Dr_beta2 - 1.0*DelBo_EA2_2_rot_z_ + DEA2_q001*psi2_1*v2_1) + DEA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*DEA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Vel_y_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double EA2_q001 = x_[5];
	double EA3_q001 = x_[6];
	double Dr_beta2 = x_[10];
	double Dr_gamma3 = x_[11];
	double DEA2_q001 = x_[12];
	double DEA3_q001 = x_[13];



	/* constant user-defined variables */ 

	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double v2_1 = data->v2_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = SCc3A3D_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = SCc3A3D_f_DelBo_EA2_2_x_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);

	(void) u_;

	myRes_ = DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*(l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*cos(r_gamma3) - 1.0*EA3_q001*phi3_1*w3_1*sin(r_gamma3))*(Dr_beta2 - 1.0*DelBo_EA2_2_rot_z_ + DEA2_q001*psi2_1*v2_1) - 1.0*Dr_gamma3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*sin(r_gamma3) + EA3_q001*phi3_1*w3_1*cos(r_gamma3)) + DEA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*DEA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Vel_z_rlc(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double r_gamma3 = x_[4];
	double EA3_q001 = x_[6];
	double Dr_gamma3 = x_[11];
	double DEA3_q001 = x_[13];



	/* constant user-defined variables */ 

	double l3 = data->l3;
	double phi3_1 = data->phi3_1;
	double w3_1 = data->w3_1;

	(void) u_;

	myRes_ = Dr_gamma3*l3*cos(r_gamma3) + DEA3_q001*phi3_1*w3_1*cos(r_gamma3) - 1.0*Dr_gamma3*EA3_q001*phi3_1*w3_1*sin(r_gamma3);

	return myRes_;
}


double SCc3A3D_output_Curvature_1_EA2(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA2_q001 = x_[5];


	(void) u_;

	myRes_ = 0.67665331847908083418019486998674*EA2_q001;

	return myRes_;
}


double SCc3A3D_output_Curvature_1_EA3(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double EA3_q001 = x_[6];


	(void) u_;

	myRes_ = 2.1640712562231518845123900973704*EA3_q001;

	return myRes_;
}


double SCc3A3D_output_DCurvature_1_EA2(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA2_q001 = x_[12];


	(void) u_;

	myRes_ = 0.67665331847908083418019486998674*DEA2_q001;

	return myRes_;
}


double SCc3A3D_output_DCurvature_1_EA3(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A3D_paraStruct *data  = (struct SCc3A3D_paraStruct *) dataPtr;


	double myRes_ = 0.0;

	/* generalized coordinates */

	double DEA3_q001 = x_[13];


	(void) u_;

	myRes_ = 2.1640712562231518845123900973704*DEA3_q001;

	return myRes_;
}


void SCc3A3D_f_desiredOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr){

	z[0] = SCc3A3D_output_des_x(t, x_, u_, dataPtr);
	z[1] = SCc3A3D_output_des_y(t, x_, u_, dataPtr);
	z[2] = SCc3A3D_output_des_Dx(t, x_, u_, dataPtr);
	z[3] = SCc3A3D_output_des_Dy(t, x_, u_, dataPtr);

}


void SCc3A3D_f_exactOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr){

	z[0] = SCc3A3D_output_Pos_x(t, x_, u_, dataPtr);
	z[1] = SCc3A3D_output_Pos_y(t, x_, u_, dataPtr);
	z[2] = SCc3A3D_output_Pos_z(t, x_, u_, dataPtr);
	z[3] = SCc3A3D_output_Vel_x(t, x_, u_, dataPtr);
	z[4] = SCc3A3D_output_Vel_y(t, x_, u_, dataPtr);
	z[5] = SCc3A3D_output_Vel_z(t, x_, u_, dataPtr);

}


void SCc3A3D_f_approxOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr){

	z[0] = SCc3A3D_output_Pos_x_rlc(t, x_, u_, dataPtr);
	z[1] = SCc3A3D_output_Pos_y_rlc(t, x_, u_, dataPtr);
	z[2] = SCc3A3D_output_Pos_z_rlc(t, x_, u_, dataPtr);
	z[3] = SCc3A3D_output_Vel_x_rlc(t, x_, u_, dataPtr);
	z[4] = SCc3A3D_output_Vel_y_rlc(t, x_, u_, dataPtr);
	z[5] = SCc3A3D_output_Vel_z_rlc(t, x_, u_, dataPtr);

}


void SCc3A3D_f_Curvature_outputs(double t, double *x_, double *u_, double *z, void *dataPtr){

	z[0] = SCc3A3D_output_Curvature_1_EA2(t, x_, u_, dataPtr);
	z[1] = SCc3A3D_output_Curvature_1_EA3(t, x_, u_, dataPtr);
	z[2] = SCc3A3D_output_DCurvature_1_EA2(t, x_, u_, dataPtr);
	z[3] = SCc3A3D_output_DCurvature_1_EA3(t, x_, u_, dataPtr);

}

