#ifndef SCc3A3D_paraStruct_H
#define SCc3A3D_paraStruct_H

#include <stdlib.h>
#include <math.h>
#include "SCc3A3D_Flexor.h"

struct SCc3A3D_constraintStructure {

	double **G_;

	double **C_;
	double *Dc_;
	double *D2c_;

	double **Jr_v;
	double **Jr_a;
	double **Jl_a;
	double *theta_;
	double *gamma_;

	double *lambda_;

};

struct SCc3A3D_paraStruct {

	/* User defined constant parameters */
	double D2EE_force_x_s;
	double D2EE_force_y_s;
	double D2EE_x_s;
	double D2EE_y_s;
	double D2EE_z_s;
	double DEE_force_x_s;
	double DEE_force_y_s;
	double DEE_x_s;
	double DEE_y_s;
	double DEE_z_s;
	double EE_force_x_d;
	double EE_force_x_s;
	double EE_force_y_d;
	double EE_force_y_s;
	double EE_x_d;
	double EE_x_s;
	double EE_y_d;
	double EE_y_s;
	double EE_z_d;
	double EE_z_s;
	double I_ee;
	double K_L;
	double K_R;
	double d_axes;
	double g;
	double l2;
	double l3;
	double m_C1;
	double m_C2;
	double m_ee;
	double phi2_1;
	double phi3_1;
	double psi2_1;
	double v2_1;
	double w2_1;
	double w3_1;
	double z_0;
	double p_a_s;
	double Dp_a_s;
	double D2p_a_s;
	double p_b_s;
	double Dp_b_s;
	double D2p_b_s;
	double r_alpha1_s;
	double Dr_alpha1_s;
	double D2r_alpha1_s;
	double r_beta2_s;
	double Dr_beta2_s;
	double D2r_beta2_s;
	double r_gamma3_s;
	double Dr_gamma3_s;
	double D2r_gamma3_s;
	double EA2_q001_s;
	double DEA2_q001_s;
	double D2EA2_q001_s;
	double EA3_q001_s;
	double DEA3_q001_s;
	double D2EA3_q001_s;

	/* Rudimental system information */
	int sysDof;
	int numBodies;
	double *externalForces;
	double *externalTorques;

	/* Abbreviation Force Elements */

	/* Independent generalized coordinates */
	int independentIndices[2];

	/* Dependent generalized coordinates */
	int dependentIndices[5];

	struct SCc3A3D_constraintStructure *con;

};

void *SCc3A3D_initializeSystemStruct(void);

void SCc3A3D_initializeConstraintStructure(struct SCc3A3D_paraStruct *data);

void SCc3A3D_freeConstraintStructure(struct SCc3A3D_paraStruct *data);

void SCc3A3D_freeStructure(void *dataPtr);

#endif

