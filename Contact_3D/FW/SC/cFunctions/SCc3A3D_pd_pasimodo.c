#include "SCc3A3D_pd_pasimodo.h"
#include <stdio.h>
#include <stdlib.h>
#include "neweul.h"
#include <math.h>
#include <string.h>

/* First part: accessFunctions.c */

GetScalarFunctionType getScalar = 0;
GetBoolFunctionType getBool = 0;
GetIntegerFunctionType getInteger = 0;

LogFunctionType logOutput = 0;
LogFunctionType debugOutput = 0;
LogFunctionType warning = 0;
LogFunctionType error = 0;

/* Second part: cbasedintegration.c */

struct SCc3A3D_paraStruct *unwrapSystemStruct(GetSystemStructFunction SCc3A3D_getSystemStruct__, const void *context) 
{
/* Fetch raw memory from C++ */
char *systemStructRawMemory = (*SCc3A3D_getSystemStruct__)(sizeof(struct SCc3A3D_paraStruct), context);

/* Cast raw pointer to appropriate type */
struct SCc3A3D_paraStruct *data = (SCc3A3D_sysParas*)(systemStructRawMemory);

return data;
}


void SCc3A3D_initMBS(int *dim, int *numbodies, GetSystemStructFunction SCc3A3D_getSystemStruct__, const void *context)
{

	LOG("\n============= Multibody System Initialization ===============\n")

	struct SCc3A3D_paraStruct *data = unwrapSystemStruct(SCc3A3D_getSystemStruct__, context);
		SCc3A3D_getSystemStruct(data, context);

    SCc3A3D_initializeSystemStruct(data);

	*dim = 2*data->sysDof;
	*numbodies = data->numBodies;
}


void SCc3A3D_calcInitialStates(double t, double *states, double *globalStates, GetSystemStructFunction SCc3A3D_getSystemStruct__, const void *context)
{

	struct SCc3A3D_paraStruct *data = unwrapSystemStruct(SCc3A3D_getSystemStruct__, context);
		SCc3A3D_getSystemStruct(data, context);

	SCc3A3D_minimalToGlobal(t, states, globalStates, data);
}


void SCc3A3D_integrateMBS(double fromT, double toT, double *forces, double *states, double *globalStates,
		GetSystemStructFunction SCc3A3D_getSystemStruct__, radauParas *radauStruct, void *context)
{

	struct SCc3A3D_paraStruct *data = unwrapSystemStruct(SCc3A3D_getSystemStruct__, context);
		SCc3A3D_getSystemStruct(data, context);

	/* insert new forces into data */
	data->externalForces = forces;


	/* INTEGRATOR CALL */
	RADAU5_( &(*radauStruct).systemDimension,&equationsOfMotion,
		  &fromT,states,&toT,
		  &(*radauStruct).hInitial,&(*radauStruct).rTol,&(*radauStruct).aTol,
		  &(*radauStruct).iTol,&jacobianMatrix,&(*radauStruct).iJac,
		  &(*radauStruct).mlJac,&(*radauStruct).muJac,
		  &massMatrix,&(*radauStruct).iMas,
		  &(*radauStruct).mlMas,&(*radauStruct).muMas,
		  &solOut,&(*radauStruct).iOut,
		  (*radauStruct).rWork,&(*radauStruct).lWork,
		  (*radauStruct).iWork,&(*radauStruct).liWork,
		  data,(*radauStruct).iPar,&(*radauStruct).idid );


	/* translate new states */
	minimalToGlobal(toT, states, globalStates, data);

}


void solOut(int *nr, double *told, double *t, double *x, double *cont, int *lrc, int *n, struct SCc3A3D_paraStruct *data, int *ipar, int *irtrn){

	LOG("%.3f,%.3f,%.3f", t[0], x[0], x[1]);
}

void SCc3A3D_equationsOfMotion(int *n, double *t_, double *x_, double *dx_, struct SCc3A3D_paraStruct *data, int *ipar){

	int i_;
	double t   = t_[0];
	double *y_  = x_;
	double *Dy_ = &(x_[7]);
	double *u_  = calloc(11, sizeof(double));

	double **M                = callocMatrix(7,7);
	double **eye              = callocMatrix(7,7);
	double **Jr               = callocMatrix(7,2);
	double **Jl               = callocMatrix(7,2);
	double *theta_            = calloc(7, sizeof(double));
	double *gamma_            = calloc(7, sizeof(double));
	kinStruct **kin     = fillStructure();
	/* Compute all inputs */
	f_endeffector_acceleration_inputs(t, y_, Dy_, &(u_[0]), data);
	f_contactforce_inputs(t, y_, Dy_, &(u_[9]), data);

	system_dynamics(t, x_, u_, &(dx_[7]), M, data);

	if ((data->externalForces!=NULL) || (data->externalTorques!=NULL)){
		/* Get kinematics */
		evalKin(kin, 0, 0, t, y_, Dy_, u_, data);
	}
	/* Add additional torques */
	if (data->externalForces!=NULL){
		for (i_=0; i_< 24; i_++){
			/* Transform forces into body system */
			transformVector('N', kin[i_]->abs_S, &(data->externalForces[3*i_]));
			MatrixVectorMultiplication('T', 7, 3, 1.0, kin[i_]->abs_Jt, &(data->externalForces[3*i_]), 1.0, &(dx_[7]));
		}
	}

	/* Add additional torques */
	if (data->externalTorques!=NULL){
		for (i_=0; i_< 24; i_++){
			/* Transform torques into body system */
			transformVector('N', kin[i_]->abs_S, &(data->externalTorques[3*i_]));
			MatrixVectorMultiplication('T', 7, 3, 1.0, kin[i_]->abs_Jr, &(data->externalTorques[3*i_]), 1.0, &(dx_[7]));
		}
	}

	memcpy(dx_, Dy_, 7*sizeof(double));
	for (i_=0; i_<7; i_++)
		eye[i_][i_] = 1.0;



	constraintEquations(t, y_, Dy_, u_, theta_, gamma_, Jr, Jl, data);

	MatrixVectorMultiplication( 'N', 7, 7, -1.0, eye, theta_, 1.0, dx);

	reduceMassMatrix( 7, 2, Jr, Jl, eye);

	MatrixVectorMultiplication( 'N', 7, 7, 1.0, eye, dx, 1.0, theta_);

	MatrixVectorMultiplication( 'N', 7, 7, -1.0, M, gamma_, 1.0, &(dx[7]));

	reduceMassMatrix( 7, 2, Jr, Jl, M);

	MatrixVectorMultiplication( 'N', 7, 7, 1.0, M, &(dx[7]), 1.0, gamma_);

	/* Build state-space vector of  */
	memcpy(&(dx_[0]), theta_, 7*sizeof(double));
	memcpy(&(dx_[7]), gamma_, 7*sizeof(double));

	/* Free allocated memory */
	freeMatrix( 7, 2, Jr);
	freeMatrix( 7, 2, Jl);
	freeMatrix(7,7,eye);
	free(theta_);
	free(gamma_);

	freeMatrix(7,7,M);
	free(u_);
	freeStructure(kin);

}
void SCc3A3D_minimalToGlobal(double t, double *minimal, double *global, struct SCc3A3D_paraStruct *data){


	/* Parameters */



	/* constant user-defined variables */ 

	double d_axes = data->d_axes;
	double z_0 = data->z_0;


	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = SCc3A3D_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = SCc3A3D_f_DelBo_EA2_2_x_(x_);
	double DelBo_EA2_3_rot_z_ = SCc3A3D_f_DelBo_EA2_3_rot_z_(x_);
	double DelBo_EA2_3_x_ = SCc3A3D_f_DelBo_EA2_3_x_(x_);
	double DelBo_EA2_3_y_ = SCc3A3D_f_DelBo_EA2_3_y_(x_);
	double DelBo_EA3_2_rot_x_ = SCc3A3D_f_DelBo_EA3_2_rot_x_(x_);
	double DelBo_EA3_2_rot_z_ = SCc3A3D_f_DelBo_EA3_2_rot_z_(x_);
	double DelBo_EA3_2_x_ = SCc3A3D_f_DelBo_EA3_2_x_(x_);
	double DelBo_EA3_2_y_ = SCc3A3D_f_DelBo_EA3_2_y_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = SCc3A3D_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = SCc3A3D_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = SCc3A3D_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_rot_x_ = SCc3A3D_f_elBo_EA3_2_rot_x_(x_);
	double elBo_EA3_2_rot_z_ = SCc3A3D_f_elBo_EA3_2_rot_z_(x_);
	double elBo_EA3_2_x_ = SCc3A3D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A3D_f_elBo_EA3_2_y_(x_);

	double p_a = minimal[0];
	double p_b = minimal[1];
	double r_alpha1 = minimal[2];
	double r_beta2 = minimal[3];
	double r_gamma3 = minimal[4];
	double EA2_q001 = minimal[5];
	double EA3_q001 = minimal[6];

	double Dp_a = minimal[7];
	double Dp_b = minimal[8];
	double Dr_alpha1 = minimal[9];
	double Dr_beta2 = minimal[10];
	double Dr_gamma3 = minimal[11];
	double DEA2_q001 = minimal[12];
	double DEA3_q001 = minimal[13];

	/* Body 1 */

	global[0] = 0;
	global[1] = p_a;
	global[2] = 0;

	global[3] = 0;
	global[4] = Dp_a;
	global[5] = 0;

	global[6] = 1.0;
	global[7] = 0;
	global[8] = 0;
	global[9] = 0;
	global[10] = 1.0;
	global[11] = 0;
	global[12] = 0;
	global[13] = 0;
	global[14] = 1.0;

	global[15] = 0;
	global[16] = 0;
	global[17] = 0;


	/* Body 2 */

	global[18] = d_axes + p_b;
	global[19] = 0;
	global[20] = 0;

	global[21] = Dp_b;
	global[22] = 0;
	global[23] = 0;

	global[24] = 1.0;
	global[25] = 0;
	global[26] = 0;
	global[27] = 0;
	global[28] = 1.0;
	global[29] = 0;
	global[30] = 0;
	global[31] = 0;
	global[32] = 1.0;

	global[33] = 0;
	global[34] = 0;
	global[35] = 0;


	/* Body 3 */

	global[36] = 0;
	global[37] = p_a;
	global[38] = z_0;

	global[39] = 0;
	global[40] = Dp_a;
	global[41] = 0;

	global[42] = cos(r_alpha1);
	global[43] = sin(r_alpha1);
	global[44] = 0;
	global[45] = -1.0*sin(r_alpha1);
	global[46] = cos(r_alpha1);
	global[47] = 0;
	global[48] = 0;
	global[49] = 0;
	global[50] = 1.0;

	global[51] = 0;
	global[52] = 0;
	global[53] = Dr_alpha1;


	/* Body 4 */

	global[54] = d_axes + p_b - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[55] = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[56] = z_0;

	global[57] = Dp_b - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[58] = DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[59] = 0;

	global[60] = cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[61] = -1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[62] = 0;
	global[63] = sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[64] = cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[65] = 0;
	global[66] = 0;
	global[67] = 0;
	global[68] = 1.0;

	global[69] = 0;
	global[70] = 0;
	global[71] = Dr_beta2 - 1.0*DelBo_EA2_2_rot_z_;


	/* Body 5 */

	global[72] = d_axes + p_b + elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[73] = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[74] = z_0;

	global[75] = Dp_b - (DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2)*(1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)) - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[76] = DelBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*(elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*DelBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	global[77] = 0;

	global[78] = cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3);
	global[79] = sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3);
	global[80] = sin(r_gamma3);
	global[81] = -1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3);
	global[82] = -1.0*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3);
	global[83] = cos(r_gamma3);
	global[84] = sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2);
	global[85] = -1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2);
	global[86] = 0;

	global[87] = Dr_gamma3*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2);
	global[88] = -1.0*Dr_gamma3*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2);
	global[89] = DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2;


	/* Body 6 */

	global[90] = d_axes + p_b + elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3);
	global[91] = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3);
	global[92] = z_0 + elBo_EA3_2_y_*cos(r_gamma3) + elBo_EA3_2_x_*sin(r_gamma3);

	global[93] = Dp_b - (DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2)*(1.0*elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)) - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*DelBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_x_*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*sin(r_gamma3))*(DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2) - 1.0*Dr_gamma3*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_y_*cos(r_gamma3) + elBo_EA3_2_x_*sin(r_gamma3));
	global[94] = DelBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*(elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*DelBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + DelBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*DelBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(r_gamma3) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_x_*cos(r_gamma3) - 1.0*elBo_EA3_2_y_*sin(r_gamma3))*(DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2) - 1.0*Dr_gamma3*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(elBo_EA3_2_y_*cos(r_gamma3) + elBo_EA3_2_x_*sin(r_gamma3));
	global[95] = DelBo_EA3_2_y_*cos(r_gamma3) + DelBo_EA3_2_x_*sin(r_gamma3) + Dr_gamma3*elBo_EA3_2_x_*cos(r_gamma3) - 1.0*Dr_gamma3*elBo_EA3_2_y_*sin(r_gamma3);

	global[96] = cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_z_)*cos(r_gamma3) + sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_) - 1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*sin(r_gamma3);
	global[97] = sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_z_)*cos(r_gamma3) - 1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_) - 1.0*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*sin(r_gamma3);
	global[98] = cos(elBo_EA3_2_rot_z_)*sin(r_gamma3) + cos(elBo_EA3_2_rot_x_)*cos(r_gamma3)*sin(elBo_EA3_2_rot_z_);
	global[99] = sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_) - 1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3)*sin(elBo_EA3_2_rot_z_) - 1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*sin(r_gamma3);
	global[100] = - 1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_) - 1.0*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3)*sin(elBo_EA3_2_rot_z_) - 1.0*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*sin(r_gamma3);
	global[101] = cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*cos(r_gamma3) - 1.0*sin(elBo_EA3_2_rot_z_)*sin(r_gamma3);
	global[102] = sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_x_) + cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(elBo_EA3_2_rot_x_)*sin(r_gamma3);
	global[103] = sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*sin(elBo_EA3_2_rot_x_)*sin(r_gamma3) - 1.0*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(elBo_EA3_2_rot_x_);
	global[104] = -1.0*cos(r_gamma3)*sin(elBo_EA3_2_rot_x_);

	global[105] = DelBo_EA3_2_rot_z_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2) + Dr_gamma3*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2) + DelBo_EA3_2_rot_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3);
	global[106] = DelBo_EA3_2_rot_x_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*cos(r_gamma3) - 1.0*Dr_gamma3*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2) - 1.0*DelBo_EA3_2_rot_z_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2);
	global[107] = DelBo_EA2_3_rot_z_ - 1.0*DelBo_EA2_2_rot_z_ + Dr_beta2 + DelBo_EA3_2_rot_x_*sin(r_gamma3);


}

void SCc3A3D_massMatrix( int *n, double *am, int *lmas, struct SCc3A3D_paraStruct *data, int *ipar ){

}

void SCc3A3D_jacobianMatrix ( int *n, double *t, double *x, double *dfy, int *ldfy, struct SCc3A3D_paraStruct *data, int *ipar ){

}

int SCc3A3D_getSystemStruct(struct SCc3A3D_paraStruct *data, const void *context){

	data->D2EE_force_x_s = getScalar(context, "D2EE_force_x_s", 0.000000e+00);
	data->D2EE_force_y_s = getScalar(context, "D2EE_force_y_s", 0.000000e+00);
	data->D2EE_x_s = getScalar(context, "D2EE_x_s", 0.000000e+00);
	data->D2EE_y_s = getScalar(context, "D2EE_y_s", 0.000000e+00);
	data->D2EE_z_s = getScalar(context, "D2EE_z_s", 0.000000e+00);
	data->DEE_force_x_s = getScalar(context, "DEE_force_x_s", 0.000000e+00);
	data->DEE_force_y_s = getScalar(context, "DEE_force_y_s", 0.000000e+00);
	data->DEE_x_s = getScalar(context, "DEE_x_s", 0.000000e+00);
	data->DEE_y_s = getScalar(context, "DEE_y_s", 0.000000e+00);
	data->DEE_z_s = getScalar(context, "DEE_z_s", 0.000000e+00);
	data->EE_force_x_d = getScalar(context, "EE_force_x_d", 0.000000e+00);
	data->EE_force_x_s = getScalar(context, "EE_force_x_s", 0.000000e+00);
	data->EE_force_y_d = getScalar(context, "EE_force_y_d", 0.000000e+00);
	data->EE_force_y_s = getScalar(context, "EE_force_y_s", 0.000000e+00);
	data->EE_x_d = getScalar(context, "EE_x_d", 0.000000e+00);
	data->EE_x_s = getScalar(context, "EE_x_s", 0.000000e+00);
	data->EE_y_d = getScalar(context, "EE_y_d", 0.000000e+00);
	data->EE_y_s = getScalar(context, "EE_y_s", 0.000000e+00);
	data->EE_z_d = getScalar(context, "EE_z_d", 0.000000e+00);
	data->EE_z_s = getScalar(context, "EE_z_s", 0.000000e+00);
	data->I_ee = getScalar(context, "I_ee", 0.000000e+00);
	data->K_L = getScalar(context, "K_L", 8.000000e+01);
	data->K_R = getScalar(context, "K_R", 1.500000e+01);
	data->d_axes = getScalar(context, "d_axes", 6.100000e-01);
	data->g = getScalar(context, "g", 9.810000e+00);
	data->l2 = getScalar(context, "l2", 1.000000e+00);
	data->l3 = getScalar(context, "l3", 4.643000e-01);
	data->m_C1 = getScalar(context, "m_C1", 6.000000e+00);
	data->m_C2 = getScalar(context, "m_C2", 6.600000e+00);
	data->m_ee = getScalar(context, "m_ee", 0.000000e+00);
	data->phi2_1 = getScalar(context, "phi2_1", 5.177806e-02);
	data->phi3_1 = getScalar(context, "phi3_1", 1.108656e-01);
	data->psi2_1 = getScalar(context, "psi2_1", 1.307493e-01);
	data->v2_1 = getScalar(context, "v2_1", 7.500000e-01);
	data->w2_1 = getScalar(context, "w2_1", 1.000000e+00);
	data->w3_1 = getScalar(context, "w3_1", 9.000000e-01);
	data->z_0 = getScalar(context, "z_0", 1.570000e-01);
	data->p_a_s = getScalar(context, "p_a_s", 0.000000e+00);
	data->Dp_a_s = getScalar(context, "Dp_a_s", 0.000000e+00);
	data->D2p_a_s = getScalar(context, "D2p_a_s", 0.000000e+00);
	data->p_b_s = getScalar(context, "p_b_s", 0.000000e+00);
	data->Dp_b_s = getScalar(context, "Dp_b_s", 0.000000e+00);
	data->D2p_b_s = getScalar(context, "D2p_b_s", 0.000000e+00);
	data->r_alpha1_s = getScalar(context, "r_alpha1_s", 0.000000e+00);
	data->Dr_alpha1_s = getScalar(context, "Dr_alpha1_s", 0.000000e+00);
	data->D2r_alpha1_s = getScalar(context, "D2r_alpha1_s", 0.000000e+00);
	data->r_beta2_s = getScalar(context, "r_beta2_s", 0.000000e+00);
	data->Dr_beta2_s = getScalar(context, "Dr_beta2_s", 0.000000e+00);
	data->D2r_beta2_s = getScalar(context, "D2r_beta2_s", 0.000000e+00);
	data->r_gamma3_s = getScalar(context, "r_gamma3_s", 0.000000e+00);
	data->Dr_gamma3_s = getScalar(context, "Dr_gamma3_s", 0.000000e+00);
	data->D2r_gamma3_s = getScalar(context, "D2r_gamma3_s", 0.000000e+00);
	data->EA2_q001_s = getScalar(context, "EA2_q001_s", 0.000000e+00);
	data->DEA2_q001_s = getScalar(context, "DEA2_q001_s", 0.000000e+00);
	data->D2EA2_q001_s = getScalar(context, "D2EA2_q001_s", 0.000000e+00);
	data->EA3_q001_s = getScalar(context, "EA3_q001_s", 0.000000e+00);
	data->DEA3_q001_s = getScalar(context, "DEA3_q001_s", 0.000000e+00);
	data->D2EA3_q001_s = getScalar(context, "D2EA3_q001_s", 0.000000e+00);

	return 0;
}
