#define S_FUNCTION_NAME SCc3A3D_Flexor_lin
#define S_FUNCTION_LEVEL 2

#include "SCc3A3D_Flexor.h"
#include "simstruc.h"
#include "SCc3A3D_paraStruct.h"
#include "SCc3A3D_pi_code.h"
#include "SCc3A3D_userDefined.h"
#ifdef  MATLAB_MEX_FILE
#include "SCc3A3D_pd_matlab.h"
#endif
#include "neweul.h"
#include "SCc3A3D_linearizeMBS.h"

#define SCc3A3D_IS_PARAM_DOUBLE(pVal) (mxIsNumeric(pVal) && !mxIsLogical(pVal) && !mxIsEmpty(pVal) && !mxIsSparse(pVal) && !mxIsComplex(pVal) && mxIsDouble(pVal))

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)

static void mdlCheckParameters(SimStruct *S) {
	if (!mxIsEmpty(ssGetSFcnParam(S,0))){
		if (!mxIsStruct(ssGetSFcnParam(S,0))) {
			ssSetErrorStatus(S,"First parameter to S-function must be sys.parameters.data");
			return;
		}
	}
	if (mxGetNumberOfElements(ssGetSFcnParam(S,1)) != SCC3A3D_SYS_DOF) {
		ssSetErrorStatus(S,"Second parameter to S-function must be 4x1 vector containing the initial values of the generalized coordinates");
		return;
	}
	if (mxGetNumberOfElements(ssGetSFcnParam(S,2)) != SCC3A3D_SYS_DOF) {
		ssSetErrorStatus(S,"Third parameter to S-function must be 4x1 vector containing the initial values of the generalized velocities");
		return;
	}
	if (mxGetNumberOfElements(ssGetSFcnParam(S,3)) != SCC3A3D_SYS_DOF) {
		ssSetErrorStatus(S,"Fourth parameter to S-function must be 4x1 vector containing the linearization points for the generalized coordinates");
		return;
	}
	if (mxGetNumberOfElements(ssGetSFcnParam(S,4)) != SCC3A3D_SYS_DOF) {
		ssSetErrorStatus(S,"Fifth parameter to S-function must be 4x1 vector containing the linearization points for the generalized velocities");
		return;
	}
}
#endif

/* INITIALIZE SIZES */

#define MDL_INITIALIZE_SIZES

static void mdlInitializeSizes(SimStruct *S) {
	int j_ = 0;
#if SCC3A3D_NUM_INPUT_GROUPS > 0
	int inputGroups[] = SCC3A3D_NUM_INPUTS;
#endif
	int outputGroups_[] = SCC3A3D_NUM_OUTPUTS;
	ssSetNumSFcnParams(S, 5); /* 1 S-function parameter */
#if defined(MATLAB_MEX_FILE)
	if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
		mdlCheckParameters(S);
		if (ssGetErrorStatus(S) != NULL) {
			return;
		}
	} else {
		return; /* Parameter mismatch will be reported by Simulink */
	}
#endif

	ssSetNumContStates(S, 2*SCC3A3D_SYS_DOF); /* Number of continous states */
	ssSetNumDiscStates(S, 0); /* Number of discrete states */

	if (!ssSetNumInputPorts(S, SCC3A3D_NUM_INPUT_GROUPS)) return; /* Number of S-function inputs */
#if SCC3A3D_NUM_INPUT_GROUPS > 0
	for (j_=0; j_< SCC3A3D_NUM_INPUT_GROUPS; j_++){
		ssSetInputPortWidth(S, j_, inputGroups[j_]);
		ssSetInputPortDirectFeedThrough(S, j_, 0);
	}
#endif

	if (!ssSetNumOutputPorts(S, SCC3A3D_NUM_OUTPUT_GROUPS+SCC3A3D_LOOPS_EXIST+1)) return; /* Number of S-function inputs */
	ssSetOutputPortWidth(S, 0, 2*SCC3A3D_SYS_DOF);
	for (j_=0; j_< SCC3A3D_NUM_OUTPUT_GROUPS; j_++){
		ssSetOutputPortWidth(S, j_+1, outputGroups_[j_]);
	}

	ssSetOutputPortWidth(S, 1+SCC3A3D_NUM_OUTPUT_GROUPS, SCC3A3D_NUM_LOOPS);

	ssSetNumSampleTimes(S,0);
	ssSetNumRWork(S, 0);
	ssSetNumIWork(S, 0);
	ssSetNumPWork(S, 6);
	ssSetNumModes(S, 0);
	ssSetNumNonsampledZCs(S, 0);

	ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);

}

/* INITIALIZE SAMPLE TIME */

#define MDL_INITIALIZE_SAMPLE_TIMES
static void mdlInitializeSampleTimes(SimStruct *S) {
	ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
	ssSetOffsetTime(S, 0, 0.0);
}

/* ENABLE */

#define MDL_ENABLE

static void mdlEnable(SimStruct *S) {
	UNUSED_ARG(S);
}

/* INITIALIZE CONDITIONS */

	#define MDL_INITIALIZE_CONDITIONS

static void mdlInitializeConditions(SimStruct *S) {

	void *dataPtr = NULL;

	int i_ = 0;

	double *x_                = ssGetContStates(S);

#ifdef  MATLAB_MEX_FILE
	const mxArray *parameters = ssGetSFcnParam(S, 0);
#endif
	const mxArray *y_WP_pt    = ssGetSFcnParam(S, 1);
	const mxArray *Dy_WP_pt   = ssGetSFcnParam(S, 2);
	const mxArray *y_init_pt  = ssGetSFcnParam(S, 3);
	const mxArray *Dy_init_pt = ssGetSFcnParam(S, 4);

	double *x_WP              = calloc(2*SCC3A3D_SYS_DOF, sizeof(double));
	double *y_WP              = mxGetPr(y_WP_pt);
	double *Dy_WP             = mxGetPr(Dy_WP_pt);
	double *y_init            = mxGetPr(y_init_pt);
	double *Dy_init           = mxGetPr(Dy_init_pt);

	double *u_                = calloc(11, sizeof(double));
	double **A                = callocMatrix(2*SCC3A3D_SYS_DOF,2*SCC3A3D_SYS_DOF);
	double **B                = callocMatrix(2*SCC3A3D_SYS_DOF,11);
	double **C                = callocMatrix(20,2*SCC3A3D_SYS_DOF);
	double **D                = callocMatrix(20,11);

	for (i_=0; i_<SCC3A3D_SYS_DOF; i_++){
		x_WP[i_] = y_WP[i_];
	}
	for (i_=0; i_<SCC3A3D_SYS_DOF; i_++){
		x_WP[i_+SCC3A3D_SYS_DOF] = Dy_WP[i_];
	}

	dataPtr = SCc3A3D_initializeSystemStruct();

	/* Check dimensions*/
	if ((mxGetM(y_WP_pt)!=SCC3A3D_SYS_DOF) || (mxGetN(y_WP_pt)!=1))
		printf("The expansion point vector of the generalized coordinates has to be a 7x1 vector!");

	if ((mxGetM(Dy_WP_pt)!=SCC3A3D_SYS_DOF) || (mxGetN(Dy_WP_pt)!=1))
		printf("The expansion point vector of the generalized velocities has to be a 7x1 vector!");

	if ((mxGetM(y_init_pt)!=SCC3A3D_SYS_DOF) || (mxGetN(y_init_pt)!=1))
		printf("The initial vector of the generalized velocities has to be a 7x1 vector!");

	if ((mxGetM(Dy_init_pt)!=SCC3A3D_SYS_DOF) || (mxGetN(Dy_init_pt)!=1))
		printf("The initial vector of the generalized velocities has to be a 7x1 vector!");

#ifdef  MATLAB_MEX_FILE
	if (!mxIsEmpty(parameters))
		SCc3A3D_getSystemStruct(dataPtr, (const void *) parameters);
#endif

	/* Linearize system dynamics */
	SCc3A3D_createSystemMatrix(0, x_WP, u_, A, SCC3A3D_SYS_DOF, dataPtr);
	SCc3A3D_createInputMatrix(0, x_WP, u_, B, SCC3A3D_SYS_DOF, 11, dataPtr);

	SCc3A3D_createOutputMatrix(0, x_WP, u_, &(C[0]), SCC3A3D_SYS_DOF, 4, dataPtr, SCc3A3D_f_desiredOut_outputs);
	SCc3A3D_createFeedthroughMatrix(0, x_WP, u_, &(D[0]), 11, 4, dataPtr, SCc3A3D_f_desiredOut_outputs);

	SCc3A3D_createOutputMatrix(0, x_WP, u_, &(C[4]), SCC3A3D_SYS_DOF, 6, dataPtr, SCc3A3D_f_exactOut_outputs);
	SCc3A3D_createFeedthroughMatrix(0, x_WP, u_, &(D[4]), 11, 6, dataPtr, SCc3A3D_f_exactOut_outputs);

	SCc3A3D_createOutputMatrix(0, x_WP, u_, &(C[10]), SCC3A3D_SYS_DOF, 6, dataPtr, SCc3A3D_f_approxOut_outputs);
	SCc3A3D_createFeedthroughMatrix(0, x_WP, u_, &(D[10]), 11, 6, dataPtr, SCc3A3D_f_approxOut_outputs);

	SCc3A3D_createOutputMatrix(0, x_WP, u_, &(C[16]), SCC3A3D_SYS_DOF, 4, dataPtr, SCc3A3D_f_Curvature_outputs);
	SCc3A3D_createFeedthroughMatrix(0, x_WP, u_, &(D[16]), 11, 4, dataPtr, SCc3A3D_f_Curvature_outputs);

	/* Read out initial conditions */
	for (i_=0; i_< SCC3A3D_SYS_DOF; i_++) {
		x_[i_] = y_init[i_];
		x_[i_+SCC3A3D_SYS_DOF] = Dy_init[i_];
	}

	free(x_WP);

	/* Store pointers in simulink system structure */
	ssSetPWorkValue(S, 0, dataPtr);
	ssSetPWorkValue(S, 1, u_);
	ssSetPWorkValue(S, 2, A);
	ssSetPWorkValue(S, 3, B);
	ssSetPWorkValue(S, 4, C);
	ssSetPWorkValue(S, 5, D);
	/* Free System Struct */
	SCc3A3D_freeStructure(dataPtr);

}

/* OUTPUTS */

static void mdlOutputs(SimStruct *S, int tid) {

	int i_        = 0;
	int j_        = 0;
	double *x_    = ssGetContStates(S);
	double *u_    = ssGetPWorkValue(S,1);
	double **C    = ssGetPWorkValue(S,4);
	double **D    = ssGetPWorkValue(S,5);
	double *states               = ssGetOutputPortRealSignal(S,0);
	double *y_desiredOut = ssGetOutputPortRealSignal(S, 1);
	double *y_exactOut = ssGetOutputPortRealSignal(S, 2);
	double *y_approxOut = ssGetOutputPortRealSignal(S, 3);
	double *y_Curvature = ssGetOutputPortRealSignal(S, 4);

	for (i_ = 0; i_<2*SCC3A3D_SYS_DOF; i_++)
		states[i_] = x_[i_];
	clearVector(4,y_desiredOut);

	for ( i_=0; i_<4;i_++){
		for ( j_=0; j_<2*SCC3A3D_SYS_DOF;j_++){
			y_desiredOut[i_] = y_desiredOut[i_] + C[0+i_][j_] * x_[j_];
		}
	}

	for ( i_=0; i_<4;i_++){
		for ( j_=0; j_<11;j_++){
			y_desiredOut[i_] = y_desiredOut[i_] + D[0+i_][j_] * u_[j_];
		}
	}
	clearVector(10,y_exactOut);

	for ( i_=0; i_<6;i_++){
		for ( j_=0; j_<2*SCC3A3D_SYS_DOF;j_++){
			y_exactOut[i_] = y_exactOut[i_] + C[4+i_][j_] * x_[j_];
		}
	}

	for ( i_=0; i_<6;i_++){
		for ( j_=0; j_<11;j_++){
			y_exactOut[i_] = y_exactOut[i_] + D[4+i_][j_] * u_[j_];
		}
	}
	clearVector(16,y_approxOut);

	for ( i_=0; i_<6;i_++){
		for ( j_=0; j_<2*SCC3A3D_SYS_DOF;j_++){
			y_approxOut[i_] = y_approxOut[i_] + C[10+i_][j_] * x_[j_];
		}
	}

	for ( i_=0; i_<6;i_++){
		for ( j_=0; j_<11;j_++){
			y_approxOut[i_] = y_approxOut[i_] + D[10+i_][j_] * u_[j_];
		}
	}
	clearVector(20,y_Curvature);

	for ( i_=0; i_<4;i_++){
		for ( j_=0; j_<2*SCC3A3D_SYS_DOF;j_++){
			y_Curvature[i_] = y_Curvature[i_] + C[16+i_][j_] * x_[j_];
		}
	}

	for ( i_=0; i_<4;i_++){
		for ( j_=0; j_<11;j_++){
			y_Curvature[i_] = y_Curvature[i_] + D[16+i_][j_] * u_[j_];
		}
	}

}

/* DERIVATIVES */

#define MDL_DERIVATIVES

static void mdlDerivatives(SimStruct *S) {

	double *x_ = ssGetContStates(S);
	double *f  = ssGetdX(S);

	int i_     = 0;
	int j_     = 0;

	double *u_ = ssGetPWorkValue(S,1);
	double **A = ssGetPWorkValue(S,2);
	double **B = ssGetPWorkValue(S,3);

	/* Inputs */
	InputRealPtrsType u_endeffector_acceleration = ssGetInputPortRealSignalPtrs(S, 0);
	InputRealPtrsType u_contactforce = ssGetInputPortRealSignalPtrs(S, 1);

	/* Combine all inputs */
	u_[0] = *u_endeffector_acceleration[0];
	u_[1] = *u_endeffector_acceleration[1];
	u_[2] = *u_endeffector_acceleration[2];
	u_[3] = *u_endeffector_acceleration[3];
	u_[4] = *u_endeffector_acceleration[4];
	u_[5] = *u_endeffector_acceleration[5];
	u_[6] = *u_endeffector_acceleration[6];
	u_[7] = *u_endeffector_acceleration[7];
	u_[8] = *u_endeffector_acceleration[8];
	u_[9] = *u_contactforce[0];
	u_[10] = *u_contactforce[1];

	clearVector(2*SCC3A3D_SYS_DOF,f);

	for ( i_=0; i_<2*SCC3A3D_SYS_DOF;i_++){
		for ( j_=0; j_<2*SCC3A3D_SYS_DOF;j_++){
			f[i_] = f[i_] + A[i_][j_] * x_[j_];
		}
	}

	for ( i_=0; i_<2*SCC3A3D_SYS_DOF;i_++){
		for ( j_=0; j_<11;j_++){
			f[i_] = f[i_] + B[i_][j_] * u_[j_];
		}
	}

}

/* TERMINATE */

#define MDL_TERMINATE

static void mdlTerminate(SimStruct *S) {

	/* Pointer casting */
	void *dataPtr = ssGetPWorkValue(S,0);
	double *u_    = ssGetPWorkValue(S,1);
	double **A    = ssGetPWorkValue(S,2);
	double **B    = ssGetPWorkValue(S,3);
	double **C    = ssGetPWorkValue(S,4);
	double **D    = ssGetPWorkValue(S,5);

	/* Free allocated memory */
	freeMatrix(2*SCC3A3D_SYS_DOF,2*SCC3A3D_SYS_DOF,A);
	freeMatrix(2*SCC3A3D_SYS_DOF,11,B);
	freeMatrix(20,2*SCC3A3D_SYS_DOF,C);
	freeMatrix(20,11,D);

	/* Free System Struct */
	SCc3A3D_freeStructure(dataPtr);
	free(u_);

}

#ifdef  MATLAB_MEX_FILE
#include "simulink.c"
#else
#include "cg_sfun.h"
#endif

