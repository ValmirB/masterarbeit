#ifndef SCc3A3D_linearizeMBS_H
#define SCc3A3D_linearizeMBS_H
#include "SCc3A3D_paraStruct.h"

/* Function prototypes */
void SCc3A3D_createSystemMatrix(double t_WP, double *x_WP, double *u_WP,
    double **A, int dim, void *dataPtr);

void SCc3A3D_createInputMatrix(double t_WP, double *x_WP, double *u_WP,
    double **B, int dim, int numIn, void *dataPtr);

void SCc3A3D_createOutputMatrix(double t_WP, double *x_WP,
    double *u_WP, double **C,
    int dim, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));

void SCc3A3D_createFeedthroughMatrix(double t_WP, double *x_WP,
    double *u_WP, double **D,
    int numIn, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));
#endif
