#ifndef SCc3A3D_constraintEquations_H
#define SCc3A3D_constraintEquations_H

#include "SCc3A3D_paraStruct.h"

void SCc3A3D_constraintEquations(double t, double *x_, double *u_, void *dataPtr);

void SCc3A3D_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr);

void SCc3A3D_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr);

void SCc3A3D_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr);

void SCc3A3D_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr);

void SCc3A3D_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr);

#endif
