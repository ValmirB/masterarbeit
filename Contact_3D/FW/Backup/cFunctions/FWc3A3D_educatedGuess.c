#include <math.h>
#include "FWc3A3D_paraStruct.h"
#include "FWc3A3D_pi_code.h"
#include "FWc3A3D_pd_matlab.h"
#include "FWc3A3D_userDefined.h"
#include "neweul.h"

void FWc3A3D_educatedGuess(double t, double *y_, void *dataPtr);

void FWc3A3D_educatedGuess(double t, double *y_, void *dataPtr){

	struct FWc3A3D_paraStruct *data  = (struct FWc3A3D_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
