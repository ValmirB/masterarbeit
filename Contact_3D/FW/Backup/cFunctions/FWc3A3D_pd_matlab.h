#ifndef FWc3A3D_pd_matlab_H
#define FWc3A3D_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "FWc3A3D_paraStruct.h"

double FWc3A3D_getScalar(const void *context, const char *name, double defaultValue);

int FWc3A3D_getSystemStruct(void *dataPtr, const void *context);

void FWc3A3D_educatedGuess(double t, double *y_, void *dataPtr);
void FWc3A3D_educatedGuess(double t, double *y_, void *dataPtr);

#endif
