function u_control = control_inputs(t, x_)
% control_inputs - Wrapper function for control inputs
% These inputs are stored under sys.in.control
% --------------------- Automatically generated file! ---------------------

u_control = zeros(9,1);

u_control(1) = f_p_a(t, x_);
u_control(2) = f_Dp_a(t, x_);
u_control(3) = f_D2p_a(t, x_);
u_control(4) = f_p_b(t, x_);
u_control(5) = f_Dp_b(t, x_);
u_control(6) = f_D2p_b(t, x_);
u_control(7) = f_r_gamma3(t, x_);
u_control(8) = f_Dr_gamma3(t, x_);
u_control(9) = f_D2r_gamma3(t, x_);

% END OF FILE

