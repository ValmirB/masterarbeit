function result_ = eqm_sysout_approxOut(t, x_, u_, varargin)
% eqm_sysout_approxOut - Vector of the system Flexor output approxOut
% 
% Entries are as in sys.model.output.approxOut

global sys;

% generalized coordinates, velocities and auxiliary coordinates

r_beta2 = x_(2);
EA2_q001 = x_(3);
EA3_q001 = x_(4);
Dr_beta2 = x_(6);
DEA2_q001 = x_(7);
DEA3_q001 = x_(8);
% system inputs
p_b = u_(4);
Dp_b = u_(5);
r_gamma3 = u_(7);
Dr_gamma3 = u_(8);

% constant user-defined variables
d_axes = sys.parameters.data.d_axes;
l2 = sys.parameters.data.l2;
l3 = sys.parameters.data.l3;
phi2_1 = sys.parameters.data.phi2_1;
phi3_1 = sys.parameters.data.phi3_1;
psi2_1 = sys.parameters.data.psi2_1;
v2_1 = sys.parameters.data.v2_1;
w2_1 = sys.parameters.data.w2_1;
w3_1 = sys.parameters.data.w3_1;
z_0 = sys.parameters.data.z_0;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

DelBo_EA2_2_rot_z_ = SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA2_2_x_ = SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_x_ =  SID_EA2_.frame.node(2).origin(1,:) + SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);

% System output approxOut vector
Pos_x_rlc = d_axes + p_b - elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + l2*cos(elBo_EA2_2_rot_z_ - r_beta2) + l3*cos(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - r_beta2) - EA3_q001*phi3_1*w3_1*cos(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
Pos_y_rlc = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) - l2*sin(elBo_EA2_2_rot_z_ - r_beta2) + l3*sin(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) + EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - r_beta2) - EA3_q001*phi3_1*w3_1*sin(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
Pos_z_rlc = z_0 + l3*sin(r_gamma3) + EA3_q001*phi3_1*w3_1*cos(r_gamma3);
Vel_x_rlc = Dp_b - (DelBo_EA2_2_rot_z_ - Dr_beta2)*(l2*sin(elBo_EA2_2_rot_z_ - r_beta2) - EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - r_beta2)) - DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) - Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) - sin(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*cos(r_gamma3) - EA3_q001*phi3_1*w3_1*sin(r_gamma3))*(Dr_beta2 - DelBo_EA2_2_rot_z_ + DEA2_q001*psi2_1*v2_1) - Dr_gamma3*cos(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*sin(r_gamma3) + EA3_q001*phi3_1*w3_1*cos(r_gamma3)) + DEA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - r_beta2) - DEA3_q001*phi3_1*w3_1*cos(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
Vel_y_rlc = DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) - (DelBo_EA2_2_rot_z_ - Dr_beta2)*(l2*cos(elBo_EA2_2_rot_z_ - r_beta2) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - r_beta2)) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) - Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + cos(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*cos(r_gamma3) - EA3_q001*phi3_1*w3_1*sin(r_gamma3))*(Dr_beta2 - DelBo_EA2_2_rot_z_ + DEA2_q001*psi2_1*v2_1) - Dr_gamma3*sin(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*(l3*sin(r_gamma3) + EA3_q001*phi3_1*w3_1*cos(r_gamma3)) + DEA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - r_beta2) - DEA3_q001*phi3_1*w3_1*sin(r_beta2 - elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
Vel_z_rlc = Dr_gamma3*l3*cos(r_gamma3) + DEA3_q001*phi3_1*w3_1*cos(r_gamma3) - Dr_gamma3*EA3_q001*phi3_1*w3_1*sin(r_gamma3);

result_ = zeros(6,1);

result_(1) = Pos_x_rlc;
result_(2) = Pos_y_rlc;
result_(3) = Pos_z_rlc;
result_(4) = Vel_x_rlc;
result_(5) = Vel_y_rlc;
result_(6) = Vel_z_rlc;

% END OF FILE 
