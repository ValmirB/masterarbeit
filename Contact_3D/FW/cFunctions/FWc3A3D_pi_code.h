#ifndef FWc3A3D_pi_ind_H
#define FWc3A3D_pi_ind_H

#include "FWc3A3D_paraStruct.h"

/* System dynamics */
void FWc3A3D_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr);

/* Auxiliary dynamics */
void FWc3A3D_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

/* Equations of motion */
void FWc3A3D_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

#endif
