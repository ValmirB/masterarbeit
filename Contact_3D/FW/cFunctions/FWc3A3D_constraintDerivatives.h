#ifndef FWc3A3D_constraintDerivatives_H
#define FWc3A3D_constraintDerivatives_H

#include "FWc3A3D_paraStruct.h"

void FWc3A3D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
