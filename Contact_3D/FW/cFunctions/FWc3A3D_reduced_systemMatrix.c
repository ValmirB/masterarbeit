#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "FWc3A3D_pi_code.h"
#include "FWc3A3D_userDefined.h"
#include "FWc3A3D_pd_matlab.h"
#include "neweul.h"
#include "FWc3A3D_paraStruct.h"
#include "FWc3A3D_Flexor.h"

#include "FWc3A3D_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double t             = mxGetScalar( prhs[0] );
	double *x_red        = mxGetPr( prhs[1] );
	double *y_ind        = &(x_red[0]);
	double *Dy_ind       = &(x_red[FWC3A3D_NUM_INDEPENDENT_GC]);
	double *x_           = calloc( 2*FWC3A3D_SYS_DOF, sizeof(double));
	double **Jr          = callocMatrix( FWC3A3D_SYS_DOF,FWC3A3D_NUM_INDEPENDENT_GC);
	double **Jl          = callocMatrix( FWC3A3D_SYS_DOF,FWC3A3D_NUM_INDEPENDENT_GC);
	double *f_           = calloc( FWC3A3D_SYS_DOF, sizeof(double));
	double **M_          = callocMatrix( FWC3A3D_SYS_DOF, FWC3A3D_SYS_DOF);
	double **Mred        = callocMatrix(FWC3A3D_NUM_INDEPENDENT_GC, FWC3A3D_NUM_INDEPENDENT_GC);
	double **MJr         = callocMatrix( FWC3A3D_SYS_DOF, FWC3A3D_NUM_INDEPENDENT_GC);
	double *KD           = calloc(2*FWC3A3D_SYS_DOF*FWC3A3D_NUM_INDEPENDENT_GC, sizeof(double));
	double *matlabReturn = NULL;
	double *u_           = calloc(FWC3A3D_NUM_ALL_INPUTS, sizeof(double));
	unsigned short *z    = calloc(FWC3A3D_NUM_INDEPENDENT_GC, sizeof(unsigned short));
	int counter_         = 0;
	int i,j;
	double myStep = 1e-8;
	struct FWc3A3D_paraStruct *data = NULL;

	dataPtr = FWc3A3D_initializeSystemStruct();

	data  = (struct FWc3A3D_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		goto clean_up;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		goto clean_up;
	}

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		FWc3A3D_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	if ((mxGetM(prhs[1])!=2*FWC3A3D_NUM_INDEPENDENT_GC) || (mxGetN(prhs[1])!=1)){
		mexPrintf("The second input argument has to be a 4x1 vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*FWC3A3D_NUM_INDEPENDENT_GC, 2*FWC3A3D_NUM_INDEPENDENT_GC, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	/* Get dependent generalized coordinates and velocities */
	for ( counter_ = 0 ; counter_ < FWC3A3D_NUM_INDEPENDENT_GC ; counter_++ ){
		x_[data->independentIndices[counter_]]  =  y_ind[counter_];
		x_[data->independentIndices[counter_]+FWC3A3D_SYS_DOF] = Dy_ind[counter_];
	}

	FWc3A3D_educatedGuess(t, x_, dataPtr);

	FWc3A3D_getDependentGenCoords(t, x_, u_, dataPtr);

	
	FWc3A3D_jacobianPartition(t, x_, u_, Jr, Jl, dataPtr);
	
	/* Loop over all position values */

	for (i=0; i<FWC3A3D_NUM_INDEPENDENT_GC; i++){
		clearVector(FWC3A3D_SYS_DOF,f_);
		clearVector(FWC3A3D_NUM_ALL_INPUTS,u_);
		clearMatrix(FWC3A3D_SYS_DOF,FWC3A3D_SYS_DOF,M_);

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		FWc3A3D_f_control_inputs(t, x_, &(u_[0]), dataPtr);
		FWc3A3D_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			KD[FWC3A3D_SYS_DOF*i+j] = f_[j];

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			x_[j] = x_[j] - 2.0*Jr[j][i]*myStep;

		clearVector(FWC3A3D_SYS_DOF,f_);
		clearMatrix(FWC3A3D_SYS_DOF,FWC3A3D_SYS_DOF,M_);

		FWc3A3D_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			KD[FWC3A3D_SYS_DOF*i+j] = (KD[FWC3A3D_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		clearVector(FWC3A3D_SYS_DOF,f_);
		clearVector(FWC3A3D_NUM_ALL_INPUTS,u_);
		clearMatrix(FWC3A3D_SYS_DOF,FWC3A3D_SYS_DOF,M_);

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			x_[j+FWC3A3D_SYS_DOF] = x_[j+FWC3A3D_SYS_DOF] + Jr[j][i]*myStep;

		FWc3A3D_f_control_inputs(t, x_, &(u_[0]), dataPtr);
		FWc3A3D_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			KD[FWC3A3D_NUM_INDEPENDENT_GC*FWC3A3D_SYS_DOF+FWC3A3D_SYS_DOF*i+j] = f_[j];

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			x_[j+FWC3A3D_SYS_DOF] = x_[j+FWC3A3D_SYS_DOF] - 2.0*Jr[j][i]*myStep;

		clearVector(FWC3A3D_SYS_DOF,f_);
		clearMatrix(FWC3A3D_SYS_DOF,FWC3A3D_SYS_DOF,M_);

		FWc3A3D_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			KD[FWC3A3D_NUM_INDEPENDENT_GC*FWC3A3D_SYS_DOF+FWC3A3D_SYS_DOF*i+j] = (KD[FWC3A3D_NUM_INDEPENDENT_GC*FWC3A3D_SYS_DOF+FWC3A3D_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<FWC3A3D_SYS_DOF; j++)
			x_[j+FWC3A3D_SYS_DOF] = x_[j+FWC3A3D_SYS_DOF] + Jr[j][i]*myStep;

	}

	MatrixMultiplication( 'N', 'N', FWC3A3D_SYS_DOF, FWC3A3D_SYS_DOF, FWC3A3D_NUM_INDEPENDENT_GC, 1.0, M_, Jr, 0.0, MJr); /* MJr allokieren */

	MatrixMultiplication( 'T', 'N', FWC3A3D_NUM_INDEPENDENT_GC, FWC3A3D_SYS_DOF, FWC3A3D_NUM_INDEPENDENT_GC, 1.0, Jl, MJr, 0.0, Mred); /* Mred allokieren */

	GaussianElimination( FWC3A3D_NUM_INDEPENDENT_GC, z, Mred);

	for (i=0; i<FWC3A3D_NUM_INDEPENDENT_GC; i++)
		matlabReturn[(2*(FWC3A3D_NUM_INDEPENDENT_GC+i))*FWC3A3D_NUM_INDEPENDENT_GC+i] = 1.0;

	for (i=0; i<2*FWC3A3D_NUM_INDEPENDENT_GC; i++){
		MatrixVectorMultiplication( 'T', FWC3A3D_NUM_INDEPENDENT_GC, FWC3A3D_SYS_DOF, 1.0, Jl, &(KD[FWC3A3D_SYS_DOF*i]), 0.0, &(matlabReturn[2*FWC3A3D_NUM_INDEPENDENT_GC*i+FWC3A3D_NUM_INDEPENDENT_GC]));
		permuteVector(FWC3A3D_NUM_INDEPENDENT_GC, z, &(matlabReturn[2*FWC3A3D_NUM_INDEPENDENT_GC*i+FWC3A3D_NUM_INDEPENDENT_GC]));
		LUForBack_1dim( FWC3A3D_NUM_INDEPENDENT_GC, Mred, &(matlabReturn[2*FWC3A3D_NUM_INDEPENDENT_GC*i+FWC3A3D_NUM_INDEPENDENT_GC]));
	}

clean_up:

	free(x_);
	freeMatrix(FWC3A3D_SYS_DOF,FWC3A3D_NUM_INDEPENDENT_GC,Jr);
	freeMatrix(FWC3A3D_SYS_DOF,FWC3A3D_NUM_INDEPENDENT_GC,Jl);
	free(f_);
	freeMatrix(FWC3A3D_SYS_DOF,FWC3A3D_SYS_DOF,M_);
	freeMatrix(FWC3A3D_NUM_INDEPENDENT_GC,FWC3A3D_NUM_INDEPENDENT_GC,Mred);
	freeMatrix(FWC3A3D_SYS_DOF,FWC3A3D_NUM_INDEPENDENT_GC,MJr);
	free(KD);
	free(u_);
	free(z);

	/* Free System Struct */
	FWc3A3D_freeStructure(dataPtr);

}
