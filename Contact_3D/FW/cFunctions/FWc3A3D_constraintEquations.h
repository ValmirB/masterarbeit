#ifndef FWc3A3D_constraintEquations_H
#define FWc3A3D_constraintEquations_H

#include "FWc3A3D_paraStruct.h"

void FWc3A3D_constraintEquations(double t, double *x_, double *u_, void *dataPtr);

void FWc3A3D_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr);

void FWc3A3D_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr);

void FWc3A3D_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr);

void FWc3A3D_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr);

void FWc3A3D_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr);

#endif
