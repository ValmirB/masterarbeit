#define S_FUNCTION_NAME SDOF_KF_snvna /* Strain Measurements */
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>
#include "neweul.h" /* Header file containing prototypes of additional algorithms */
#include "SDOF_Kalman_snvna.h"

/* Single Degrees of Freedom Linear Kalman Filter for multiple inputs */

/************************************************************************/
/******** PLEASE DEFINE SUPPLEMENTARY PARAMETERS FOR THE SDOF KF ********/
/************************************************************************/
#define NUMOUT (2)        /* Number of Signals fed to SDOF_KF (needs to be set only in RTW) */
#define OUTPUTKGAINS (0)  /* Flag whether Kalman Gains are passed to simulink */
/************************************************************************/

/* The Filter can be used for any number of noisy input signals, which will 
 * be processed seperately. However, for the use of the Steady State Kalman 
 * Gains the number of inputs must be specified by the preprocessor 
 * directive NUMOUT (See above).
 *
 * The Input Signals are passed directly to mdlOUTPUT enforcing direct
 * feedthrough to be enabled. In this function all the steps are executed
 * before the estimated and corrected states are passed to simulink. The 
 * memory for the required variables of the Kalman Filter is allocated in
 * mdlInitializeConditions and referenced by Call-by-reference (Pointers).
 * The pointers will be stored in SimStruct to provide them in all other 
 * C-function within the S-function.
 *
 * Properties:
 *  -   System Functions are collected in SDOF_Kalman.c and the Prototypes
 *      and the preprocessor function can be found in SDOF_Kalman.h
 *  -   It is possible to use multiple Kalman Filters (S-Function) with the
 *      exact same SDOF_Kalman.c and SDOF_Kalman.h. Hence, individual modi-
 *      fication should be applied within the specific S-Function.
 *  -   Compiliation is executed using "Compile_SDOF.m", which demands the name
 *      of this S-Function (w/ or w/o trailing extension) as first input
 *      argument.
 *  -   For the discrete-time State Transition Matrix to compute the a 
 *      priori state estimate, a pade approximation PHI(T) is utilized
 *  -   The S-Function awaits dialogParameters which are checked when 
 *      simulation run is initialized. All the dialogParameters must have 
 *      the same number elements as there are signals into the S-Function.
 *  -   Input Signals:
 *      Port 0:     Noise measurement signals   y
 *  -   Output Signals: (depends on the value of OUTPUTSNV)
 *      OUTPUTSNV == 1
 *          Port 0: Estimated and corrected coordinate (first state of each input)
 *          Port 1: Estimated and corrected velocities
 *          Port 2: Kalman Gains for Data Assimilation Step
 *      OUTPUTSNV == 0
 *          Port 0: Estimated and corrected states (coordinate, velocity, ...) 
 *          Port 1: Kalman Gains for Data Assimilation Step   
 * 
 * Input arguments (Dialog Parameters):     TYPE, R_K, EPS, TAU, RJCTTYPE, SSKG
 *      TYPE        Type of Noise Model (see "sdofModelCovariance" for details)
 *      R_K         Constant discrete-time noise variance (square matrix).
 *                  The # of rows and columns must equal the # of input signals.
 *      EPS         Fitting Parameters for the process noise model. The meaning
 *                  of this parameter depends on the type of noise model used.
 *      TAU         "Correlation Maneuver Time" [s]
 *                  This parameter is only used for the exponentially colored
 *                  noise model (Singer Models), which differs slightly in
 *                  the state transition and also in the noise model.
 *                  For all other noise models, this parameter remains untouched.
 *      RJCTTYPE    Type of outlier rejection: (0) None, (1) saturation, (2) rejection
 *      0           none. The outlier criteria is ignored completely.
 *      1           Saturate the innovation to the value of the outlier criteria
 *      2           Reset innovation to zero completely.
 *      SSKG        Steady-State Kalman Gain of System --> R^(DIM x NUM_MA)
 *                  (This parameter is only used if STEADYSTATE == 1)
 *                  If "standard" functioning is applied (STEADYSTATE == 0), 
 *                  this parameter can be defined with zero.
 *
 * For each input signal, those parameters can be defined individually, where
 * as the index of these arrays correspond to the order of the signals entering
 * the s-function block. It an dialogParameter does not have the required 
 * number of elements, an error Message is displayed and simulation is aborted.
 *      
 * Note: Although the SDOF Kalman Filter can be used for multiple inputs,
 * this should be circumvented by using more than one S-function, since the
 * total dimension of the state vector will be increased each time and also
 * the computation time. Hence, this should ONLY be used, if there exists a
 * resonable coupling inbetween those signals.
 */

#define DIM (DOF*NUMOUT)   /* System dimension of UKF */

#if CONSTRAINTS
    #if !NUM_CON
    #undef CONSTRAINTS
    #define CONSTRAINTS (0)
    #endif
#else  /* Reset Number of Constraint equations if no constraints are present */ 
    #if (NUM_CON > 0)
    #undef NUM_CON
    #define NUM_CON (0)
    #endif
#endif

#define NUM_MA (NUMOUT + NUM_CON) /* Measurement augmentation by constraints */
    
#define MDL_CHECK_PARAMETERS
#if (defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE))
static void mdlCheckParameters(SimStruct *S) {
    /* The checkParameters Function can be found in SDOF_sysDef.c */
    checkParams(S, NUMOUT, NUM_MA);
}
#endif /* MDL_CHECK_PARAMETERS */

/* INITIALIZE SIZES */
#define MDL_INITIALIZE_SIZES
static void mdlInitializeSizes(SimStruct *S) {
    
    ssSetNumSFcnParams(S, NPARAMS); /* S-function parameters */
    
    /* Check Dialog Parameters only if it used in Simulink (not in RTW) */
    #if ( defined(MATLAB_MEX_FILE) && MATLAB_MEX_FILE == 1 )
        /* Parameter mismatch will be reported by Simulink */
        if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) return;
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) return;
    #endif    

    /* Number and Signal Dimensions of Input and Output Ports */
    #if ( USEADES == 1 )
        if (!ssSetNumInputPorts(S, 2)) return; /* Number of S-function inputs */  
        #if ( defined(MATLAB_MEX_FILE) && MATLAB_MEX_FILE == 1 )
            ssSetInputPortWidth(S, 1, DYNAMICALLY_SIZED); /* Number of system outputs */    
        #else        
            ssSetInputPortWidth(S, 1, NUMOUT); /* Number of system outputs */
        #endif
        ssSetInputPortDirectFeedThrough(S, 1, 1); /* Activate direct feed through*/
    #else
        if (!ssSetNumInputPorts(S, 1)) return; /* Number of S-function inputs */
    #endif
    ssSetInputPortWidth(S, 0, NUMOUT); /* Number of system outputs */
    ssSetInputPortDirectFeedThrough(S, 0, 1); /* Activate direct feed through*/ 
    #if ( defined(OUTPUTSNV) && OUTPUTSNV == 1 )  
        if (!ssSetNumOutputPorts(S, 3)) return; /* Number of S-function outputs */
        ssSetOutputPortWidth(S, 0, NUMOUT);     /* Estimated coordinates */
        ssSetOutputPortWidth(S, 1, NUMOUT);     /* Estimated velocities */
        #if ( defined(OUTPUTKGAINS) && OUTPUTKGAINS == 1 )
            ssSetOutputPortWidth(S, 2, NUM_MA*DIM); /* Kalman Gains K_k */
        #else
            ssSetOutputPortWidth(S, 2, 1);    
        #endif  
    #else
        if (!ssSetNumOutputPorts(S, 2)) return; /* Number of S-function outputs */
        ssSetOutputPortWidth(S, 0, DIM);        /* Estimated & corrected states */
        #if ( defined(OUTPUTKGAINS) && OUTPUTKGAINS == 1 )
            ssSetOutputPortWidth(S, 1, NUM_MA*DIM); /* Kalman Gains K_k */
        #else
            ssSetOutputPortWidth(S, 1, 1);    
        #endif  
    #endif 

          
                
                
    ssSetNumContStates(S, 0);       /* Number of continous states */
    ssSetNumDiscStates(S, 0);       /* Number of discrete states */    
    ssSetNumSampleTimes(S, 1);
    ssSetNumIWork(S, 2);            /* Persistent counters */
    ssSetNumRWork(S, 0); 
    ssSetNumPWork(S, 20);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

/* INITIALIZE SAMPLE TIME */
#define MDL_INITIALIZE_SAMPLE_TIMES
static void mdlInitializeSampleTimes(SimStruct *S) {
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}

/* ENABLE */
#define MDL_ENABLE
static void mdlEnable(SimStruct *S) {
    UNUSED_ARG(S);
}

/* INITIALIZE CONDITIONS */
#define MDL_INITIALIZE_CONDITIONS 
static void mdlInitializeConditions(SimStruct *S) {

    int *counters = ssGetIWork(S);
    
    int i      = 0;
    int numout = 0;
    int dim    = 0;
    int num_ma = 0;
    
    /* POINTER DECLARATION */
    double **P_estimated = NULL;
    double **errorUpdate = NULL;
    double **K_k         = NULL;
    double **denominator = NULL;
    double **matrix1     = NULL;
    double **matrix2     = NULL;
    double **matrix3     = NULL;
    double **matrix4     = NULL;
    double **Phi         = NULL;
    double **C           = NULL;
    double **Qconst      = NULL;
    
    double *xhat        = NULL;
    double *xbak        = NULL;
    double *yhat        = NULL;
    double *ymeas       = NULL;
    double *measurementDiff = NULL;
    double *abar        = NULL;
    double *Rvotf       = NULL;
    double *tmin1       = NULL;   
    double **dlgParam   = NULL; /* Pointer to Pointer to DialogParameters */
    
    double tstep = 0.0; 
    
    /* DETERMINATION OF SIGNAL DIMENSIONS */
    #if ( defined(MATLAB_MEX_FILE) && MATLAB_MEX_FILE == 1 )
        numout = (int) ssGetInputPortWidth(S, 0);  
        dim = (int) DOF * numout;
        num_ma = numout + (int) NUM_CON;
    #else
        numout = (int) NUMOUT;
        dim = (int) DIM;
        num_ma = (int) NUM_MA;
    #endif

    /* Miscellaneous needed counters */ 
    counters[0] = 0; /* number of timesteps */
    counters[1] = 0; /* number of failed cholesky decompositions */            
                
    /* MEMORY ALLOCATION */            
    P_estimated = callocMatrix(dim,     dim); /* Estimated error covariance matrix Pxx- */      
    errorUpdate = callocMatrix(dim,     dim); /* Updated error covariance matrix Pxx+ */
    K_k         = callocMatrix(num_ma,  dim); /* Transposed Kalman-Gain-Matrix */   
    denominator = callocMatrix(num_ma,  num_ma); /* Denominator of the Kalman-Gain-Matrix */
    
    matrix1 = callocMatrix(dim,     dim);  /* Memory to compute Pxx */
    matrix2 = callocMatrix(num_ma,  num_ma); /* Memory to store denominator */
    matrix3 = callocMatrix(dim,     num_ma); /* Memory for Result of Dyadic Product to compute Pxy */     
    matrix4 = callocMatrix(numout,  numout); /* Memory for MeasurementCovariance */     
    
    /* Allocate vectors in order to compare simulated and measured system outputs */
    xhat            = (double *) calloc(dim,    sizeof(double));
    xbak            = (double *) calloc(dim,    sizeof(double));
    abar            = (double *) calloc(numout, sizeof(double)); /* Mean Acceleration */
    yhat            = (double *) calloc(num_ma, sizeof(double));
    ymeas           = (double *) calloc(num_ma, sizeof(double));
    measurementDiff = (double *) calloc(num_ma, sizeof(double));
    tmin1           = (double *) calloc(1,      sizeof(double)); 
    
    /* Allocation of System Matrices */
    Phi     = callocMatrix(dim,     dim);   /* Discrete Transition Matrix */
    C       = callocMatrix(num_ma,  dim);   /* Output and Constraint Matrix */
    Qconst  = callocMatrix(dim,     dim);   /* Additive Model Covariance */
    
    /* Memory Allocation to compute the Adaptive Noise Variance (Rvotf) */
    #if (defined(RVONTHEFLY) && RVONTHEFLY == 1)
        /* Memory required for variables are all referenced by one pointer */
        /* Rvotf = [k,sumsq,ykmin1,delysq] --> NWIN+3 doubles for each input */
        Rvotf = (double *) calloc(1+numout*(NWIN+2), sizeof(double));
    #endif
    
    /* Recast DialogParameters since RTW does not like mxGetPr in sysDef */
    #if (NPARAMS > 0)
        dlgParam = (double **) calloc( NPARAMS, sizeof(double *));
        for (i = 0; i < NPARAMS; i++){
           dlgParam[i] = (double *)  mxGetPr(ssGetSFcnParam(S, i));
        }          
    #endif          
            
    /* Compute constant model covariance before simulation starts */
    #if (defined(CONSTANTQ) && CONSTANTQ == 1 )
        #if defined(VARIABLESTEP) && VARIABLESTEP
            tstep  = (double) DELT;    /* Expected update period */
        #else        
            tstep  = ssGetFixedStepSize(S); /* Fixed StepSize of Simulation */        
        #endif 
        sdofModelCov(S, tstep, dim, xhat, Qconst, abar, dlgParam);
    #endif
                
    /* Steady-State transposed Kalman Matrix is read from DialogParameters */        
    #if ( defined(STEADYSTATE) && STEADYSTATE == 1 )      
        sdofSteadyStateK_k(S, num_ma, dim, K_k, dlgParam);    
    #endif                  
                
    /* Storing pointers to s-function structure */
    ssSetPWorkValue(S, 0, P_estimated);
    ssSetPWorkValue(S, 1, errorUpdate);
    ssSetPWorkValue(S, 2, K_k);
    ssSetPWorkValue(S, 3, denominator);
    ssSetPWorkValue(S, 4, ymeas);
    ssSetPWorkValue(S, 5, matrix1);
    ssSetPWorkValue(S, 6, matrix2);
    ssSetPWorkValue(S, 7, matrix3);
    ssSetPWorkValue(S, 8, matrix4);
    ssSetPWorkValue(S, 9, xhat);
    ssSetPWorkValue(S, 10, yhat);
    ssSetPWorkValue(S, 11, measurementDiff);    
    ssSetPWorkValue(S, 12, xbak);
    ssSetPWorkValue(S, 13, Phi);
    ssSetPWorkValue(S, 14, C);
    ssSetPWorkValue(S, 15, abar);
    ssSetPWorkValue(S, 16, Qconst);
    ssSetPWorkValue(S, 17, Rvotf);
    ssSetPWorkValue(S, 18, tmin1);
    ssSetPWorkValue(S, 19, dlgParam);
}

/* OUTPUTS */
static void mdlOutputs(SimStruct *S, int tid) {
    
    int i        = 0; 
    int j        = 0;
    int numout   = 0;
    int dim      = 0; 
    int num_ma   = 0; 
    double t     = 0.0; 
    double tstep = 0.0;
    
    /* POINTER DECLARATION */
    double **P_estimated =      ssGetPWorkValue(S, 0); /* Estimated error covariance matrix */
    double **errorUpdate =      ssGetPWorkValue(S, 1); /* Corrected error covariance matrix */
    double **K_k =              ssGetPWorkValue(S, 2); /* Transposed Kalman-Gain-Matrix */
    double **denominator =      ssGetPWorkValue(S, 3); /* Sym. pos. def. denominator matrix*/
    double *ymeas  =            ssGetPWorkValue(S, 4);
    double **matrix1 =          ssGetPWorkValue(S, 5);
    double **matrix2 =          ssGetPWorkValue(S, 6);
    double **matrix3 =          ssGetPWorkValue(S, 7);
    double **matrix4 =          ssGetPWorkValue(S, 8);
    double *xhat =              ssGetPWorkValue(S, 9); /* Simulated system states */
    double *yhat =              ssGetPWorkValue(S, 10); /* Simulated system outputs */
    double *measurementDiff =   ssGetPWorkValue(S, 11); /* Difference between simulated & measured outputs */    
    double *xbak =              ssGetPWorkValue(S, 12); 
    double **Phi =              ssGetPWorkValue(S, 13); 
    double **C =                ssGetPWorkValue(S, 14); 
    double *abar =              ssGetPWorkValue(S, 15);
    double **Qconst =           ssGetPWorkValue(S, 16);
    double *Rvotf =             ssGetPWorkValue(S, 17); /* Memory for the Adaptive Noise Variance */
    double *tmin1 =             ssGetPWorkValue(S, 18); /* Memory for Time of Previous Time Step */
    double **dlgParam =         ssGetPWorkValue(S, 19); /* Pointer to Dialog Parameters */
    
    /* Output vectors */
    #if defined(OUTPUTSNV) && OUTPUTSNV 
        double *xest = ssGetOutputPortRealSignal(S, 0); /* Estimated coordinates */
        double *vest = ssGetOutputPortRealSignal(S, 1); /* Estimated velocities */
        double *kalmangains = ssGetOutputPortRealSignal(S, 2);  /* Kalman Gains */
    #else        
        double *systemstates = ssGetOutputPortRealSignal(S, 0); /* Corrected states */
        double *kalmangains = ssGetOutputPortRealSignal(S, 1);  /* Kalman Gains */
    #endif
    
    /* Input vectors */
    InputRealPtrsType measurementPtrs = ssGetInputPortRealSignalPtrs(S, 0);
    
    /* Work vector */
    int *counters = ssGetIWork(S);    
    
    UNUSED_ARG(tid);
    
    /* DETERMINATION OF SIGNAL DIMENSIONS */
    #if ( defined(MATLAB_MEX_FILE) && MATLAB_MEX_FILE == 1 )
        numout = (int) ssGetInputPortWidth(S, 0);  
        dim = (int) DOF * numout;
        num_ma = numout + (int) NUM_CON;
    #else
        numout = (int) NUMOUT;
        dim = (int) DIM;
        num_ma = (int) NUM_MA;
    #endif
    
    if (ssIsMajorTimeStep(S)) {
           
        t = ssGetT(S); /* Current Simulation Time */
        
        /****************************************************************/        
        /* Set Initial Estimates for states and error covariance */
        if (counters[0] == 0) {
            #if ( defined(VARIABLESTEP) && VARIABLESTEP == 1 )
                tstep  = (double) DELT;    /* Expected update period */
            #else        
                tstep  = ssGetFixedStepSize(S); /* Fixed StepSize of Simulation */        
            #endif 
                    
            inixhatPhat(S, tstep, dim, xhat, dim, errorUpdate, dlgParam);
            counters[0] = 1;
            
        } else { 
            #if ( defined(VARIABLESTEP) && VARIABLESTEP == 1 )
                tstep = t - tmin1[0];           /* Current time update period */
            #else        
                tstep  = ssGetFixedStepSize(S); /* Fixed StepSize of Simulation */        
            #endif  
        }
        tmin1[0] = t;       /* Store simulation time for next time step */
        
        
        /****************************************************************/
        /** TIME UPDATE - A PRIORI ESTIMATES - FORECAST STEP **/
        
        /* Compute the System Dynamics x(k) = Phi * x(k-1) */
        memcpy(xbak, xhat, dim*sizeof(double)); /* Copy a posteriori estimate to backup spot */
        sdofDynamics(S, tstep, dim, Phi, xhat, xbak, abar, dlgParam); 

        /* Compute P_xx(-) = Q + Phi * Pxx(+) * Phi^T --> errorUpdate */
        #if ( (!defined(STEADYSTATE)) || (STEADYSTATE == 0) )
            clearMatrix(dim, dim, P_estimated);
            #if (defined(CONSTANTQ) && CONSTANTQ)
                for (i = 0; i < dim; i++) /* Copy constant Covariance rowwise */
                    memcpy(&(P_estimated[i][0]), &(Qconst[i][0]), dim * sizeof(double));
            #else /* Compute Model Covariance on every time step */
                sdofModelCov(S, tstep, dim, xhat, P_estimated, abar, dlgParam); /* errorUpdate = G * Q * G^T */
            #endif
            MatrixMultiplication('N', 'N', dim, dim, dim, 1.0, Phi, errorUpdate, 0.0, matrix1);
            MatrixMultiplication('N', 'T', dim, dim, dim, 1.0, matrix1, Phi, 1.0, P_estimated);
        #endif
        
        /****************************************************************/
        /** MEASUREMENT UPDATE - A POSTERIORI - DATA ASSIMILATION STEP **/
        
        /* Compute simulated output hhat = [yhat; dhat] */
        sdofOutput(S, tstep, numout, dim, C, xhat, yhat, dlgParam);
        #if (defined(CONSTRAINTS) && CONSTRAINTS)
            sdofConstraints(S, tstep, NUM_CON, dim, &(C[numout]), xhat, &(y[numout]), dlgParam);    
        #endif   
                
        #if defined(STEADYSTATE) && STEADYSTATE /* Steady State Kalman Filter */ 
            #if (defined(OUTLIER_REJECTION) && (OUTLIER_REJECTION > 0))
                sdofMeasurementCov(S, tstep, numout, xhat, matrix4, Rvotf, dlgParam); 
            #endif 
                
        #else    /* Linear Kalman Filter - A posteriori step */ 
                
            /* Compute Pyy = R + C*Pxx(-)*C^T --> denominator */
            clearMatrix(num_ma, num_ma, denominator);
            sdofMeasurementCov(S, tstep, numout, xhat, denominator, Rvotf, dlgParam); 
            #if (defined(OUTLIER_REJECTION) && (OUTLIER_REJECTION > 0))
                for (i = 0; i < numout; i++) /* Store MeasurementCovariance rowwise */
                    memcpy( &(matrix4[i][0]), &(denominator[i][0]), numout*sizeof(double));
            #endif
            #if CONSTRAINTS /* Append Measurement Covariance when Constraints are present */
                for (i = 0; i < NUM_CON; i++)
                    denominator[numout+i][numout+i] = (double) DELTA;
            #endif
            MatrixMultiplication('N', 'N', num_ma, dim, dim, 1.0, C, P_estimated, 0.0, K_k);
            MatrixMultiplication('N', 'T', num_ma, dim, num_ma, 1.0, K_k, C, 1.0, denominator);

            /* Store Pyy to matrix2 since cholDecomposition2dim modifies it */
            for (i = 0; i < num_ma; i++)
                memcpy( &(matrix2[i][0]), &(denominator[i][0]), num_ma * sizeof(double));

            /* Compute K_k^T = Pxy * Pyy^-(1) */ 
            if ( cholDecomposition2dim(num_ma, dim, denominator, K_k) != 0){ 
                counters[1] = counters[1] + 1;
                #if (defined(MATLAB_MEX_FILE) && MATLAB_MEX_FILE == 1)          
                    printf("SDOF KF: A Cholesky Decomposition failed @ t=%.4f\n", t);
                #endif
                /* Pass a priori estimated states to simulink */
                #if (defined(OUTPUTSNV) && OUTPUTSNV == 1 )/* Only coordinate and velocity */
                    for (i = 0; i < numout; i++){
                        xest[i] = xhat[i*DOF]; /* coordinates */
                        vest[i] = xhat[i*DOF + 1]; /* velocities */
                    }
                #else /* Entire state vector is passed to first output */
                    for (i = 0; i < dim; i++)
                        systemstates[i] = xhat[i];
                #endif 
                        
                #if ( defined(OUTPUTKGAINS) && OUTPUTKGAINS == 1 )
                    for (j = 0; j < (num_ma*dim); j++) /* Kalman Gain Matrix */
                        kalmangains[j] = 0.0;
                #else
                    kalmangains[0] = 0.0;     
                #endif        
        
                return; /* Exit computation at this time step */
            }

            /* Compute a posteriori ErrorCovariance --> P_estimated = P_estimated - (K_K * Pyy) * K_k^T */ 
            MatrixMultiplication('T', 'N', dim, num_ma, num_ma, 1.0, K_k, matrix2, 0.0, matrix3);
            MatrixMultiplication('N', 'N', dim, num_ma, dim, -1.0, matrix3, K_k, 1.0, P_estimated);

            for (i = 0; i < dim; i++){ /* Store a posteriori covariance */
                memcpy( &(errorUpdate[i][0]), &(P_estimated[i][0]), dim * sizeof(double));
                errorUpdate[i][i] = errorUpdate[i][i] + (double) DELTA;
            }
        #endif
        
        /***************************************************************/
        /* Update A priori states with Measurements using Kalman Gains */
        
        /* Compute Measurement Vector h = [y; d] */
        clearVector(num_ma, ymeas);
        for (i = 0; i < numout; i++)
            ymeas[i] = *measurementPtrs[i];
        
        /* Compute Innovation = Difference between h_sim und h_measured */
        for (i = 0; i < num_ma; i++)
            measurementDiff[i] = ymeas[i] - yhat[i];  
        
        #if (defined(OUTLIER_REJECTION) && (OUTLIER_REJECTION == 1))
            sdofOutlierRejection(S, tstep, dim, numout, xhat, ymeas, matrix4, measurementDiff, dlgParam);
        #elif (defined(OUTLIER_REJECTION) && (OUTLIER_REJECTION == 2))
            sdofOutlierRejection(S, tstep, dim, numout, xhat, ymeas, matrix2, measurementDiff, dlgParam);
        #endif
        
        /* Update xhat with measurement --> xhat = xhat + K_k*(y - yhat) */
        MatrixVectorMultiplication('T', dim, num_ma, 1.0, K_k, measurementDiff, 1.0, xhat);
       
        
        /****************************************************************/  
        /* FORMAT S-FUNCTION OUTPUTS */
                    
        /* Passing estimated and corrected states to simulink */
        #if ( defined(OUTPUTSNV) && OUTPUTSNV == 1 )
            for (i = 0; i < numout; i++){
                xest[i] = xhat[i*DOF]; /* coordinates */
                vest[i] = xhat[i*DOF + 1]; /* velocities */
            }
        #else /* Entire state vector is passed to first output */
            for (i = 0; i < dim; i++)
                systemstates[i] = xhat[i];
        #endif   
                
        #if ( defined(OUTPUTKGAINS) && OUTPUTKGAINS == 1 )
            for (j = 0; j < num_ma; j++){ /* Passing Kalman Gains columnwise to simulink */
                for (i = 0; i < dim; i++)    
                    kalmangains[j*dim + i] = K_k[j][i]; /* K_k = Transposed Kalman Matrix */
            } 
        #else
            kalmangains[0] = 0.0;     
        #endif                  
    }
}

/* TERMINATE */
#define MDL_TERMINATE
static void mdlTerminate(SimStruct *S) {
    
    int numout  = 0;
    int dim     = 0;
    int num_ma  = 0;
    
    /* POINTER DECLARATION */
    double **P_estimated =      ssGetPWorkValue(S, 0); /* Estimated error covariance matrix */
    double **errorUpdate =      ssGetPWorkValue(S, 1); /* Corrected error covariance matrix */
    double **K_k =              ssGetPWorkValue(S, 2); /* Transposed Kalman-Gain-Matrix */
    double **denominator =      ssGetPWorkValue(S, 3); /* Sym. pos. def. denominator matrix*/
    double *ymeas  =            ssGetPWorkValue(S, 4);
    double **matrix1 =          ssGetPWorkValue(S, 5);
    double **matrix2 =          ssGetPWorkValue(S, 6);
    double **matrix3 =          ssGetPWorkValue(S, 7);
    double **matrix4 =          ssGetPWorkValue(S, 8);
    double *xhat =              ssGetPWorkValue(S, 9); /* Simulated system states */
    double *yhat =              ssGetPWorkValue(S, 10); /* Simulated system outputs */
    double *measurementDiff =   ssGetPWorkValue(S, 11); /* Difference between simulated & measured outputs */    
    double *xbak =              ssGetPWorkValue(S, 12); 
    double **Phi =              ssGetPWorkValue(S, 13); 
    double **C =                ssGetPWorkValue(S, 14); 
    double *abar =              ssGetPWorkValue(S, 15);
    double **Qconst =           ssGetPWorkValue(S, 16);
    #if (defined(RVONTHEFLY) && RVONTHEFLY == 1)
        double *Rvotf =             ssGetPWorkValue(S, 17); /* Memory for the Adaptive Noise Variance */
    #endif
    double *tmin1 =             ssGetPWorkValue(S, 18); /* Memory for Time of Previous Time Step */
    double **dlgParam =         ssGetPWorkValue(S, 19); /* Pointer to Dialog Parameters */
    
    /* DETERMINATION OF SIGNAL DIMENSIONS */
    #if defined(MATLAB_MEX_FILE) && MATLAB_MEX_FILE
        numout = (int) ssGetInputPortWidth(S, 0);  
        dim = ((int) DOF) * numout;
        num_ma = numout + (int) NUM_CON;
    #else
        numout = (int) NUMOUT;
        dim = (int) DIM;
        num_ma = (int) NUM_MA;
    #endif
   
    /* FREE ALLOCATED MEMORY */
    freeMatrix(dim, dim, P_estimated);
    freeMatrix(dim, dim, errorUpdate);
    freeMatrix(num_ma, dim, K_k);
    freeMatrix(num_ma, num_ma, denominator); 
    freeMatrix(dim, dim, matrix1);
    freeMatrix(num_ma, num_ma, matrix2);
    freeMatrix(dim, num_ma, matrix3);
    freeMatrix(numout, numout, matrix4);
    free(ymeas);
    free(xhat);
    free(yhat);
    free(measurementDiff);
    free(xbak);
    freeMatrix(dim, dim, Phi);
    freeMatrix(num_ma, dim, C);
    free(abar);
    freeMatrix(dim, dim, Qconst);
    #if (defined(RVONTHEFLY) && RVONTHEFLY)
        free(Rvotf);
    #endif
    free(tmin1);
    #if (NPARAMS > 0)
        free(dlgParam);
    #endif
}

#ifdef  MATLAB_MEX_FILE 
#include "simulink.c" 
#else 
#include "cg_sfun.h" 
#endif 
