/* This file contains additional math operations. The function
 * declarations are contained in the header file "neweul.h".
 * Please include this header file in codes that use these functions.
 *
 * most recent amendment: 2014-04-22
 *
 * Version 0.9
 *
 *  Change log:
 *
 *  Neweul-M2
 *  Multibody systems based on symbolic Newton-Euler-Equations
 *  Copyright (c) ITM University of Stuttgart, www.itm.uni-stuttgart.de
 */

#include "neweul.h"

short separateMatrices(unsigned short m, unsigned short n, double *beta, double **A, double **Q);

/* FUNCTION DEFINITIONS*/
short cholesky(unsigned short n, double **A) {

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;
    double      s = 0.0;

    /* Cholesky decomposition */

    for (i = 0; i < n; i++) {
        for (j = 0; j <= i; j++) {
            summe = 0.0;
            for (k = 0; k <= (j - 1); k++) {
                summe = summe + A[i][k] * A[j][k];
            }
            s = A[i][j] - summe;
            if (i == j) {
                if (s <= EPS)
                    return 129;
                else
                    A[i][i] = sqrt(s);
            } else {
                A[i][j] = s / A[j][j];
                A[j][i] = 0.0;
            }
        }
    }

    return 0;
}

short LDLT(unsigned short n, double **A) {

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;
    double      s = 0.0;

    /* LDL^T decomposition */

    for (i = 0; i < n; i++) {
        for (j = 0; j <= i; j++) {
            summe = 0.0;
            for (k = 0; k <= (j - 1); k++) {
                summe = summe + A[i][k] * A[j][k] * A[k][k];
            }
            s = A[i][j] - summe;
            if (i == j) {
                if (s <= EPS)
                    return 129;
                else
                    A[i][i] = s;
            } else {
                A[i][j] = s / A[j][j];
                A[j][i] = 0.0;
            }
        }
    }

    return 0;
}

short cholDecomposition(unsigned short n, double **A, double *b) {

    if (cholesky(n, A)!=0)
        return 129;

    LLTForBack(n, A, b);

    return 0;
}

short cholDecomposition2dim(unsigned short n, unsigned short nrhs, double **A, double **B) {

	int             i;
    int             j;
    int             k;
    double      summe;

    /* Cholesky decomposition */

    if (cholesky(n, A) != 0)
        return -1;

    /* Forward substitution */

    for (k = 0; k < nrhs; k++) {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (j = 0; j <= (i - 1); j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            B[i][k] = (1 / A[i][i]) * (B[i][k] - summe);
        }
    }

    /* Backward substitution */

    for (k = 0; k < nrhs; k++) {
        for (i = (n - 1); i >= 0; i--) {
            summe = 0.0;
            for (j = i + 1; j < n; j++) {
                summe = summe + A[j][i] * B[j][k];
            }
            B[i][k] = (1 / A[i][i]) * (B[i][k] - summe);
        }
    }

    return 0;
}

short LDLTDecomposition2dim(unsigned short n, unsigned short nrhs, double **A, double **B) {

    /* Cholesky decomposition */

    if (LDLT(n, A) != 0)
        return -1;

    LDLTForBack2dim(n, nrhs,  A, B);

    return 0;
}

short LDLTDecomposition(unsigned short n, double **A, double *b) {

    /* Cholesky decomposition */

    if (LDLT(n, A) != 0)
        return -1;

    LDLTForBack(n, A, b);

    return 0;
}

short solveLES(unsigned short n, double **A, double *b) {
    
    unsigned short i_, j_, k_;
    double *beta = calloc( (size_t) n, sizeof(double));

    double *v = calloc( (size_t) n, sizeof(double));
    double *a = calloc( (size_t) n, sizeof(double));
    
    short err_;
    
    if ((((v == NULL) || (a == NULL)) || (beta ==NULL))) {
        if (v != NULL)
            free(v);
        if (a != NULL)
            free(a);
        if (beta != NULL)
            free(beta);
        return -1;
    }

    for (j_=0; j_<n-1; j_++) {

        /* PART 1: [v, beta] = house(A(j:n,j)) */
        for (i_=j_; i_<n; i_++)
            a[i_] = A[i_][j_];

        house(n-j_, (const double *) &(a[j_]), &(v[j_]), &(beta[j_]));
	
        /* PART 2: A(j:n,j:n) = (I_{n-j+1}-beta*v*v^T)*A(j:n,j:n) */
        memcpy(&(a[j_]),&(A[j_][j_]),(n-j_)*sizeof(double));
        for (i_=j_+1; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                a[k_] += v[i_]*A[i_][k_];
            }
        }

        for (i_=j_; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                A[i_][k_] -= beta[j_]*v[i_]*a[k_];
            }
        }
	
        /* Apply Householder to right-hand size */
        a[0]=b[j_];
        for (i_=j_+1; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            a[0] += v[i_]*b[i_];
        }

        for (i_=j_; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            b[i_] -= beta[j_]*v[i_]*a[0];
        }

        /* PART 3:  */

        if (j_<(n-1)) {
            for (i_=j_+1; i_<n; i_++) {
                A[i_][j_] = v[i_];
            }
        }
    }

    /* Backward substitution */
    err_ = backSub(n, A, b);
    
    free(v);
    free(beta);
    free(a);

    return err_;
  

}

short solveLES2dim(unsigned short n, unsigned short nrhs, double **A, double **B){
  
    unsigned short i_, j_, k_;
    double *beta = calloc( (size_t) n, sizeof(double));

    double *v = calloc( (size_t) n, sizeof(double));
    double *a = NULL;
    short err_;
    
    if (nrhs>n)
        a = calloc( (size_t) nrhs, sizeof(double));
    else
        a = calloc( (size_t) n, sizeof(double));

    if (((v == NULL) || (a == NULL)) || (beta ==NULL)) {
        if (v != NULL)
            free(v);
        if (a != NULL)
            free(a);
        if (beta != NULL)
            free(beta);
        return -1;
    }

    for (j_=0; j_<n-1; j_++) {

        /* PART 1: [v, beta] = house(A(j:n,j)) */
        for (i_=j_; i_<n; i_++)
            a[i_] = A[i_][j_];

        house(n-j_, (const double *) &(a[j_]), &(v[j_]), &(beta[j_]));

        /* PART 2: A(j:n,j:n) = (I_{n-j+1}-beta*v*v^T)*A(j:n,j:n) */
        memcpy(&(a[j_]),&(A[j_][j_]),(n-j_)*sizeof(double));
        for (i_=j_+1; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                a[k_] += v[i_]*A[i_][k_];
            }
        }

        for (i_=j_; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                A[i_][k_] -= beta[j_]*v[i_]*a[k_];
            }
        }

        /* Apply Householder to right-hand size */
        memcpy(a,B[j_],nrhs*sizeof(double));
        for (i_=j_+1; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=0; k_<nrhs; k_++){
                a[k_] += v[i_]*B[i_][k_];
            }
        }

        for (i_=j_; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=0; k_<nrhs; k_++){
                B[i_][k_] -= beta[j_]*v[i_]*a[k_];
            }
        }

        /* PART 3:  */
        if (j_<(n-1)) {
            for (i_=j_+1; i_<n; i_++) {
                A[i_][j_] = v[i_];
            }
        }
        
    }

    /* Backward substitution */
    err_ = backSub2dim(n, nrhs, A, B);

    free(v);
    free(beta);
    free(a);

    return err_;
  
}

void MatrixMultiplication(char nt, char nt2, unsigned short n, unsigned short m, unsigned short l, double alpha,  double **A,
                          double **B, double beta, double **C) {

    /* DGEMM
     * C = alpha*A*B + beta*C
     * A[n][m], B[m][l], C[n][l]  */

    double  summe = 0.0;
    unsigned short  i;
    unsigned short  j;
    unsigned short  k;

    if (nt == 'N') {

        if (nt2 == 'N') {
	  
	    for (i=0; i< n; i++){
            double *avec = A[i];
            double *temp = C[i];
            for (j = 0; j < l; j++)
                temp[j] = beta*temp[j];
            for (k = 0; k<m; k++) {
                double a =  alpha*avec[k];
                double *b = B[k];
                for (j = 0; j < l; j++)
                    temp[j] += a*b[j];
            }
	    }
	  
	    /* Slow version */
            /*for (i = 0; i < n; i++) {
                for (j = 0; j < l; j++) {
                    summe = 0.0;
                    for (k = 0; k < m; k++)
                        summe = summe + A[i][k] * B[k][j];
                    C[i][j] = beta*C[i][j] + alpha*summe;
                }
            }*/
        }

        else if (nt2 == 'T') {

            for (i = 0; i < n; i++) {
                for (j = 0; j < l; j++) {
                    summe = 0.0;
                    for (k = 0; k < m; k++)
                        summe = summe + A[i][k] * B[j][k];
                    C[i][j] = beta*C[i][j] + alpha*summe;
                }
            }
        }
    }

    else if (nt == 'T') {

        if (nt2 == 'N') {

            for (i=0; i< n; i++){
                double *temp = C[i];
                for (j = 0; j < l; j++){
                    temp[j] = beta*temp[j];
                }
                for (k = 0; k<m; k++) {
                    double *avec = A[k];
                    double a =  alpha*avec[i];
                    double *b = B[k];
                    for (j = 0; j < l; j++){
                        temp[j] += a*b[j];
                    }
                }
            }
	    
            /*for (i = 0; i < n; i++) {
                for (j = 0; j < l; j++) {
                    summe = 0.0;
                    for (k = 0; k < m; k++)
                        summe = summe + A[k][i] * B[k][j];
                    C[i][j] = beta*C[i][j] + alpha*summe;
                }
            }*/
        }

        else if (nt2 == 'T') {
  
            for (i = 0; i < n; i++) {
                for (j = 0; j < l; j++) {
                    summe = 0.0;
                    for (k = 0; k < m; k++)
                        summe = summe + A[k][i] * B[j][k];
                    C[i][j] = beta*C[i][j] + alpha*summe;
                }
            }
        }

    }
}

void MatrixMultiplicationMod(char nt, char nt2, unsigned short n, unsigned short m, unsigned short l,
                             double alpha,  double **A, double **B,
                             double beta, double **C, double **D) {

    /* DGEMM
     * D = alpha*A*B + beta*C
     * A[n][m], B[m][l], C[n][l], D[n][l]  */

    double  summe = 0.0;
    unsigned short         i;
    unsigned short         j;
    unsigned short         k;

    if (nt == 'N') {

        if (nt2 == 'N') {
            
            for (i=0; i< n; i++){
                double *avec = A[i];
                double *temp = D[i];
                double *tempC = C[i];
                for (j = 0; j < l; j++){
                    temp[j] = beta*tempC[j];
                }
                for (k = 0; k<m; k++) {
                    double a =  alpha*avec[k];
                    double *b = B[k];
                    for (j = 0; j < l; j++){
                        temp[j] += a*b[j];
                    }
                }
            }
        }

        else if (nt2 == 'T') {

            for (i = 0; i < n; i++) {
                for (j = 0; j < l; j++) {
                    summe = 0.0;
                    for (k = 0; k < m; k++)
                        summe = summe + A[i][k] * B[j][k];
                    D[i][j] = beta*C[i][j] + alpha*summe;
                }
            }
        }
    }

    else if (nt == 'T') {

        if (nt2 == 'N') {

            for (i=0; i< n; i++){
                double *tempC = C[i];
                double *temp = D[i];
                for (j = 0; j < l; j++){
                    temp[j] = beta*tempC[j];
                }
                for (k = 0; k<m; k++) {
                    double *avec = A[k];
                    double a =  alpha*avec[i];
                    double *b = B[k];
                    for (j = 0; j < l; j++){
                        temp[j] += a*b[j];
                    }
                }
            }
        }

        else if (nt2 == 'T') {

            for (i = 0; i < n; i++) {
                for (j = 0; j < l; j++) {
                    summe = 0.0;
                    for (k = 0; k < m; k++)
                        summe = summe + A[k][i] * B[j][k];
                    D[i][j] = beta*C[i][j] + alpha*summe;
                }
            }
        }
    }
}

void MatrixVectorMultiplication(char nt, unsigned short n, unsigned short m, double alpha,  double **A,
                                double *b, double beta, double *c) {

    /*  DEGMV
     *  c = alpha*A*b + beta*c
     *  A[n][m], b[m], c[n]  */

    double  summe = 0.0;
    unsigned short         i,k;

    if (nt == 'N') {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (k = 0; k < m; k++)
                summe = summe + A[i][k] * b[k];
            c[i] = beta*c[i] + alpha * summe;
        }
    } else if (nt == 'T') {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (k = 0; k < m; k++)
                summe = summe + A[k][i] * b[k];
            c[i] = beta * c[i] + alpha * summe;
        }
    }
}

void MatrixVectorMultiplicationMod(char nt, unsigned short n, unsigned short m, double alpha,  double **A,
                                   double *b, double beta, double *c, double *d) {

    /*  DEGMV
     *  d = alpha*A*b + beta*c
     *  A[n][m], b[m], c[n], d[n]  */

    double  summe = 0.0;
    unsigned short         i,k;

    if (nt == 'N') {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (k = 0; k < m; k++)
                summe = summe + A[i][k] * b[k];
            d[i] = beta*c[i] + alpha * summe;
        }
    } else if (nt == 'T') {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (k = 0; k < m; k++)
                summe = summe + A[k][i] * b[k];
            d[i] = beta * c[i] + alpha * summe;
        }
    }
}

void VectorCrossProduct(double alpha,  double *r,  double *b, double beta, double *c) {

    /*  DEGMV
     *  c = alpha*r_tilde*b + beta*c
     *  r[3], b[3], c[3]  */

    c[0] = beta*c[0] - alpha*r[2]*b[1] + alpha*r[1]*b[2];
    c[1] = beta*c[1] + alpha*r[2]*b[0] - alpha*r[0]*b[2];
    c[2] = beta*c[2] - alpha*r[1]*b[0] + alpha*r[0]*b[1];

}

void MatrixCrossProduct(unsigned short n, double alpha,  double *r,  double **B, double beta, double **C) {

    /*  DEGMV
     *  C = alpha*r_tilde*B + beta*C
     *  r[3], B[3][n], C[3][n]  */

    unsigned short i_;

    for (i_=0; i_<n; i_++) {
        C[0][i_] = beta*C[0][i_] - alpha*r[2]*B[1][i_] + alpha*r[1]*B[2][i_];
        C[1][i_] = beta*C[1][i_] + alpha*r[2]*B[0][i_] - alpha*r[0]*B[2][i_];
        C[2][i_] = beta*C[2][i_] - alpha*r[1]*B[0][i_] + alpha*r[0]*B[1][i_];
    }
}

void transformVector(char nt,  double **S, double *b) {

    /*  DEGMV
     *  b = S*b
     *  S[3][3], b[3]  */

    double temp0;
    double temp1;
    double temp2;

    temp0 = b[0];
    temp1 = b[1];
    temp2 = b[2];

    if (nt == 'N') {

        b[0] = S[0][0]*temp0 + S[0][1]*temp1 + S[0][2]*temp2;
        b[1] = S[1][0]*temp0 + S[1][1]*temp1 + S[1][2]*temp2;
        b[2] = S[2][0]*temp0 + S[2][1]*temp1 + S[2][2]*temp2;

    } else {

        b[0] = S[0][0]*temp0 + S[1][0]*temp1 + S[2][0]*temp2;
        b[1] = S[0][1]*temp0 + S[1][1]*temp1 + S[2][1]*temp2;
        b[2] = S[0][2]*temp0 + S[1][2]*temp1 + S[2][2]*temp2;

    }

}

void transformMatrix(char nt, unsigned short n,  double **S, double **B) {

    /*  DEGMV
     *  B = S*B
     *  S[3][3], B[3][n]  */

    double temp0;
    double temp1;
    double temp2;

    unsigned short j_;

    if (nt == 'N') {

        for (j_=0; j_<n; j_++) {
            temp0 = B[0][j_];
            temp1 = B[1][j_];
            temp2 = B[2][j_];

            B[0][j_] = S[0][0]*temp0 + S[0][1]*temp1 + S[0][2]*temp2;
            B[1][j_] = S[1][0]*temp0 + S[1][1]*temp1 + S[1][2]*temp2;
            B[2][j_] = S[2][0]*temp0 + S[2][1]*temp1 + S[2][2]*temp2;
        }

    } else {

        for (j_=0; j_<n; j_++) {
            temp0 = B[0][j_];
            temp1 = B[1][j_];
            temp2 = B[2][j_];

            B[0][j_] = S[0][0]*temp0 + S[1][0]*temp1 + S[2][0]*temp2;
            B[1][j_] = S[0][1]*temp0 + S[1][1]*temp1 + S[2][1]*temp2;
            B[2][j_] = S[0][2]*temp0 + S[1][2]*temp1 + S[2][2]*temp2;
        }

    }

}

short GaussianElimination(unsigned short n, unsigned short *z, double **A) {

    unsigned short             i;
    unsigned short             j;
    unsigned short             s;
    double      l;
    double      piv=0.0;

    for (i = 0; i < n; i++)
        z[i] = i;

    for (s = 0; s < (n - 1); s++) {
        unsigned short p = s;
        if ((A != NULL) && (A[s] != NULL))
            piv = fabs(A[s][s]);
        else
            return -1;

        for (i = s + 1; i < n; i++) {

            if (fabs(A[i][s]) > piv) {
                p = i;
                piv = fabs(A[i][s]);
            }
        }
        if (piv <= EPS) {
            return 129;
        } else {
            if (p != s) {
                for (j = 0; j < n; j++) {
                    l = A[s][j];
                    A[s][j] = A[p][j];
                    A[p][j] = l;
                }
                j = z[s];
                z[s] = z[p];
                z[p] = j;
            }

            for (i = (s + 1); i < n; i++) {
                l = A[i][s] / A[s][s];
                A[i][s] = l;
                for (j = (s + 1); j < n; j++) {
                    A[i][j] = A[i][j] - l * A[s][j];
                }
            }
        }
    }
    return 0;
}

short InversionByLU(unsigned short n, unsigned short nrhs, unsigned short *z, double **A, double **B) {

    /* X = inv(A) * B */
    
    if ( GaussianElimination( n, z, A) !=0 )
        return 129;
    
    permuteMatrix( n, nrhs, z, B);

    if ( LUForBack( n, nrhs, A, B ) !=0 )
        return 129;
    
    return 0;
}

short LUForBack(unsigned short n, unsigned short nrhs, double **A, double **B) {
    /* Forward and backward substitution for LU-decomposed matrix A
     * Please keep in mind that B has to be permuted with "permuteMatrix"
     */

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;

    /* Forward substitution */

    for (i = 0; i < n; i++) {
        for (k = 0; k < nrhs; k++) {
            summe = 0.0;
            for (j = 0; j < i; j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            B[i][k] = (B[i][k] - summe);
        }
    }

    /* Backward substitution */

    for (i = (n - 1); i < n; --i) {
        for (k = 0; k < nrhs; k++) {
            summe = 0.0;
            for (j = i + 1; j < n; j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            if (fabs(A[i][i])>=EPS)
                B[i][k] = (B[i][k] - summe)/(A[i][i]);
            else
                return 129;
        }
    }

    return 0;
}

short InversionByLU1dim(unsigned short n, unsigned short *z, double **A, double *B) {

    /* X = inv(A) * B */

    if ( GaussianElimination(n, z, A)!=0 )
        return 129;

    permuteVector(n, z, B);

    if ( LUForBack_1dim( n, A, B )!=0 )
        return 129;

    return 0;
}

short LUForBack_1dim(unsigned short n, double **A, double *B) {
    /* Forward and backward substitution for LU-decomposed matrix A
     * Please keep in mind that B has to be permuted with "permuteMatrix"
     */

    unsigned short             i;
    unsigned short             j;

    double      summe;

    /* Forward substitution */

    for (i = 0; i < n; i++) {
        summe = 0.0;
        for (j = 0; j < i; j++) {
            summe = summe + A[i][j] * B[j];
        }
        B[i] = (B[i] - summe);
    }


    /* Backward substitution */

    for (i = (n - 1); i < n; --i) {
        summe = 0.0;
        for (j = i + 1; j < n; j++) {
            summe = summe + A[i][j] * B[j];
        }
        if (fabs(A[i][i])>=EPS)
            B[i] = (1 / A[i][i]) * (B[i] - summe);
        else
            return 129;
    }

    return 0;

}

void squareMatrixTranspose(unsigned short n, double **A) {

    unsigned short i, j;
    double temp;

    for (i = 0; i < n; i++) {
        for (j = 0; j < i; j++) {
            temp = A[i][j];
            A[i][j] = A[j][i];
            A[j][i] = temp;
        }
    }
}

void permuteMatrix(unsigned short n, unsigned short nrhs,  unsigned short *z, double **A) {
  
    double      **temp = NULL;
    unsigned short         i;

    if (0==nrhs)
      return;
    
    if ( 0!=n )
        temp = (double **) calloc ( ( size_t ) n, sizeof ( double * ) );
        
    if (temp ==NULL)
        return;
      
    
    for (i = 0; i < n; i++)
        temp[i] = A[i];
    for (i = 0; i < n; i++)
        A[i] = temp[z[i]];

    free(temp);

}

void permuteVector(unsigned short n,  unsigned short *z, double *A) {

    double      *temp = NULL;
    unsigned short         i;

    if (n == 0)
        return;

    temp = calloc( (size_t) n, sizeof(double));
    
    for (i = 0; i < n; i++) {
        temp[i] = A[i];
    }
    for (i = 0; i < n; i++) {
        A[i] = temp[z[i]];
    }

    free(temp);

}

short forwardSub2dim(unsigned short n, unsigned short nrhs,  double **A, double **B) {

    /* Forward substitution. Solves equations of the type L*X = B
     * The matrix A is lower triangular matrix
     */

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;

    for (k = 0; k < nrhs; k++) {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (j = 0; j <= (i - 1); j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            if (fabs(A[i][i]) >= EPS)
                B[i][k] = (1 / A[i][i]) * (B[i][k] - summe);
            else
                return 129;
        }
    }

    return 0;

}

short forwardSubNT(char nt, unsigned short n,  double **A, double *B) {

    /* Solve equations of type L*x = b (nt='N') and L^T*x = b (nt='T') */

    unsigned short i, j;
    double summe;

    /* Forward substitution */
    if (nt == 'N') { /* Normal matrices */

        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (j = 0; j <= (i - 1); j++) {
                summe = summe + A[i][j] * B[j];
            }
            if (fabs(A[i][i])>= EPS)
                B[i] = (1 / A[i][i]) * (B[i] - summe);
            else
                return 129;
        }

    } else if (nt == 'T') { /*Transposed Matrices */

        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (j = 0; j <= (i - 1); j++) {
                summe = summe + A[j][i] * B[j];
            }
            if (fabs(A[i][i])>= EPS)
                B[i] = (1 / A[i][i]) * (B[i] - summe);
            else
                return 129;
        }

    }

    return 0;

}

short backSub2dim(unsigned short n, unsigned short nrhs,  double **A, double **B) {

    /* Backward substitution. Solves equations of the type A*X = B
     * The matrix A is upper triangular matrix
     */

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;

    for (k = 0; k < nrhs; k++) {
        for (i = (n - 1); i < n; --i) {
            summe = 0.0;
            for (j = i + 1; j < n; j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            if (fabs(A[i][i])>=EPS)
                B[i][k] = (1 / A[i][i]) * (B[i][k] - summe);
            else
                return 129;
        }
    }

    return 0;
}

short forwardSub2dimTransposed(unsigned short n, unsigned short nrhs,  double **A, double **B) {

    /* Forward substitution. Solves equations of the type A*X = B
     * The transposed matrix A is lower triangular matrix
     */

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;

    for (k = 0; k < nrhs; k++) {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (j = 0; j <= (i - 1); j++) {
                summe = summe + A[j][i] * B[j][k];
            }
            if (fabs(A[i][i]) >= EPS)
                B[i][k] = (1 / A[i][i]) * (B[i][k] - summe);
            else
                return 129;
        }
    }

    return 0;

}

short backSub2dimTransposed(unsigned short n, unsigned short nrhs,  double **A, double **B) {

    /* Backward substitution. Solves equations of the type A*X = B
     * The transposed matrix A is upper triangular matrix
     */

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;

    for (i = (n - 1); i < n; --i) {
	for (k = 0; k < nrhs; k++) {
            summe = 0.0;
            for (j = i + 1; j < n; j++) {
                summe = summe + A[j][i] * B[j][k];
            }
            if (fabs(A[i][i])>=EPS)
                B[i][k] = (1 / A[i][i]) * (B[i][k] - summe);
            else
                return 129;
        }
    }

    return 0;
}

void LLTForBack2dim(unsigned short n, unsigned short nrhs,  double **A, double **B) {

    /* Forward and backward substitutions for the cholesky decomposition
     * PLEASE KEEP IN MIND THAT THERE IS NO R-MATRIX, JUST A TRANSPOSED L-MATRIX!  */

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;
    double      Ainv;

    for (i = 0; i < n; i++) {
	Ainv = 1.0/(A[i][i]);
	for (k = 0; k < nrhs; k++) {
            summe = 0.0;
            for (j = 0; j <= (i - 1); j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            B[i][k] = Ainv*(B[i][k] - summe);
        }
    }

    for (i = (n - 1); i < n; --i) {
	Ainv = 1.0/(A[i][i]);
	for (k = 0; k < nrhs; k++) {
            summe = 0.0;
            for (j = i + 1; j < n; j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            B[i][k] = Ainv*(B[i][k] - summe);
        }
    }
}

void LDLTForBack2dim(unsigned short n, unsigned short nrhs,  double **A, double **B) {

    /* Forward and backward substitutions for the cholesky decomposition
     * PLEASE KEEP IN MIND THAT THERE IS NO R-MATRIX, JUST A TRANSPOSED L-MATRIX!  */

    unsigned short             i;
    unsigned short             j;
    unsigned short             k;
    double      summe;


    for (k = 0; k < nrhs; k++) {
        for (i = 0; i < n; i++) {
            summe = 0.0;
            for (j = 0; j <= (i - 1); j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            B[i][k] = (B[i][k] - summe);
        }
    }

    for (i = 0; i < n; i++) {
        summe = 1.0/A[i][i];
        for (j = 0; j < nrhs; j++) {
            B[i][j] = summe*B[i][j];
        }
    }
    
    
    for (k = 0; k < nrhs; k++) {
        for (i = (n - 1); i < n; --i) {
            summe = 0.0;
            for (j = i + 1; j < n; j++) {
                summe = summe + A[i][j] * B[j][k];
            }
            B[i][k] = (B[i][k] - summe);
        }
    }
}

void LDLTForBack(unsigned short n, double **A, double *b) {

    /* Forward and backward substitutions for the cholesky decomposition
     * PLEASE KEEP IN MIND THAT THERE IS NO R-MATRIX, JUST A TRANSPOSED L-MATRIX!  */

    unsigned short             i;
    unsigned short             j;

    double      summe;


    for (i = 0; i < n; i++) {
        summe = 0.0;
        for (j = 0; j <= (i - 1); j++) {
            summe = summe + A[i][j] * b[j];
        }
        b[i] = (b[i] - summe);
    }

    for (i = 0; i < n; i++) {
        b[i] = b[i]/A[i][i];
    }
    
    for (i = (n - 1); i < n; --i) {
        summe = 0.0;
        for (j = i + 1; j < n; j++) {
            summe = summe + A[i][j] * b[j];
        }
        b[i] = (b[i] - summe);
    }

}

short forwardSub(unsigned short n,  double **A, double *B) {

    /* Solve equations of the type A*x = b
     * The matrix A is a lower triangular matrix
     */

    unsigned short i, j;

    /* Forward substitution */

    for (i = 0; i < n; i++) {
        double summe = 0.0;
        for (j = 0; j <= (i - 1); j++) {
            summe = summe + A[i][j] * B[j];
        }
        if (fabs(A[i][i])>= EPS)
            B[i] = (1 / A[i][i]) * (B[i] - summe);
        else
            return 129;
    }

    return 0;

}

short backSub(unsigned short n,  double **A, double *B) {

    /* Backward substitution. Solves equations of the type A*x = b
     * * The matrix A is a upper triangular matrix
     */

    unsigned short i, j;

    for (i = (n - 1); i < n; --i) {
        double summe = 0.0;
        for (j = i + 1; j < n; j++) {
            summe = summe + A[i][j] * B[j];
        }
        if (fabs(A[i][i])>= EPS)
            B[i] = (1 / A[i][i]) * (B[i] - summe);
        else
            return 129;
    }

    return 0;

}

void LLTForBack(unsigned short n,  double **A, double *B) {

    /* Forward and backward substitutions for the cholesky decomposition
     * PLEASE KEEP IN MIND THAT THERE IS NO R-MATRIX, JUST A TRANSPOSED L-MATRIX!  */

    unsigned short i, j;
    double summe;


    for (i = 0; i < n; i++) {
        summe = 0.0;
        for (j = 0; j <= (i - 1); j++) {
            summe = summe + A[i][j] * B[j];
        }
        B[i] = (1 / A[i][i]) * (B[i] - summe);
    }

    for (i = (n - 1); i < n; --i) {
        summe = 0.0;
        for (j = i + 1; j < n; j++) {
            summe = summe + A[j][i] * B[j];
        }
        B[i] = (1 / A[i][i]) * (B[i] - summe);
    }

}

double infinityNorm(unsigned short n, unsigned short m,  double **A) {

    double      infnorm = 0.0;
    unsigned short             i;
    unsigned short             j;

    for (i = 0; i < n; i++) {
        double summe = 0.0;
        for (j = 0; j < m; j++) {
            summe = summe + fabs(A[i][j]);
        }
        if (summe > infnorm)
            infnorm = summe;
    }
    return infnorm;
}

double maxDouble(double a, double b) {

    if (a > b)
        return a;
    else if (a < b)
        return b;
    else
        return a;

}

double minDouble(double a, double b) {

    if (a >= b)
        return b;
    else
        return a;
}

double signum(double a) {

    if (a<0.0)
        return -1.0;
    else
        return 1.0;

}

void clearMatrix(unsigned short n, unsigned short nrhs, double **A) {

    unsigned short     i;

    for (i=0; i<n; i++) {
        memset(A[i],0,nrhs*sizeof(double));
    }
}

void clearVector(unsigned short n, double *x) {

    memset(x,0,n*sizeof(double));
}

short inversion2x2(double **A) {
  
    double          a = A[0][0];
    double          b = A[0][1];
    double          c = A[1][0];
    double          d = A[1][1];
    double          det = (a*d - b*c);
    
    if (fabs(det) < EPS)
        return 129;
    else {
        double detInv = 1.0/det;
        A[0][0] =  d*detInv;
        A[0][1] = -b*detInv;
        A[0][0] = -c*detInv;
        A[0][0] =  a*detInv;

        return 0;
    }

}

void dyadicProduct(unsigned short dim, double *vector, double **matrix) {

    unsigned short     i;
    unsigned short     j;

    for (i = 0; i < dim; i++) {
        for (j = 0; j < dim; j++) {
            matrix[i][j] = vector[i] * vector[j];
        }
    }

}

void dyadicProduct2(unsigned short n, unsigned short m, double *vector1, double *vector2, double **matrix) {

    unsigned short     i;
    unsigned short     j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < m; j++) {
            matrix[i][j] = vector1[i] * vector2[j];
        }
    }

}

void house(unsigned short n, const double *x, double *v, double *beta) {

    /* n = length(x) */
    double sigma = 0.0;
    unsigned short    i_=0;

    for (i_=1; i_<n; i_++) {
        sigma = sigma + x[i_]*x[i_];
    }

    v[0] = 1.0;
    for (i_=1; i_<n; i_++) {
        v[i_] = x[i_];
    }

    if ((sigma == 0.0 ) && (x[0] >= 0.0)) {
        *beta = 0.0;
    } else if ((sigma == 0.0 ) && (x[0] < 0.0)) {
        *beta = -2.0;
    } else {
        double mu = sqrt((x[0]*x[0] + sigma));
        double v0inv;
        if (x[0] <= 0)
            v[0] = x[0] - mu;
        else
            v[0] = (-sigma)/(x[0] + mu);

        *beta = (2.0*v[0]*v[0])/(sigma + v[0]*v[0]);
	
        v0inv = 1.0/v[0];
        
        for (i_ = (n - 1); i_ < n; --i_)
            v[i_] = v0inv*v[i_];
    }
}

short householderBidiag(unsigned short m, unsigned short n, double **A, double **U, double **V) {
    /* Decomposition of a m x n matrix A with m>=n s.t.
     * B = U^T * A * V
     * B replaces A
     *
     * Algorithm taken from : Golub, van Loan: Matrix Computations, Alg 5.4.2
     *
     */

    unsigned short i_, j_, k_, l_;
    double *beta = calloc( (size_t) n, sizeof(double));
    double *gamma = calloc( (size_t) n, sizeof(double));
    double sum;
    double *v = calloc( (size_t) m, sizeof(double));
    double *a = calloc( (size_t) m, sizeof(double));
    double **B = callocMatrix(m,m);

    (void) U;
    (void) V;

    for (j_ = 0; j_ < n; j_++ ) {
        /* PART 1: house(unsigned short n, double *x, double *v, double *beta); */
        for (i_=j_; i_<m; i_++)
            a[i_] = A[i_][j_];

        house(m-j_, (const double *) &(a[j_]), &(v[j_]), &(beta[j_]));

        /* PART 2: A(j:m,j:n) = (I_{m-j+1}-beta*v*v^T)*A(j:m,j:n) */
        memcpy(&(a[j_]),&(A[j_][j_]),(n-j_)*sizeof(double));
	
        for (i_=j_+1; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                a[k_] += v[i_]*A[i_][k_];
            }
        }
	
        for (i_=j_; i_<n; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                A[i_][k_] -= beta[j_]*v[i_]*a[k_];
            }
        }

        /* PART 3: res(j+1:m,j) = v(j+1:m); */
        for (i_=j_+1; i_<m; i_++) {
            A[i_][j_] = v[i_];
        }

        if (j_ < (n-2)) {

            /* PART 4: [v,gamma(j)] = house(res(j,j+1:n)'); */
            for (i_=j_+1; i_<n; i_++)
                a[i_] = A[j_][i_];

            house(n-(j_+1), (const double *) &(a[j_+1]), &(v[j_+1]), &(gamma[j_]));

            /* PART 5: A(j:m,j+1:n) = A(j:m,j+1:n)*(eye(n-j)-gamma(j)*(v*v')); */
            for (i_=j_; i_<m; i_++) {
                for (k_=j_+1; k_<n; k_++) {
                    sum = 0.0;
                    for (l_=j_+1; l_<n; l_++) {
                        if (i_!=l_)
                            sum = sum - A[i_][l_]*beta[j_]*v[l_]*v[k_];
                        else
                            sum = sum + (1.0 - A[i_][l_]*beta[j_]*v[l_]*v[k_]);
                    }
                    B[i_][k_] = sum;
                }
            }

            for (i_=j_; i_<m; i_++) {
                for (k_=j_; k_<n; k_++) {
                    A[i_][k_] = B[i_][k_];
                }
            }

            /* PART 6: A(j,j+2:n) = v(j+2:n)'; */
            for (i_=j_+2; i_<n; i_++) {
                A[j_][i_] = v[i_];
            }

        }

    }

    freeMatrix(m,m,B);

    free(v);
    free(beta);
    free(gamma);
    free(a);

    return 0;
}

short separateMatrices(unsigned short m, unsigned short n, double *beta, double **A, double **Q) {

    unsigned short i_,j_,k_;
    double *v = calloc((size_t) m, sizeof(double));
    double *a = calloc((size_t) m, sizeof(double));

    /* Initialize Q=I_n */

    if ((v == NULL) || (a == NULL) ) {
        if (v != NULL)
            free(v);
	if (a != NULL)
            free(a);
        return -1;
    }

    for (i_=0; i_<m; i_++)
        Q[i_][i_] = 1.0;

    for (j_ = (n - 1); j_ < n; --j_) {

        /* Initialize v(j:n)=[1; A(j+1:n,j) */
	
        v[j_] = 1.0;
        for (i_=j_+1; i_<m; i_++) {
            v[i_] = A[i_][j_];
            A[i_][j_] = 0.0;
        }
        
        for (i_=j_; i_<m; i_++)
            a[i_] = 0.0;
	
        for (i_=j_; i_<m; i_++){
            for (k_=j_; k_<m; k_++){
              a[k_] += v[i_]*Q[i_][k_];
            }
        }
	
        for (i_=j_; i_<m; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<m; k_++){
                Q[i_][k_] -= beta[j_]*v[i_]*a[k_];
            }
        }

    }

    free(v);
    free(a);

    return 0;

}

short householderQR(unsigned short m, unsigned short n, double **A, double **Q) {

    unsigned short i_, j_, k_;
    double *beta = calloc( (size_t) n, sizeof(double));

    double *v = calloc( (size_t) m, sizeof(double));
    double *a = NULL;
    
    short err_ = 0;

    if (m>n)
        a = calloc( (size_t) m, sizeof(double));
    else
        a = calloc( (size_t) n, sizeof(double));
    
    if (((v == NULL) || (a == NULL)) || beta ==NULL) {
        if (v != NULL)
            free(v);
        if (a != NULL)
            free(a);
        if (beta != NULL)
            free(beta);
        return -1;
    }

    for (j_=0; j_< (unsigned short) minDouble((double) n,(double) m-1); j_++) {


        /* PART 1: [v, beta] = house(A(j:m,j)) */
        for (i_=j_; i_<m; i_++)
            a[i_] = A[i_][j_];

        house(m-j_, (const double *) &(a[j_]), &(v[j_]), &(beta[j_]));

        /* PART 2: A(j:m,j:n) = (I_{m-j+1}-beta*v*v^T)*A(j:m,j:n) */
    	memcpy(&(a[j_]),&(A[j_][j_]),(n-j_)*sizeof(double));

        for (i_=j_+1; i_<m; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                a[k_] += v[i_]*A[i_][k_];
            }
        }
	
        for (i_=j_; i_<m; i_++){ /* v[j_] = 1.0, a = A[:][j_] */
            for (k_=j_; k_<n; k_++){
                A[i_][k_] -= beta[j_]*v[i_]*a[k_];
            }
        }
	
        /* PART 3:  */
        if (j_<(m-1)) {
            for (i_=j_+1; i_<m; i_++) {
                A[i_][j_] = v[i_];
            }
        }
    }
    
    /* Compute the matrices */
    err_ = separateMatrices(m, n, beta, A, Q);

    free(v);
    free(beta);
    free(a);

    return err_;

}

void givensrotation(double a, double b, double *c, double *s) {

    if (0.0 == b) {
        *c = 1.0;
        *s = 0.0;
    } else {
        double r;
        if (fabs(b) > fabs(a)) {
            r = (-a)/b;
            *s = 1.0/(sqrt(1.0 + r*r));
            *c = (*s)*r;
        } else {
            r = (-b)/a;
            *c = 1.0/(sqrt(1.0 + r*r));
            *s = (*c)*r;
        }
    }

}

short givensQR(unsigned short m, unsigned short n, double **A, double **Q) {
    /* A[m][n] = Q[m][m]*R[m][n]  R in A */

    unsigned short i_, j_, k_;

    double temp_1;
    double temp_2;

    double **c = callocMatrix(m,m);
    double **s = callocMatrix(m,m);;
    
    /* double c;
    double s; */

    clearMatrix(m, m, Q);
    
    for (i_=0; i_<m; i_++)
        Q[i_][i_] = 1.0;

    for (i_=1; i_<m; i_++){ /* i=2:m */
        for (j_=0; j_<i_; j_++) { /* j=1:i-1 */

            givensrotation(A[j_][j_], A[i_][j_], &(c[i_][j_]), &(s[i_][j_]));

            /* A[j i][j:n] = (c -s; s c)*A[j i][j:n] */
            for (k_=j_; k_<n; k_++){
                temp_1 = c[i_][j_]*A[j_][k_] - s[i_][j_]*A[i_][k_];
                temp_2 = s[i_][j_]*A[j_][k_] + c[i_][j_]*A[i_][k_];
                
                A[j_][k_] = temp_1;
                A[i_][k_] = temp_2;
            }
            
            /* Q[0:m][j i] = Q[0:m][j i]*(c s; -s c) */            
            /* slow version */
            /*for (k_=0; k_<m; k_++){
                temp_1 = c[i_][j_]*Q[k_][j_] - s[i_][j_]*Q[k_][i_];
                temp_2 = s[i_][j_]*Q[k_][j_] + c[i_][j_]*Q[k_][i_];
                
                Q[k_][j_] = temp_1;
                Q[k_][i_] = temp_2;
            }*/
        }
    }
    
    for (k_=0; k_<m; k_++){
        double *q = Q[k_];
        for (i_=1; i_<m; i_++){
            for (j_=0; j_<i_; j_++) { 
                temp_1 = c[i_][j_]*q[j_] - s[i_][j_]*q[i_];
                temp_2 = s[i_][j_]*q[j_] + c[i_][j_]*q[i_];

                q[j_] = temp_1;
                q[i_] = temp_2;
            }
        }
    }

    freeMatrix(m, m, c);
    freeMatrix(m, m, s);
    
    return 0;

}

double **callocMatrix( unsigned short m, unsigned short n) {

    unsigned short i_ = 0;
    double **A=NULL;

    if (( 0==m ) || ( 0==n ))
        return NULL;
    
    A = (double **) calloc( (size_t) m, sizeof(double *));
    
    if (A==NULL)
        return NULL;
    
    for ( i_=0 ; i_<m ; i_++ )
        A[i_] = (double *) calloc( (size_t) n, sizeof(double));

    return A;
}

double ** createRandomMatrix(unsigned short m, unsigned short n, double minVal, double maxVal){
    
    double randomDouble;
    double **A = callocMatrix( m, n);
    
    unsigned short i_,j_;
    
    for (i_ = 0; i_<m; i_++){
        for (j_ = 0; j_<n; j_++){
            randomDouble=((double)rand()/(double)RAND_MAX);
            A[i_][j_] = (maxVal - minVal)*randomDouble+minVal;
        }
    }
    
    return A;
}

double ***callocTensor( unsigned short m, unsigned short n, unsigned short l) {

    unsigned short i_ = 0;
    unsigned short j_ = 0;
    double ***A=NULL;

    if ( ( (0==m) || (0==n) ) || (0==l) )
        return NULL;
    
    A = (double ***) calloc( (size_t) m, sizeof(double **));
    
    if (A==NULL)
        return NULL;

    for ( i_=0 ; i_<m ; i_++ ) {
        A[i_] = (double **) calloc( (size_t) n, sizeof(double *));
        if (A[i_] != NULL) {
            for (j_=0; j_<n; j_++)
                A[i_][j_] = (double *) calloc( (size_t) l, sizeof(double));
        }
    }

    return A;
}

void freeMatrix( unsigned short m, unsigned short n, double **A) {

    (void)(n);
    if (A != NULL) {
        unsigned short i_ = 0;
        for (i_ = 0; i_<m ; i_++) {
            free(A[i_]);
        }
        free(A);
    }

}

void freeIntMatrix( unsigned short m, unsigned short n, int **A) {

    (void)(n);
    if (A != NULL) {
        unsigned short i_ = 0;
        for (i_ = 0; i_<m ; i_++) {
            free(A[i_]);
        }
        free(A);
    }

}

void freeTensor( unsigned short m, unsigned short n, unsigned short l, double ***A) {

    (void)(l);

    if (A!=NULL) {
        unsigned short i_ = 0;
        unsigned short j_ = 0;
        for (i_ = 0; i_<m  ; i_++) {
            for (j_ = 0; j_< n; j_++) {
                free(A[i_][j_]);
            }
            free(A[i_]);
        }
        free(A);
    }

}

short reduceMassMatrix(unsigned short oldDim, unsigned short newDim, double **Jr,  double **Jl, double **M) {

    double **MJr  = callocMatrix(oldDim, newDim);
    double **JlT  = callocMatrix(newDim, oldDim);
    double **Mred = callocMatrix(newDim, newDim);
    unsigned short *z = NULL;

    unsigned short i_,j_;
    
    if (newDim!=0)
        z = calloc( (size_t) newDim, sizeof(unsigned short));

    if ((MJr == NULL) || (JlT == NULL) || (Mred == NULL) || (z == NULL)) {
        if (MJr != NULL)
            freeMatrix(oldDim, newDim, MJr);
        if (JlT != NULL)
            freeMatrix(newDim, oldDim, JlT);
        if (Mred != NULL)
            freeMatrix(newDim, newDim, Mred);
        if ( z != NULL)
            free(z);
        return -1;
    }

    for (i_=0; i_<newDim; i_++) {
        for (j_=0; j_<oldDim; j_++) {
            JlT[i_][j_] = Jl[i_][j_];
        }
    }


    MatrixMultiplication( 'N', 'N', oldDim, oldDim, newDim, 1.0, M, Jr, 0.0, MJr);

    MatrixMultiplication( 'N', 'N', newDim, oldDim, newDim, 1.0, JlT, MJr, 0.0, Mred);

    if (InversionByLU(newDim, oldDim, z, Mred, JlT)!=0) {
        free(z);
        freeMatrix(newDim, newDim, Mred);
        freeMatrix(newDim, oldDim, JlT);
        freeMatrix(oldDim, newDim, MJr);
        return -1;
    }

    MatrixMultiplication( 'N', 'N', oldDim, newDim, oldDim, 1.0, Jr, JlT, 0.0, M);

    free(z);
    freeMatrix(newDim, newDim, Mred);
    freeMatrix(newDim, oldDim, JlT);
    freeMatrix(oldDim, newDim, MJr);

    return 0;
}

void getProjectionMatrix(unsigned m, unsigned n, double **C_, double **Q_, double **R_){
    
    /* C_[m][n]
     * R_[n][m]
     * Q_[n][n]
     *
     * C' = Q*R 
     */
    
    unsigned short i_,j_;
    
    for (i_=0; i_<m; i_++){
        for (j_=0; j_<n; j_++){
            R_[j_][i_] = C_[i_][j_];
        }
    }
    
    householderQR(n, m, R_, Q_);
    
}

short projectSystemDynamics(unsigned short ext, unsigned short stateSpace, unsigned short subSpace, double **Jr,  double **Jl, double **M, double *f, double *gamma){
  
    double *temp1 = calloc(subSpace, (size_t) sizeof(double));
    double **Mred = callocMatrix(subSpace, subSpace);
    double **MJr  = callocMatrix(stateSpace, subSpace);
    unsigned short *z = NULL;
    
    if (subSpace > 0)
        z = (unsigned short *) calloc( (size_t) subSpace, sizeof(unsigned short));
    
    MatrixVectorMultiplication('N', stateSpace, stateSpace, -1.0, M, gamma, 1.0, f);
    
    MatrixVectorMultiplication('N', subSpace, stateSpace, 1.0, Jl, f, 0.0, temp1);
    
    if (ext==1){
        MatrixMultiplication( 'N', 'N', stateSpace, stateSpace, subSpace, 1.0, M, Jr, 0.0, MJr);
        MatrixMultiplication( 'N', 'N', subSpace, stateSpace, subSpace, 1.0, Jl, MJr, 0.0, Mred);
    } else {
        MatrixMultiplication( 'N', 'N', subSpace, stateSpace, subSpace, 1.0, Jl, Jr, 0.0, Mred);
    }

    if (InversionByLU1dim(subSpace, z, Mred, temp1)!=0) {
        free(z);
	free(temp1);
	
	freeMatrix(subSpace, subSpace, Mred);
	freeMatrix(stateSpace, subSpace, MJr);
        return -1;
    }
    
    memcpy(f, gamma, stateSpace*sizeof(double));
    
    MatrixVectorMultiplication( 'N', stateSpace, subSpace, 1.0, Jr, temp1, 1.0, f);
    
    if (subSpace > 0)
        free(z);
    
    free(temp1);
    freeMatrix(subSpace, subSpace, Mred);
    freeMatrix(stateSpace, subSpace, MJr);
    
    return 0;

}  
  

void posUpdate( double *r,  double **S, double *v, double *y) {

    unsigned short i_, j_;

    for ( i_=0; i_<3; i_++) {
        y[i_] = r[i_];
        for (j_=0; j_<3; j_++) {
            y[i_] = y[i_] + S[i_][j_] * v[j_];
        }
    }

}

double hornerScheme(unsigned short n, double *a, double x_) {

    unsigned short i_;

    double res_ = a[0];

    for ( i_=1; i_<n; i_++) {
        res_ = a[i_] + x_*res_;
    }

    return res_;

}

double taxicabNorm_vec(unsigned short dim, double *vec) {

    unsigned short i_ = 0;
    double norm_ = 0.0;

    for ( i_ = 0 ; i_ < dim ; i_++ ) {
        norm_ = norm_ + fabs(vec[i_]);
    }

    return norm_;
}

double maximumNorm_vec(unsigned short dim, double *vec) {

    unsigned short i_ = 0;
    double norm_ = 0.0;

    for ( i_ = 0 ; i_ < dim ; i_++ ) {
        norm_ = maxDouble(norm_,fabs(vec[i_]));
    }

    return norm_;
}

double euclideanNorm_vec(unsigned short dim, double *vec) {

    unsigned short i_ = 0;
    double norm_ = 0.0;

    for ( i_ = 0 ; i_ < dim ; i_++ ) {
        norm_ = norm_ + vec[i_]*vec[i_];
    }

    return sqrt(norm_);
}

void rotmat2quat(double **S, double *w, double *x, double *y, double *z) {

    double t_ = S[0][0] + S[1][1] + S[2][2];
    double r_ = 0.0;
    double s_ = 0.0;

    if (t_>0) {
        r_ = sqrt(1+t_);
        s_ = 0.5/r_;
        *w = 0.5*r_;
        *x = (S[2][1]-S[1][2])*s_;
        *y = (S[0][2]-S[2][0])*s_;
        *z = (S[1][0]-S[0][1])*s_;
    } else {
        if ((S[0][0]>=S[1][1]) && (S[0][0]>=S[2][2])) {
            r_ = sqrt(1+S[0][0]-S[1][1]-S[2][2]);
            s_ = 0.5/r_;
            *w = (S[2][1]-S[1][2])*s_;
            *x = 0.5*r_;
            *y = (S[0][1]+S[1][0])*s_;
            *z = (S[2][0]+S[0][2])*s_;
        } else if ((S[1][1]>=S[0][0]) && (S[1][1]>=S[2][2])) {
            r_ = sqrt(1-S[0][0]+S[1][1]-S[2][2]);
            s_ = 0.5/r_;
            *w = (S[0][2]-S[2][0])*s_;
            *x = (S[0][1]+S[1][0])*s_;
            *y = 0.5*r_;
            *z = (S[2][1]+S[1][2])*s_;
        } else if ((S[2][2]>=S[0][0]) && (S[2][2]>=S[1][1])) {
            r_ = sqrt(1-S[0][0]-S[1][1]+S[2][2]);
            s_ = 0.5/r_;
            *w = (S[1][0]-S[0][1])*s_;
            *x = (S[0][2]+S[2][0])*s_;
            *y = (S[2][1]+S[1][2])*s_;
            *z = 0.5*r_;
        }
    }
}

void quat2rotmat(double **S, double *w, double *x, double *y, double *z) {

    S[0][0] = 1.0 - 2.0*y[0]*y[0] - 2.0*z[0]*z[0];
    S[0][1] = 2.0*x[0]*y[0] - 2.0*z[0]*w[0];
    S[0][2] = 2.0*x[0]*z[0] + 2.0*y[0]*w[0];
    S[1][0] = 2.0*x[0]*y[0] + 2.0*z[0]*w[0];
    S[1][1] = 1.0 - 2.0*x[0]*x[0] - 2.0*z[0]*z[0];
    S[1][2] = 2.0*y[0]*z[0] - 2.0*x[0]*w[0];
    S[2][0] = 2.0*x[0]*z[0] - 2.0*y[0]*w[0];
    S[2][1] = 2.0*y[0]*z[0] + 2.0*x[0]*w[0];
    S[2][2] = 1.0 - 2.0*x[0]*x[0] - 2.0*y[0]*y[0];

}

short getPhi(double **S, double *phi, const char *type) {
        
    int rotAx1 = fmod( type[0], 71 )-49;
    int rotAx2 = fmod( type[1], 71 )-49;
    int rotAx3 = fmod( type[2], 71 )-49;
    
    if (3 == (rotAx1+rotAx2+rotAx3)) {
        
        if ((rotAx2 == rotAx1+1)||(rotAx2 == rotAx1-2)) {
            phi[0] = atan2(-S[rotAx2][rotAx3],S[rotAx3][rotAx3]);
            phi[1] = atan2(S[rotAx1][rotAx3],S[rotAx3][rotAx3]/cos(phi[0]));
            phi[2] = atan2(-S[rotAx1][rotAx2],S[rotAx1][rotAx1]);
        } else {
            phi[0] = atan2(S[rotAx2][rotAx3],S[rotAx3][rotAx3]);
            phi[1] = atan2(-S[rotAx1][rotAx3],S[rotAx3][rotAx3]/cos(phi[0]));
            phi[2] = atan2(S[rotAx1][rotAx2],S[rotAx1][rotAx1]);
        }
                
    } else {
        
        if (1 == (rotAx1+rotAx2)) {
            rotAx3 = 2;
        } else if (2 == (rotAx1+rotAx2)) {
            rotAx3 = 1;
        } else {
            rotAx3 = 0;
        }
        
        if (1 == fmod(((rotAx2+1)-(rotAx1+1)),3)) {
            phi[0] = atan2(S[rotAx2][rotAx1],-S[rotAx3][rotAx1]);
            phi[1] = atan2(S[rotAx2][rotAx1]/sin(phi[0]),S[rotAx1][rotAx1]);
            phi[2] = atan2(S[rotAx1][rotAx2],S[rotAx1][rotAx3]);
        } else {
            phi[0] = atan2(S[rotAx2][rotAx1],S[rotAx3][rotAx1]);
            phi[1] = atan2(S[rotAx3][rotAx1]/cos(phi[0]),S[rotAx1][rotAx1]);
            phi[2] = atan2(S[rotAx1][rotAx2],-S[rotAx1][rotAx3]);
        }
        
    }
    
    return 0;
    
}

double Euler(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *)){
	
	/* Euler */
	
	double *x_temp = (double *) calloc(nx, sizeof(double));
	double *dx_0   = (double *) calloc(nx, sizeof(double));
	
	int i_;
	
	/* Get dx_0 */
	(*systemDynamics)(t_k, x_k, u_k, dx_0, data);
	
	/* Get x_k1 */
	for (i_=0; i_<nx; i_++){
		x_k1[i_] = x_k[i_] + h*dx_0[i_];
	}
	
	free(x_temp);
	free(dx_0);
	
	return t_k+h;
}

double Heun2(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *)){
	
	/* second-order Heun */
	
	double *x_temp = (double *) calloc(nx, sizeof(double));
	double *dx_0   = (double *) calloc(nx, sizeof(double));
	double *dx_1   = (double *) calloc(nx, sizeof(double));
	
	int i_;
	
	/* Get dx_0 */
	(*systemDynamics)(t_k, x_k, u_k, dx_0, data);
	
	/* Get x_1 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + h*dx_0[i_];
	}
	
	/* Get dx_1 */
	(*systemDynamics)(t_k+h, x_temp, u_k, dx_1, data);
	
	
	/* Get x_k1 */
	for (i_=0; i_<nx; i_++){
		x_k1[i_] = x_k[i_] + 0.5*h*(dx_0[i_] + dx_1[i_]);
	}
	
	free(x_temp);
	free(dx_0);
	free(dx_1);
	
	return t_k+h;
}

double Heun3(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *)){
	
	/* third-order Heun */
	
	double *x_temp = (double *) calloc(nx, sizeof(double));
	double *dx_0   = (double *) calloc(nx, sizeof(double));
	double *dx_1   = (double *) calloc(nx, sizeof(double));
	double *dx_2   = (double *) calloc(nx, sizeof(double));
	
	int i_;
	
	/* Get dx_0 */
	(*systemDynamics)(t_k, x_k, u_k, dx_0, data);
	
	/* Get x_1 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + (1.0/3.0)*h*dx_0[i_];
	}
	
	/* Get dx_1 */
	(*systemDynamics)(t_k+(1.0/3.0)*h, x_temp, u_k, dx_1, data);
	
	/* Get x_2 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + (2.0/3.0)*h*dx_1[i_];
	}
	
	/* Get dx_2 */
	(*systemDynamics)(t_k+(2.0/3.0)*h, x_temp, u_k, dx_2, data);
	
	/* Get x_k1 */
	for (i_=0; i_<nx; i_++){
		x_k1[i_] = x_k[i_] + h*((1.0/4.0)*dx_0[i_] + (3.0/4.0)*dx_2[i_]);
	}
	
	free(x_temp);
	free(dx_0);
	free(dx_1);
	free(dx_2);
	
	return t_k+h;
}

double RK2(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *)){
	
	/* second-order Runge-Kutta */
	
	double *x_temp = (double *) calloc(nx, sizeof(double));
	double *dx_0   = (double *) calloc(nx, sizeof(double));
	double *dx_1   = (double *) calloc(nx, sizeof(double));
	
	int i_;
	
	/* Get dx_0 */
	(*systemDynamics)(t_k, x_k, u_k, dx_0, data);
	
	/* Get x_1 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + 0.5*h*dx_0[i_];
	}
	
	/* Get dx_1 */
	(*systemDynamics)(t_k+0.5*h, x_temp, u_k, dx_1, data);
	
	
	/* Get x_k1 */
	for (i_=0; i_<nx; i_++){
		x_k1[i_] = x_k[i_] + h*dx_1[i_];
	}
	
	free(x_temp);
	free(dx_0);
	free(dx_1);

	return t_k+h;
}

double RK3(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *)){
	
	/* third-order Runge-Kutta */
	
	double *x_temp = (double *) calloc(nx, sizeof(double));
	double *dx_0   = (double *) calloc(nx, sizeof(double));
	double *dx_1   = (double *) calloc(nx, sizeof(double));
	double *dx_2   = (double *) calloc(nx, sizeof(double));
	
	int i_;
	
	/* Get dx_0 */
	(*systemDynamics)(t_k, x_k, u_k, dx_0, data);
	
	/* Get x_1 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + 0.5*h*dx_0[i_];
	}
	
	/* Get dx_1 */
	(*systemDynamics)(t_k+0.5*h, x_temp, u_k, dx_1, data);
	
	/* Get x_2 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + h*(2.0*dx_1[i_] - dx_0[i_]);
	}
	
	/* Get dx_2 */
	(*systemDynamics)(t_k+h, x_temp, u_k, dx_2, data);
	
	/* Get x_k1 */
	for (i_=0; i_<nx; i_++){
		x_k1[i_] = x_k[i_] + (1.0/6.0)*h*(dx_0[i_] + 4.0*dx_1[i_] + dx_2[i_]);
	}
	
	free(x_temp);
	free(dx_0);
	free(dx_1);
	free(dx_2);
	
	return t_k+h;
}

double RK4(int nx, double h, double t_k, double *x_k, double *u_k, double *x_k1, void *data, void (*systemDynamics)(double, double *, double *, double *, void *)){
	
	/* fourth-order Runge-Kutta */
	
	double *x_temp = (double *) calloc(nx, sizeof(double));
	double *dx_0   = (double *) calloc(nx, sizeof(double));
	double *dx_1   = (double *) calloc(nx, sizeof(double));
	double *dx_2   = (double *) calloc(nx, sizeof(double));
	double *dx_3   = (double *) calloc(nx, sizeof(double));
	
	int i_;
	
	/* Get dx_0 */
	(*systemDynamics)(t_k, x_k, u_k, dx_0, data);
	
	/* Get x_1 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + 0.5*h*dx_0[i_];
	}
	
	/* Get dx_1 */
	(*systemDynamics)(t_k+0.5*h, x_temp, u_k, dx_1, data);
	
	/* Get x_2 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + 0.5*h*dx_1[i_];
	}
	
	/* Get dx_2 */
	(*systemDynamics)(t_k+0.5*h, x_temp, u_k, dx_2, data);
	
	/* Get x_3 */
	for (i_=0; i_<nx; i_++){
		x_temp[i_] = x_k[i_] + h*dx_2[i_];
	}
	
	/* Get dx_3 */
	(*systemDynamics)(t_k+h, x_temp, u_k, dx_3, data);
	
	/* Get x_k1 */
	for (i_=0; i_<nx; i_++){
		x_k1[i_] = x_k[i_] + (1.0/6.0)*h*(dx_0[i_] + 2.0*(dx_1[i_] + dx_2[i_]) + dx_3[i_]);
	}
	
	free(x_temp);
	free(dx_0);
	free(dx_1);
	free(dx_2);
	free(dx_3);
	
	return t_k+h;
}

double getDirectionT(double *r, double *v){
    
    double Delta_ = 0.0;
    double **A = callocMatrix(3, 1);
    double **Q = callocMatrix(3, 3);
    
    A[0][0] = r[0];
    A[1][0] = r[1];
    A[2][0] = r[2];
    
    householderQR(3, 1, A, Q);
    
    if (fabs(A[0][0])<EPS){
        A[0][0] = v[0];
        A[1][0] = v[1];
        A[2][0] = v[2];   
        householderQR(3, 1, A, Q);
    } else {
        Delta_ = A[0][0];
    }
        
    r[0] = Q[0][0];
    r[1] = Q[1][0];
    r[2] = Q[2][0];
    
    freeMatrix(3,3,A);
    freeMatrix(3,3,Q);
    
    return Delta_;
}

double getDirectionR(double *d, double **S){
    
    double Delta_ = 0.0;
    double **A = callocMatrix(3, 1);
    double **Q = callocMatrix(3, 3);
    
    A[0][0] = S[2][1]-S[1][2];
    A[1][0] = S[0][2]-S[2][0];
    A[2][0] = S[1][0]-S[0][1];
    
    householderQR(3, 1, A, Q);
    
    if (fabs(A[0][0])>=EPS){
        Delta_ = A[0][0];
    }
        
    d[0] = Q[0][0];
    d[1] = Q[1][0];
    d[2] = Q[2][0];
    
    freeMatrix(3,3,A);
    freeMatrix(3,3,Q);
    
    return Delta_;
}

double scalarProduct(unsigned short n, double *a, double *b){
    
    double scalar_= 0.0;
    unsigned short k_;
    
    for (k_=0; k_<n; k_++)
        scalar_+=a[k_]*b[k_];
    
    return scalar_;
    
}

void statusOutput(const char *logMessage) {

    FILE * Output;
#if defined(__linux__)
    time_t result = time(NULL);
    struct tm tm = *localtime(&result);
#endif
    
    Output = fopen("neweulm2Log.txt", "a");
#if defined(__linux__)
    fprintf(Output, "%d-%d-%d %d:%d:%d : ", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
#endif
    fprintf(Output, "%s", logMessage);
    fprintf(Output, "\n");
    fclose(Output);
    return;

}

void printMatrix(unsigned short n, unsigned short m, double **A, const char *ident){
  
    unsigned short i,j;
  
    printf("\n");
    printf("%s = [ ", ident);
    for (i = 0; i<n; i++){
        for (j = 0; j<m; j++){
            printf("\t% -8e", A[i][j]);
        }
        if ((i+1)<n)
            printf(";\n");
        else
            printf("];\n");
    }
    printf("\n");
  
}
