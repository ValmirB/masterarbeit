#include "SCc3A3D_linearizeMBS.h"
#include "SCc3A3D_pi_code.h"
#include "SCc3A3D_userDefined.h"
#include "SCc3A3D_pd_matlab.h"
#include "neweul.h"
#include "SCc3A3D_Flexor.h"
#include <math.h>

void SCc3A3D_createSystemMatrix(double t_WP, double *x_WP, double *u_WP, 
		double **A, int dim, void *dataPtr){
	
	double *dx = calloc(2*dim,sizeof(double));
	int i,j;
	double myStep = 1e-6;
	
	/* Loop over all states */
	
	for (i=0; i<2*dim; i++){
		
		clearVector(2*dim,dx);
		
		x_WP[i] = x_WP[i] + myStep;
		
		SCc3A3D_equations_of_motion(t_WP, x_WP, u_WP, dx, dataPtr);
		
		for (j=0; j<2*dim; j++)
			A[j][i] = dx[j];
		
		x_WP[i] = x_WP[i] - 2.0*myStep;
		
		clearVector(2*dim,dx);
		
		SCc3A3D_equations_of_motion(t_WP, x_WP, u_WP, dx, dataPtr);
		
		for (j=0; j<2*dim; j++)
			A[j][i] = (A[j][i] - dx[j])/(2.0*myStep);
		
		x_WP[i] = x_WP[i] + myStep;
		
	}
	
	free(dx);
    
}

void SCc3A3D_createInputMatrix(double t_WP, double *x_WP, double *u_WP, 
		double **B, int dim, int numIn, void *dataPtr){
	
	double *dx = calloc(2*dim, sizeof(double));
	int i,j;
	double myStep = 1e-6;
	
	/* Loop over all inputs */
	
	for (i=0; i<numIn; i++){
		
		clearVector(2*dim,dx);
		
		u_WP[i] = u_WP[i] + myStep;
		
		SCc3A3D_equations_of_motion(t_WP, x_WP, u_WP, dx, dataPtr);
		
		for (j=0; j<2*dim; j++)
			B[j][i] = dx[j];
		
		u_WP[i] = u_WP[i] - 2.0*myStep;
		
		clearVector(2*dim,dx);
		
		SCc3A3D_equations_of_motion(t_WP, x_WP, u_WP, dx, dataPtr);
		
		for (j=0; j<2*dim; j++)
			B[j][i] = (B[j][i] - dx[j])/(2.0*myStep);
		
		u_WP[i] = u_WP[i] + myStep;
		
	}
	
	free(dx);
}

void SCc3A3D_createOutputMatrix(double t_WP, double *x_WP,
		double *u_WP, double **C, 
		int dim, int numOut, void *dataPtr,
		void (*outputFunction)(double, double *, double *, double *, void *)){
	
	double *z = calloc(numOut, sizeof(double));
	int i,j;
	double myStep = 1e-6;
	
	/* Loop over all states */
	
	for (i=0; i<2*dim; i++){
		
		clearVector(numOut, z);
		
		x_WP[i] = x_WP[i] + myStep;

		(*outputFunction) (t_WP, x_WP, u_WP, z, dataPtr);
		
		for (j=0; j<numOut; j++)
			C[j][i] = z[j];
		
		x_WP[i] = x_WP[i] - 2.0*myStep;
		
		clearVector(numOut,z);
		
		(*outputFunction) (t_WP, x_WP, u_WP, z, dataPtr);
		
		for (j=0; j<numOut; j++)
			C[j][i] = (C[j][i] - z[j])/(2.0*myStep);
		
		x_WP[i] = x_WP[i] + myStep;
		
	}
	
	free(z);
}

void SCc3A3D_createFeedthroughMatrix(double t_WP, double *x_WP,
		double *u_WP, double **D, 
		int numIn, int numOut, void *dataPtr,
		void (*outputFunction)(double, double *, double *, double *, void *)){
	
	double *z = calloc(numOut, sizeof(double));
	int i,j;
	double myStep = 1e-6;
	
	/* Loop over all inputs */
	
	for (i=0; i<numIn; i++){
		
		clearVector(numOut, z);
		
		u_WP[i] = u_WP[i] + myStep;
		
		(*outputFunction) (t_WP, x_WP, u_WP, z, dataPtr);
		
		for (j=0; j<numOut; j++)
			D[j][i] = z[j];
		
		u_WP[i] = u_WP[i] - 2.0*myStep;
		
		clearVector(numOut,z);
		
		(*outputFunction) (t_WP, x_WP, u_WP, z, dataPtr);
		
		for (j=0; j<numOut; j++)
			D[j][i] = (D[j][i] - z[j])/(2.0*myStep);
		
		u_WP[i] = u_WP[i] + myStep;
		
	}
	free(z);
    
}
