#ifndef SCc3A3D_constraintDerivatives_H
#define SCc3A3D_constraintDerivatives_H

#include "SCc3A3D_paraStruct.h"

void SCc3A3D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
