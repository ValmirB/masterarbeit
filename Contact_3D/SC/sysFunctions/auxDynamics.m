function Dz_ = auxDynamics(t, x_, u_, varargin)


global sys

if ~isempty(varargin)&&isstruct(varargin{1})
	kin = varargin{1};
else
	kin = evalAbsoluteKinematics(t, x_, u_, 1);
end

% Return values

Dz_ = zeros(0, 1);


end

