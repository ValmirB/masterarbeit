function u_endeffector_acceleration = endeffector_acceleration_inputs(t, x_)
% endeffector_acceleration_inputs - Wrapper function for endeffector_acceleration inputs
% These inputs are stored under sys.in.endeffector_acceleration
% --------------------- Automatically generated file! ---------------------

u_endeffector_acceleration = zeros(9,1);

u_endeffector_acceleration(1) = f_EE_x(t, x_);
u_endeffector_acceleration(2) = f_DEE_x(t, x_);
u_endeffector_acceleration(3) = f_D2EE_x(t, x_);
u_endeffector_acceleration(4) = f_EE_y(t, x_);
u_endeffector_acceleration(5) = f_DEE_y(t, x_);
u_endeffector_acceleration(6) = f_D2EE_y(t, x_);
u_endeffector_acceleration(7) = f_EE_z(t, x_);
u_endeffector_acceleration(8) = f_DEE_z(t, x_);
u_endeffector_acceleration(9) = f_D2EE_z(t, x_);

% END OF FILE

