function EE_z = f_EE_z(t, x_, varargin)
% f_EE_z - definition of time-depending user-defined variable EE_z

global sys;


% constant user-defined variables
EE_z_d = sys.parameters.data.EE_z_d;
EE_z = zeros(1,1);

EE_z(1) = EE_z_d;

% END OF FILE

