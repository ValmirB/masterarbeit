%% Redefined Calculate the Initiala Conditions
clear all
clc 
close all
%%
currentFolder = pwd;
cd(currentFolder)
addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))
% addpath(genpath([currentFolder '/ForwardDynamic']))

load('sys.mat')

% SampleTime = 1e-3;
% sampleTime = 1e-3;

%% Trajectory

sys.parameters.data.EE_x_d = 0.8;
sys.parameters.data.EE_y_d = 0.75;
sys.parameters.data.EE_z_d = 0.2;
sys.parameters.data.z_0 = 0.17;


Fd = [0; 0; 0];
 
F_contact = Fd; 
 
sys.parameters.data.EE_force_x_d = F_contact(1);
sys.parameters.data.EE_force_y_d = F_contact(2);

%% Redefined Output

% w2_1 = 0.8;
% w3_1 = 0.8;
% v2_1 = 1.07;
% v3_1 = 0.5;

w2_1 = 1;
w3_1 = 1;
v2_1 = 1;
v3_1 = 1;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;

[y0_redef, dy0_redef, ~, ~] = calcInitConditions(0,[0,0,-0.7,0.7,0,0,0,zeros(1,7)]', zeros(1,9)'); %-1 und 1 sind initial guesses der Winkel, zeros(3,1) sind Geschwindigkeit

y0_red = staticEquilibrium(0,y0_redef);


pos_EE = eqm_sysout_exactOut(0,[y0_red; dy0_redef],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');
pos_redef = eqm_sysout_approxOut(0,[y0_red; dy0_redef],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');


save('init_redefined.mat','y0_red')