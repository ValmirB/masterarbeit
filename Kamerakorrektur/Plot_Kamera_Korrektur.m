load('Test_Trajektorie.mat')
meas = h5load('KameraKorrektur.h5');

plot(traj.data(:,1),traj.data(:,4))
hold
plot(meas.Test3.SimulinkData.CameraData.EEx,meas.Test3.SimulinkData.CameraData.EEy)
plot(meas.Test3.SimulinkData.TestSignals.TestSignal1,meas.Test3.SimulinkData.TestSignals.TestSignal2)
