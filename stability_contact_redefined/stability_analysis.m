% Eigenvalues plotten

% Hier werden die Stabilit�t bei Kontakt (force,eigenavlue)
% des linearisierten Systems �berpr�ft.
% 1.) Anfangspunkt definieren --> Anfangsbedingung y0
% 2.) Statische durchbiegung berechnen y0 = staticEquilibrium(tstart,y0);
% 3.) Systemmatrix linearisieren
% 4.) Eigenwerte berechnen

currentFolder = pwd;
cd(currentFolder)

addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))


load('sys.mat')
global sys 

trajectoryTime_ = 2; % 4 urspr�nglich


for i = 1:1:26
    
sys.parameters.data.force_start = -1+i;
sys.parameters.data.force_end = 5;

sys.parameters.data.traj_x_start = 1.31;
sys.parameters.data.traj_x_end = 1.31;

sys.parameters.data.traj_y_start = 0.55;
sys.parameters.data.traj_y_end = 0.30; % 35cm urspr�nglich

sys.parameters.data.traj_theta_start = pi/6;
sys.parameters.data.traj_theta_end = pi/6;

% %Faktoren f�r relocated output
w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;

% w2_1 = 1;
% w3_1 = 1;
% v2_1 = 1;
% v3_1 = 1;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;

% %model
sys.parameters.data.traj_end = sys.parameters.data.traj_start + trajectoryTime_;
tstart = sys.parameters.data.traj_start;

[y0, Dy0, ~, ~] = calcInitConditions(tstart,[0;0;-0.7;0.7;zeros(2*sys.counters.genCoord-4,1)]);   % initial guess necessary
y0 = staticEquilibrium(tstart,y0);


A_ = reduced_systemMatrix_new(0,[y0; zeros(length(y0),1)]);

force(i,1) = sys.parameters.data.force_start;
eigenvalue(i,1:4) =  eig(A_);
end

%% plot
figure
% plot(force,real(eigenvalue))
plot(force(1:24),real(eigenvalue(1:24,:)))
% plot3(force,real(eigenvalue),imag(eigenvalue))
grid on

