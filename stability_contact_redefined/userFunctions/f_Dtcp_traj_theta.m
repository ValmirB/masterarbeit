function Dtcp_traj_theta = f_Dtcp_traj_theta(t, varargin)
% f_Dtcp_traj_theta - definition of smooth transition Dtcp_traj_theta

global sys

% constant user-defined variables
traj_end = sys.parameters.data.traj_end;
traj_start = sys.parameters.data.traj_start;
traj_theta_end = sys.parameters.data.traj_theta_end;
traj_theta_start = sys.parameters.data.traj_theta_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = traj_theta_start;
endValue(1) = traj_theta_end;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = sys.parameters.data.Dtcp_traj_theta.poly;

Dtcp_traj_theta = 0.5*(endValue-startValue)*(2/(endTime-startTime))*polyval(transitionPoly_,max(min(scaledTime_,1),-1));

% END OF FILE
