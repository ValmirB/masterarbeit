function Dtcp_traj1 = f_Dtcp_traj1(t, varargin)
% f_Dtcp_traj1 - definition of smooth transition Dtcp_traj1

global sys

% constant user-defined variables
traj_end = sys.parameters.data.traj_end;
traj_start = sys.parameters.data.traj_start;
traj_x_end = sys.parameters.data.traj_x_end;
traj_x_start = sys.parameters.data.traj_x_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = traj_x_start;
endValue(1) = traj_x_end;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = sys.parameters.data.Dtcp_traj1.poly;

Dtcp_traj1 = 0.5*(endValue-startValue)*(2/(endTime-startTime))*polyval(transitionPoly_,max(min(scaledTime_,1),-1));

% END OF FILE
