function [Jr_a, Jl_a, Jr_v, theta_, gamma_] = constraintEquations(t, x_, u_, varargin)
% copied file
% NewtonEuler/Minimal, qr decomposition,
% position constraints

global sys

if ~isempty(varargin)&&isstruct(varargin{1})
    kin = varargin{1};
else
    kin = evalAbsoluteKinematics(t, x_, u_);
end

% Constraint derivatives
[C_, G_, Dc_, D2c_] = constraintDerivatives(t, x_, u_, kin);

% Perform a QR decomposition
[Q_c_, R_c_] = qr(transpose(C_));

% subspace dimensions of constraints acting on the acceleration
dep_acc = 1:sys.counters.posConstraints+sys.counters.velConstraints;
ind_acc = setdiff(1:sys.counters.genCoord, dep_acc);

% subspace dimensions of constraints acting on the velocity
dep_vel = 1:sys.counters.posConstraints;
ind_vel = setdiff(1:sys.counters.genCoord, dep_vel);

% Get subspace
Q_acc = Q_c_(:,dep_acc);
Q_vel = Q_c_(:,dep_vel);

% Jacobian mapping independent coordinates to all genCoords:
Jr_a = Q_c_(:,ind_acc);
Jr_v = Q_c_(:,ind_vel);

R_c_acc = R_c_(dep_acc, :);
R_c_vel = R_c_(dep_vel, :);

% Local velocities
theta_ = -Q_vel*(transpose(R_c_vel)\Dc_);

% Local accelarations
gamma_ = -Q_acc*(transpose(R_c_acc)\D2c_);

if ~isempty(G_)
    [Q_g_, R_g_] = qr(G_);
    Jl_a = transpose(Q_g_(:,ind_acc));
else
    Jl_a = transpose(Jr_a);
end