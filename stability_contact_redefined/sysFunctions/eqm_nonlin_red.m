function Dx_red = eqm_nonlin_red(t, x_red)
% Dx_red = eqm_nonlin_red(t, x_red)
% EQM_NONLIN_RED - Nonlinear equations of motion in
% reduced state space.
% The system contains constraint equations,
% uses an index 0 formulation.

global sys;

dof_ = sys.counters.genCoord;
red_ = sys.counters.genCoord;
aux_ = sys.counters.auxCoord;

% Indices of the independent and dependent coordinates
id_dep_coord = sys.parameters.coordinates.idx_depCoord;
id_ind_velo  = sys.parameters.coordinates.idx_indVelo;

% Dependent coordinates
y_  = educatedGuess(t, x_red(1 : sys.counters.genCoord-sys.counters.posConstraints));
y_  = findroot(@positionConstraints, y_,[],'index',id_dep_coord,'preargs',t);
Dy_ = zeros(sys.counters.genCoord,1);
Dy_(id_ind_velo) = x_red(1+sys.counters.genCoord-sys.counters.posConstraints : 2*(sys.counters.genCoord-sys.counters.posConstraints));
Dy_ = dependent_velocities(t, y_, Dy_(id_ind_velo));
z_ = x_red(1+2*(sys.counters.genCoord-sys.counters.posConstraints) : end);

x_ = [y_; Dy_; z_];

% Evaluate all inputs
u_ = inputs_all(t, x_);

% Evaluate absolute kinematics
kin = evalAbsoluteKinematics(t, x_, u_, 1);

% Dynamics of the unconstrained system
[M_, f_] = system_dynamics(t, x_, u_, kin);

% Jacobian, velocities and accelerations due to the constraints
[Jr_a, Jl_a, Jr_v, theta_, gamma_] = constraintEquations(t, x_, u_, kin);

% Allocate return vector
Dx_ = zeros(2*dof_+aux_,1);

% Projection of the velocities
Dx_(1:dof_) = Jr_v*transpose(Jr_v)*(x_(dof_+1:2*dof_) - theta_) + theta_;

% Projection of the accelerations
Dx_(dof_+1:2*dof_) = Jr_a*((Jl_a*M_*Jr_a)\Jl_a)*(f_ - M_*gamma_)  + gamma_;

% Auxiliary dynamics
Dx_(2*dof_+1:2*dof_+aux_) = auxDynamics(t, x_, u_);

% Compose return vector

Dx_red = [Dx_(sys.parameters.coordinates.idx_indCoord); Dx_(dof_+sys.parameters.coordinates.idx_indCoord); Dx_(2*dof_+1:2*dof_+aux_)];



% END OF FILE

