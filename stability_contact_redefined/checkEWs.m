% initial conditions

load('sys.mat')
global sys %EEexport

%sys.parameters.data.w2_1 =0.95% 0.95%0.947;
% Trajectory definition
trajectoryTime_ = 2; % 4 urspr�nglich

sys.parameters.data.force_start = 2;
sys.parameters.data.force_end = 5;

sys.parameters.data.traj_x_start = 1.31;
sys.parameters.data.traj_x_end = 1.31;

sys.parameters.data.traj_y_start = 0.55;
sys.parameters.data.traj_y_end = 0.30; % 35cm urspr�nglich

sys.parameters.data.traj_theta_start = pi/6;
sys.parameters.data.traj_theta_end = pi/6;

% %Faktoren f�r relocated output
w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;

% %model
sys.parameters.data.traj_end = sys.parameters.data.traj_start + trajectoryTime_;
tstart = sys.parameters.data.traj_start;

[y0, Dy0, ~, ~] = calcInitConditions(tstart,[0;0;-0.7;0.7;zeros(2*sys.counters.genCoord-4,1)]);   % initial guess necessary
y0 = staticEquilibrium(tstart,y0);


A_ = reduced_systemMatrix_new(0,[y0; zeros(length(y0),1)]);

c_ = real(eig(A_));
if max(c_) > 0
    disp('unstable!!!')
else
    disp('Stable.')
end
eig(A_)