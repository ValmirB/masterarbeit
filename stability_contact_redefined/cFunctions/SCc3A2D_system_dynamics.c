#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "SCc3A2D_pi_code.h"
#include "SCc3A2D_userDefined.h"
#include "SCc3A2D_pd_matlab.h"
#include "neweul.h"
#include "SCc3A2D_paraStruct.h"
#include "SCc3A2D_Flexor.h"


void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	double *t_                = NULL;
	double *x_                = NULL;
	double *u_                = NULL;
	double *y_                = NULL;
	double *Dy_               = NULL;
	double *f_                = NULL;
	double **M_               = callocMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF);

	void *dataPtr = NULL;

	unsigned short numStates;

	int count_;

	/* Extract parameters of the system structure */
	dataPtr = SCc3A2D_initializeSystemStruct();

	/* Check number of input arguments */
	if (nrhs<3){
		mexPrintf("Please provide three or four input arguments (t, x_, u_, sys.parameters.data)!\n");
		goto clean_up;
	}

	/* Check dimension of state vector */
	if ((unsigned short) mxGetM(prhs[1]) != (SCC3A2D_SYS_DOF*2)) {
		mexPrintf("The first dimension of the second argument has to be %d!\n", 2*SCC3A2D_SYS_DOF);
		goto clean_up;
	}

	/* Check dimension of input vector */
	if ((unsigned short) mxGetM(prhs[2]) != (SCC3A2D_NUM_ALL_INPUTS)) {
		mexPrintf("The first dimension of the third argument has to be %d!\n", 2*SCC3A2D_NUM_ALL_INPUTS);
		goto clean_up;
	}

	/* Check second dimension of the first three inputs */
	if ((unsigned short) mxGetN(prhs[0]) <= (unsigned short) mxGetN(prhs[1])) {
		numStates = (unsigned short) mxGetN(prhs[0]);
	} else {
		numStates = (unsigned short) mxGetN(prhs[1]);
	}
	if ((unsigned short) mxGetN(prhs[2]) < numStates) {
		numStates = (unsigned short) mxGetN(prhs[2]);
	}

	/* Get pointers of the input arguments */
	t_ = mxGetPr( prhs[0] );
	x_ = mxGetPr( prhs[1] );
	u_ = mxGetPr( prhs[2] );

	/* Error handling*/
	if (nrhs<3){
		printf("Please provide two or three input arguments!\n");
		goto clean_up;
	}

	if ((nrhs==4) && (mxIsStruct(prhs[3]))){
		SCc3A2D_getSystemStruct(dataPtr, (const void *) prhs[3]);
	}

	if (nlhs==1){
		plhs[0] = mxCreateDoubleMatrix(SCC3A2D_SYS_DOF, numStates, mxREAL);
		f_ = mxGetPr(plhs[0]);
	} else if (nlhs==2) { /*  */
		if (numStates == 1)
			plhs[0] = mxCreateDoubleMatrix(SCC3A2D_SYS_DOF, SCC3A2D_SYS_DOF, mxREAL);
		else {
			mwSignedIndex dims[3];
			dims[0] = SCC3A2D_SYS_DOF;
			dims[1] = SCC3A2D_SYS_DOF;
			dims[2] = (mwSignedIndex) numStates;
			plhs[0] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
		}
		plhs[1] = mxCreateDoubleMatrix(SCC3A2D_SYS_DOF, numStates, mxREAL);

		f_ = mxGetPr(plhs[1]);
	} else {
		goto clean_up;
	}

	for (count_=0; count_<numStates; count_++){

		/* Evaluate System Dynamics */
		SCc3A2D_system_dynamics(t_[count_], &(x_[count_*2*SCC3A2D_SYS_DOF]), &(u_[count_*SCC3A2D_NUM_ALL_INPUTS]), &(f_[count_*SCC3A2D_SYS_DOF]), M_, dataPtr);

		if (nlhs==2) {

			short i_, j_;
			double *matlabMass = mxGetPr(plhs[0]);
			for (i_=0; i_<SCC3A2D_SYS_DOF; i_++){
				for (j_=0; j_<SCC3A2D_SYS_DOF; j_++){
					matlabMass[count_*SCC3A2D_SYS_DOF*SCC3A2D_SYS_DOF+j_*SCC3A2D_SYS_DOF+i_] = M_[i_][j_];
					M_[i_][j_] = 0.0;
				}
			}
		}
	}

clean_up:
	SCc3A2D_freeStructure(dataPtr);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M_);

}

