#ifndef SCc3A2D_constraintDerivatives_H
#define SCc3A2D_constraintDerivatives_H

#include "SCc3A2D_paraStruct.h"

void SCc3A2D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
