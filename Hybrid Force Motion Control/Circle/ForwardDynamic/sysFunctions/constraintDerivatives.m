function [C_, G_, dc_, d2c_] = constraintDerivatives(t, x_, u_, varargin)

global sys;

% generalized coordinates, velocities and auxiliary coordinates

r_alpha1 = x_(1);
r_beta2 = x_(2);
EA2_q001 = x_(3);
EA3_q001 = x_(4);
Dr_alpha1 = x_(5);
Dr_beta2 = x_(6);
DEA2_q001 = x_(7);
DEA3_q001 = x_(8);
% system inputs
Dp_a = u_(2);
D2p_a = u_(3);
p_b = u_(4);
Dp_b = u_(5);
D2p_b = u_(6);
r_gamma3 = u_(7);
Dr_gamma3 = u_(8);
D2r_gamma3 = u_(9);

% constant user-defined variables
Ox = sys.parameters.data.Ox;
Oy = sys.parameters.data.Oy;
d_axes = sys.parameters.data.d_axes;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

DelBo_EA1_2_x_ = SID_EA1_.frame.node(2).phi(1,:) * x_(sys.model.body.EA1.data.edof.idx+4);
DelBo_EA2_1_x_ = SID_EA2_.frame.node(1).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA2_2_rot_z_ = SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA2_2_x_ = SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
elBo_EA1_2_x_ =  SID_EA1_.frame.node(2).origin(1,:) + SID_EA1_.frame.node(2).phi(1,:) * x_(sys.model.body.EA1.data.edof.idx);
elBo_EA2_1_rot_z_ =  SID_EA2_.frame.node(1).orientation(3) + SID_EA2_.frame.node(1).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_1_x_ =  SID_EA2_.frame.node(1).origin(1,:) + SID_EA2_.frame.node(1).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_x_ =  SID_EA2_.frame.node(2).origin(1,:) + SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);



% Jacobian matrix of the constraints

C_ = zeros(sys.counters.constraint,sys.counters.genCoord);
C_(1,1) = elBo_EA1_2_x_*sin(r_alpha1);
C_(2,1) = - ...
	elBo_EA1_2_x_*cos(r_alpha1);
C_(1,2) = sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2) - ...
	 elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - ...
	 cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2) - ...
	 elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
C_(2,2) = - ...
	 cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2) - ...
	 elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - ...
	 sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2) - ...
	 elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
C_(3,2) = 2*((3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 + ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) - ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000) + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936)*(Ox - ...
	 d_axes - ...
	 p_b + ...
	 (3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) + ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000)) - ...
	 2*((3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) + ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000))*((3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 Oy + ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) - ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000) + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936);
C_(1,3) = - ...
	 sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*((3120578861353123*sin(elBo_EA2_1_rot_z_))/39614081257132168796771975168 - ...
	 (4583155844332741*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_))/36028797018963968 + ...
	 cos(elBo_EA2_1_rot_z_)*((4583155844332741*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2)/36028797018963968 + ...
	 (4583155844332741*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)/36028797018963968)) - ...
	 cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*((3120578861353123*cos(elBo_EA2_1_rot_z_))/39614081257132168796771975168 + ...
	 (4583155844332741*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_))/36028797018963968 - ...
	 sin(elBo_EA2_1_rot_z_)*((4583155844332741*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2)/36028797018963968 + ...
	 (4583155844332741*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)/36028797018963968));
C_(2,3) = cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*((3120578861353123*sin(elBo_EA2_1_rot_z_))/39614081257132168796771975168 - ...
	 (4583155844332741*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_))/36028797018963968 + ...
	 cos(elBo_EA2_1_rot_z_)*((4583155844332741*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2)/36028797018963968 + ...
	 (4583155844332741*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)/36028797018963968)) - ...
	 sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*((3120578861353123*cos(elBo_EA2_1_rot_z_))/39614081257132168796771975168 + ...
	 (4583155844332741*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_))/36028797018963968 - ...
	 sin(elBo_EA2_1_rot_z_)*((4583155844332741*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2)/36028797018963968 + ...
	 (4583155844332741*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)/36028797018963968));
C_(3,3) = 2*((3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 Oy + ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) - ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000) + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936)*((46153948040356261*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/360287970189639680 + ...
	 (7477566071302533*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/9903520314283042199192993792 - ...
	 (17099766347908059189622063840583*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/2596148429267413814265248164610048 + ...
	 (4583155844332741*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1))/36028797018963968 - ...
	 (509614377493539633539496778503*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/1298074214633706907132624082305024 - ...
	 (127583615252853*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000))/36028797018963968) - ...
	 2*((46153948040356261*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/360287970189639680 - ...
	 (7477566071302533*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/9903520314283042199192993792 - ...
	 (509614377493539633539496778503*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/1298074214633706907132624082305024 + ...
	 (4583155844332741*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1))/36028797018963968 + ...
	 (127583615252853*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000))/36028797018963968 + ...
	 (17099766347908059189622063840583*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/2596148429267413814265248164610048)*(Ox - ...
	 d_axes - ...
	 p_b + ...
	 (3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) + ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000));
C_(3,4) = 2*((3994355987511051*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 - ...
	 (2447179947670465*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/309485009821345068724781056)*((3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 Oy + ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) - ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000) + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936) + ...
	 2*((2447179947670465*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/309485009821345068724781056 + ...
	 (3994355987511051*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968)*(Ox - ...
	 d_axes - ...
	 p_b + ...
	 (3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) + ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000));


% Local velocities of the constraints

dc_ = zeros(3,1);

dc_(1) = cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(cos(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 Dp_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2)) - ...
	 sin(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 Dp_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + ...
	 sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(cos(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 Dp_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)) + ...
	 sin(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 Dp_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2)));
dc_(2) = sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(cos(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 Dp_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2)) - ...
	 sin(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 Dp_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - ...
	 cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(cos(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 Dp_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)) + ...
	 sin(elBo_EA2_1_rot_z_)*(Dp_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 Dp_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2))) - ...
	 Dp_a*cos(r_alpha1)^2 - ...
	 Dp_a*sin(r_alpha1)^2;
dc_(3) = - ...
	 2*(Dp_b - ...
	 (3994355987511051*Dr_gamma3*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 Dr_gamma3*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000))*(Ox - ...
	 d_axes - ...
	 p_b + ...
	 (3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) + ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000)) - ...
	 2*((3994355987511051*Dr_gamma3*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 Dr_gamma3*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000))*((3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 Oy + ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) - ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000) + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936);


% Local accelerations of the constraints

d2c_ = zeros(3,1);

d2c_(1) = sin(r_alpha1)*(D2p_a*cos(r_alpha1) + ...
	 2*DelBo_EA1_2_x_*Dr_alpha1) - ...
	 cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + ...
	 2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 D2p_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)) + ...
	 (DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)*(2*DelBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_) + ...
	 elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)) - ...
	 cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - ...
	 2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 D2p_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2))) + ...
	 sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*((DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)*(2*DelBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) - ...
	 elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)) + ...
	 sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - ...
	 2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 D2p_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2)) + ...
	 cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + ...
	 2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 D2p_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + ...
	 cos(r_alpha1)*(Dr_alpha1^2*elBo_EA1_2_x_ - ...
	 D2p_a*sin(r_alpha1));
d2c_(2) = sin(r_alpha1)*(Dr_alpha1^2*elBo_EA1_2_x_ - ...
	 D2p_a*sin(r_alpha1)) - ...
	 sin(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*(sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + ...
	 2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 D2p_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)) + ...
	 (DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)*(2*DelBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_) + ...
	 elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)) - ...
	 cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - ...
	 2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 D2p_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2))) - ...
	 cos(elBo_EA2_1_rot_z_ - ...
	 elBo_EA2_2_rot_z_ + ...
	 r_beta2)*((DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)*(2*DelBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) - ...
	 elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - ...
	 Dr_beta2)) + ...
	 sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - ...
	 2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)^2 + ...
	 elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)^2)) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*cos(r_beta2) + ...
	 D2p_b*sin(elBo_EA2_2_rot_z_)*sin(r_beta2)) + ...
	 cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - ...
	 sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_rot_z_^2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + ...
	 2*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + ...
	 Dr_beta2*(2*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + ...
	 2*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + ...
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + ...
	 D2p_b*cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - ...
	 D2p_b*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - ...
	 cos(r_alpha1)*(D2p_a*cos(r_alpha1) + ...
	 2*DelBo_EA1_2_x_*Dr_alpha1);
d2c_(3) = 2*((7477566071302533*DEA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/9903520314283042199192993792 + ...
	 (3994355987511051*DEA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 - ...
	 (2447179947670465*DEA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/309485009821345068724781056 + ...
	 (3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/5 + ...
	 (3731002594871963*DEA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2) - ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3))/36028797018963968 - ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3) - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/72057594037927936)^2 + ...
	 2*(Dp_b + ...
	 (3731002594871963*DEA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 - ...
	 (2447179947670465*DEA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/309485009821345068724781056 - ...
	 (3994355987511051*DEA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 (3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/5 - ...
	 (7477566071302533*DEA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/9903520314283042199192993792 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2) - ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3))/36028797018963968 + ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3) + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/72057594037927936)^2 - ...
	 2*((3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 Oy + ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) - ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000) + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936)*((3*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2)^2)/5 + ...
	 (3731002594871963*EA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2)^2)/72057594037927936 + ...
	 (2447179947670465*DEA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3))/154742504910672534362390528 + ...
	 (3994355987511051*DEA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3))/18014398509481984 + ...
	 sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2)^2 + ...
	 (3994355987511051*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3)^2)/36028797018963968 + ...
	 (3994355987511051*D2r_gamma3*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 - ...
	 (7477566071302533*DEA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/4951760157141521099596496896 + ...
	 D2r_gamma3*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000) + ...
	 (3731002594871963*DEA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/36028797018963968 - ...
	 sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3)^2) - ...
	 2*(Ox - ...
	 d_axes - ...
	 p_b + ...
	 (3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/5 - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2))/72057594037927936 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1) + ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000))*(D2p_b + ...
	 (3*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2)^2)/5 - ...
	 (3731002594871963*EA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2)^2)/72057594037927936 - ...
	 (3994355987511051*DEA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3))/18014398509481984 + ...
	 (2447179947670465*DEA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3))/154742504910672534362390528 + ...
	 cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((7477566071302533*EA2_q001)/9903520314283042199192993792 - ...
	 1)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2)^2 - ...
	 (3994355987511051*D2r_gamma3*EA3_q001*cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3))/36028797018963968 + ...
	 (3994355987511051*EA3_q001*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3)^2)/36028797018963968 + ...
	 (3731002594871963*DEA2_q001*cos((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/36028797018963968 + ...
	 (7477566071302533*DEA2_q001*sin((4583155844332741*EA2_q001)/36028797018963968 - ...
	 r_beta2)*((4583155844332741*DEA2_q001)/36028797018963968 - ...
	 Dr_beta2))/4951760157141521099596496896 + ...
	 cos((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000)*((127583615252853*DEA2_q001)/36028797018963968 + ...
	 Dr_beta2 + ...
	 Dr_gamma3)^2 + ...
	 D2r_gamma3*sin((127583615252853*EA2_q001)/36028797018963968 + ...
	 r_beta2 + ...
	 r_gamma3)*((2447179947670465*EA3_q001)/309485009821345068724781056 - ...
	 4643/10000));


% Input matrix of the constraints

G_ = [];
