function data_ = absolutePositions(t, x_, u_, varargin)
% data_ = absolutePositions(t, x_, u_, varargin)
% absolutePositions - calculates position, angle and rotational matrix
% of all coordinate systems. Optional you can calculate the absolute
% velocities of all coordinate systems.
%
% Input arguments
% t .......... Simulation time
% x_ ......... State vector
% u_ ......... Input vector
%
% Optional input argument
% varargin ... Logical. If true, velocities will be calculated.

global sys

if ~isempty(varargin)&&(isnumeric(varargin{1})||islogical(varargin{1}))
	calcVelo_ = varargin{1};
else
	calcVelo_ = 0;
end


% generalized coordinates, velocities and auxiliary coordinates

r_alpha1 = x_(1);
r_beta2 = x_(2);
EA2_q001 = x_(3);
EA3_q001 = x_(4);
% system inputs
p_a = u_(1);
p_b = u_(4);
r_gamma3 = u_(7);

% constant user-defined variables
d_axes = sys.parameters.data.d_axes;
l2 = sys.parameters.data.l2;
l3 = sys.parameters.data.l3;
phi2_1 = sys.parameters.data.phi2_1;
phi3_1 = sys.parameters.data.phi3_1;
psi2_1 = sys.parameters.data.psi2_1;
psi3_1 = sys.parameters.data.psi3_1;
v2_1 = sys.parameters.data.v2_1;
v3_1 = sys.parameters.data.v3_1;
w2_1 = sys.parameters.data.w2_1;
w3_1 = sys.parameters.data.w3_1;
z_0 = sys.parameters.data.z_0;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

elBo_EA1_2_x_ =  SID_EA1_.frame.node(2).origin(1,:) + SID_EA1_.frame.node(2).phi(1,:) * x_(sys.model.body.EA1.data.edof.idx);
elBo_EA2_1_rot_z_ =  SID_EA2_.frame.node(1).orientation(3) + SID_EA2_.frame.node(1).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_1_x_ =  SID_EA2_.frame.node(1).origin(1,:) + SID_EA2_.frame.node(1).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_x_ =  SID_EA2_.frame.node(2).origin(1,:) + SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_rot_z_ =  SID_EA2_.frame.node(3).orientation(3) + SID_EA2_.frame.node(3).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_x_ =  SID_EA2_.frame.node(3).origin(1,:) + SID_EA2_.frame.node(3).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_y_ =  SID_EA2_.frame.node(3).origin(2,:) + SID_EA2_.frame.node(3).phi(2,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA3_2_rot_x_ =  SID_EA3_.frame.node(2).orientation(1) + SID_EA3_.frame.node(2).psi(1,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_rot_z_ =  SID_EA3_.frame.node(2).orientation(3) + SID_EA3_.frame.node(2).psi(3,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_x_ =  SID_EA3_.frame.node(2).origin(1,:) + SID_EA3_.frame.node(2).phi(1,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_y_ =  SID_EA3_.frame.node(2).origin(2,:) + SID_EA3_.frame.node(2).phi(2,:) * x_(sys.model.body.EA3.data.edof.idx);
if calcVelo_

	% generalized coordinates, velocities and auxiliary coordinates

	Dr_alpha1 = x_(5);
	Dr_beta2 = x_(6);
	DEA2_q001 = x_(7);
	DEA3_q001 = x_(8);
	% system inputs
	Dp_a = u_(2);
	Dp_b = u_(5);
	Dr_gamma3 = u_(8);

	% Automatically introduced abbreviations

	SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
	SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
	SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

	DelBo_EA1_2_x_ = SID_EA1_.frame.node(2).phi(1,:) * x_(sys.model.body.EA1.data.edof.idx+4);
	DelBo_EA2_1_rot_z_ = SID_EA2_.frame.node(1).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx+4);
	DelBo_EA2_1_x_ = SID_EA2_.frame.node(1).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
	DelBo_EA2_2_rot_z_ = SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx+4);
	DelBo_EA2_2_x_ = SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
	DelBo_EA2_3_rot_z_ = SID_EA2_.frame.node(3).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx+4);
	DelBo_EA2_3_x_ = SID_EA2_.frame.node(3).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
	DelBo_EA2_3_y_ = SID_EA2_.frame.node(3).phi(2,:) * x_(sys.model.body.EA2.data.edof.idx+4);
	DelBo_EA3_2_rot_x_ = SID_EA3_.frame.node(2).psi(1,:) * x_(sys.model.body.EA3.data.edof.idx+4);
	DelBo_EA3_2_rot_z_ = SID_EA3_.frame.node(2).psi(3,:) * x_(sys.model.body.EA3.data.edof.idx+4);
	DelBo_EA3_2_x_ = SID_EA3_.frame.node(2).phi(1,:) * x_(sys.model.body.EA3.data.edof.idx+4);
	DelBo_EA3_2_y_ = SID_EA3_.frame.node(2).phi(2,:) * x_(sys.model.body.EA3.data.edof.idx+4);
end

%% Absolute kinematics of coordinate system ISYS


% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;


% Vector of absolute position
r_abs_ = r_rel_;
data_.ISYS.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_rel_;
data_.ISYS.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_rel_;
	data_.ISYS.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_rel_;
	data_.ISYS.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system A1

r_ref_ = data_.ISYS.r_abs;
S_ref_ = data_.ISYS.S_abs;
if calcVelo_
	v_ref_ = data_.ISYS.v_abs;
	omega_ref_ = data_.ISYS.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.A1.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.A1.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.A1.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.A1.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system C1

r_ref_ = data_.A1.r_abs;
S_ref_ = data_.A1.S_abs;
if calcVelo_
	v_ref_ = data_.A1.v_abs;
	omega_ref_ = data_.A1.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(2) = p_a;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.C1.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.C1.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(2) = Dp_a;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.C1.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.C1.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system C1_cg

r_ref_ = data_.C1.r_abs;
S_ref_ = data_.C1.S_abs;
if calcVelo_
	v_ref_ = data_.C1.v_abs;
	omega_ref_ = data_.C1.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.C1_cg.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.C1_cg.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.C1_cg.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.C1_cg.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA1

r_ref_ = data_.C1.r_abs;
S_ref_ = data_.C1.S_abs;
if calcVelo_
	v_ref_ = data_.C1.v_abs;
	omega_ref_ = data_.C1.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(3) = z_0;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(r_alpha1);
S_rel_(2,1) = sin(r_alpha1);
S_rel_(1,2) = -1.0*sin(r_alpha1);
S_rel_(2,2) = cos(r_alpha1);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA1.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA1.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = Dr_alpha1;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA1.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA1.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA1_1

r_ref_ = data_.EA1.r_abs;
S_ref_ = data_.EA1.S_abs;
if calcVelo_
	v_ref_ = data_.EA1.v_abs;
	omega_ref_ = data_.EA1.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA1_1.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA1_1.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA1_1.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA1_1.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA1_2

r_ref_ = data_.EA1.r_abs;
S_ref_ = data_.EA1.S_abs;
if calcVelo_
	v_ref_ = data_.EA1.v_abs;
	omega_ref_ = data_.EA1.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = elBo_EA1_2_x_;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA1_2.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA1_2.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(1) = DelBo_EA1_2_x_;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA1_2.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA1_2.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system A2

r_ref_ = data_.ISYS.r_abs;
S_ref_ = data_.ISYS.S_abs;
if calcVelo_
	v_ref_ = data_.ISYS.v_abs;
	omega_ref_ = data_.ISYS.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = d_axes;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.A2.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.A2.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.A2.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.A2.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system C2

r_ref_ = data_.A2.r_abs;
S_ref_ = data_.A2.S_abs;
if calcVelo_
	v_ref_ = data_.A2.v_abs;
	omega_ref_ = data_.A2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = p_b;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.C2.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.C2.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(1) = Dp_b;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.C2.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.C2.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system C2_cg

r_ref_ = data_.C2.r_abs;
S_ref_ = data_.C2.S_abs;
if calcVelo_
	v_ref_ = data_.C2.v_abs;
	omega_ref_ = data_.C2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.C2_cg.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.C2_cg.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.C2_cg.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.C2_cg.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA2_2

r_ref_ = data_.C2.r_abs;
S_ref_ = data_.C2.S_abs;
if calcVelo_
	v_ref_ = data_.C2.v_abs;
	omega_ref_ = data_.C2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(3) = z_0;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(r_beta2);
S_rel_(2,1) = sin(r_beta2);
S_rel_(1,2) = -1.0*sin(r_beta2);
S_rel_(2,2) = cos(r_beta2);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA2_2.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA2_2.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = Dr_beta2;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA2_2.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA2_2.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA2

r_ref_ = data_.EA2_2.r_abs;
S_ref_ = data_.EA2_2.S_abs;
if calcVelo_
	v_ref_ = data_.EA2_2.v_abs;
	omega_ref_ = data_.EA2_2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = -1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_);
r_rel_(2) = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_);

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(elBo_EA2_2_rot_z_);
S_rel_(2,1) = -1.0*sin(elBo_EA2_2_rot_z_);
S_rel_(1,2) = sin(elBo_EA2_2_rot_z_);
S_rel_(2,2) = cos(elBo_EA2_2_rot_z_);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA2.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA2.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(1) = DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_);
	v_rel_(2) = DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_);

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = -1.0*DelBo_EA2_2_rot_z_;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA2.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA2.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA2_1

r_ref_ = data_.EA2.r_abs;
S_ref_ = data_.EA2.S_abs;
if calcVelo_
	v_ref_ = data_.EA2.v_abs;
	omega_ref_ = data_.EA2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = elBo_EA2_1_x_;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(elBo_EA2_1_rot_z_);
S_rel_(2,1) = sin(elBo_EA2_1_rot_z_);
S_rel_(1,2) = -1.0*sin(elBo_EA2_1_rot_z_);
S_rel_(2,2) = cos(elBo_EA2_1_rot_z_);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA2_1.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA2_1.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(1) = DelBo_EA2_1_x_;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = DelBo_EA2_1_rot_z_;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA2_1.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA2_1.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA2_3

r_ref_ = data_.EA2.r_abs;
S_ref_ = data_.EA2.S_abs;
if calcVelo_
	v_ref_ = data_.EA2.v_abs;
	omega_ref_ = data_.EA2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = elBo_EA2_3_x_;
r_rel_(2) = elBo_EA2_3_y_;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(elBo_EA2_3_rot_z_);
S_rel_(2,1) = sin(elBo_EA2_3_rot_z_);
S_rel_(1,2) = -1.0*sin(elBo_EA2_3_rot_z_);
S_rel_(2,2) = cos(elBo_EA2_3_rot_z_);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA2_3.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA2_3.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(1) = DelBo_EA2_3_x_;
	v_rel_(2) = DelBo_EA2_3_y_;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = DelBo_EA2_3_rot_z_;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA2_3.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA2_3.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA3

r_ref_ = data_.EA2_3.r_abs;
S_ref_ = data_.EA2_3.S_abs;
if calcVelo_
	v_ref_ = data_.EA2_3.v_abs;
	omega_ref_ = data_.EA2_3.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(r_gamma3);
S_rel_(2,1) = sin(r_gamma3);
S_rel_(1,2) = -1.0*sin(r_gamma3);
S_rel_(2,2) = cos(r_gamma3);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA3.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA3.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = Dr_gamma3;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA3.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA3.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA3_1

r_ref_ = data_.EA3.r_abs;
S_ref_ = data_.EA3.S_abs;
if calcVelo_
	v_ref_ = data_.EA3.v_abs;
	omega_ref_ = data_.EA3.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA3_1.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA3_1.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA3_1.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA3_1.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EA3_2

r_ref_ = data_.EA3.r_abs;
S_ref_ = data_.EA3.S_abs;
if calcVelo_
	v_ref_ = data_.EA3.v_abs;
	omega_ref_ = data_.EA3.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = elBo_EA3_2_x_;
r_rel_(2) = elBo_EA3_2_y_;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(elBo_EA3_2_rot_z_);
S_rel_(2,1) = cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
S_rel_(3,1) = sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
S_rel_(1,2) = -1.0*sin(elBo_EA3_2_rot_z_);
S_rel_(2,2) = cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_);
S_rel_(3,2) = cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_);
S_rel_(2,3) = -1.0*sin(elBo_EA3_2_rot_x_);
S_rel_(3,3) = cos(elBo_EA3_2_rot_x_);

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EA3_2.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EA3_2.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(1) = DelBo_EA3_2_x_;
	v_rel_(2) = DelBo_EA3_2_y_;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(1) = DelBo_EA3_2_rot_x_;
	omega_rel_(3) = DelBo_EA3_2_rot_z_;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EA3_2.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EA3_2.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EE

r_ref_ = data_.EA3_2.r_abs;
S_ref_ = data_.EA3_2.S_abs;
if calcVelo_
	v_ref_ = data_.EA3_2.v_abs;
	omega_ref_ = data_.EA3_2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EE.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EE.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EE.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EE.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EE_cg

r_ref_ = data_.EE.r_abs;
S_ref_ = data_.EE.S_abs;
if calcVelo_
	v_ref_ = data_.EE.v_abs;
	omega_ref_ = data_.EE.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EE_cg.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EE_cg.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EE_cg.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EE_cg.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system HD_pos_rlc

r_ref_ = data_.EA2.r_abs;
S_ref_ = data_.EA2.S_abs;
if calcVelo_
	v_ref_ = data_.EA2.v_abs;
	omega_ref_ = data_.EA2.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = l2;
r_rel_(2) = EA2_q001*phi2_1*w2_1;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.HD_pos_rlc.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.HD_pos_rlc.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(2) = DEA2_q001*phi2_1*w2_1;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.HD_pos_rlc.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.HD_pos_rlc.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system HD_rot_rlc

r_ref_ = data_.HD_pos_rlc.r_abs;
S_ref_ = data_.HD_pos_rlc.S_abs;
if calcVelo_
	v_ref_ = data_.HD_pos_rlc.v_abs;
	omega_ref_ = data_.HD_pos_rlc.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(r_gamma3 + EA2_q001*psi2_1*v2_1);
S_rel_(2,1) = sin(r_gamma3 + EA2_q001*psi2_1*v2_1);
S_rel_(1,2) = -1.0*sin(r_gamma3 + EA2_q001*psi2_1*v2_1);
S_rel_(2,2) = cos(r_gamma3 + EA2_q001*psi2_1*v2_1);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.HD_rot_rlc.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.HD_rot_rlc.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = Dr_gamma3 + DEA2_q001*psi2_1*v2_1;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.HD_rot_rlc.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.HD_rot_rlc.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EE_pos_rlc

r_ref_ = data_.HD_rot_rlc.r_abs;
S_ref_ = data_.HD_rot_rlc.S_abs;
if calcVelo_
	v_ref_ = data_.HD_rot_rlc.v_abs;
	omega_ref_ = data_.HD_rot_rlc.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);

r_rel_(1) = l3;
r_rel_(2) = EA3_q001*phi3_1*w3_1;

% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = 1.0;
S_rel_(2,2) = 1.0;
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EE_pos_rlc.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EE_pos_rlc.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);

	v_rel_(2) = DEA3_q001*phi3_1*w3_1;

	% Relative rotational velocity
	omega_rel_ = zeros(3,1);


	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EE_pos_rlc.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EE_pos_rlc.omega_abs = omega_abs_;
end


%% Absolute kinematics of coordinate system EErlc

r_ref_ = data_.EE_pos_rlc.r_abs;
S_ref_ = data_.EE_pos_rlc.S_abs;
if calcVelo_
	v_ref_ = data_.EE_pos_rlc.v_abs;
	omega_ref_ = data_.EE_pos_rlc.omega_abs;
end

% Vector of relative position
r_rel_ = zeros(3,1);


% Relative rotational matrix
S_rel_ = zeros(3,3);

S_rel_(1,1) = cos(EA3_q001*psi3_1*v3_1);
S_rel_(2,1) = sin(EA3_q001*psi3_1*v3_1);
S_rel_(1,2) = -1.0*sin(EA3_q001*psi3_1*v3_1);
S_rel_(2,2) = cos(EA3_q001*psi3_1*v3_1);
S_rel_(3,3) = 1.0;

% Vector of absolute position
r_abs_ = r_ref_ + S_ref_*r_rel_;
data_.EErlc.r_abs = r_abs_;

% Absolute rotational matrix
S_abs_ = S_ref_*S_rel_;
data_.EErlc.S_abs = S_abs_;

if calcVelo_
	% Vector of relative velocity
	v_rel_ = zeros(3,1);


	% Relative rotational velocity
	omega_rel_ = zeros(3,1);

	omega_rel_(3) = DEA3_q001*psi3_1*v3_1;

	% Vector of absolute velocity
	v_abs_ = v_ref_ + S_ref_*v_rel_ - vec2skew(S_ref_*r_rel_)*omega_ref_;
	data_.EErlc.v_abs = v_abs_;

	% Absolute rotational velocity
	omega_abs_ = omega_ref_ + S_ref_*omega_rel_;
	data_.EErlc.omega_abs = omega_abs_;
end


% END OF FILE

