function result_ = eqm_sysout_Curvature(t, x_, u_, varargin)
% eqm_sysout_Curvature - Vector of the system Flexor output Curvature
% 
% Entries are as in sys.model.output.Curvature

global sys;

% generalized coordinates, velocities and auxiliary coordinates

EA2_q001 = x_(3);
EA3_q001 = x_(4);
DEA2_q001 = x_(7);
DEA3_q001 = x_(8);

% System output Curvature vector
Curvature_1_EA2 = (1523687816480699*EA2_q001)/2251799813685248;
Curvature_1_EA3 = (2436527625782447*EA3_q001)/1125899906842624;
DCurvature_1_EA2 = (1523687816480699*DEA2_q001)/2251799813685248;
DCurvature_1_EA3 = (2436527625782447*DEA3_q001)/1125899906842624;

result_ = zeros(4,1);

result_(1) = Curvature_1_EA2;
result_(2) = Curvature_1_EA3;
result_(3) = DCurvature_1_EA2;
result_(4) = DCurvature_1_EA3;

% END OF FILE 
