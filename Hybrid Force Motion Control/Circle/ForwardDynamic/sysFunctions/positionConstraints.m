function c_ = positionConstraints(t, x_, u_, varargin)

global sys;

% generalized coordinates, velocities and auxiliary coordinates

r_alpha1 = x_(1);
r_beta2 = x_(2);
EA2_q001 = x_(3);
EA3_q001 = x_(4);
% system inputs
p_a = u_(1);
p_b = u_(4);
r_gamma3 = u_(7);

% constant user-defined variables
Ox = sys.parameters.data.Ox;
Oy = sys.parameters.data.Oy;
Radius = sys.parameters.data.Radius;
d_axes = sys.parameters.data.d_axes;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

elBo_EA1_2_x_ =  SID_EA1_.frame.node(2).origin(1,:) + SID_EA1_.frame.node(2).phi(1,:) * x_(sys.model.body.EA1.data.edof.idx);
elBo_EA2_1_x_ =  SID_EA2_.frame.node(1).origin(1,:) + SID_EA2_.frame.node(1).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_x_ =  SID_EA2_.frame.node(2).origin(1,:) + SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);


% Constraints on position level

c_ = zeros(3,1);

c_(1) = d_axes + p_b - 1.0*elBo_EA1_2_x_*cos(r_alpha1) + elBo_EA2_1_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
c_(2) = - 1.0*p_a - 1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_1_x_ - 1.0*elBo_EA2_2_x_) - 1.0*elBo_EA1_2_x_*sin(r_alpha1);
c_(3) = (0.6*sin(0.12720812859558897067735472319328*EA2_q001 - 1.0*r_beta2) - 1.0*Oy + 0.05177806232203822911497326231256*EA2_q001*cos(0.12720812859558897067735472319328*EA2_q001 - 1.0*r_beta2) + 0.11086564964710307612527628862154*EA3_q001*cos(0.0035411566804658678453421316589811*EA2_q001 + r_beta2 + r_gamma3) + sin(0.12720812859558897067735472319328*EA2_q001 - 1.0*r_beta2)*(0.00000000000075504122110177805935058228092758*EA2_q001 - 1.0) - 1.0*sin(0.0035411566804658678453421316589811*EA2_q001 + r_beta2 + r_gamma3)*(0.0000000000079072648755528962870184095894835*EA3_q001 - 0.4643))^2 - 1.0*Radius^2 + (Ox - 1.0*d_axes - 1.0*p_b + 0.6*cos(0.12720812859558897067735472319328*EA2_q001 - 1.0*r_beta2) - 0.05177806232203822911497326231256*EA2_q001*sin(0.12720812859558897067735472319328*EA2_q001 - 1.0*r_beta2) + 0.11086564964710307612527628862154*EA3_q001*sin(0.0035411566804658678453421316589811*EA2_q001 + r_beta2 + r_gamma3) + cos(0.12720812859558897067735472319328*EA2_q001 - 1.0*r_beta2)*(0.00000000000075504122110177805935058228092758*EA2_q001 - 1.0) + cos(0.0035411566804658678453421316589811*EA2_q001 + r_beta2 + r_gamma3)*(0.0000000000079072648755528962870184095894835*EA3_q001 - 0.4643))^2;


% END OF FILE

