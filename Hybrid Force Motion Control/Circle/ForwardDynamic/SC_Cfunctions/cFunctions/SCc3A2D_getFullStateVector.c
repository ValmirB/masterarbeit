#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "SCc3A2D_pi_code.h"
#include "SCc3A2D_userDefined.h"
#include "SCc3A2D_pd_matlab.h"
#include "neweul.h"
#include "SCc3A2D_paraStruct.h"
#include "SCc3A2D_Flexor.h"

#include "SCc3A2D_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double *t_vec             = mxGetPr( prhs[0] );
	double t                  = 0;
	double *x                 = mxGetPr( prhs[1] );
	double *y_ind             = NULL;
	double *Dy_ind            = NULL;
	double *matlabReturn      = NULL;
	double *u_                = NULL;
	size_t numStates          = 1;
	int counter_              = 0;
	int vec_loop              = 0;
	struct SCc3A2D_paraStruct *data = NULL;

#if defined SCC3A2D_NUM_ALL_INPUTS && SCC3A2D_NUM_ALL_INPUTS > 0
	u_ = calloc(SCC3A2D_NUM_ALL_INPUTS, sizeof(double));
#endif

	/* Initialize System Struct */
	dataPtr = SCc3A2D_initializeSystemStruct();

	data  = (struct SCc3A2D_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!");
		goto clean_up;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!");
		goto clean_up;
	}

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		SCc3A2D_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	/* Get number of right-hand sides */
	numStates = mxGetN(prhs[1]);

	if (((mxGetM(prhs[0]) * mxGetN(prhs[0])) != numStates)){
		mexPrintf("The dimensions of the passed arguments don not match!\n");
		goto clean_up;
	}

	if ((mxGetM(prhs[1])!=2*SCC3A2D_NUM_INDEPENDENT_GC)){
		mexPrintf("The second input argument has to be a 4xnumStates vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*SCC3A2D_SYS_DOF, numStates, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	for ( vec_loop = 0; vec_loop < (int) numStates; vec_loop++){

		t = t_vec[vec_loop];
		y_ind = &(x[2*SCC3A2D_NUM_INDEPENDENT_GC*vec_loop]);
		Dy_ind = &(x[2*SCC3A2D_NUM_INDEPENDENT_GC*vec_loop+SCC3A2D_NUM_INDEPENDENT_GC]);

		/* Get dependent generlaized coordinates and velocities */
		for ( counter_ = 0 ; counter_ < SCC3A2D_NUM_INDEPENDENT_GC ; counter_++ ){
			matlabReturn[vec_loop*2*SCC3A2D_SYS_DOF+data->independentIndices[counter_]]  =  y_ind[counter_];
			matlabReturn[vec_loop*2*SCC3A2D_SYS_DOF+data->independentIndices[counter_]+SCC3A2D_SYS_DOF] = Dy_ind[counter_];
		}

		SCc3A2D_educatedGuess(t, &(matlabReturn[vec_loop*2*SCC3A2D_SYS_DOF]), dataPtr);

	/* Compute all inputs */
	SCc3A2D_f_endeffector_acceleration_inputs(t, &(matlabReturn[vec_loop*2*SCC3A2D_SYS_DOF]), &(u_[0]), dataPtr);
	SCc3A2D_f_contactforce_inputs(t, &(matlabReturn[vec_loop*2*SCC3A2D_SYS_DOF]), &(u_[9]), dataPtr);

		SCc3A2D_getDependentGenCoords(t, &(matlabReturn[vec_loop*2*SCC3A2D_SYS_DOF]), u_, dataPtr);

	}

	/* Free allocated memory */
clean_up:
	SCc3A2D_freeStructure(dataPtr);
#if defined SCC3A2D_NUM_ALL_INPUTS && SCC3A2D_NUM_ALL_INPUTS > 0
	free(u_);
#endif

}

