#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "SCc3A2D_pi_code.h"
#include "SCc3A2D_userDefined.h"
#include "SCc3A2D_pd_matlab.h"
#include "neweul.h"
#include "SCc3A2D_paraStruct.h"
#include "SCc3A2D_Flexor.h"

#include "SCc3A2D_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double t             = mxGetScalar( prhs[0] );
	double *x_red        = mxGetPr( prhs[1] );
	double *y_ind        = &(x_red[0]);
	double *Dy_ind       = &(x_red[SCC3A2D_NUM_INDEPENDENT_GC]);
	double *x_           = calloc( 2*SCC3A2D_SYS_DOF, sizeof(double));
	double **Jr          = callocMatrix( SCC3A2D_SYS_DOF,SCC3A2D_NUM_INDEPENDENT_GC);
	double **Jl          = callocMatrix( SCC3A2D_SYS_DOF,SCC3A2D_NUM_INDEPENDENT_GC);
	double *f_           = calloc( SCC3A2D_SYS_DOF, sizeof(double));
	double **M_          = callocMatrix( SCC3A2D_SYS_DOF, SCC3A2D_SYS_DOF);
	double **Mred        = callocMatrix(SCC3A2D_NUM_INDEPENDENT_GC, SCC3A2D_NUM_INDEPENDENT_GC);
	double **MJr         = callocMatrix( SCC3A2D_SYS_DOF, SCC3A2D_NUM_INDEPENDENT_GC);
	double *KD           = calloc(2*SCC3A2D_SYS_DOF*SCC3A2D_NUM_INDEPENDENT_GC, sizeof(double));
	double *matlabReturn = NULL;
	double *u_           = calloc(SCC3A2D_NUM_ALL_INPUTS, sizeof(double));
	unsigned short *z    = calloc(SCC3A2D_NUM_INDEPENDENT_GC, sizeof(unsigned short));
	int counter_         = 0;
	int i,j;
	double myStep = 1e-8;
	struct SCc3A2D_paraStruct *data = NULL;

	dataPtr = SCc3A2D_initializeSystemStruct();

	data  = (struct SCc3A2D_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		goto clean_up;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		goto clean_up;
	}

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		SCc3A2D_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	if ((mxGetM(prhs[1])!=2*SCC3A2D_NUM_INDEPENDENT_GC) || (mxGetN(prhs[1])!=1)){
		mexPrintf("The second input argument has to be a 4x1 vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*SCC3A2D_NUM_INDEPENDENT_GC, 2*SCC3A2D_NUM_INDEPENDENT_GC, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	/* Get dependent generalized coordinates and velocities */
	for ( counter_ = 0 ; counter_ < SCC3A2D_NUM_INDEPENDENT_GC ; counter_++ ){
		x_[data->independentIndices[counter_]]  =  y_ind[counter_];
		x_[data->independentIndices[counter_]+SCC3A2D_SYS_DOF] = Dy_ind[counter_];
	}

	SCc3A2D_educatedGuess(t, x_, dataPtr);

	SCc3A2D_getDependentGenCoords(t, x_, u_, dataPtr);

	
	SCc3A2D_jacobianPartition(t, x_, u_, Jr, Jl, dataPtr);
	
	/* Loop over all position values */

	for (i=0; i<SCC3A2D_NUM_INDEPENDENT_GC; i++){
		clearVector(SCC3A2D_SYS_DOF,f_);
		clearVector(SCC3A2D_NUM_ALL_INPUTS,u_);
		clearMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M_);

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		SCc3A2D_f_endeffector_acceleration_inputs(t, x_, &(u_[0]), dataPtr);
		SCc3A2D_f_contactforce_inputs(t, x_, &(u_[9]), dataPtr);
		SCc3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			KD[SCC3A2D_SYS_DOF*i+j] = f_[j];

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			x_[j] = x_[j] - 2.0*Jr[j][i]*myStep;

		clearVector(SCC3A2D_SYS_DOF,f_);
		clearMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M_);

		SCc3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			KD[SCC3A2D_SYS_DOF*i+j] = (KD[SCC3A2D_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		clearVector(SCC3A2D_SYS_DOF,f_);
		clearVector(SCC3A2D_NUM_ALL_INPUTS,u_);
		clearMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M_);

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			x_[j+SCC3A2D_SYS_DOF] = x_[j+SCC3A2D_SYS_DOF] + Jr[j][i]*myStep;

		SCc3A2D_f_endeffector_acceleration_inputs(t, x_, &(u_[0]), dataPtr);
		SCc3A2D_f_contactforce_inputs(t, x_, &(u_[9]), dataPtr);
		SCc3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			KD[SCC3A2D_NUM_INDEPENDENT_GC*SCC3A2D_SYS_DOF+SCC3A2D_SYS_DOF*i+j] = f_[j];

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			x_[j+SCC3A2D_SYS_DOF] = x_[j+SCC3A2D_SYS_DOF] - 2.0*Jr[j][i]*myStep;

		clearVector(SCC3A2D_SYS_DOF,f_);
		clearMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M_);

		SCc3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			KD[SCC3A2D_NUM_INDEPENDENT_GC*SCC3A2D_SYS_DOF+SCC3A2D_SYS_DOF*i+j] = (KD[SCC3A2D_NUM_INDEPENDENT_GC*SCC3A2D_SYS_DOF+SCC3A2D_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<SCC3A2D_SYS_DOF; j++)
			x_[j+SCC3A2D_SYS_DOF] = x_[j+SCC3A2D_SYS_DOF] + Jr[j][i]*myStep;

	}

	MatrixMultiplication( 'N', 'N', SCC3A2D_SYS_DOF, SCC3A2D_SYS_DOF, SCC3A2D_NUM_INDEPENDENT_GC, 1.0, M_, Jr, 0.0, MJr); /* MJr allokieren */

	MatrixMultiplication( 'T', 'N', SCC3A2D_NUM_INDEPENDENT_GC, SCC3A2D_SYS_DOF, SCC3A2D_NUM_INDEPENDENT_GC, 1.0, Jl, MJr, 0.0, Mred); /* Mred allokieren */

	GaussianElimination( SCC3A2D_NUM_INDEPENDENT_GC, z, Mred);

	for (i=0; i<SCC3A2D_NUM_INDEPENDENT_GC; i++)
		matlabReturn[(2*(SCC3A2D_NUM_INDEPENDENT_GC+i))*SCC3A2D_NUM_INDEPENDENT_GC+i] = 1.0;

	for (i=0; i<2*SCC3A2D_NUM_INDEPENDENT_GC; i++){
		MatrixVectorMultiplication( 'T', SCC3A2D_NUM_INDEPENDENT_GC, SCC3A2D_SYS_DOF, 1.0, Jl, &(KD[SCC3A2D_SYS_DOF*i]), 0.0, &(matlabReturn[2*SCC3A2D_NUM_INDEPENDENT_GC*i+SCC3A2D_NUM_INDEPENDENT_GC]));
		permuteVector(SCC3A2D_NUM_INDEPENDENT_GC, z, &(matlabReturn[2*SCC3A2D_NUM_INDEPENDENT_GC*i+SCC3A2D_NUM_INDEPENDENT_GC]));
		LUForBack_1dim( SCC3A2D_NUM_INDEPENDENT_GC, Mred, &(matlabReturn[2*SCC3A2D_NUM_INDEPENDENT_GC*i+SCC3A2D_NUM_INDEPENDENT_GC]));
	}

clean_up:

	free(x_);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_NUM_INDEPENDENT_GC,Jr);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_NUM_INDEPENDENT_GC,Jl);
	free(f_);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M_);
	freeMatrix(SCC3A2D_NUM_INDEPENDENT_GC,SCC3A2D_NUM_INDEPENDENT_GC,Mred);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_NUM_INDEPENDENT_GC,MJr);
	free(KD);
	free(u_);
	free(z);

	/* Free System Struct */
	SCc3A2D_freeStructure(dataPtr);

}
