#include "simstruc.h"
#include "malloc.h"
#include <math.h>
#include "SDOF_Kalman_snvna.h"
#include "neweul.h"

#if defined(MATLAB_MEX_FILE) && MATLAB_MEX_FILE
void checkParams(SimStruct *S, int numout, int num_ma){
 
    /* Dialog Parameters of the S-Functions are checked */
    
    int ii=0;
    double *typePtr = NULL;
    double *tauPtr  = NULL;
    double *RPtr    = NULL;
    double *epsPtr  = NULL;
    double *rjctPtr = NULL;
    
    /* Double Check whether all but the last Dialog Parameters have the correct number of elements */
    for (ii=0; ii<(NPARAMS-1); ii++){ /* Order = DialogParameters */
        if ((ii!=1) && (((int) mxGetNumberOfElements(ssGetSFcnParam(S,ii))) != numout)){
            /* If an empty array has been provided */
            ssSetErrorStatus(S,"Not all Dialog Parameters have the correct number of elements");
            return;
        }
    }     
    if (((int)  mxGetNumberOfElements(ssGetSFcnParam(S,1))) != pow(numout, 2)) {
            ssSetErrorStatus(S,"Measurement Noise Variance R must equal the dimensions: (numout)x(numout)");
            return;
    }
    
    typePtr  = (double *) mxGetPr(ssGetSFcnParam(S, 0)); /* Type of noise Model */
    RPtr     = (double *) mxGetPr(ssGetSFcnParam(S, 1));  /* Variances for Sensors */
    epsPtr   = (double *) mxGetPr(ssGetSFcnParam(S, 2));  /* Tune Parameters for the Noise Model */
    tauPtr   = (double *) mxGetPr(ssGetSFcnParam(S, 3)); /* Correlation Time */    
    rjctPtr  = (double *) mxGetPr(ssGetSFcnParam(S, 4)); /* Rejection Type */ 
    
    /* Each Parameter for each input signal is double-checked */
    for (ii=0; ii < numout; ii++) {
        if ( (((int) typePtr[ii]) < 0) || (((int) typePtr[ii]) > ((int) NTYPES)) ) {
            ssSetErrorStatus(S,"Type of Additive Noise Model must be inbetween 0 (None) and 13");
            return;
        }
        /* The Diagonal Elements of the Measurement Variance Matrix must be bigger than zero */
        if (RPtr[ii*numout+ii]<=0.0){
            ssSetErrorStatus(S,"The Diagonal elements of the Measurement Variance Rk must be bigger than zero");
            return;
        }
        if (epsPtr[ii]<=0.0){
            ssSetErrorStatus(S,"Tune Parameters for Additive Model Variances must be bigger than zero");
            return;
        }
        if (((((int) typePtr[ii])==5) || (((int) typePtr[ii])==6)) && (tauPtr[ii]<=0.0)){ /* Correlation Times must be bigger than zero */
            ssSetErrorStatus(S,"Correlation Time tau must be greater than zero for Singer Acceleration Model");
            return;
        }        
        if (((((int) typePtr[ii])==11) || (((int) typePtr[ii])==12)) && (tauPtr[ii]<=0.0)){ /* Correlation Times must be bigger than zero */
            ssSetErrorStatus(S,"Correlation Time tau must be greater than zero for Singer Acceleration Model");
            return;
        }
        if (((((int) typePtr[ii])==17) || (((int) typePtr[ii])==18)) && (tauPtr[ii]<=0.0)){ /* Correlation Times must be bigger than zero */
            ssSetErrorStatus(S,"Correlation Time tau must be greater than zero for Singer Jerk Model");
            return;
        }
        if (((int) rjctPtr[ii] != 0) && ((int) rjctPtr[ii] != 1) && ((int) rjctPtr[ii] != 2)){
            ssSetErrorStatus(S,"Switch for Outlier Rejection must be either 0,1,2");
            return;
        }       
    }
    #if ( defined(STEADYSTATE) && STEADYSTATE )
        /* Parameter 6 must be the steady state Kalman Gains of the system */
        if (((int)  mxGetNumberOfElements(ssGetSFcnParam(S, 5))) != (num_ma*DOF*numout)) {
            ssSetErrorStatus(S,"Steady State Kalman Gain (Parameter 6) must equal the dimensions: dim x num_ma");
            return;
        }   
    #endif
}
#endif

void sdofSteadyStateK_k(SimStruct *S, int numout, int dim, double **K_k, double **dlgParam){
 
    /* The Kalman Gain must be defined as the transposed one (numout x dim)  */
    
    int i = 0;
    int j = 0;
    double *sskgPtr = NULL;
    
    sskgPtr = dlgParam[5];
    
    /* Copy Matrix from Dialog Parameters to transposed! Kalman Gain Matrix */
    for (i = 0; i < numout; i++) {    /* COLS */     
        for (j = 0; j < dim; j++)    /* ROWS */
            K_k[i][j] = sskgPtr[i * dim + j];
    }
}


void inixhatPhat(SimStruct *S, double tstep, int dim, double *xhat, int dimP, double **errorUpdate, double **dlgParam){

    /* Estimators for States after the first measurement have been made */
    /* S                S-Function Structure of Simulink
     * TSTEP            (Current) Simulation Time Step [s]
     * DIM              System Dimension (Length of the state vector)
     * XHAT             Overall State Vector (of all inputs)
     * DIMP             Dimension / Size of the trailing errorUpdate 
     * ERRORUPDATE      A posteriori Error Covariance of the last time step
     * DLGPARAM         Pointer to the dialog Parameters (In RTW Applications, 
     *                  mx* C-Functions cannot be used in an external sourcecode
     */
    
    int i   = 0;
    int dof = 0;
    int numout = ssGetInputPortWidth(S, 0); 
    
    InputRealPtrsType meas = ssGetInputPortRealSignalPtrs(S, 0); /* Sensor Data*/    
       
    dof = (int) dim/numout; /* dof = degrees of freedom per measurement */
    
    /* Position estimate will be updated by Sensor Data */
    for (i=0; i<numout; i++)
        xhat[i*dof] = *meas[i];
    
    /* Initialize Error Covariance with an Eye Matrix */
    for (i = 0; i < dimP; i++)
        errorUpdate[i][i] = 1.0;   
}

void sdofDynamics(SimStruct *S, double tstep, int dim, double **Phi, double *x, double *xhat, double *abar, double **dlgParam){

    /* Discrete-time system Dynamics of the Single Degrees of Freedom System */
    /* S                S-Function Structure of Simulink
     * TSTEP            (Current) Simulation Time Step [s]
     * DIM              System Dimension (Length of the state vector)
     * PHI              Discrete-Time State Transition Matrix 
     * X                Current Sigma Point (of all inputs)
     * XHAT             A posteriori state estimate of last time step
     * ABAR             Mean-Adaptive Velocity/Acceleration/Jerk (depends on noise model)
     * DLGPARAM         Pointer to the dialog Parameters (In RTW Applications, 
     *                  mx* C-Functions cannot be used in an external sourcecode
     */
    
    int i       = 0;
    int n       = 0;
    int dof     = 0;
    int ii      = 0;
    int jj      = 0;
    int nstates = 0;
    
    double *type = NULL;
    double *tau  = NULL;
    double *tmpx = NULL;
    double *fac  = NULL; 
    
    double E     = 0.0;
    double the   = 0.0;
    double summe = 0.0;
    
    int numout = ssGetInputPortWidth(S, 0);
    
    #if USEADES
        InputRealPtrsType ades = ssGetInputPortRealSignalPtrs(S, 1); /* Desired Acceleration */          
    #endif
    
    type = dlgParam[0]; /* Type of Noise Model */
    tau  = dlgParam[3];  /* Correlation Time */

    dof = (int) dim/numout; /* dof = degrees of freedom per measurement */

    tmpx = (double *) calloc(dim, sizeof(double));  
    
    memcpy(tmpx, x, dim*sizeof(double)); /* Store "old" values of xhat */
    clearMatrix(dim, dim, Phi); /* Reset Discrete Transition Matrix */
    clearVector(dim, x);  /* Reset States */
    
    fac = (double *) calloc(dof, sizeof(double)); /* Memory Allocation for the factorial */
    
    fac[0] = 1.0;
    for ( i=1 ; i < dof; i++) /* Computation of the factorial */
        fac[i]=((double) i)*fac[i-1];
    
    /* STARTING COMPUTATIONS */
    for (i=0; i<numout; i++) { /* Separate Calculations for each input */
        n = (int) dof * i; /* Start element in x for each input */

        /* Type of Noise Model determines number order of Pade-Approximation */
        if ((int) type[i] == 0) {
            nstates = dof;
        } else if ((int) type[i] <= 6) { 
            nstates = 2; 
        } else if ((int) type[i] <= 12) { 
            nstates = 3; 
        } else if ((int) type[i] <= 18) {
            nstates = 4; 
        } else {
            nstates = dof;
        }
        
        /* Pade-Approximation Phi(T)= T^(j-i)/(j-i)! */
        for (ii = 0; ii < nstates; ii++) {
            for (jj = ii; jj < nstates; jj++) /* Upper triangle */
                Phi[n+ii][n+jj] = pow(tstep, (double) jj-ii)/fac[jj-ii];
        }
           
        /* Modification of the last column for the exponential correlated noise models */
        if (((int) type[i] == 5) || ((int) type[i] == 6)){ /* Singer Velocity Model */
            E = exp(-tstep/tau[i]);  /* Abbrevation 1 */
            /* the = tstep/tau[i];      Abbrevation 2 */
            
            Phi[n+0][n+1] = tau[i] * (1.0 - E);
            Phi[n+1][n+1] = E;    
               
            if ((int) type[i] == 6) { /* Mean Adaptive Singer Velocity Model */
                
                #if CAPAMEAN /* Sigma Points Mean Velocity used */
                    /*abar[i] = tmpx[n+1]; */
                    abar[i] = E * tmpx[n+1] + (1.0 - E) * abar[i];
                #else
                    /*abar[i] = xhat[n+1]; */
                    abar[i] = E * xhat[n+1] + (1.0 - E) * abar[i];       
                #endif
                        
                /* Compute Correction of States by Mean Acceleration */        
                x[n+0] = abar[i] * (tstep - Phi[n+0][n+1]);
                x[n+1] = abar[i] * (1.0 - Phi[n+1][n+1]);         
            } 
            
        } else if (((int) type[i] == 11) || ((int) type[i] == 12)){ /* Singer Acceleration Model */
                       
            E = exp(-tstep/tau[i]);  /* Abbrevation 1 */
            the = tstep/tau[i];      /* Abbrevation 2 */
            
            Phi[n+0][n+2] = pow(tau[i], 2.0)* (the - 1.0 + E);
            Phi[n+1][n+2] = tau[i]*(1.0 - E);
            Phi[n+2][n+2] = E;     

            if ((int) type[i] == 12) { /* Mean Adaptive Singer Acceleration Model */
                     
                /* Calculate the "Current Acceleration" */
                #if USEADES
                    abar[i] = *ades[i];    
                #else
                    #if CAPAMEAN /* Sigma Points Acceleration used */
                        /*abar[i] = tmpx[n+2]; */
                        abar[i] = E * tmpx[n+2] + (1.0 - E) * abar[i];
                    #else
                        /*abar[i] = xhat[n+2]; */
                        abar[i] = E * xhat[n+2] + (1.0 - E) * abar[i];       
                    #endif
                #endif
                            
                /* Compute Correction of States by Mean Acceleration */        
                x[n+0] = abar[i] * (0.5 * pow(tstep, 2.0) - Phi[n+0][n+2]);
                x[n+1] = abar[i] * (tstep - Phi[n+1][n+2]);
                x[n+2] = abar[i] * (1.0 - Phi[n+2][n+2]);
            }
                
        } else if (((int) type[i] == 17) || ((int) type[i] == 18)){ /* Singer Jerk Model */
            
            E = exp(-tstep/tau[i]);  /* Abbrevation 1 */
            the = tstep/tau[i];      /* Abbrevation 2 */
            
            Phi[n+0][n+3] = pow(tau[i], 3.0)* (1.0 - the + 0.5*pow(the, 2.0) - E);
            Phi[n+1][n+3] = pow(tau[i], 2.0)* (the - 1.0 + E);
            Phi[n+2][n+3] = tau[i]*(1.0 - E);
            Phi[n+3][n+3] = E;    
               
            if ((int) type[i] == 18) { /* Mean Adaptive Singer Acceleration Model */
                
                #if CAPAMEAN /* Sigma Points Jerk used */
                    /*abar[i] = tmpx[n+3]; */
                    abar[i] = E * tmpx[n+3] + (1.0 - E) * abar[i];
                #else
                    /*abar[i] = xhat[n+3]; */
                    abar[i] = E * xhat[n+3] + (1.0 - E) * abar[i];       
                #endif
                        
                /* Compute Correction of States by Mean Acceleration */        
                x[n+0] = abar[i] * (pow(tstep, 3.0)/6.0 - Phi[n+0][n+3]);
                x[n+1] = abar[i] * (pow(tstep, 2.0)/2.0 - Phi[n+1][n+3]);
                x[n+2] = abar[i] * (tstep - Phi[n+2][n+3]);
                x[n+3] = abar[i] * (1.0 - Phi[n+3][n+3]);         
            }
        }
        
        /* Obtain new states by x(k+1) = Phi(T) * x(k) */
        for (ii = 0; ii < nstates; ii++) {
            for (jj = ii, summe = 0.0; jj < nstates; jj++) /* Upper triangle */
                summe = summe + Phi[n+ii][n+jj] * tmpx[n+jj];
            x[n+ii] = x[n+ii] + summe;
        } 
    }
    free(tmpx);
    free(fac);
}
    
void sdofOutput(SimStruct *S, double tstep, int numout, int dim, double **C, double *x, double *y, double **dlgParam){

    /* System Output Function y = h(x,t) = C * x */
    /* S                S-Function Structure of Simulink
     * TSTEP            (Current) Simulation Time Step [s]
     * NUMOUT           Number of System Output (Dimension of y)
     * DIM              System Dimension (Length of the state vector)
     * C                linear output matrix of system (NUMOUT x DIM)
     * X                Current Sigma Point (of all inputs)
     * Y                Simulated system outputs (pointer to the results of sdofOutput)
     * DLGPARAM         Pointer to the dialog Parameters (In RTW Applications, 
     *                  mx* C-Functions cannot be used in an external sourcecode
     */
    
    int i   = 0;
    int dof = 0;
    
    dof = (int) dim/numout; /* dof = degrees of freedom per measurement */
    
    clearMatrix(numout, dim, C);
    
    /* Only the Coordinate Signals are the system output --> H = [1 0 0] */
    for (i=0; i<numout; i++) {
        y[i] = x[i*dof];
        C[i][i*dof] = 1.0;
    }    
    
    UNUSED_ARG(S);
}

void sdofConstraints(SimStruct *S, double tstep, int ncon, int dim, double **D, double *x, double *d, double **dlgParam){
    
    /* System Constraints (if any) d = g(x,t) = D * x */
    /* S                S-Function Structure of Simulink
     * TSTEP            (Current) Simulation Time Step [s]
     * NCON             Number of System Constraints (Dimension of d)
     * DIM              System Dimension (Length of the state vector)
     * D (captial)      Linear constraint matix of system (NCON x DIM)
     * X                Current Sigma Point (of all inputs)
     * D                Simulated system constraints (pointer to the results of sdofOutput)
     * DLGPARAM         Pointer to the dialog Parameters (In RTW Applications, 
     *                  mx* C-Functions cannot be used in an external sourcecode
     */
    
    UNUSED_ARG(S);
}

void sdofModelCov(SimStruct *S, double tstep, int q, double *x, double **Q_k, double *abar, double **dlgParam){

    /* Discrete-time Model Covariance (Process Noise) --> Q_K */ 
    /* S                S-Function Structure of Simulink
     * TSTEP            (Current) Simulation Time Step [s]
     * Q                Dimenstion of the square matrix Q_K (Dimension of x)
     * X                Overall State Vector (also dimension = q)
     * Q_k              Model Covariance Matrix
     * ABAR             Addaptive mean velocity/acceleration/jerk (depends on type of noise model)
     * DLGPARAM         Pointer to the dialog Parameters (In RTW Applications, 
     *                  mx* C-Functions cannot be used in an external sourcecode
     */   
    
    double *tau  = NULL;
    double *eps2 = NULL;
    double *type = NULL;
    double alp      = 0.0;
    double vark     = 0.0;
    double E        = 0.0;
    double the      = 0.0;
    double typetemp = 0.0;
    
    int i   = 0;
    int ii  = 0;
    int jj  = 0;
    int n   = 0;
    int dof = 0;
    int adapt = 0;
    
    int numout = ssGetInputPortWidth(S, 0);
    
    
    double T = tstep; /* StepSize of Simulation */
   
    type = dlgParam[0];  /* Type of Noise Models */
    eps2 = dlgParam[2];  /* Tuning Parameter for Process Variance */  
    tau = dlgParam[3];   /* Correlation Time Constant */
     
    /* dof = (int) q/numout; */

    /* Fill it at least with some small value to prevent singularities */
    for (i = 0; i < q; i++)
        Q_k[i][i] = Q_k[i][i] + (double) DELTA;    
    
    for (i=0; i<numout; i++){   /* Same noise model used for all inputs */
        n = (int) DOF * i; /* Start element in Q for each input */
        
        if ((int) type[i] == 6){ /* Singer Velocity Model with adaptive mean */
            typetemp = 5.0;
            adapt = 1;
        } else if ((int) type[i] == 12){ /* Singer Acceleration Model with adaptive mean */
            typetemp = 11.0;
            adapt = 1;
        } else if ((int) type[i] == 18){ /* Singer Jerk Model with adaptive mean */
            typetemp = 17.0;
            adapt = 1;
        } else {
            adapt = 0;
            typetemp = type[i];
        }
        
        switch ((int) typetemp){ /* Different Type of Noise Models */
                
            case 1: /* Random Velocity Jump just before observation */   
                Q_k[n+0][n+0] = 0.0; 
                Q_k[n+1][n+1] = eps2[i]; 
                break;
            
            case 2: /* Wiener Sequence Velocity Model (2 States) */
                Q_k[n+0][n+0] = pow(T , 2.0)/2.0 * eps2[i]; 
                Q_k[n+0][n+1] = tstep * eps2[i];
                
                Q_k[n+1][n+0] = Q_k[n+0][n+1];
                Q_k[n+1][n+1] = eps2[i]; 
                
                break;
        
            case 3: /* Random Constant Acceleration inbetween samples (2 States) */    
                Q_k[n+0][n+0] = pow(T , 4.0)/4.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 3.0)/2.0 * eps2[i];
                
                Q_k[n+1][n+0] = Q_k[n+0][n+1];
                Q_k[n+1][n+1] = pow(T , 2.0)     * eps2[i];
                                
                break;
                
            case 4: /* White Noise Acceleration Model (2 States) */
                Q_k[n+0][n+0] = pow(T , 3.0)/3.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 2.0)/2.0 * eps2[i];
                
                Q_k[n+1][n+0] = Q_k[n+0][n+1];
                Q_k[n+1][n+1] = T * eps2[i];
                                
                break;
                
            case 5: /* Singer Velocity Model (2 States) */
                alp = 1/tau[i];     /* Correlation coefficient is inverse of maneuver time */     
                E = exp(-alp*T);    /* Abbrevation 1 */
                the = alp*T;        /* Abbrevation 2 */

                /* Adaptive Variance via "Current Velocity" */
                if (adapt == 1){ 
                    vark = eps2[i] - fabs(abar[i]); 
                    /* vark = eps2[i] - fabs(x[n+2]); */
                    vark = (4.0 - PI)/PI * pow( vark, 2.0);
                } else {
                    vark = eps2[i]; /* No Adaption Algorithm */
                }
                
                Q_k[n+0][n+0] = vark/pow(alp, 2.0)*(4.0*E - 3.0 - pow(E, 2.0) + 2.0*the); 
                Q_k[n+0][n+1] = vark/alp * (pow(E, 2.0) + 1.0 - 2.0*E);     

                Q_k[n+1][n+0] = Q_k[n+0][n+1]; 
                Q_k[n+1][n+1] = vark * (1.0 - pow(E, 2.0)); 
                
            case 7: /* Random Acceleration Jump just before observation */    
                
                Q_k[n+0][n+0] = 0.0; 
                Q_k[n+1][n+1] = 0.0; 
                Q_k[n+2][n+2] = eps2[i]; 
                break;
                
            case 8: /* Wiener Sequence Acceleration Model (3 States) */
                Q_k[n+0][n+0] = pow(T , 4.0)/4.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 3.0)/2.0 * eps2[i];
                Q_k[n+0][n+2] = pow(T , 2.0)/2.0 * eps2[i];
                
                Q_k[n+1][n+0] = Q_k[n+0][n+1];
                Q_k[n+1][n+1] = pow(T , 2.0)/2.0 * eps2[i]; 
                Q_k[n+1][n+2] = tstep * eps2[i];
                
                Q_k[n+2][n+0] = Q_k[n+0][n+2];
                Q_k[n+2][n+1] = Q_k[n+1][n+2];
                Q_k[n+2][n+2] = eps2[i]; 
                                
                break;
            
            case 9: /* Random Constant Jerk inbetween Samples (3 States) */
                Q_k[n+0][n+0] = pow(T , 6.0)/36.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 5.0)/12.0 * eps2[i];
                Q_k[n+0][n+2] = pow(T , 4.0)/6.0 * eps2[i];

                Q_k[n+1][n+0] = Q_k[n+1][n+2];
                Q_k[n+1][n+1] = pow(T , 4.0)/4.0 * eps2[i];
                Q_k[n+1][n+2] = pow(T , 3.0)/2.0 * eps2[i];  

                Q_k[n+2][n+0] = Q_k[n+0][n+2];
                Q_k[n+2][n+1] = Q_k[n+1][n+2];
                Q_k[n+2][n+2] = pow(T , 2.0) * eps2[i]; 
                                
                break;
                
            case 10: /* White-noise jerk model (3 States) */   
                Q_k[n+0][n+0] = pow(T , 5.0)/20.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 4.0)/8.0 * eps2[i];
                Q_k[n+0][n+2] = pow(T , 3.0)/6.0 * eps2[i];

                Q_k[n+1][n+0] = Q_k[n+1][n+2];
                Q_k[n+1][n+1] = pow(T , 3.0)/3.0 * eps2[i];
                Q_k[n+1][n+2] = pow(T , 2.0)/2.0 * eps2[i];  

                Q_k[n+2][n+0] = Q_k[n+0][n+2];
                Q_k[n+2][n+1] = Q_k[n+1][n+2];
                Q_k[n+2][n+2] = tstep * eps2[i];   
                                
                break;
                
            case 11: /* Singer Acceleration Model (Exp. correlated noise) */
                alp = 1/tau[i];     /* Correlation coefficient is inverse of maneuver time */     
                E = exp(-alp*T);    /* Abbrevation 1 */
                the = alp*T;        /* Abbrevation 2 */

                /* Adaptive Variance via "Current Acceleration" */
                if (adapt == 1){
                    vark = eps2[i] - fabs(abar[i]); 
                    /* vark = eps2[i] - fabs(x[n+2]); */
                    vark = (4.0 - PI)/PI * pow( vark, 2.0);
                } else {
                    vark = eps2[i]; /* No Adaption Algorithm */
                }

                /* Computation of the covariance Matrix*/
                Q_k[n+0][n+0] = vark/pow(alp,4.0)*(1.0 - pow(E, 2.0) + 2.0*the + 2.0/3.0*pow(the,3.0) 
                                - 2.0*pow(the,2.0) - 4.0*E*the);
                Q_k[n+0][n+1] = vark/pow(alp,3.0)*(1.0 + pow(E, 2.0) - 2.0*E 
                                + 2.0*E*the - 2.0*the + pow(the,2.0));
                Q_k[n+0][n+2] = vark/pow(alp,2.0)*(1.0 - pow(E, 2.0) - 2.0*E*the);

                Q_k[n+1][n+0] = Q_k[n+0][n+1];
                Q_k[n+1][n+1] = vark/pow(alp,2.0)*(4.0*E - 3.0 - pow(E, 2.0) + 2.0*the); 
                Q_k[n+1][n+2] = vark/alp*(pow(E, 2.0) + 1.0 - 2.0*E);     

                Q_k[n+2][n+0] = Q_k[n+0][n+2]; 
                Q_k[n+2][n+1] = Q_k[n+1][n+2]; 
                Q_k[n+2][n+2] = vark*(1.0 - pow(E, 2.0)); 
                                
                break;
                
            case 13: /* Random Jerk Jump just before observation */
                
                Q_k[n+0][n+0] = 0.0; 
                Q_k[n+1][n+1] = 0.0;
                Q_k[n+2][n+2] = 0.0; 
                Q_k[n+3][n+3] = eps2[i];    
                
                break;
                
            case 14:  /* Wiener Sequence Jerk Model */ 
                Q_k[n+0][n+0] = pow(T , 6.0)/36.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 5.0)/12.0 * eps2[i];
                Q_k[n+0][n+2] = pow(T , 4.0)/6.0 * eps2[i];
                Q_k[n+0][n+3] = pow(T , 3.0)/6.0 * eps2[i];

                Q_k[n+1][n+0] = Q_k[n+1][n+2];
                Q_k[n+1][n+1] = pow(T , 4.0)/4.0 * eps2[i];
                Q_k[n+1][n+2] = pow(T , 3.0)/2.0 * eps2[i];  
                Q_k[n+1][n+3] = pow(T , 2.0)/2.0 * eps2[i];

                Q_k[n+2][n+0] = Q_k[n+0][n+2];
                Q_k[n+2][n+1] = Q_k[n+1][n+2];
                Q_k[n+2][n+2] = pow(T , 2.0) * eps2[i]; 
                Q_k[n+2][n+3] = T * eps2[i];

                Q_k[n+3][n+0] = Q_k[n+0][n+3];
                Q_k[n+3][n+1] = Q_k[n+1][n+3];
                Q_k[n+3][n+2] = Q_k[n+2][n+3]; 
                Q_k[n+3][n+3] = eps2[i];
                                
                break;
                
            case 15: /* Random Constant Pop inbetween samples */   
                Q_k[n+0][n+0] = pow(T , 8.0)/576.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 7.0)/144.0 * eps2[i];
                Q_k[n+0][n+2] = pow(T , 6.0)/48.0 * eps2[i];
                Q_k[n+0][n+3] = pow(T , 5.0)/24.0 * eps2[i];

                Q_k[n+1][n+0] = Q_k[n+0][n+1];
                Q_k[n+1][n+1] = pow(T , 6.0)/36.0 * eps2[i];
                Q_k[n+1][n+2] = pow(T , 5.0)/12.0 * eps2[i];  
                Q_k[n+1][n+3] = pow(T , 4.0)/6.0 * eps2[i];

                Q_k[n+2][n+0] = Q_k[n+0][n+2];
                Q_k[n+2][n+1] = Q_k[n+1][n+2];
                Q_k[n+2][n+2] = pow(T , 4.0)/4.0 * eps2[i];  
                Q_k[n+2][n+3] = pow(T , 3.0)/2.0 * eps2[i];

                Q_k[n+3][n+0] = Q_k[n+0][n+3];
                Q_k[n+3][n+1] = Q_k[n+1][n+3];
                Q_k[n+3][n+2] = Q_k[n+2][n+3];  
                Q_k[n+3][n+3] = pow(T , 2.0) * eps2[i]; 
                                
                break;
                
            case 16: /* White-noise pop model */
                Q_k[n+0][n+0] = pow(T , 7.0)/252.0 * eps2[i]; 
                Q_k[n+0][n+1] = pow(T , 6.0)/72.0 * eps2[i];
                Q_k[n+0][n+2] = pow(T , 5.0)/30.0 * eps2[i];
                Q_k[n+0][n+3] = pow(T , 4.0)/24.0 * eps2[i];

                Q_k[n+1][n+0] = Q_k[n+1][n+2];
                Q_k[n+1][n+1] = pow(T , 5.0)/20.0 * eps2[i];
                Q_k[n+1][n+2] = pow(T , 4.0)/8.0 * eps2[i];  
                Q_k[n+1][n+3] = pow(T , 3.0)/6.0 * eps2[i];

                Q_k[n+2][n+0] = Q_k[n+0][n+2];
                Q_k[n+2][n+1] = Q_k[n+1][n+2];
                Q_k[n+2][n+2] = pow(T , 3.0)/3.0 * eps2[i];  
                Q_k[n+2][n+3] = pow(T , 2.0)/2.0 * eps2[i];

                Q_k[n+3][n+0] = Q_k[n+0][n+3];
                Q_k[n+3][n+1] = Q_k[n+1][n+3];
                Q_k[n+3][n+2] = Q_k[n+2][n+3];  
                Q_k[n+3][n+3] = T * eps2[i]; 
                                
                break;
                
            case 17: /* Singer Jerk Model (Exp. correlated noise) */    
                alp = 1/tau[i];     /* Correlation coefficient is inverse of maneuver time */     
                E = exp(-alp*T);    /* Abbrevation 1 */
                the = alp*T;        /* Abbrevation 2 */

                /* Adaptive Variance via "Current Jerk" */
                if (adapt == 1){
                    vark = eps2[i] - fabs(abar[i]); 
                    /* vark = eps2[i] - fabs(x[n+2]); */
                    vark = (4.0 - PI)/PI * pow( vark, 2.0);
                } else {
                    vark = eps2[i]; /* No Adaption Algorithm */
                }

                /* Computation of the covariance Matrix*/
                Q_k[n+0][n+0] = vark/pow(alp, 6.0)*(pow(the,5.0)/10.0-pow(the,4.0)/2.0+4.0/3.0*pow(the,3.0)
                                                    -2.0*pow(the,2.0)+2.0*the-3.0+4.0*E+2.0*E*pow(the,2.0)-pow(E,2.0));
                Q_k[n+0][n+1] = vark/pow(alp, 5.0)*(1.0-2.0*the+2.0*pow(the,2.0)-pow(the,3.0)+pow(the,4.0)/4.0
                                                    +pow(E,2.0)+2*E*the+2.0*E-E*pow(the,2.0));
                Q_k[n+0][n+2] = vark/pow(alp, 4.0)*(2.0*the-pow(the,2.0)+pow(the,3.0)/3.0-3.0-pow(E,2.0)+
                                                    4.0*E+ E*pow(the,2.0));
                Q_k[n+0][n+3] = vark/pow(alp, 3.0)*(4.0*E - pow(E, 2.0) + 2.0*the - 3.0);        

                Q_k[n+1][n+0] = Q_k[n+0][n+1];
                Q_k[n+1][n+1] = vark/pow(alp,4.0)*(1.0 - pow(E, 2.0) + 2.0*the + 2.0/3.0*pow(the,3.0) 
                                                   - 2.0*pow(the,2.0) - 4.0*E*the);
                Q_k[n+1][n+2] = vark/pow(alp,3.0)*(1.0 + pow(E, 2.0) - 2.0*E 
                                                   + 2.0*E*the - 2.0*the + pow(the,2.0));
                Q_k[n+1][n+3] = vark/pow(alp,2.0)*(1.0 - pow(E, 2.0) - 2.0*E*the);

                Q_k[n+2][n+0] = Q_k[n+0][n+2];
                Q_k[n+2][n+1] = Q_k[n+1][n+2];
                Q_k[n+2][n+2] = vark/pow(alp,2.0)*(4.0*E - 3.0 - pow(E, 2.0) + 2.0*the); 
                Q_k[n+2][n+3] = vark/alp*(pow(E, 2.0) + 1.0 - 2.0*E);     

                Q_k[n+3][n+0] = Q_k[n+0][n+3];
                Q_k[n+3][n+1] = Q_k[n+1][n+3]; 
                Q_k[n+3][n+2] = Q_k[n+2][n+3]; 
                Q_k[n+3][n+3] = vark*(1.0 - pow(E, 2.0));              
                break;
        }
    }
}

void sdofMeasurementCov(SimStruct *S, double tstep, int r, double *x, double **R_k, double *Rvotf, double **dlgParam){

    /* Discrete-time Measurement Covariance (Sensor Noise) --> R_K */ 
    /* S                S-Function Structure of Simulink
     * TSTEP            (Current) Simulation Time Step [s]
     * R                Dimenstion of the square matrix R_K
     * X                Overall State Vector (dimension available as DIM [preprocessor])
     * R_K              Measurement Covariance Matrix
     * RVOTF            Pointer to variables necessare to compute the noise variance on the fly
     *      [0]         Counter for the Time Steps k
     *      [1]         r Sum of squared differences --> (y[k] - y[k-1])^2
     *      [1+r]       r Measurement samples of previous timestep
     *      [1+2*r]     r*NWIN Squared difference between neighboring samples
     * DLGPARAM         Pointer to the dialog Parameters (In RTW Applications, 
     *                  mx* C-Functions cannot be used in an external sourcecode
     */     
    
    int i = 0;
    int j = 0;
    double *RPtr   = NULL;
    double *sumsq  = NULL;
    double *delysq = NULL;
    double *ykmin1 = NULL;

    #if (defined(RVONTHEFLY) && RVONTHEFLY == 1)    
        InputRealPtrsType meas = ssGetInputPortRealSignalPtrs(S, 0);
    #endif
    
    /* Constant Noise Variance (from the Dialog Parameters of S-Function */
    RPtr = dlgParam[1];
    for (i=0; i<r; i++) { /* COLS */
        for (j=0; j<r; j++) /* ROWS */
            R_k[j][i] = RPtr[i*r+j];
    }
    
    #if (defined(RVONTHEFLY) && RVONTHEFLY == 1) /* Adaptive Noise Variance */
    
        Rvotf[0] = Rvotf[0] + 1.0; /* Step Counter is incremented */
        /* Pointers for start elements of different variables --> readability!!! */
        sumsq = &(Rvotf[1]);        /* Sum of squared difference --> (y[k] - y[k-1])^2 */
        ykmin1 = &(Rvotf[1+r]);     /* Measurement samples of previous timestep */
        delysq = &(Rvotf[1+2*r]);   /* Squared difference between samples */
        
        /* Uncorrelated measurements --> only diagonal entries will be computed */
        for (i = 0; i < r; i++){ /* Compute noise variance seperately */
            sumsq[i] = sumsq[i] - delysq[i*NWIN]; /* Remove oldest squared difference */    
            memmove( &(delysq[i*NWIN]), &(delysq[i*NWIN+1]), (NWIN-1)*sizeof(double));  
            delysq[(i+1)*NWIN-1] = pow( (*meas[i] - ykmin1[i]), 2.0); /* Compute newest one */
            sumsq[i] = sumsq[i] + delysq[(i+1)*NWIN-1]; /* Add it to sum */
            ykmin1[i] = *meas[i]; /* Store most recent measurement for next time step */    
            if (Rvotf[0] > ((double) NWIN)) /* Only if NWIN samples have been elapsed */
                R_k[i][i] = 1.0/(2.0*((double) NWIN) - 2.0) * sumsq[i];
        }
    #endif
}

void sdofOutlierRejection(SimStruct *S, double tstep, int dim, int numout, double *x, double *y, double **R_k, double *inno, double **dlgParam){
    
    /* Outlier Rejection Algorithm which acts on INNO */
    /* S                S-Function Structure of Simulink
     * TSTEP            (Current) Simulation Time Step [s]
     * DIM              Overall System Dimension (Dimension of x)
     * NUMOUT           Number of System Outputs (Dimension of y and R_k)
     * X                Overall State Vector (here: a priori estimates)
     * Y                Current Measurements (S-Function inputs)
     * R_K              Measurement Covariance Matrix
     * INNO             Difference between measurements y and a priori estimates x
     * DLGPARAM         Pointer to the dialog Parameters (In RTW Applications, 
     *                  mx* C-Functions cannot be used in an external sourcecode
     */ 
    
    int i = 0;
    double *rjcttype = NULL;
    double outliercrit = 0.0;
    
    /*rjcttype = (double *) mxGetPr(ssGetSFcnParam(S, 4)); */
    rjcttype = dlgParam[4];  /* Rejection Type */
    
    /* Outlier Detection is done with the diagonal elements of measurement covariance matrix */
    for (i = 0; i < numout; i++){
        outliercrit = ((double) FACTOR_STDDEV) * sqrt(R_k[i][i]); /* Criteria for outlier detection */ 
        if ((rjcttype[i] > 0) && (R_k[i][i] > 0.0) && (fabs(inno[i]) > outliercrit)){
            if (rjcttype[i] == 1){ /* Saturation of innovation */
                if (inno[i] > 0.0){ /* Saturate it to the maximum value */
                    inno[i] = outliercrit;
                } else { /* Saturate it to the minimum value */
                    inno[i] = - outliercrit;
                }
            } else if (rjcttype[i] == 2){ /* Rejection of  innovation */
                inno[i] = 0.0;   /* Newest innovation is discarded */
            }
        }
    }
}

/*************************************************************************/

void UKFSigmaPs(int dim, int exdim, double alpha, double W0, double *gamma, double *Wm, double *Wc){
    
    int i = 0;
    int j = 0;
    double w1     = 0.0;
    double lambda = 0.0;
    double den    = 0.0;
    double truej  = 0.0;
    double beta = 2.0; /* beta = 1.0*/ 
    
    #if defined(SPHERICAL) && SPHERICAL /* Spherical Sigma Points */
        
        w1 = (1.0 - W0)/((double) dim + 1.0) / pow(alpha,2.0);       
          
        /* Same Weights for covariance Wc and for Wm */        
        Wm[0] = 1.0 + (W0 - 1.0) / pow(alpha,2.0);
        Wc[0] = Wm[0] + (1.0 - pow(alpha,2.0) + beta);
        for (i = 1; i < exdim; i++) {
            Wm[i] = w1;
            Wc[i] = w1;
        }      

        /* Compute the Spherical Sigma Points */
        gamma[dim] = -1.0 / sqrt(2.0 * w1);
        gamma[2*dim] = -1.0 * gamma[dim];
        for (j = 1; j < dim; j++) { /* Outer Loop (States = Rows) */
            truej = (double) j + 1.0; /* Actual State Index j = 1,...,DIM */
            den = sqrt(truej * (truej + 1.0) * w1); /* Denominator */
            for (i = 1; i<=(j+1); i++) { /* Inner Loop (Sigma Points = Columns) */ 
                gamma[i*dim+j] = - 1.0 / den;
            }
            gamma[(j+2)*dim+j] = truej / den;
        }    
    
    #else        /* Conventional Sigma Points */
        
        double kappa = 3.0 - (double) dim; /* kappa = 0.0 */
                
        lambda = pow(alpha,2.0) * ((double) dim + kappa) - (double) dim; 
        gamma[0] = (double) dim + lambda;
        gamma[0] = sqrt(gamma[0]);
        Wm[0] = lambda / (lambda + (double) dim);
        Wc[0] = lambda / (lambda + (double) dim) + (1.0 - pow(alpha,2.0) + beta);
        for (i = 1; i < exdim; i++) {
            Wm[i] = 1.0 / (2.0*((double) dim + lambda));
            Wc[i] = 1.0 / (2.0*((double) dim + lambda));
        }
    #endif       
}

void UKFpartI(int dim, int exdim, double *gamma, double *xhat, double *x, double **P){

    int i = 0;
    int j = 0;

    /* Perform cholesky decomposition of covariance matrix P */ 
    if (cholesky(dim, P)!= 0)
        return;  
    
    /* Computation of Sigma points  */
    #if defined(SPHERICAL) && SPHERICAL
        for (i = 0; i < exdim; i++) {    
            MatrixVectorMultiplicationMod('N',dim,dim,1.0,P,&(gamma[i*dim]),1.0,xhat,&(x[i*dim]));
        }
    #else
        for (i = 0; i < dim; i++) {
            x[i] = xhat[i];
            for (j = 0; j < dim; j++) {
                x[dim+(j*dim)+i] =  xhat[i] + gamma[0] * P[i][j];
                x[dim+dim*dim+(j*dim)+i] = xhat[i] - gamma[0] * P[i][j];
            }
        }
    #endif
}    
        
void UKFpartII(int dim, int exdim, double *xhat, double *x, double *Wm) {
    /* dim = Number of states, exdim = Number of SigmaPoints */

    int i = 0;
    int j = 0;
    double summe = 0.0;
    
    for (i = 0; i < dim; i++) { /* ROWS */
        summe = 0.0;
        for (j = 0; j < exdim; j++) { /* COLUMNS */
            summe = summe +  Wm[j] * x[j*dim+i];
        }
        xhat[i] = summe;
    }
}

void UKFpartIII(int dim, int exdim, double *x, double *xhat, double *vector, double **P, double *Wc, double **matrix){
    /* dim = Number of independent states, exdim = Number of SigmaPoints */

    int i = 0;
    int j = 0;
    int k = 0;
    
    for (i = 0; i < exdim; i++) {
        for (j = 0; j < dim; j++)
            vector[j] = x[i*dim+j] - xhat[j];

        dyadicProduct(dim, vector, matrix);

        for (j = 0; j < dim; j++) {
            for (k = 0; k < dim; k++)
                P[j][k] = P[j][k] + Wc[i] * matrix[j][k];
        }
    }    
}

void UKFpartIV(int n, int m, int exdim, double *x, double *xhat, double *y, double *yhat, double *vector1, double *vector2, double **Pxy, double **matrix, double *Wc) {
    /* n = Number of independent states, m = Number of Outputs */
    /* exdim = Number of SigmaPoints */
    
    int i = 0;
    int j = 0;
    int k = 0;

    for (i = 0; i < exdim; i++) {

        for (j = 0; j < n; j++) {
            vector1[j] = x[i*n+j] - xhat[j];
        }
        for (k = 0; k < m; k++) {
            vector2[k] = y[i*m+k] - yhat[k];    /* m --> n */
        }

        dyadicProduct2(n, m, vector1, vector2, matrix);

        for (j = 0; j < n; j++) {
            for (k = 0; k < m; k++) {
                Pxy[k][j] = Pxy[k][j] + Wc[i] * matrix[j][k];
            }
        }
    }     
}

void UKFpartV(int dim, int exdim, double *xhat, double *x, double *Wm) {
    /* dim = Number of states, exdim = Number of SigmaPoints */

    int i = 0;
    int j = 0;
    double summe = 0.0;
    
    for (i = 0; i < dim; i++) { /* ROWS */
        summe = 0.0;
        for (j = 0; j < exdim; j++) { /* COLUMNS */
            summe = summe +  Wm[j] * x[j*dim+i];
        }
        xhat[i] = summe;
    }
}
   
void UKFpartVI(int dim, int exdim, double *x, double *xhat, double *vector, double **P, double *Wc, double **matrix) {

    int     i,j,k;

    for (i = 0; i < exdim; i++) {

        for (j = 0; j < dim; j++)
            vector[j] = x[i*dim+j] - xhat[j];

        dyadicProduct(dim, vector, matrix);

        for (j = 0; j < dim; j++) {
            for (k = 0; k < dim; k++) {
                P[j][k] = P[j][k] + Wc[i] * matrix[j][k];
            }
        }
    }
}
