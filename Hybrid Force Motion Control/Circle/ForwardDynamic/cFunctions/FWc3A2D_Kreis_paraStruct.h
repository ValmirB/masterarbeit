#ifndef FWc3A2D_Kreis_paraStruct_H
#define FWc3A2D_Kreis_paraStruct_H

#include <stdlib.h>
#include <math.h>
#include "FWc3A2D_Kreis_Flexor.h"

struct FWc3A2D_Kreis_constraintStructure {

	double **G_;

	double **C_;
	double *Dc_;
	double *D2c_;

	double **Jr_v;
	double **Jr_a;
	double **Jl_a;
	double *theta_;
	double *gamma_;

	double *lambda_;

};

struct FWc3A2D_Kreis_paraStruct {

	/* User defined constant parameters */
	double D2p_a_s;
	double D2p_b_s;
	double D2r_gamma3_s;
	double Dp_a_s;
	double Dp_b_s;
	double Dr_gamma3_s;
	double I_ee;
	double K_L;
	double K_R;
	double Ox;
	double Oy;
	double Radius;
	double d_axes;
	double g;
	double l2;
	double l3;
	double m_C1;
	double m_C2;
	double m_ee;
	double p_a_d;
	double p_a_s;
	double p_b_d;
	double p_b_s;
	double phi2_1;
	double phi3_1;
	double psi2_1;
	double psi3_1;
	double r_gamma3_d;
	double r_gamma3_s;
	double v2_1;
	double v3_1;
	double w2_1;
	double w3_1;
	double z_0;
	double r_alpha1_s;
	double Dr_alpha1_s;
	double D2r_alpha1_s;
	double r_beta2_s;
	double Dr_beta2_s;
	double D2r_beta2_s;
	double EA2_q001_s;
	double DEA2_q001_s;
	double D2EA2_q001_s;
	double EA3_q001_s;
	double DEA3_q001_s;
	double D2EA3_q001_s;

	/* Rudimental system information */
	int sysDof;
	int numBodies;
	double *externalForces;
	double *externalTorques;

	/* Abbreviation Force Elements */

	/* Independent generalized coordinates */
	int independentIndices[1];

	/* Dependent generalized coordinates */
	int dependentIndices[3];

	struct FWc3A2D_Kreis_constraintStructure *con;

};

void *FWc3A2D_Kreis_initializeSystemStruct(void);

void FWc3A2D_Kreis_initializeConstraintStructure(struct FWc3A2D_Kreis_paraStruct *data);

void FWc3A2D_Kreis_freeConstraintStructure(struct FWc3A2D_Kreis_paraStruct *data);

void FWc3A2D_Kreis_freeStructure(void *dataPtr);

#endif

