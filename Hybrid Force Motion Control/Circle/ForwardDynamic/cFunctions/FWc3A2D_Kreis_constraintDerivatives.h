#ifndef FWc3A2D_Kreis_constraintDerivatives_H
#define FWc3A2D_Kreis_constraintDerivatives_H

#include "FWc3A2D_Kreis_paraStruct.h"

void FWc3A2D_Kreis_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
