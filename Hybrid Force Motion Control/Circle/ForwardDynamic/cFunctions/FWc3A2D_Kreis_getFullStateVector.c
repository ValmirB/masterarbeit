#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "FWc3A2D_Kreis_pi_code.h"
#include "FWc3A2D_Kreis_userDefined.h"
#include "FWc3A2D_Kreis_pd_matlab.h"
#include "neweul.h"
#include "FWc3A2D_Kreis_paraStruct.h"
#include "FWc3A2D_Kreis_Flexor.h"

#include "FWc3A2D_Kreis_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double *t_vec             = mxGetPr( prhs[0] );
	double t                  = 0;
	double *x                 = mxGetPr( prhs[1] );
	double *y_ind             = NULL;
	double *Dy_ind            = NULL;
	double *matlabReturn      = NULL;
	double *u_                = NULL;
	size_t numStates          = 1;
	int counter_              = 0;
	int vec_loop              = 0;
	struct FWc3A2D_Kreis_paraStruct *data = NULL;

#if defined FWC3A2D_KREIS_NUM_ALL_INPUTS && FWC3A2D_KREIS_NUM_ALL_INPUTS > 0
	u_ = calloc(FWC3A2D_KREIS_NUM_ALL_INPUTS, sizeof(double));
#endif

	/* Initialize System Struct */
	dataPtr = FWc3A2D_Kreis_initializeSystemStruct();

	data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!");
		goto clean_up;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!");
		goto clean_up;
	}

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		FWc3A2D_Kreis_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	/* Get number of right-hand sides */
	numStates = mxGetN(prhs[1]);

	if (((mxGetM(prhs[0]) * mxGetN(prhs[0])) != numStates)){
		mexPrintf("The dimensions of the passed arguments don not match!\n");
		goto clean_up;
	}

	if ((mxGetM(prhs[1])!=2*FWC3A2D_KREIS_NUM_INDEPENDENT_GC)){
		mexPrintf("The second input argument has to be a 2xnumStates vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*FWC3A2D_KREIS_SYS_DOF, numStates, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	for ( vec_loop = 0; vec_loop < (int) numStates; vec_loop++){

		t = t_vec[vec_loop];
		y_ind = &(x[2*FWC3A2D_KREIS_NUM_INDEPENDENT_GC*vec_loop]);
		Dy_ind = &(x[2*FWC3A2D_KREIS_NUM_INDEPENDENT_GC*vec_loop+FWC3A2D_KREIS_NUM_INDEPENDENT_GC]);

		/* Get dependent generlaized coordinates and velocities */
		for ( counter_ = 0 ; counter_ < FWC3A2D_KREIS_NUM_INDEPENDENT_GC ; counter_++ ){
			matlabReturn[vec_loop*2*FWC3A2D_KREIS_SYS_DOF+data->independentIndices[counter_]]  =  y_ind[counter_];
			matlabReturn[vec_loop*2*FWC3A2D_KREIS_SYS_DOF+data->independentIndices[counter_]+FWC3A2D_KREIS_SYS_DOF] = Dy_ind[counter_];
		}

		FWc3A2D_Kreis_educatedGuess(t, &(matlabReturn[vec_loop*2*FWC3A2D_KREIS_SYS_DOF]), dataPtr);

	/* Compute all inputs */
	FWc3A2D_Kreis_f_control_inputs(t, &(matlabReturn[vec_loop*2*FWC3A2D_KREIS_SYS_DOF]), &(u_[0]), dataPtr);

		FWc3A2D_Kreis_getDependentGenCoords(t, &(matlabReturn[vec_loop*2*FWC3A2D_KREIS_SYS_DOF]), u_, dataPtr);

	}

	/* Free allocated memory */
clean_up:
	FWc3A2D_Kreis_freeStructure(dataPtr);
#if defined FWC3A2D_KREIS_NUM_ALL_INPUTS && FWC3A2D_KREIS_NUM_ALL_INPUTS > 0
	free(u_);
#endif

}

