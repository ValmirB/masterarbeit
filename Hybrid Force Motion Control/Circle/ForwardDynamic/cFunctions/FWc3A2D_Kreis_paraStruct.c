#include <string.h>
#include <stdlib.h>
#include "neweul.h"
#include "FWc3A2D_Kreis_paraStruct.h"
#include "FWc3A2D_Kreis_userDefined.h"
#include <stdio.h>

/* Initialize structure with system parameters */
void *FWc3A2D_Kreis_initializeSystemStruct(void){

	struct FWc3A2D_Kreis_paraStruct *data = calloc(1, sizeof(struct FWc3A2D_Kreis_paraStruct));

	/* User defined constant parameters */
	data->D2p_a_s = 0.000000e+00;
	data->D2p_b_s = 0.000000e+00;
	data->D2r_gamma3_s = 0.000000e+00;
	data->Dp_a_s = 0.000000e+00;
	data->Dp_b_s = 0.000000e+00;
	data->Dr_gamma3_s = 0.000000e+00;
	data->I_ee = 0.000000e+00;
	data->K_L = 8.000000e+01;
	data->K_R = 1.500000e+01;
	data->Ox = 0.000000e+00;
	data->Oy = 0.000000e+00;
	data->Radius = 3.800000e-01;
	data->d_axes = 6.100000e-01;
	data->g = 9.810000e+00;
	data->l2 = 1.000000e+00;
	data->l3 = 4.643000e-01;
	data->m_C1 = 6.000000e+00;
	data->m_C2 = 6.600000e+00;
	data->m_ee = 0.000000e+00;
	data->p_a_d = 0.000000e+00;
	data->p_a_s = 0.000000e+00;
	data->p_b_d = 0.000000e+00;
	data->p_b_s = 0.000000e+00;
	data->phi2_1 = 5.177806e-02;
	data->phi3_1 = 1.108656e-01;
	data->psi2_1 = 1.307493e-01;
	data->psi3_1 = 4.181412e-01;
	data->r_gamma3_d = 0.000000e+00;
	data->r_gamma3_s = 0.000000e+00;
	data->v2_1 = 9.250000e-01;
	data->v3_1 = 9.250000e-01;
	data->w2_1 = 9.250000e-01;
	data->w3_1 = 9.250000e-01;
	data->z_0 = 1.570000e-01;
	data->r_alpha1_s = 0.000000e+00;
	data->Dr_alpha1_s = 0.000000e+00;
	data->D2r_alpha1_s = 0.000000e+00;
	data->r_beta2_s = 0.000000e+00;
	data->Dr_beta2_s = 0.000000e+00;
	data->D2r_beta2_s = 0.000000e+00;
	data->EA2_q001_s = 0.000000e+00;
	data->DEA2_q001_s = 0.000000e+00;
	data->D2EA2_q001_s = 0.000000e+00;
	data->EA3_q001_s = 0.000000e+00;
	data->DEA3_q001_s = 0.000000e+00;
	data->D2EA3_q001_s = 0.000000e+00;

	/* Rudimental system information */
	data->sysDof = 4;
	data->numBodies = 6;
	data->externalForces = NULL;
	data->externalTorques = NULL;

	/* Independent generalized coordinates */
	data->independentIndices[0] = 3;

	/* Dependent generalized coordinates */
	data->dependentIndices[0] = 0;
	data->dependentIndices[1] = 1;
	data->dependentIndices[2] = 2;

	FWc3A2D_Kreis_initializeConstraintStructure(data);

	return (void *)data;

}


void FWc3A2D_Kreis_initializeConstraintStructure(struct FWc3A2D_Kreis_paraStruct *data){

	data->con = malloc(sizeof(struct FWc3A2D_Kreis_constraintStructure));

	data->con->C_      = callocMatrix((FWC3A2D_KREIS_NUM_POSCONSTRAINTS+FWC3A2D_KREIS_NUM_VELCONSTRAINTS),FWC3A2D_KREIS_SYS_DOF);
	data->con->Dc_     = calloc((FWC3A2D_KREIS_NUM_POSCONSTRAINTS+FWC3A2D_KREIS_NUM_VELCONSTRAINTS),sizeof(double));
	data->con->D2c_    = calloc((FWC3A2D_KREIS_NUM_POSCONSTRAINTS+FWC3A2D_KREIS_NUM_VELCONSTRAINTS),sizeof(double));
	data->con->G_      = callocMatrix(FWC3A2D_KREIS_SYS_DOF,(FWC3A2D_KREIS_NUM_POSCONSTRAINTS+FWC3A2D_KREIS_NUM_VELCONSTRAINTS));

	data->con->Jr_v    = callocMatrix(FWC3A2D_KREIS_SYS_DOF,FWC3A2D_KREIS_NUM_INDEPENDENT_GC);
	data->con->Jr_a    = callocMatrix(FWC3A2D_KREIS_SYS_DOF,FWC3A2D_KREIS_NUM_INDEPENDENT_GV);
	data->con->Jl_a    = callocMatrix(FWC3A2D_KREIS_NUM_INDEPENDENT_GV,FWC3A2D_KREIS_SYS_DOF);
	data->con->theta_  = calloc(FWC3A2D_KREIS_SYS_DOF,sizeof(double));
	data->con->gamma_  = calloc(FWC3A2D_KREIS_SYS_DOF,sizeof(double));

	data->con->lambda_ = calloc((FWC3A2D_KREIS_NUM_POSCONSTRAINTS+FWC3A2D_KREIS_NUM_VELCONSTRAINTS),sizeof(double));

}

void FWc3A2D_Kreis_freeConstraintStructure(struct FWc3A2D_Kreis_paraStruct *data) {

	freeMatrix((FWC3A2D_KREIS_NUM_POSCONSTRAINTS+FWC3A2D_KREIS_NUM_VELCONSTRAINTS),FWC3A2D_KREIS_SYS_DOF,data->con->C_);
	free(data->con->Dc_);
	free(data->con->D2c_);
	freeMatrix(FWC3A2D_KREIS_SYS_DOF,(FWC3A2D_KREIS_NUM_POSCONSTRAINTS+FWC3A2D_KREIS_NUM_VELCONSTRAINTS),data->con->G_);

	freeMatrix(FWC3A2D_KREIS_SYS_DOF,FWC3A2D_KREIS_NUM_INDEPENDENT_GC,data->con->Jr_v);
	freeMatrix(FWC3A2D_KREIS_SYS_DOF,FWC3A2D_KREIS_NUM_INDEPENDENT_GV,data->con->Jr_a);
	freeMatrix(FWC3A2D_KREIS_NUM_INDEPENDENT_GV,FWC3A2D_KREIS_SYS_DOF,data->con->Jl_a);
	free(data->con->theta_);
	free(data->con->gamma_);

	free(data->con->lambda_);

	free(data->con);

}

void FWc3A2D_Kreis_freeStructure(void *dataPtr){

	struct FWc3A2D_Kreis_paraStruct *data  = (struct FWc3A2D_Kreis_paraStruct *) dataPtr;

	FWc3A2D_Kreis_freeConstraintStructure(data);
	free(data);

}
