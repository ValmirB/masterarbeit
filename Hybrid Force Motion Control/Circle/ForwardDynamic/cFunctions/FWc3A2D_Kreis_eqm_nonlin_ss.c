#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "FWc3A2D_Kreis_pi_code.h"
#include "FWc3A2D_Kreis_userDefined.h"
#include "FWc3A2D_Kreis_pd_matlab.h"
#include "neweul.h"
#include "FWc3A2D_Kreis_paraStruct.h"
#include "FWc3A2D_Kreis_Flexor.h"

#include "FWc3A2D_Kreis_constraintEquations.h"
void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr = NULL;

	double t      = mxGetScalar( prhs[0] );
	double *x     = mxGetPr( prhs[1] );
	double *f     = NULL;
	double *u_    = NULL;

#if defined FWC3A2D_KREIS_NUM_ALL_INPUTS && FWC3A2D_KREIS_NUM_ALL_INPUTS > 0
	u_ = calloc( FWC3A2D_KREIS_NUM_ALL_INPUTS, sizeof(double));
#endif

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		free(u_);
		return;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		free(u_);
		return;
	}

	if ((mxGetM(prhs[1])!=(2*FWC3A2D_KREIS_SYS_DOF+FWC3A2D_KREIS_AUX_DOF)) || (mxGetN(prhs[1])!=1)){
		mexPrintf("The second input argument has to be a 8x1 vector!\n");
		free(u_);
		return;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*FWC3A2D_KREIS_SYS_DOF+FWC3A2D_KREIS_AUX_DOF, 1, mxREAL);
	f = mxGetPr(plhs[0]);

	/* Initialize System Struct */
	dataPtr = FWc3A2D_Kreis_initializeSystemStruct();

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		FWc3A2D_Kreis_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	/* Compute all inputs */
	FWc3A2D_Kreis_f_control_inputs(t, x, &(u_[0]), dataPtr);

	/* Evaluate System Dynamics */
	FWc3A2D_Kreis_equations_of_motion(t, x, u_, f, dataPtr);

	/* Evaluate Auxiliary Dynamics */
	FWc3A2D_Kreis_auxiliary_dynamics(t, x, u_, f, dataPtr);

	/* Free System Struct */
	FWc3A2D_Kreis_freeStructure(dataPtr);

#if defined FWC3A2D_KREIS_NUM_ALL_INPUTS && FWC3A2D_KREIS_NUM_ALL_INPUTS > 0
	free(u_);
#endif

}

