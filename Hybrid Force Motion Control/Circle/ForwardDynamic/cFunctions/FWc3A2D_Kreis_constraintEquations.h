#ifndef FWc3A2D_Kreis_constraintEquations_H
#define FWc3A2D_Kreis_constraintEquations_H

#include "FWc3A2D_Kreis_paraStruct.h"

void FWc3A2D_Kreis_constraintEquations(double t, double *x_, double *u_, void *dataPtr);

void FWc3A2D_Kreis_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr);

void FWc3A2D_Kreis_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr);

void FWc3A2D_Kreis_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr);

void FWc3A2D_Kreis_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr);

void FWc3A2D_Kreis_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr);

#endif
