#ifndef FWc3A2D_Kreis_linearizeMBS_H
#define FWc3A2D_Kreis_linearizeMBS_H
#include "FWc3A2D_Kreis_paraStruct.h"

/* Function prototypes */
void FWc3A2D_Kreis_createSystemMatrix(double t_WP, double *x_WP, double *u_WP,
    double **A, int dim, void *dataPtr);

void FWc3A2D_Kreis_createInputMatrix(double t_WP, double *x_WP, double *u_WP,
    double **B, int dim, int numIn, void *dataPtr);

void FWc3A2D_Kreis_createOutputMatrix(double t_WP, double *x_WP,
    double *u_WP, double **C,
    int dim, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));

void FWc3A2D_Kreis_createFeedthroughMatrix(double t_WP, double *x_WP,
    double *u_WP, double **D,
    int numIn, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));
#endif
