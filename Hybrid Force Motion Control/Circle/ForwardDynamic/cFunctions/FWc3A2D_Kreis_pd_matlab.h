#ifndef FWc3A2D_Kreis_pd_matlab_H
#define FWc3A2D_Kreis_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "FWc3A2D_Kreis_paraStruct.h"

double FWc3A2D_Kreis_getScalar(const void *context, const char *name, double defaultValue);

int FWc3A2D_Kreis_getSystemStruct(void *dataPtr, const void *context);

void FWc3A2D_Kreis_educatedGuess(double t, double *y_, void *dataPtr);
void FWc3A2D_Kreis_educatedGuess(double t, double *y_, void *dataPtr);

#endif
