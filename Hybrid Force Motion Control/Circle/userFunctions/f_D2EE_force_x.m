function D2EE_force_x = f_D2EE_force_x(t, x_, varargin)
% f_D2EE_force_x - definition of time-depending user-defined variable D2EE_force_x

global sys;

D2EE_force_x = zeros(1,1);


% END OF FILE

