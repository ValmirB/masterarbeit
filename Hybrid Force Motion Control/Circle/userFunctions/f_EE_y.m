function EE_y = f_EE_y(t, x_, varargin)
% f_EE_y - definition of time-depending user-defined variable EE_y

global sys;


% constant user-defined variables
EE_y_d = sys.parameters.data.EE_y_d;
EE_y = zeros(1,1);

EE_y(1) = EE_y_d;

% END OF FILE

