function EE_force_x = f_EE_force_x(t, x_, varargin)
% f_EE_force_x - definition of time-depending user-defined variable EE_force_x

global sys;


% constant user-defined variables
EE_force_x_d = sys.parameters.data.EE_force_x_d;
EE_force_x = zeros(1,1);

EE_force_x(1) = EE_force_x_d;

% END OF FILE

