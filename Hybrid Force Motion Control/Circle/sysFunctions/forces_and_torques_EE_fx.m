function varargout = forces_and_torques_EE_fx(t, x_, u_, kin)

global sys


% Force law parameters
f_and_l_ = zeros(6,1);

f_and_l_(1) = -1.0*EE_force_x;

varargout{1} = f_and_l_(1);
varargout{2} = f_and_l_(2);
varargout{3} = f_and_l_(3);

varargout{4} = f_and_l_(4);
varargout{5} = f_and_l_(5);
varargout{6} = f_and_l_(6);

