clear all 
close all 
clc
%%
currentFolder = pwd;
cd(currentFolder)
addpath(genpath([currentFolder '/SDOF_FilterValmir']))
addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))
addpath(genpath([currentFolder '/Trajectory']))
addpath(genpath([currentFolder '/neweulm2_20151213']))
% addpath(genpath([currentFolder '/ForwardDynamic']))
% addpath(genpath([currentFolder '/redefined_observer']))

load('sys.mat')
load('sysFW.mat')
load('snvLambda.mat')


SampleTime = 1e-3;
sampleTime = 1e-3;


%% Trajectory

sys.parameters.data.EE_x_d = 1.31;
sys.parameters.data.EE_y_d = 0.55;
sys.parameters.data.EE_Rot_d = 30*pi/180;


alpha =  pi/2;
Sw = [cos(alpha), -sin(alpha), 0; ...
     sin(alpha),  cos(alpha), 0; ...
     0,          0,           1];

% S = sysFW.kinematics(24).absolute.S;
% Sw = S'; % Rotation of the Wall

Fd = [5; 0; 0];
 
F_contact = Fd; 
 
sys.parameters.data.EE_force_x_d = F_contact(1);
sys.parameters.data.EE_force_y_d = F_contact(2);

%% Initial Condition

w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;

sysFW.parameters.data.w2_1 = w2_1;
sysFW.parameters.data.w3_1 = w3_1;
sysFW.parameters.data.v2_1 = v2_1;
sysFW.parameters.data.v3_1 = v3_1;

[y0_redef, dy0, ~, ~] = calcInitConditions(0,[0,0,-0.7,0.7,0,0,0,zeros(1,7)]', zeros(1,9)'); %-1 und 1 sind initial guesses der Winkel, zeros(3,1) sind Geschwindigkeit

y0_red = staticEquilibrium(0,y0_redef);

pos_redef = eqm_sysout_exactOut(0,[y0_redef; dy0],[y0(1,1),0,0,y0(2,1),0,0,y0(3,1),0,0]');
% pos_EE = eqm_sysout_exactOut(0,[y0_EE; dy0],[y0_EE(1,1),0,0,y0_EE(2,1),0,0,y0_EE(3,1),0,0]');

x_EE0 = pos_redef(1,1);
y_EE0 = pos_redef(2,1);

t_end   = 5;
t_start = 1;
x_end   = x_EE0;
y_end   = 0.30;

t = t_start:0.01:t_end;


run('traj_v_x.m')
run('traj_v_y.m')

%% Model Errors

% psi2_1 = -0.0093 ; % Wert aus Sens
% psi3_1 = -0.2091; % Wert aus Sens

% a_error = 0;
% b_error = 0;
% alpha_error = 0;
% beta_error = 0;
% gam_error = 0 * pi/180; 
% q1_error = 0 * pi/180 * 1/psi2_1; 
% q2_error = 0 * pi/180 * 1/psi3_1;
% 
% y_error = [a_error; b_error; alpha_error; beta_error; gam_error; q1_error; q2_error];
% y0_error = y0 + y_error;

% sysFW.parameters.data.m_ee = 0;
% sysFW.parameters.data.l3 = 0.4760; %0.4760

%%
% w2_1 = 1;
% w3_1 = 1;
% v2_1 = 1;
% v3_1 = 1;
% % 
% sysFW.parameters.data.w2_1 = w2_1;
% sysFW.parameters.data.w3_1 = w3_1;
% sysFW.parameters.data.v2_1 = v2_1;
% sysFW.parameters.data.v3_1 = v3_1;

% 
%    -0.2023
%     0.0518
%    -0.7414
%     0.8980
%    -0.3052
%    -0.3603
%    -0.3091


% actuators
% sys.parameters.data.p_a_d = -0.2023;
% sys.parameters.data.p_b_d = 0.0518;
% sys.parameters.data.r_gamma3_d = -0.3052;
% 
% [y0, Dy0] = calcInitConditions(0,[-1;1;0;0]);   % initial guess necessary
% y0 = staticEquilibrium(0,y0);


%% Controller


P_tilde = [0;0;0];
P = diag(P_tilde);

D_tilde = [0;0;0];
D = diag(D_tilde);

I = 0;

