#ifndef FDc3A2D_userDefined_H
#define FDc3A2D_userDefined_H

#include "FDc3A2D_paraStruct.h"
/* Time dependent values */
double FDc3A2D_f_p_a(double t, void *dataPtr);
double FDc3A2D_f_Dp_a(double t, void *dataPtr);
double FDc3A2D_f_D2p_a(double t, void *dataPtr);
double FDc3A2D_f_p_b(double t, void *dataPtr);
double FDc3A2D_f_Dp_b(double t, void *dataPtr);
double FDc3A2D_f_D2p_b(double t, void *dataPtr);
double FDc3A2D_f_r_gamma3(double t, void *dataPtr);
double FDc3A2D_f_Dr_gamma3(double t, void *dataPtr);
double FDc3A2D_f_D2r_gamma3(double t, void *dataPtr);

/* User-defined signals */

/* State dependent values */

/* Abbreviations */

double FDc3A2D_f_DelBo_EA1_2_x_(double *x_);

double FDc3A2D_f_DelBo_EA2_1_rot_z_(double *x_);

double FDc3A2D_f_DelBo_EA2_1_x_(double *x_);

double FDc3A2D_f_DelBo_EA2_2_rot_z_(double *x_);

double FDc3A2D_f_DelBo_EA2_2_x_(double *x_);

double FDc3A2D_f_DelBo_EA2_3_rot_z_(double *x_);

double FDc3A2D_f_DelBo_EA2_3_x_(double *x_);

double FDc3A2D_f_DelBo_EA2_3_y_(double *x_);

double FDc3A2D_f_DelBo_EA3_2_rot_x_(double *x_);

double FDc3A2D_f_DelBo_EA3_2_rot_z_(double *x_);

double FDc3A2D_f_DelBo_EA3_2_x_(double *x_);

double FDc3A2D_f_DelBo_EA3_2_y_(double *x_);

double FDc3A2D_f_elBo_EA1_2_x_(double *x_);

double FDc3A2D_f_elBo_EA2_1_rot_z_(double *x_);

double FDc3A2D_f_elBo_EA2_1_x_(double *x_);

double FDc3A2D_f_elBo_EA2_2_rot_z_(double *x_);

double FDc3A2D_f_elBo_EA2_2_x_(double *x_);

double FDc3A2D_f_elBo_EA2_3_rot_z_(double *x_);

double FDc3A2D_f_elBo_EA2_3_x_(double *x_);

double FDc3A2D_f_elBo_EA2_3_y_(double *x_);

double FDc3A2D_f_elBo_EA3_2_rot_x_(double *x_);

double FDc3A2D_f_elBo_EA3_2_rot_z_(double *x_);

double FDc3A2D_f_elBo_EA3_2_x_(double *x_);

double FDc3A2D_f_elBo_EA3_2_y_(double *x_);

/* Input groups */

void FDc3A2D_f_control_inputs(double t, double *x_, double *u_, void *dataPtr);

/* System outputs */

double FDc3A2D_output_Pos_x(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Pos_y(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Rot_angle_theta(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Vel_x(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Vel_y(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Rot_velocity(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Pos_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Pos_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Rot_angle_rlc_theta(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Vel_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Vel_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Rot_vel_rlc(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Curvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_Curvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_DCurvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double FDc3A2D_output_DCurvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

/* Output groups */

void FDc3A2D_f_exactOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void FDc3A2D_f_approxOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void FDc3A2D_f_Curvature_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

#endif
