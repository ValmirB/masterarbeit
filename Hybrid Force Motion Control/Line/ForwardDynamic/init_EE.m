%% Init Forward Model and Simulink

clear all 
clc
close all

%%

currentFolder = pwd;
cd(currentFolder)
addpath(genpath([currentFolder '/SDOF_FilterValmir']))
addpath(genpath([currentFolder '/Trajectory']))
addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))
addpath(genpath([currentFolder '/SC_Cfunctions/cFunctions']))

load('init_redefined.mat')
load('sys.mat')
load('sysSC')
load('sysFW')
load('snvLambda.mat')


SampleTime = 1e-3;
sampleTime = 1e-3;


alpha =  pi/2;
Sw = [cos(alpha), -sin(alpha), 0; ...
     sin(alpha),  cos(alpha), 0; ...
     0,          0,           1];
 
Fd = [-7; 0; 0];

F_start = Fd(1);
F_end = -3;


 
 
%% redefined output

w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;

w2_1 = 0;
w3_1 = 0;
v2_1 = 0;
v3_1 = 0;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;


sysSC.parameters.data.w2_1 = w2_1;
sysSC.parameters.data.w3_1 = w3_1;
sysSC.parameters.data.v2_1 = v2_1;
sysSC.parameters.data.v3_1 = v3_1;

dy0_red = zeros(7,1);

pos_redef_ = eqm_sysout_exactOut(0,[[y0_red(3:4);y0_red(6:7)]; zeros(4,1)],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');
pos_redef = eqm_sysout_approxOut(0,[[y0_red(3:4);y0_red(6:7)]; zeros(4,1)],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');

x_EE0 = pos_redef(1,1);
y_EE0 = pos_redef(2,1);
theta0 = pos_redef(3,1);

%% exact output

% actuators
sys.parameters.data.p_a_d = y0_red(1,1);
sys.parameters.data.p_b_d = y0_red(2,1);
sys.parameters.data.r_gamma3_d = y0_red(5,1);

[y0, dy0] = calcInitConditions(0,[-1;1;0;0], [zeros(3,1);0]);   % initial guess necessary
y0 = staticEquilibrium(0,y0);

pos_EE = eqm_sysout_exactOut(0,[y0; dy0],[y0_red(1,1),0,0,y0_red(2,1),0,0,y0_red(5,1),0,0]');



t_end   = 3;
t_start = 1;
x_end   = x_EE0;
y_end   = 0.30;

t = 0:1e-3:t_end+2;


run('traj_v_x.m')
run('traj_v_y.m')
run('Force_Traj.m') 

%% Controller


P_tilde = [0;0;0];
P = diag(P_tilde);

D_tilde = [0;0;0];
D = diag(D_tilde);

I = 0;


%%

% x_EE_6 = simout.data(:,1);
% y_EE_6 = simout.data(:,2);
% 
% save('EE_1e-5.mat','x_EE_6','y_EE_6')

