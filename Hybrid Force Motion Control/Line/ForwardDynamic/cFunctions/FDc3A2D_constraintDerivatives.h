#ifndef FDc3A2D_constraintDerivatives_H
#define FDc3A2D_constraintDerivatives_H

#include "FDc3A2D_paraStruct.h"

void FDc3A2D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
