% Velocity in y direction

% constant user-defined variables
force_end = F_end;
force_start = F_start;
traj_end = t_end;
traj_start = t_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = force_start;
endValue(1) = force_end;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = [-0.3125; 0; 1.3125; 0; -2.1875; 0; 2.1875; 0];

F_contact = 0.5*(endValue-startValue)*polyval(transitionPoly_,max(min(scaledTime_,1),-1)) ...
		+ 0.5*(startValue+endValue);
    
Fd = [t.' F_contact.'];
% END OF FILE
