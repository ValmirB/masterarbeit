function c_ = positionConstraints(t, x_, u_, varargin)

global sys;

% generalized coordinates, velocities and auxiliary coordinates

r_alpha1 = x_(1);
r_beta2 = x_(2);
% system inputs
p_a = u_(1);
p_b = u_(4);
r_gamma3 = u_(7);

% constant user-defined variables
d_axes = sys.parameters.data.d_axes;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

elBo_EA1_2_x_ =  SID_EA1_.frame.node(2).origin(1,:) + SID_EA1_.frame.node(2).phi(1,:) * x_(sys.model.body.EA1.data.edof.idx);
elBo_EA2_1_x_ =  SID_EA2_.frame.node(1).origin(1,:) + SID_EA2_.frame.node(1).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_x_ =  SID_EA2_.frame.node(2).origin(1,:) + SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_rot_z_ =  SID_EA2_.frame.node(3).orientation(3) + SID_EA2_.frame.node(3).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_x_ =  SID_EA2_.frame.node(3).origin(1,:) + SID_EA2_.frame.node(3).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_y_ =  SID_EA2_.frame.node(3).origin(2,:) + SID_EA2_.frame.node(3).phi(2,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA3_2_x_ =  SID_EA3_.frame.node(2).origin(1,:) + SID_EA3_.frame.node(2).phi(1,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_y_ =  SID_EA3_.frame.node(2).origin(2,:) + SID_EA3_.frame.node(2).phi(2,:) * x_(sys.model.body.EA3.data.edof.idx);


% Constraints on position level

c_ = zeros(3,1);

c_(1) = d_axes + p_b - 1.0*elBo_EA1_2_x_*cos(r_alpha1) + elBo_EA2_1_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
c_(2) = - 1.0*p_a - 1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_1_x_ - 1.0*elBo_EA2_2_x_) - 1.0*elBo_EA1_2_x_*sin(r_alpha1);
c_(3) = d_axes + p_b + elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*elBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.31;


% END OF FILE

