function A_ = reduced_systemMatrix(t, x_red)
% REDUCED_SYSTEMMATRIX - Jacobian of the nonlinear reduced
% equations of motion in state space form.
% ATTENTION: for QR the coordinates are not x_red but zi from the QR
global sys;

% Indices of the independent and dependent coordinates
id_dep_coord = sys.parameters.coordinates.idx_depCoord;
id_ind_velo  = sys.parameters.coordinates.idx_indVelo;

% Dependent coordinates
y_  = educatedGuess(t, x_red(1 : sys.counters.genCoord-sys.counters.posConstraints));
y_  = findroot(@positionConstraints, y_,[],'index',id_dep_coord,'preargs',t);
Dy_ = zeros(sys.counters.genCoord,1);
Dy_(id_ind_velo) = x_red(1+sys.counters.genCoord-sys.counters.posConstraints : 2*(sys.counters.genCoord-sys.counters.posConstraints));
Dy_ = dependent_velocities(t, y_, Dy_(id_ind_velo));
z_ = x_red(1+2*(sys.counters.genCoord-sys.counters.posConstraints) : end);

x_ = [y_; Dy_; z_];

% Evaluate all inputs
u_ = inputs_all(t, x_);

% Dynamics of the unconstrained system
[M_0, f_] = system_dynamics(t,[y_;Dy_], u_);

% Jacobian, velocities and accelerations due to the constraints
[Jr_a, Jl_a, Jr_v, theta_, gamma_] = constraintEquations(t, x_, u_);
KD_min = finiteDifferences(@(x) system_dynamics(t, x_ + [Jr_v*x(1:sys.counters.genCoord-sys.counters.posConstraints); Jr_a*x(sys.counters.genCoord-sys.counters.posConstraints+1:end)], u_), ...
    zeros(2*sys.counters.genCoord-2*sys.counters.posConstraints-sys.counters.velConstraints, 1), 1e-8);

A_ = [zeros(numel(id_ind_velo)) eye(numel(id_ind_velo)); ...
    (Jl_a*M_0*Jr_a)\(Jl_a*KD_min)];

% END OF FILE

