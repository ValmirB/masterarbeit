function u_ = inputs_all(t, x_)
% u_ = inputs_all(t, x_)
% inputs_all - Function to compose the complete input vector

% Vector of the system inputs

u_ = zeros(11,1);
u_(1:9) = endeffector_acceleration_inputs(t, x_);
u_(10:11) = contactforce_inputs(t, x_);

% END OF FILE

