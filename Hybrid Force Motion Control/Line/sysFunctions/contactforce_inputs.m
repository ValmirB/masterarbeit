function u_contactforce = contactforce_inputs(t, x_)
% contactforce_inputs - Wrapper function for contactforce inputs
% These inputs are stored under sys.in.contactforce
% --------------------- Automatically generated file! ---------------------

u_contactforce = zeros(2,1);

u_contactforce(1) = f_EE_force_x(t, x_);
u_contactforce(2) = f_EE_force_y(t, x_);

% END OF FILE

