function D2EE_force_y = f_D2EE_force_y(t, x_, varargin)
% f_D2EE_force_y - definition of time-depending user-defined variable D2EE_force_y

global sys;

D2EE_force_y = zeros(1,1);


% END OF FILE

