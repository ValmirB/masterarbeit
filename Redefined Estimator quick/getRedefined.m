%% Berechnung des Redefined Outputs 

function r_redefined = getRedefined(u)


%   p_a = u(1);
    p_b = u(2);
% 	p_alpha =  u(3); 
	r_beta2 = u(4);
    r_gamma3 = u(5);
    EA2_q001 = u(6);
	EA3_q001 = u(7);
    
    % redefined output factors
    w2_1 = 0.8;
    w3_1 = 0.8;
    v2_1 = 1.07;
    v3_1 = 0.5;

    elBo_EA2_2_rot_z_ = 0.12720812859558897067735472319328*EA2_q001;
    elBo_EA2_2_x_ = 0.6;
    d_axes = 6.100000e-01;

    r_redefined = zeros(3,1);
    r_redefined(1) = d_axes + p_b + l3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
    r_redefined(2) = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + EA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
    r_redefined(3) = r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1;

end
