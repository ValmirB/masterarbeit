function A_ = reduced_systemMatrix_new(t, x_)
% REDUCED_SYSTEMMATRIX - Jacobian of the nonlinear reduced
% equations of motion in state space form.
% ATTENTION: for QR the coordinates are not x_red but zi from the QR

%ATTENTION: ROOT FINDING IS DELETED HERE, x_ needs to be consistent!!
global sys;

% Indices of the independent and dependent coordinates
id_ind_velo  = sys.parameters.coordinates.idx_indVelo;

% Evaluate all inputs
u_ = inputs_all(t, x_);

% Dynamics of the unconstrained system
[M_0, f_] = system_dynamics(t,x_, u_);

% Jacobian, velocities and accelerations due to the constraints
[Jr_a, Jl_a, Jr_v, theta_, gamma_] = constraintEquations(t, x_, u_);
KD_min = finiteDifferences(@(x) system_dynamics(t, x_ + [Jr_v*x(1:sys.counters.genCoord-sys.counters.posConstraints); Jr_a*x(sys.counters.genCoord-sys.counters.posConstraints+1:end)], u_), ...
    zeros(2*sys.counters.genCoord-2*sys.counters.posConstraints-sys.counters.velConstraints, 1), 1e-8);

A_ = [zeros(numel(id_ind_velo)) eye(numel(id_ind_velo)); ...
    (Jl_a*M_0*Jr_a)\(Jl_a*KD_min)];

% END OF FILE

