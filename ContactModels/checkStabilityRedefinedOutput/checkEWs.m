% initial conditions
y0 = [0, 0, -1.194071810969746, 0.708785443371100, 0 , 0, 0]';

% endeffector position
output_ = eqm_sysout_exactOut(0, [y0; zeros(length(y0),1)], 0);
sys.parameters.data.EE_x_d = output_(1);
sys.parameters.data.EE_y_d = output_(2);
sys.parameters.data.EE_Rot_d = output_(3);


A_ = reduced_systemMatrix_new(0,[y0; zeros(length(y0),1)]);

c_ = real(eig(A_));
if max(c_) > 0
    disp('unstable!!!')
else
    disp('Stable.')
end
eig(A_)