function angleVec = getRotationAngle(frame,varargin)
% only 1 argument = take generalized coordinates for the Jacobian
% calculation (use this for gencoord formulation)
% e.g. getRotationAngle('EE')
% ATTENTION: THIS FUNCTION SHOULD ONLY WORK BETWEEN -pi/2...pi/2 as
% rotmat2kardan is limited to that

%2nd argument to specify coordinates (use this for timedep formulation)
% e.g. getRotationAngle('EE',[sym('p_a') sym('p_b') sym('r_alpha1'), sym('r_beta2'), sym('r_gamma3'), sym('EA2_q001'), sym('EA3_q001')])

if nargin == 2
    myCoords = varargin{1};
end

global sys

indexFrame = find(strcmp({sys.kinematics.id}, frame)==1); % getAbsoluteKinematics_new('r','ISYS',frame) ==> do not use this as this can change the sys? 
angleVec = rotmat2kardan(sys.kinematics(indexFrame).absolute.S);
vars = findSyms(angleVec);
vars = intersect(vars, sys.parameters.abbreviations.elastic);

%%%
 % Replace even the scalar abbreviations
    xyz_ = {'x','y','z'};
   for h_ = 1:numel(vars)
            if  strncmp(vars{h_},'elBo',4) || strncmp(vars{h_},'DelBo',5)
                [elb_, string_] = strtok(vars{h_},'_');
                [body_, string_] = strtok(string_,'_');
                [frameNr_, string_] = strtok(string_,'_');
                [direction_,string_] = strtok(string_,'_');
                if strcmp(direction_,'rot')
                    origin_ = 'orientation';
                    type_ = 'Psi';
                    [direction_] = strtok(string_,'_');
                else
                    origin_ = 'origin';
                    type_ = 'Phi';
                end

                sidIdx_ = sys.model.body.(body_).data.sidIdx;
                if strncmp(elb_,'D',1)
                    mySym_ = sym(sys.model.sid(sidIdx_).frame.node(str2double(frameNr_)).(lower(type_))(strcmp(xyz_,direction_),:)) * ...
                    cell2sym(sys.parameters.genVelo(sys.model.body.(body_).data.edof.idx)).';
                else
                    mySym_ = sym(sys.model.sid(sidIdx_).frame.node(str2double(frameNr_)).(origin_)(strcmp(xyz_,direction_),:)) + ...
                        sym(sys.model.sid(sidIdx_).frame.node(str2double(frameNr_)).(lower(type_))(strcmp(xyz_,direction_),:)) * ...
                    cell2sym(sys.parameters.genCoord(sys.model.body.(body_).data.edof.idx)).';
                end
                angleVec = mapleSubs(angleVec, vars{h_}, mySym_);
            else
                warning('Problem');
            end            
            
   end