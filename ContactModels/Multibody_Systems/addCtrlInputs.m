function addCtrlInputs(varargin)
% add trajectory to sysFunctions/inputs_all.m

for n_ = 2 : 2 : length(varargin)
    switch varargin{n_ - 1}
        case 'use_third_Arm'
            use_third_Arm_ = varargin{n_};
        otherwise
            disp('---------- Attention: Input does not match any option! ----------');
            keyboard;
    end
end

global sys
cd sysFunctions % switch
fid = fopen( 'inputs_all.m', 'wt');
% initialize
fprintf(fid, 'function u_ = inputs_all(t, x_) \n');
fprintf(fid, '%% u_ = inputs_all(t, x_) \n');
fprintf(fid, '%% inputs_all - Function to compose the complete input vector \n');
fprintf(fid, 'global desired sys \n\n');
fprintf(fid, '%% Vector of the system inputs \n');

if use_third_Arm_
    fprintf(fid, 'u_ = zeros(9,1); \n');
    fprintf(fid, 'u_(1:9,1) = interp1(desired.desired.time,[desired.desired.a, desired.desired.Da, desired.desired.D2a, desired.desired.b, desired.desired.Db, desired.desired.D2b, desired.desired.gamma, desired.desired.Dgamma, desired.desired.D2gamma],t,''spline'');%%control_inputs(t, x_); \n');
else
    fprintf(fid, 'u_ = zeros(6,1); \n');
    fprintf(fid, 'u_(1:6,1) = interp1(desired.desired.time,[desired.desired.a, desired.desired.Da, desired.desired.D2a, desired.desired.b, desired.desired.Db, desired.desired.D2b],t,''spline'');%%control_inputs(t, x_); \n');
end
    %     fprintf(fid, 'u_(1:6) = [-0.1*cos(2*t); 0.2*sin(2*t); 0.4*cos(2*t); -0.1*cos(2*t); 0.2*sin(2*t); 0.4*cos(2*t)]; \n');
    
    %     fprintf(fid, 'polya = polyfit(desired.desired.time,desired.desired.a,10); \n');
    %     fprintf(fid, 'polyDa = polyder(polya); \n');
    %     fprintf(fid, 'polyD2a = polyder(polyDa); \n');
    %     fprintf(fid, 'polyb = polyfit(desired.desired.time,desired.desired.b,10); \n');
    %     fprintf(fid, 'polyDb = polyder(polyb); \n');
    %     fprintf(fid, 'polyD2b = polyder(polyDb); \n');
    %     fprintf(fid, 'u_(1:6) = [polyval(polya,t);polyval(polyDa,t);polyval(polyD2a,t);polyval(polyb,t);polyval(polyDb,t);polyval(polyD2b,t)]; \n');
    
    fprintf(fid, '%% END OF FILE');
    fclose(fid);
    cd ../
end


