function Dtcp_traj2 = f_Dtcp_traj2(t, varargin)
% f_Dtcp_traj2 - definition of smooth transition Dtcp_traj2

global sys

% constant user-defined variables
traj_end = sys.parameters.data.traj_end;
traj_start = sys.parameters.data.traj_start;
traj_y_end = sys.parameters.data.traj_y_end;
traj_y_start = sys.parameters.data.traj_y_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = traj_y_start;
endValue(1) = traj_y_end;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = sys.parameters.data.Dtcp_traj2.poly;

Dtcp_traj2 = 0.5*(endValue-startValue)*(2/(endTime-startTime))*polyval(transitionPoly_,max(min(scaledTime_,1),-1));

% END OF FILE
