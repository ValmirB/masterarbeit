function y_ = educatedGuess(t, y_ind_)
% EDUCATEDGUESS - Function that computes an educated guess for the root search

global sys;

y_ = zeros(sys.counters.genCoord,1);
y_(sys.parameters.coordinates.idx_depCoord,1) = [0;0;-1;1;0];
y_(sys.parameters.coordinates.idx_indCoord,1) = y_ind_;



% END OF FILE

