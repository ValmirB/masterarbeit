function DF_contact = f_DF_contact(t, varargin)
% f_DF_contact - definition of smooth transition DF_contact

global sys

% constant user-defined variables
force_end = sys.parameters.data.force_end;
force_start = sys.parameters.data.force_start;
traj_end = sys.parameters.data.traj_end;
traj_start = sys.parameters.data.traj_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = force_start;
endValue(1) = force_end;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = sys.parameters.data.DF_contact.poly;

DF_contact = 0.5*(endValue-startValue)*(2/(endTime-startTime))*polyval(transitionPoly_,max(min(scaledTime_,1),-1));

% END OF FILE
