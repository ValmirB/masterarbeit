function u_ = inputs_all(t, x_)
% u_ = inputs_all(t, x_)
% inputs_all - Function to compose the complete input vector

% Vector of the system inputs

u_ = zeros(0,1);

% END OF FILE

