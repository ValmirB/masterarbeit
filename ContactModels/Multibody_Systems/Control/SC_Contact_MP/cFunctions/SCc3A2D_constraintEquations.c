#include "SCc3A2D_userDefined.h"
#include <stdio.h>
#include <math.h>
#include "SCc3A2D_pi_code.h"
#include <stdlib.h>
#include "neweul.h"
#include "SCc3A2D_paraStruct.h"
#include <string.h>
#include "SCc3A2D_Flexor.h"
#include "SCc3A2D_constraintEquations.h"
#include "SCc3A2D_constraintDerivatives.h"

void SCc3A2D_constraintEquations(double t, double *x_, double *u_, void *dataPtr){
/* SCc3A2D_CONSTRAINT_EQUATIONS - Assembly of the Jacobian matrix,         */
/* the velocity and accelerations due to the constraints.            */
/* Usage of the qr decomposition to remove the Lagrange multipliers. */

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;

	int i_ = 0;
	int j_ = 0;
	int k_ = 0;

	double **C_     = data->con->C_;
	double *Dc_     = data->con->Dc_;
	double *D2c_    = data->con->D2c_;
	double **G_     = data->con->G_;

	double **Jr_v  = data->con->Jr_v;
	double **Jr_a  = data->con->Jr_a;
	double **Jl_a  = data->con->Jl_a;
	double *theta_ = data->con->theta_;
	double *gamma_ = data->con->gamma_;

	double **Q_c_  = callocMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF);
	double **R_c_  = callocMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS));
	double **Q_g_  = callocMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF);
	double **R_g_  = callocMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS));
	double **Q_acc = callocMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS));
	double **Q_vel = callocMatrix(SCC3A2D_SYS_DOF,SCC3A2D_NUM_POSCONSTRAINTS);
	double **R_acc = callocMatrix((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS));
	double **R_vel = callocMatrix((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),SCC3A2D_NUM_POSCONSTRAINTS);
	double *temp_  = calloc((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),sizeof(double));
	unsigned short *z = calloc((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),sizeof(unsigned short));

	SCc3A2D_constraint_derivatives(t, x_, u_, data);

	for(i_ = 0; i_ < (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); i_++){
		for(j_ = 0; j_ < SCC3A2D_SYS_DOF; j_++){
			R_c_[j_][i_] = C_[i_][j_];
		}
	}

	/* Perform a qr decomposition */
	householderQR(SCC3A2D_SYS_DOF, (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), R_c_, Q_c_);

	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); j_++){
			R_g_[i_][j_] = G_[i_][j_];
		}
	}

	householderQR(SCC3A2D_SYS_DOF, (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), R_g_, Q_g_);

	/* Get subspace */
	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); j_++){
			Q_acc[i_][j_] = Q_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < SCC3A2D_NUM_POSCONSTRAINTS; j_++){
			Q_vel[i_][j_] = Q_c_[i_][j_];
		}
	}

	/* Jacobian mapping independent coordinates to all genCoords: */
	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); j_ < SCC3A2D_SYS_DOF; j_++){
			Jr_a[i_][j_-(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS)] = Q_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = SCC3A2D_NUM_POSCONSTRAINTS; j_ < SCC3A2D_SYS_DOF; j_++){
			Jr_v[i_][j_-SCC3A2D_NUM_POSCONSTRAINTS] = Q_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); j_ < SCC3A2D_SYS_DOF; j_++){
			Jl_a[j_-(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS)][i_] = Q_g_[i_][j_];
		}
	}

	for(i_ = 0; i_ < (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); i_++){
		for(j_ = 0; j_ < (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); j_++){
			R_acc[j_][i_] = R_c_[i_][j_];
		}
	}

	for(i_ = 0; i_ < SCC3A2D_NUM_POSCONSTRAINTS; i_++){
		for(j_ = 0; j_ < (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); j_++){
			R_vel[j_][i_] = R_c_[i_][j_];
		}
	}

	/* Local velocities */
	memcpy(temp_, Dc_, (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS)*sizeof(double));
	InversionByLU1dim((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), z, R_vel, temp_);
	MatrixVectorMultiplication('N', SCC3A2D_SYS_DOF, SCC3A2D_NUM_POSCONSTRAINTS, -1.0, Q_vel, temp_, 0.0, theta_);

	/* Local accelarations */
	memcpy(temp_, D2c_, (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS)*sizeof(double));
	InversionByLU1dim((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), z, R_acc, temp_);
	MatrixVectorMultiplication('N', SCC3A2D_SYS_DOF, (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), -1.0, Q_acc, temp_, 0.0, gamma_);

	free(z);
	free(temp_);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,Q_c_);
	freeMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),R_c_);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,Q_g_);
	freeMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),R_g_);
	freeMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),Q_acc);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_NUM_POSCONSTRAINTS,Q_vel);
	freeMatrix((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),R_acc);
	freeMatrix((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),SCC3A2D_NUM_POSCONSTRAINTS,R_vel);

}

void SCc3A2D_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;

	double *deltaY       = calloc(5,sizeof(double));
	double *Dy_ind       = calloc(2,sizeof(double));
	double **C_dep       = callocMatrix(5,5);
	double **C_ind       = callocMatrix(5,2);

	double normRes_      = 1.0;
	double myTol_        = 1e-8;

	unsigned short *z    = calloc(5,sizeof(unsigned short));
	int counter_         = 0;
	int numIter_         = 0;
	int maxIter_         = 15;
	int k_ = 0;

	/* y_ contains the educated guess */
	while ( (normRes_ > myTol_) && (numIter_ < maxIter_) ){

		/* get constraintEq */
		SCc3A2D_positionConstraints(t, x_, u_, deltaY, C_dep, dataPtr);
		normRes_ = taxicabNorm_vec(5,deltaY);

		/* get deltaY */
		InversionByLU1dim(5, z, C_dep, deltaY);

		normRes_ = maxDouble(taxicabNorm_vec(5,deltaY),normRes_);

		for ( counter_ = 0 ; counter_ < 5 ; counter_++ ){
			x_[data->dependentIndices[counter_]] = x_[data->dependentIndices[counter_]] - deltaY[counter_];
		}

		numIter_ += 1;

	}

	for ( counter_ = 0 ; counter_ < 2 ; counter_++ ){
		Dy_ind[counter_] = x_[data->independentIndices[counter_] + SCC3A2D_SYS_DOF];
	}

	for ( counter_ = 0 ; counter_ < 5 ; counter_++ ){
		deltaY[counter_] = 0.0;
	}

	SCc3A2D_localVelocityConstraints( t, x_, u_, deltaY, C_ind, dataPtr);

	MatrixVectorMultiplication('N', 5, 2, -1.0,  C_ind, Dy_ind, -1.0, deltaY);

	permuteVector(5, z, deltaY);

	LUForBack_1dim(5, C_dep, deltaY);

	for ( counter_ = 0 ; counter_ < 5 ; counter_++ ){
		x_[data->dependentIndices[counter_] + SCC3A2D_SYS_DOF] = deltaY[counter_];
	}

	/* Freeing allocated memory */
	free(deltaY);
	free(Dy_ind);
	free(z);
	freeMatrix(5,5,C_dep);
	freeMatrix(5,2,C_ind);

}

void SCc3A2D_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;

	int i_ = 0;
	int j_ = 0;

	double **C_     = data->con->C_;
	double *Dc_     = data->con->Dc_;
	double *D2c_    = data->con->D2c_;
	double **G_     = data->con->G_;
	double *lambda_ = data->con->lambda_;

	double **M     = callocMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF);
	double **MTemp = callocMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF);
	double **mCxmC = callocMatrix((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS));
	double **GTemp = callocMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS));
	double *f      = calloc(SCC3A2D_SYS_DOF,sizeof(double));
	unsigned short *z = calloc((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),sizeof(unsigned short));

	SCc3A2D_constraint_derivatives(t, x_, u_, dataPtr);


	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS); j_++){
			GTemp[i_][j_] = G_[i_][j_];
		}
	}

	/* Evaluate system dynamics */
	SCc3A2D_system_dynamics(t, x_, u_, f, M, dataPtr);

	for(i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++){
		for(j_ = 0; j_ < SCC3A2D_SYS_DOF; j_++){
			MTemp[i_][j_] = M[i_][j_];
		}
	}

	/* f = M^(-1)*f */
	cholDecomposition(SCC3A2D_SYS_DOF, M, f);

	/* lambda_ =  C*M^(-1)*f + D2c_ */
	MatrixVectorMultiplicationMod('N', (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), SCC3A2D_SYS_DOF, 1.0, C_, f, 1.0, D2c_, lambda_);

	/* GTemp = M^(-1)*G */
	cholDecomposition2dim(SCC3A2D_SYS_DOF, (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), MTemp, GTemp);

	/* mCxmC = -C*M^(-1)*G */
	MatrixMultiplication('N', 'N', (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), SCC3A2D_SYS_DOF, (SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), -1.0, C_, GTemp, 0.0, mCxmC);

	/* lambda_ = mCxmC^(-1) * lambda_ ; please note that cholesky is not possible here */
	InversionByLU1dim((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS), z, mCxmC, lambda_);

	free(z);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,MTemp);
	freeMatrix((SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),mCxmC);
	freeMatrix(SCC3A2D_SYS_DOF,(SCC3A2D_NUM_POSCONSTRAINTS+SCC3A2D_NUM_VELCONSTRAINTS),GTemp);
	free(f);

}

void SCc3A2D_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double d_axes = data->d_axes;
	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double psi3_1 = data->psi3_1;
	double v2_1 = data->v2_1;
	double v3_1 = data->v3_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* user-defined signals */

	double tcp_traj1 = SCc3A2D_f_tcp_traj1(t, data);
	double tcp_traj2 = SCc3A2D_f_tcp_traj2(t, data);
	double tcp_traj_theta = SCc3A2D_f_tcp_traj_theta(t, data);


	/* Automatically introduced abbreviations */

	double elBo_EA1_2_x_ = SCc3A2D_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = SCc3A2D_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = SCc3A2D_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A2D_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double p_a = x_[0];
	double p_b = x_[1];
	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double EA2_q001 = x_[5];
	double EA3_q001 = x_[6];



	/* Dependent constraint matrix C */
	C_dep[0][0] = 0.0;
	C_dep[1][0] = - 1.0*pow(cos(r_alpha1),2.0)- 1.0*pow(sin(r_alpha1),2.0);
	C_dep[2][0] = 0.0;
	C_dep[3][0] = 0.0;
	C_dep[4][0] = 0.0;
	C_dep[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_dep[1][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_dep[2][1] = cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(cos(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + sin(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) + sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(sin(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))));
	C_dep[3][1] = cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(sin(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) - 1.0*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(cos(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + sin(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))));
	C_dep[4][1] = 0.0;
	C_dep[0][2] = elBo_EA1_2_x_*sin(r_alpha1);
	C_dep[1][2] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	C_dep[2][2] = 0.0;
	C_dep[3][2] = 0.0;
	C_dep[4][2] = 0.0;
	C_dep[0][3] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_dep[1][3] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_dep[2][3] = 1.0*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(1.0*cos(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA3_q001*phi3_1*w3_1 + EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)) - 1.0*sin(EA3_q001*psi3_1*v3_1)*(l3 - 1.0*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1))) - 1.0*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(sin(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA3_q001*phi3_1*w3_1 + EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)) + cos(EA3_q001*psi3_1*v3_1)*(l3 - 1.0*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)));
	C_dep[3][3] = 1.0*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(1.0*cos(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA3_q001*phi3_1*w3_1 + EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)) - 1.0*sin(EA3_q001*psi3_1*v3_1)*(l3 - 1.0*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1))) + sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(sin(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA3_q001*phi3_1*w3_1 + EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)) + cos(EA3_q001*psi3_1*v3_1)*(l3 - 1.0*cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)));
	C_dep[4][3] = -1.0;
	C_dep[0][4] = 0.0;
	C_dep[1][4] = 0.0;
	C_dep[2][4] = - 1.0*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(l3*sin(EA3_q001*psi3_1*v3_1) - 1.0*EA3_q001*phi3_1*w3_1*cos(EA3_q001*psi3_1*v3_1)) - 1.0*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(l3*cos(EA3_q001*psi3_1*v3_1) + EA3_q001*phi3_1*w3_1*sin(EA3_q001*psi3_1*v3_1));
	C_dep[3][4] = sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(l3*cos(EA3_q001*psi3_1*v3_1) + EA3_q001*phi3_1*w3_1*sin(EA3_q001*psi3_1*v3_1)) - 1.0*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(l3*sin(EA3_q001*psi3_1*v3_1) - 1.0*EA3_q001*phi3_1*w3_1*cos(EA3_q001*psi3_1*v3_1));
	C_dep[4][4] = -1.0;

	/* Constraint equations on position level */
	con_eq[0] = d_axes + p_b - 1.0*elBo_EA1_2_x_*cos(r_alpha1) + elBo_EA2_1_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	con_eq[1] = - 1.0*p_a - 1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_1_x_ - 1.0*elBo_EA2_2_x_) - 1.0*elBo_EA1_2_x_*sin(r_alpha1);
	con_eq[2] = tcp_traj2 - 1.0*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*l3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) + l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	con_eq[3] = tcp_traj1 - 1.0*p_b - 1.0*d_axes - 1.0*l3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + EA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	con_eq[4] = 2.0*atan(sin(tcp_traj_theta)/(cos(tcp_traj_theta) +sqrt( (pow(cos(tcp_traj_theta),2.0)+pow( sin(tcp_traj_theta),2.0))))) - 2.0*atan(sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)/(cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1) +sqrt( (pow(cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1),2.0)+pow( sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1),2.0)))));

}

void SCc3A2D_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double l2 = data->l2;
	double l3 = data->l3;
	double phi2_1 = data->phi2_1;
	double phi3_1 = data->phi3_1;
	double psi2_1 = data->psi2_1;
	double psi3_1 = data->psi3_1;
	double v2_1 = data->v2_1;
	double v3_1 = data->v3_1;
	double w2_1 = data->w2_1;
	double w3_1 = data->w3_1;


	/* user-defined signals */

	double Dtcp_traj1 = SCc3A2D_f_Dtcp_traj1(t, data);
	double Dtcp_traj2 = SCc3A2D_f_Dtcp_traj2(t, data);
	double Dtcp_traj_theta = SCc3A2D_f_Dtcp_traj_theta(t, data);
	double tcp_traj_theta = SCc3A2D_f_tcp_traj_theta(t, data);


	/* Automatically introduced abbreviations */

	double elBo_EA2_1_rot_z_ = SCc3A2D_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = SCc3A2D_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A2D_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double EA2_q001 = x_[5];
	double EA3_q001 = x_[6];


	/* Independent constraint matrix C */
	C_ind[0][0] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));
	C_ind[1][0] = cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_ind[2][0] = - 1.0*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(cos(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*EA3_q001*phi3_1*w3_1*(psi2_1*v2_1 - 0.12720812859558897067735472319328)) + sin(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + l3*(psi2_1*v2_1 - 0.12720812859558897067735472319328) - 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1))) - 1.0*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(1.0*cos(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + l3*(psi2_1*v2_1 - 0.12720812859558897067735472319328) - 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)) - 1.0*sin(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*EA3_q001*phi3_1*w3_1*(psi2_1*v2_1 - 0.12720812859558897067735472319328)));
	C_ind[3][0] = 1.0*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(1.0*cos(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + l3*(psi2_1*v2_1 - 0.12720812859558897067735472319328) - 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)) - 1.0*sin(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*EA3_q001*phi3_1*w3_1*(psi2_1*v2_1 - 0.12720812859558897067735472319328))) - 1.0*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*(cos(EA3_q001*psi3_1*v3_1)*(sin(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*cos(r_gamma3 + EA2_q001*psi2_1*v2_1) - 1.0*EA3_q001*phi3_1*w3_1*(psi2_1*v2_1 - 0.12720812859558897067735472319328)) + sin(EA3_q001*psi3_1*v3_1)*(cos(r_gamma3 + EA2_q001*psi2_1*v2_1)*(phi2_1*w2_1 - 0.12720812859558897067735472319328*l2 + 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + l3*(psi2_1*v2_1 - 0.12720812859558897067735472319328) - 0.12720812859558897067735472319328*EA2_q001*phi2_1*w2_1*sin(r_gamma3 + EA2_q001*psi2_1*v2_1)));
	C_ind[4][0] = 0.12720812859558897067735472319328 - 1.0*psi2_1*v2_1;
	C_ind[0][1] = 0.0;
	C_ind[1][1] = 0.0;
	C_ind[2][1] = - 1.0*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*sin(EA3_q001*psi3_1*v3_1) - 1.0*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*cos(EA3_q001*psi3_1*v3_1);
	C_ind[3][1] = phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*cos(EA3_q001*psi3_1*v3_1) - 1.0*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + r_gamma3 + EA2_q001*psi2_1*v2_1 + EA3_q001*psi3_1*v3_1)*sin(EA3_q001*psi3_1*v3_1);
	C_ind[4][1] = -1.0*psi3_1*v3_1;

	/* Local constraint equations on velocity level */
	Dc_[0] = 0.0;
	Dc_[1] = 0.0;
	Dc_[2] = sin(tcp_traj_theta)*(Dtcp_traj1*cos(tcp_traj_theta) + Dtcp_traj2*sin(tcp_traj_theta)) + cos(tcp_traj_theta)*(Dtcp_traj2*cos(tcp_traj_theta) - 1.0*Dtcp_traj1*sin(tcp_traj_theta));
	Dc_[3] = cos(tcp_traj_theta)*(Dtcp_traj1*cos(tcp_traj_theta) + Dtcp_traj2*sin(tcp_traj_theta)) - 1.0*sin(tcp_traj_theta)*(Dtcp_traj2*cos(tcp_traj_theta) - 1.0*Dtcp_traj1*sin(tcp_traj_theta));
	Dc_[4] = Dtcp_traj_theta;

}

void SCc3A2D_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double K_L = data->K_L;
	double K_R = data->K_R;


	/* Automatically introduced abbreviations */

	double elBo_EA1_2_x_ = SCc3A2D_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = SCc3A2D_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = SCc3A2D_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A2D_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];

	/* Help vars */
	double **GT_ind      = callocMatrix(5,2);
	double **GT_dep      = callocMatrix(5,5);
	double **C_dep       = callocMatrix(5,5);
	double **C_ind       = callocMatrix(5,2);
	unsigned short *z    = calloc(5,sizeof(unsigned short));
	int i_               = 0;
	int j_               = 0;

	double **C_    = data->con->C_;

	SCc3A2D_constraint_derivatives(t, x_, u_, dataPtr);

	/* Independent constraint matrix C (sign changed) */
	C_ind[0][0] = -C_[0][5];
	C_ind[0][1] = -C_[0][6];
	C_ind[1][0] = -C_[1][5];
	C_ind[1][1] = -C_[1][6];
	C_ind[2][0] = -C_[2][5];
	C_ind[2][1] = -C_[2][6];
	C_ind[3][0] = -C_[3][5];
	C_ind[3][1] = -C_[3][6];
	C_ind[4][0] = -C_[4][5];
	C_ind[4][1] = -C_[4][6];

	/* Dependent constraint matrix C */
	C_dep[0][0] = C_[0][0];
	C_dep[0][1] = C_[0][1];
	C_dep[0][2] = C_[0][2];
	C_dep[0][3] = C_[0][3];
	C_dep[0][4] = C_[0][4];
	C_dep[1][0] = C_[1][0];
	C_dep[1][1] = C_[1][1];
	C_dep[1][2] = C_[1][2];
	C_dep[1][3] = C_[1][3];
	C_dep[1][4] = C_[1][4];
	C_dep[2][0] = C_[2][0];
	C_dep[2][1] = C_[2][1];
	C_dep[2][2] = C_[2][2];
	C_dep[2][3] = C_[2][3];
	C_dep[2][4] = C_[2][4];
	C_dep[3][0] = C_[3][0];
	C_dep[3][1] = C_[3][1];
	C_dep[3][2] = C_[3][2];
	C_dep[3][3] = C_[3][3];
	C_dep[3][4] = C_[3][4];
	C_dep[4][0] = C_[4][0];
	C_dep[4][1] = C_[4][1];
	C_dep[4][2] = C_[4][2];
	C_dep[4][3] = C_[4][3];
	C_dep[4][4] = C_[4][4];

	/* Transposed independent input matrix G (sign changed)*/
	GT_ind[0][0] = cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) + sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));
	GT_ind[1][0] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));

	/* Transposed dependent input matrix G */
	GT_dep[1][0] = - 1.0*pow(cos(r_alpha1),2.0)- 1.0*pow(sin(r_alpha1),2.0);
	GT_dep[2][0] = K_L;
	GT_dep[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_dep[1][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_dep[3][1] = K_L;
	GT_dep[0][2] = elBo_EA1_2_x_*sin(r_alpha1);
	GT_dep[1][2] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	GT_dep[0][3] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	GT_dep[1][3] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	GT_dep[4][4] = K_R;

	InversionByLU(5, 2, z, C_dep, C_ind);

	InversionByLU(5, 2, z, GT_dep, GT_ind);

	for ( i_ = 0 ; i_ < 2 ; i_++ ){
		Jr[data->independentIndices[i_]][i_]  =  1.0;
		Jl[data->independentIndices[i_]][i_]  =  1.0;
		for ( j_ = 0 ; j_ < 5 ; j_++ ){
			Jr[data->dependentIndices[j_]][i_]  =  C_ind[j_][i_];
			Jl[data->dependentIndices[j_]][i_]  =  GT_ind[j_][i_];
		}
	}

	freeMatrix(5,2,GT_ind);
	freeMatrix(5,5,GT_dep);
	freeMatrix(5,5,C_dep);
	freeMatrix(5,2,C_ind);
	free(z);

}
