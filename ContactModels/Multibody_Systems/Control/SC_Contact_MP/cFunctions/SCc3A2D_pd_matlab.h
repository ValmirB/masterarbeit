#ifndef SCc3A2D_pd_matlab_H
#define SCc3A2D_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "SCc3A2D_paraStruct.h"

double SCc3A2D_getScalar(const void *context, const char *name, double defaultValue);

int SCc3A2D_getSystemStruct(void *dataPtr, const void *context);

void SCc3A2D_educatedGuess(double t, double *y_, void *dataPtr);
void SCc3A2D_educatedGuess(double t, double *y_, void *dataPtr);

#endif
