#ifndef SCc3A2D_userDefined_H
#define SCc3A2D_userDefined_H

#include "SCc3A2D_paraStruct.h"
/* Time dependent values */

/* User-defined signals */

double SCc3A2D_f_tcp_traj1(double t, void *dataPtr);

double SCc3A2D_f_Dtcp_traj1(double t, void *dataPtr);

double SCc3A2D_f_D2tcp_traj1(double t, void *dataPtr);

double SCc3A2D_f_tcp_traj2(double t, void *dataPtr);

double SCc3A2D_f_Dtcp_traj2(double t, void *dataPtr);

double SCc3A2D_f_D2tcp_traj2(double t, void *dataPtr);

double SCc3A2D_f_tcp_traj_theta(double t, void *dataPtr);

double SCc3A2D_f_Dtcp_traj_theta(double t, void *dataPtr);

double SCc3A2D_f_D2tcp_traj_theta(double t, void *dataPtr);

double SCc3A2D_f_F_contact(double t, void *dataPtr);

double SCc3A2D_f_DF_contact(double t, void *dataPtr);

double SCc3A2D_f_D2F_contact(double t, void *dataPtr);

/* State dependent values */

/* Abbreviations */

double SCc3A2D_f_DelBo_EA1_2_x_(double *x_);

double SCc3A2D_f_DelBo_EA2_1_rot_z_(double *x_);

double SCc3A2D_f_DelBo_EA2_1_x_(double *x_);

double SCc3A2D_f_DelBo_EA2_2_rot_z_(double *x_);

double SCc3A2D_f_DelBo_EA2_2_x_(double *x_);

double SCc3A2D_f_DelBo_EA2_3_rot_z_(double *x_);

double SCc3A2D_f_DelBo_EA2_3_x_(double *x_);

double SCc3A2D_f_DelBo_EA2_3_y_(double *x_);

double SCc3A2D_f_DelBo_EA3_2_rot_x_(double *x_);

double SCc3A2D_f_DelBo_EA3_2_rot_z_(double *x_);

double SCc3A2D_f_DelBo_EA3_2_x_(double *x_);

double SCc3A2D_f_DelBo_EA3_2_y_(double *x_);

double SCc3A2D_f_elBo_EA1_2_x_(double *x_);

double SCc3A2D_f_elBo_EA2_1_rot_z_(double *x_);

double SCc3A2D_f_elBo_EA2_1_x_(double *x_);

double SCc3A2D_f_elBo_EA2_2_rot_z_(double *x_);

double SCc3A2D_f_elBo_EA2_2_x_(double *x_);

double SCc3A2D_f_elBo_EA2_3_rot_z_(double *x_);

double SCc3A2D_f_elBo_EA2_3_x_(double *x_);

double SCc3A2D_f_elBo_EA2_3_y_(double *x_);

double SCc3A2D_f_elBo_EA3_2_rot_x_(double *x_);

double SCc3A2D_f_elBo_EA3_2_rot_z_(double *x_);

double SCc3A2D_f_elBo_EA3_2_x_(double *x_);

double SCc3A2D_f_elBo_EA3_2_y_(double *x_);

/* Force elements */
double forces_CF(double t, double *x_, double *u_, double *f_CF, void *dataPtr);
/* Input groups */

/* System outputs */

double SCc3A2D_output_des_x(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_des_y(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_des_Dx(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_des_Dy(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Pos_x(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Pos_y(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Rot_angle_theta(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Vel_x(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Vel_y(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Rot_velocity(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Pos_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Pos_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Rot_angle_rlc_theta(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Vel_x_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Vel_y_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Rot_vel_rlc(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Curvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_Curvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_DCurvature_1_EA2(double t, double *x_, double *u_, void *dataPtr);

double SCc3A2D_output_DCurvature_1_EA3(double t, double *x_, double *u_, void *dataPtr);

/* Output groups */

void SCc3A2D_f_desiredOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void SCc3A2D_f_exactOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void SCc3A2D_f_approxOut_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

void SCc3A2D_f_Curvature_outputs(double t, double *x_, double *u_, double *z, void *dataPtr);

#endif
