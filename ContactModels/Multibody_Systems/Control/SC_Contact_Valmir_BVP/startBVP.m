%% Initialize
% if BVP does not work try:
% - mass balance to make minimum phase or
% - output redefinition to make minimum phase
% ==> then use approximate solution as initial guess
% ==> maybe use finiteDifferences or reducedSystemMatrix (switching might
% help, finiteDifferences should be more accurate)

% Clean up
clear all; close all; clc;

% Add folder of the sysDef_basic
if ~(exist('sysDef_basic','file')==2)
    currentDir_ = pwd;
    folderName_ = 'Multibody_Systems';
    strEnd_ = strfind(currentDir_,[filesep,folderName_,filesep]) + length(folderName_);
    addpath(currentDir_(1:strEnd_));
end

currentFolder = pwd;
addpath(genpath(currentFolder));
load('sys.mat');
global sys


%% Trajectory definition
trajectoryTime_ = 2; % in seconds

sys.parameters.data.force_start = 7;
sys.parameters.data.force_end = 3;

sys.parameters.data.traj_x_start = 1.31;
sys.parameters.data.traj_x_end = 1.31;

sys.parameters.data.traj_y_start = 0.55;
sys.parameters.data.traj_y_end = 0.30;

sys.parameters.data.traj_theta_start = pi/6;
sys.parameters.data.traj_theta_end = pi/6;



%% output redefinition
sys.parameters.data.w2_1 = 1
sys.parameters.data.w3_1 = 1
sys.parameters.data.v2_1 = 1
sys.parameters.data.v3_1 = 1


%% Start BVP

sys.parameters.data.traj_end = sys.parameters.data.traj_start + trajectoryTime_;
numSamples = 1 + (2+sys.parameters.data.traj_end-sys.parameters.data.traj_start)/(5e-4);

bvp_res = startBVP_basic(numSamples);


%% Postprocess & Export

global EEexport;
EEexport.output = 'desOut'; % this exports the written output to the hdf5file as desired EE
dateString = strrep(strrep(datestr(now),':','_'),' ','_');
hdf5filename = (['FF_',sys.info.systemSetup.trajectoryType,'_',num2str(sys.parameters.data.traj_end-sys.parameters.data.traj_start) ,'s_traj_',num2str(numel(sys.parameters.coordinates.elastic)),'edof_EETP',sys.info.systemSetup.EE_Tracking_Point,'_',EEexport.output,'_',dateString,'.h5']);
feedbackFF = postProcessBVP(bvp_res, numSamples, hdf5filename);

%     EEexport.output = 'rlcNLG'; % this exports the relocatedNLGEOM output to the hdf5file as desired EE although the SCs are used for a non-relocated output
%     hdf5filename_rlc = (['FF_',trajectoryType_,'_',num2str(sys.parameters.data.traj_end-sys.parameters.data.traj_start) ,'s_traj_',num2str(numel(sys.parameters.coordinates.elastic)),'edof_SCNLG_desrlcNLG_w',num2str(sys.parameters.data.w1),'_',dateString,'.h5']);
%     [feedbackFF_rlc, FF_test_struct_rlc] = postProcessBVP(bvp_res, numSamples, hdf5filename_rlc);

clearvars -global EEexport

%% get accelerations
feedbackFF.Dy = zeros(sys.counters.genCoord*2, length(feedbackFF.x));
for ii = 1:length(feedbackFF.x)
    dx_ = eqm_nonlin_ss(feedbackFF.x(ii),feedbackFF.y(:,ii));
    feedbackFF.Dy(:,ii) = dx_;
end

%%
disp('Calculation Complete')