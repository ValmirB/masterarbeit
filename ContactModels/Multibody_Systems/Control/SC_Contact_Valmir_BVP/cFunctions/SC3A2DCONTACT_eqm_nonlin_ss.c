#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "SC3A2DCONTACT_pi_code.h"
#include "SC3A2DCONTACT_userDefined.h"
#include "SC3A2DCONTACT_pd_matlab.h"
#include "neweul.h"
#include "SC3A2DCONTACT_paraStruct.h"
#include "SC3A2DCONTACT_Flexor.h"

#include "SC3A2DCONTACT_constraintEquations.h"
void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr = NULL;

	double t      = mxGetScalar( prhs[0] );
	double *x     = mxGetPr( prhs[1] );
	double *f     = NULL;
	double *u_    = NULL;

#if defined SC3A2DCONTACT_NUM_ALL_INPUTS && SC3A2DCONTACT_NUM_ALL_INPUTS > 0
	u_ = calloc( SC3A2DCONTACT_NUM_ALL_INPUTS, sizeof(double));
#endif

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		free(u_);
		return;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		free(u_);
		return;
	}

	if ((mxGetM(prhs[1])!=(2*SC3A2DCONTACT_SYS_DOF+SC3A2DCONTACT_AUX_DOF)) || (mxGetN(prhs[1])!=1)){
		mexPrintf("The second input argument has to be a 14x1 vector!\n");
		free(u_);
		return;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*SC3A2DCONTACT_SYS_DOF+SC3A2DCONTACT_AUX_DOF, 1, mxREAL);
	f = mxGetPr(plhs[0]);

	/* Initialize System Struct */
	dataPtr = SC3A2DCONTACT_initializeSystemStruct();

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		SC3A2DCONTACT_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	/* Compute all inputs */

	/* Evaluate System Dynamics */
	SC3A2DCONTACT_equations_of_motion(t, x, u_, f, dataPtr);

	/* Evaluate Auxiliary Dynamics */
	SC3A2DCONTACT_auxiliary_dynamics(t, x, u_, f, dataPtr);

	/* Free System Struct */
	SC3A2DCONTACT_freeStructure(dataPtr);

#if defined SC3A2DCONTACT_NUM_ALL_INPUTS && SC3A2DCONTACT_NUM_ALL_INPUTS > 0
	free(u_);
#endif

}

