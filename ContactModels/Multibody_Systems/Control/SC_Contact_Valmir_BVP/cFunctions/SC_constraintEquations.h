#ifndef SC_constraintEquations_H
#define SC_constraintEquations_H

#include "SC_paraStruct.h"

void SC_constraintEquations(double t, double *x_, double *u_, void *dataPtr);

void SC_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr);

void SC_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr);

void SC_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr);

void SC_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr);

void SC_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr);

#endif
