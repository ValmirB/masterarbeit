#ifndef SC3A2DCONTACT_constraintEquations_H
#define SC3A2DCONTACT_constraintEquations_H

#include "SC3A2DCONTACT_paraStruct.h"

void SC3A2DCONTACT_constraintEquations(double t, double *x_, double *u_, void *dataPtr);

void SC3A2DCONTACT_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr);

void SC3A2DCONTACT_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr);

void SC3A2DCONTACT_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr);

void SC3A2DCONTACT_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr);

void SC3A2DCONTACT_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr);

#endif
