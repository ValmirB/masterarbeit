#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "SC3A2DCONTACT_pi_code.h"
#include "SC3A2DCONTACT_userDefined.h"
#include "SC3A2DCONTACT_pd_matlab.h"
#include "neweul.h"
#include "SC3A2DCONTACT_paraStruct.h"
#include "SC3A2DCONTACT_Flexor.h"

#include "SC3A2DCONTACT_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double *t_vec        = NULL;
	double t             = 0;
	double *x_red        = NULL;
	double *y_ind        = NULL;
	double *Dy_ind       = NULL;
	double *x_           = calloc(2*SC3A2DCONTACT_SYS_DOF, sizeof(double));
	double *dx_          = calloc(2*SC3A2DCONTACT_SYS_DOF, sizeof(double));
	double *matlabReturn = NULL;
	double *u_           = NULL;
	size_t numStates     = 1;
	int counter_         = 0;
	int vec_loop         = 0;
	struct SC3A2DCONTACT_paraStruct *data = NULL;

#if defined SC3A2DCONTACT_NUM_ALL_INPUTS && SC3A2DCONTACT_NUM_ALL_INPUTS > 0
	u_ = calloc( SC3A2DCONTACT_NUM_ALL_INPUTS, sizeof(double));
#endif

	/* Initialize System Struct */
	dataPtr = SC3A2DCONTACT_initializeSystemStruct();

	data  = (struct SC3A2DCONTACT_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		goto clean_up;
	}

	t_vec = mxGetPr( prhs[0] );
	x_red = mxGetPr( prhs[1] );

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		goto clean_up;
	}

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		SC3A2DCONTACT_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	/* Get number of right-hand sides */
	numStates = mxGetN(prhs[1]);

	if (((mxGetM(prhs[0]) * mxGetN(prhs[0])) != numStates)){
		mexPrintf("The dimensions of the passed arguments don not match!\n");
		goto clean_up;
	}

	if ((mxGetM(prhs[1])!=2*SC3A2DCONTACT_NUM_INDEPENDENT_GC)){
		mexPrintf("The second input argument has to be a 4xnumStates vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*SC3A2DCONTACT_NUM_INDEPENDENT_GC, numStates, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	for ( vec_loop = 0; vec_loop < (int) numStates; vec_loop++){

		t = t_vec[vec_loop];
		y_ind = &(x_red[2*SC3A2DCONTACT_NUM_INDEPENDENT_GC*vec_loop]);
		Dy_ind = &(x_red[2*SC3A2DCONTACT_NUM_INDEPENDENT_GC*vec_loop+SC3A2DCONTACT_NUM_INDEPENDENT_GC]);

		/* Get dependent generalized coordinates and velocities */
		for ( counter_ = 0 ; counter_ < SC3A2DCONTACT_NUM_INDEPENDENT_GC ; counter_++ ){
			x_[data->independentIndices[counter_]] = y_ind[counter_];
			x_[data->independentIndices[counter_]+SC3A2DCONTACT_SYS_DOF] = Dy_ind[counter_];
		}

		SC3A2DCONTACT_educatedGuess(t, x_, dataPtr);

		SC3A2DCONTACT_getDependentGenCoords(t, x_, u_, dataPtr);

		/* Compute all inputs */

		/* Evaluate system dynamics */
		SC3A2DCONTACT_equations_of_motion(t, x_, u_, dx_, dataPtr);

		/* Move temporary memory to Matlab output pointer */
		for ( counter_ = 0 ; counter_ < SC3A2DCONTACT_NUM_INDEPENDENT_GC ; counter_++ ){
			matlabReturn[vec_loop*2*SC3A2DCONTACT_NUM_INDEPENDENT_GC+counter_] = dx_[data->independentIndices[counter_]];
			matlabReturn[vec_loop*2*SC3A2DCONTACT_NUM_INDEPENDENT_GC+counter_+SC3A2DCONTACT_NUM_INDEPENDENT_GC] = dx_[data->independentIndices[counter_]+SC3A2DCONTACT_SYS_DOF];
		}

	}

	/* Free System Struct */
clean_up:
	SC3A2DCONTACT_freeStructure(dataPtr);
#if defined SC3A2DCONTACT_NUM_ALL_INPUTS && SC3A2DCONTACT_NUM_ALL_INPUTS > 0
	free(u_);
#endif
	free(x_);
	free(dx_);

}

