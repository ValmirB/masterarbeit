#ifndef SC_pi_ind_H
#define SC_pi_ind_H

#include "SC_paraStruct.h"

/* System dynamics */
void SC_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr);

/* Auxiliary dynamics */
void SC_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

/* Equations of motion */
void SC_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

#endif
