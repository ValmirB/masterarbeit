#ifndef SC3A2DCONTACT_pd_matlab_H
#define SC3A2DCONTACT_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "SC3A2DCONTACT_paraStruct.h"

double SC3A2DCONTACT_getScalar(const void *context, const char *name, double defaultValue);

int SC3A2DCONTACT_getSystemStruct(void *dataPtr, const void *context);

void SC3A2DCONTACT_educatedGuess(double t, double *y_, void *dataPtr);
void SC3A2DCONTACT_educatedGuess(double t, double *y_, void *dataPtr);

#endif
