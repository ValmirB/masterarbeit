function J_ = myJac(t,x)

J_ = finiteDifferences(@eqm_nonlin_red, x,1e-8,'preargs',t);