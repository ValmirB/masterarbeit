function FJac = FJacFiniteDiff(t,x)
global sys
FJac = finiteDifferences(@(x_)SC_eqm_nonlin_red(t,x + x_,sys.parameters.data), zeros(2*(sys.counters.genCoord-sys.counters.constraint),1), 1e-8);