function M_ = eqm_nonlin_M(t, x_, u_, varargin)
% EQM_NONLIN_M - Mass matrix of the system Flexor

% Mass and Jacobian matrix

Mq_ = eqm_nonlin_Mq(t, x_, u_);
Jg_ = eqm_nonlin_Jg(t, x_, u_);


% Mass matrix

M_ = transpose(Jg_)*Mq_*Jg_;


% END OF FILE

