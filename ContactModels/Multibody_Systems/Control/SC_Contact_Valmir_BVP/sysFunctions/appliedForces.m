function qa_ = appliedForces(t, x_, u_, varargin)
% qa_ = appliedForces(t, x_, u_, varargin)
% Applied forces qa_ in the system
% Vector of force as in the equations of motion before multiplication with J.'

global sys;

% generalized coordinates, velocities and auxiliary coordinates

r_beta2 = x_(4);
r_gamma3 = x_(5);
EA2_q001 = x_(6);
EA3_q001 = x_(7);

% constant user-defined variables
g = sys.parameters.data.g;
m_C1 = sys.parameters.data.m_C1;
m_C2 = sys.parameters.data.m_C2;
m_ee = sys.parameters.data.m_ee;
m_mb = sys.parameters.data.m_mb;

% standardized signals

F_contact = f_F_contact(t);

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_rot_z_ =  SID_EA2_.frame.node(3).orientation(3) + SID_EA2_.frame.node(3).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA3_2_rot_x_ =  SID_EA3_.frame.node(2).orientation(1) + SID_EA3_.frame.node(2).psi(1,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_rot_z_ =  SID_EA3_.frame.node(2).orientation(3) + SID_EA3_.frame.node(2).psi(3,:) * x_(sys.model.body.EA3.data.edof.idx);

% Applied forces
qa_ = zeros(44,1);

qa_(3) = -1.0*g*m_C1;
qa_(9) = -1.0*g*m_C2;
qa_(15) = -2.158*g;
qa_(17) = 0.41626*g;
qa_(21) = -5.582176000000005799961400043685*g;
qa_(22) = -0.10280645791469449068511465839038*EA2_q001*g;
qa_(23) = -0.00000000000000000000000000040389678347315804437080502542479*g*(4709707183750797.0*EA2_q001 - 8804736243303947221300412416.0);
qa_(28) = -1.953632*g;
qa_(29) = -0.082637824577984173024525205164537*EA3_q001*g;
qa_(30) = -0.0000000000000000000000000016155871338926321774832201016991*g*(5153537714989119.0*EA3_q001 - 284882298419310817479491584.0);
qa_(33) = 1.0*F_contact*(cos(elBo_EA3_2_rot_z_)*(1.0*sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - cos(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) + 1.0*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))))) - 1.0*g*m_ee*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
qa_(34) = - F_contact*(sin(elBo_EA3_2_rot_z_)*(1.0*sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - cos(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) - cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))))) - 1.0*g*m_ee*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_);
qa_(35) = - 1.0*F_contact*sin(elBo_EA3_2_rot_x_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) - 1.0*g*m_ee*cos(elBo_EA3_2_rot_x_);
qa_(41) = -1.0*g*m_mb;

% END OF FILE

