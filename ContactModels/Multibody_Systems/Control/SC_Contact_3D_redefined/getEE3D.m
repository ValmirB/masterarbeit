function r_EE = getEE3D(x)

d_axes = 0.610000000000000;
l2 = 1;
l3 = 0.476000000000000;
phi2_1 = 0.051778062322038;
phi3_1 = 0.063005536839437;
psi2_1 = 0.130749285276055;
v2_1 = 1;
w2_1 = 1;
w3_1 = 1;
z_0 = 0.17;

p_b = x(2);
r_beta2 = x(3);
r_gamma3 = x(4);
EA2_q001 = x(5);
EA3_q001 = x(6);

elBo_EA2_2_rot_z_ = 0.12720812859558897067735472319328*EA2_q001;
elBo_EA2_2_x_ = 0.6;

% elBo_EA2_2_rot_z_ = SCc3A3D_f_elBo_EA2_2_rot_z_(x_);
% elBo_EA2_2_x_ = SCc3A3D_f_elBo_EA2_2_x_(x_);

r_EE = zeros (3,1);

r_EE(1,1) = d_axes + p_b + l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) + EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
r_EE(2,1) = elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) + EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
r_EE(3,1) = z_0 + l3*sin(r_gamma3) + EA3_q001*phi3_1*w3_1*cos(r_gamma3);
