%% Initialize
% Clean up
clear all; close all; clc;
load('sys.mat')
global sys EEexport


% Trajectory definition
trajectoryTime_ = 4; % 4 ursprünglich
r_w1 = [1; 0.5]; %[1.15; 0.5]
r_w2 = r_w1 + [+0.48; -0.48];

r0 = [1/2*(r_w1(1) + r_w2(1) + r_w2(2) - r_w1(2)); ...
           r_w1(1) + r_w2(2) - 1/2*(r_w1(1) + r_w2(1) + r_w2(2) - r_w1(2))]; %Neues KOS des Kreises


x_0 = r0(1);
y_0 = r0(2);
sys.parameters.data.r_ec_x = x_0;
sys.parameters.data.r_ec_y = y_0;
sys.parameters.data.z_0 = 0.17;


sigma0 = atan2(r_w2(2)-y_0, r_w2(1)-x_0);

sys.parameters.data.force_start = -10;
sys.parameters.data.force_end = -5;

sys.parameters.data.start_angle = sigma0+20*pi/180;%-20*pi/180;
sys.parameters.data.final_angle = sigma0+75*pi/180;

sys.parameters.data.traj_z_start = 0.5;
sys.parameters.data.traj_z_end = 0.35;

sys.parameters.data.radius = 0.48-0.04;       
%  sys.parameters.data.traj_start = 1;

% Add folder of the sysDef_basic & add Neweul_M2
if ~(exist('sysDef_basic','file')==2)
    currentDir_ = pwd;
    folderName_ = 'Multibody_Systems';
    strEnd_ = strfind(currentDir_,[filesep,folderName_,filesep]) + length(folderName_);
    addpath(currentDir_(1:strEnd_));
end

currentFolder = pwd;
addpath(genpath(currentFolder));

%% output redefinition - VB ursprünglich

%VB - geht bei 4s Traj Time
sys.parameters.data.w2_1 = 0.8;
sys.parameters.data.w3_1 = 0.8;
sys.parameters.data.v2_1 = 1.07;
sys.parameters.data.v3_1 = 0.5;

sys.parameters.data.w2_1 = 0;
sys.parameters.data.w3_1 = 0;
sys.parameters.data.v2_1 = 0;
sys.parameters.data.v3_1 = 0;


%% Start Time Int

sys.parameters.data.traj_end = sys.parameters.data.traj_start + trajectoryTime_;
tstart = sys.parameters.data.traj_start;

[y0, Dy0, ~, ~] = calcInitConditions(tstart,[0;0;-0.7;0.7;zeros(2*sys.counters.genCoord-4,1)]);   % initial guess necessary
y0 = staticEquilibrium(tstart,y0);

disp('Start TimeInt ...')
feedbackFF = timeInt('waitbar',true,'function','SCc3A3D_eqm_nonlin_ss','x0', [y0;Dy0], 'time', 0:0.5e-3:(tstart + trajectoryTime_ + 1), 'integrator', @ode4);

%% get accelerations
disp('Start Postprocessing ...')
feedbackFF.Dy = zeros(sys.counters.genCoord*2, length(feedbackFF.x));
for ii = 1:length(feedbackFF.x)
    dx_ = eqm_nonlin_ss(feedbackFF.x(ii),feedbackFF.y(:,ii));
    feedbackFF.Dy(:,ii) = dx_;
end

%%
dateString = strrep(strrep(datestr(now),':','_'),' ','_');

if ~isempty(sys.parameters.coordinates.elastic)
    EEexport.output = 'desOut'; % this exports the written output to the hdf5file as desired EE
    hdf5filename = (['FF_',sys.info.systemSetup.trajectoryType,'_',num2str(sys.parameters.data.traj_end-sys.parameters.data.traj_start) ,'s_traj_',num2str(numel(sys.parameters.coordinates.elastic)),'edof_EETP',sys.info.systemSetup.EE_Tracking_Point,'_',EEexport.output,'_',dateString,'.h5']);
else
    hdf5filename = (['FF_',sys.info.systemSetup.trajectoryType,'_',num2str(sys.parameters.data.traj_end-sys.parameters.data.traj_start) ,'s_traj_',num2str(numel(sys.parameters.coordinates.elastic)),'edof_',dateString,'.h5']);
end

if exist('h5create','file')==2
    exportHDF5File(feedbackFF,hdf5filename);
end
clearvars -global EEexport


disp('Calculation Complete')