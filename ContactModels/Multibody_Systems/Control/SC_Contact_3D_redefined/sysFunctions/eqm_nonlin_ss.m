function Dx_ = eqm_nonlin_ss(t, x_)
% Dx_ = eqm_nonlin_ss(t, x_)
% EQM_NONLIN_SS - Nonlinear equations of motion in
% state space form.
% The system contains constraint equations,
% uses an index 0 formulation.

global sys;

dof_ = sys.counters.genCoord;
aux_ = sys.counters.auxCoord;

% Evaluate all inputs
u_ = inputs_all(t, x_);

% Evaluate absolute kinematics
kin = evalAbsoluteKinematics(t, x_, u_, 1);

% Dynamics of the unconstrained system
[M_, f_] = system_dynamics(t, x_, u_, kin);

% Jacobian, velocities and accelerations due to the constraints
[Jr_a, Jl_a, Jr_v, theta_, gamma_] = constraintEquations(t, x_, u_, kin);

% Allocate return vector
Dx_ = zeros(2*dof_+aux_,1);

% Projection of the velocities
Dx_(1:dof_) = Jr_v*transpose(Jr_v)*(x_(dof_+1:2*dof_) - theta_) + theta_;

% Projection of the accelerations
Dx_(dof_+1:2*dof_) = Jr_a*((Jl_a*M_*Jr_a)\Jl_a)*(f_ - M_*gamma_)  + gamma_;

% Auxiliary dynamics
Dx_(2*dof_+1:2*dof_+aux_) = auxDynamics(t, x_, u_);

% END OF FILE

