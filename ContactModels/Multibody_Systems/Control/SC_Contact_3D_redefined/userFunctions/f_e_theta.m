function e_theta = f_e_theta(t, varargin)
% f_e_theta - definition of smooth transition e_theta

global sys

% constant user-defined variables
final_angle = sys.parameters.data.final_angle;
start_angle = sys.parameters.data.start_angle;
traj_end = sys.parameters.data.traj_end;
traj_start = sys.parameters.data.traj_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = start_angle;
endValue(1) = final_angle;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = sys.parameters.data.e_theta.poly;

e_theta = 0.5*(endValue-startValue)*polyval(transitionPoly_,max(min(scaledTime_,1),-1)) ...
		+ 0.5*(startValue+endValue);

% END OF FILE
