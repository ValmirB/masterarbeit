function bvp_res = startBVP_basic(numSamples)

global sys

tstart = sys.parameters.data.traj_start;
tfinal = sys.parameters.data.traj_end;


% Compute residuum
[y0, ~, ~, ~] = calcInitConditions(tstart,[0;0;-0.7;0.7;zeros(2*sys.counters.genCoord-4,1)]);   % initial guess necessary 
sys.tmp.y_s_start = staticEquilibrium(tstart,y0);

[y0, ~, ~, ~] = calcInitConditions(tfinal,[0;0;-0.7;0.7;zeros(2*sys.counters.genCoord-4,1)]);   % initial guess necessary
sys.tmp.y_s_end = staticEquilibrium(tfinal,y0);

% here finite Differences and reduced_SystemMatrix should be equal as
% equilibrium points are considered
A_init = finiteDifferences(@(x_)SCc3A3D_eqm_nonlin_red(tstart,[sys.tmp.y_s_start(sys.parameters.coordinates.idx_indCoord); zeros(numel(sys.parameters.coordinates.idx_indCoord),1)] + x_,sys.parameters.data), zeros(2*(sys.counters.genCoord-sys.counters.constraint),1), 1e-8); 
%A_init = SC_reduced_systemMatrix(tstart,[sys.tmp.y_s_start(sys.parameters.coordinates.idx_indCoord); zeros(numel(sys.parameters.coordinates.idx_indCoord),1)],sys.parameters.data); %finiteDifferences(@eqm_nonlin_red, ...

A_final = finiteDifferences(@(x_)SCc3A3D_eqm_nonlin_red(tfinal,[sys.tmp.y_s_end(sys.parameters.coordinates.idx_indCoord); zeros(numel(sys.parameters.coordinates.idx_indCoord),1)] + x_,sys.parameters.data), zeros(2*(sys.counters.genCoord-sys.counters.constraint),1), 1e-8); 
%A_final = SC_reduced_systemMatrix(tfinal,[sys.tmp.y_s_end(sys.parameters.coordinates.idx_indCoord); zeros(numel(sys.parameters.coordinates.idx_indCoord),1)],sys.parameters.data); %finiteDifferences(@eqm_nonlin_red, ...

[M_init, E_init] = eigreal(A_init);

[M_final, E_final] = eigreal(A_final);

sys.tmp.T_init = M_init\eye(size(A_init));

sys.tmp.T_final = M_final\eye(size(A_final));

sys.tmp.id_init =  find(real(diag(E_init))<=0);

sys.tmp.id_final =  find(real(diag(E_final))>0);

sys.tmp.M_init = M_init;

sys.tmp.M_final = M_final;

sys.tmp.A_init = A_init;

sys.tmp.A_final = A_final;

bvp_start = tstart - 1;
bvp_final = tfinal + 1;

solinit.x = linspace(bvp_start,bvp_final, numSamples);
%solinit.x = [solinit.x(1:find(solinit.x==tstart)),solinit.x(find(solinit.x==tstart):end)]; % multipoint BVP
solinit.y = interp1([bvp_start tstart tfinal bvp_final],[sys.tmp.y_s_start(sys.parameters.coordinates.idx_indCoord) ...
                    sys.tmp.y_s_start(sys.parameters.coordinates.idx_indCoord) ...
                    sys.tmp.y_s_end(sys.parameters.coordinates.idx_indCoord) ...
                    sys.tmp.y_s_end(sys.parameters.coordinates.idx_indCoord); ...
                    zeros(sys.counters.genCoord-sys.counters.constraint,4)]',solinit.x)';
                
% if single parameters are "optimized"
% solinit.parameters = [sys.parameters.data.traj_y_start, sys.parameters.data.traj_y_end];


%% if eqm_nonlin_ss is used set this if to true

if false
    A_init_ss = finiteDifferences(@(x_)SCc3A3D_eqm_nonlin_ss(tstart,[sys.tmp.y_s_start; zeros(sys.counters.genCoord,1)] + x_,sys.parameters.data), zeros(2*(sys.counters.genCoord),1), 1e-8);
    A_final_ss = finiteDifferences(@(x_)SCc3A3D_eqm_nonlin_ss(tfinal,[sys.tmp.y_s_end; zeros(sys.counters.genCoord,1)] + x_,sys.parameters.data), zeros(2*(sys.counters.genCoord),1), 1e-8);
    
    sys.tmp.bvp_start = bvp_start;
    sys.tmp.bvp_final = bvp_final;
    
    [M_init_ss, E_init_ss] = eigreal(A_init_ss);
    
    [M_final_ss, E_final_ss] = eigreal(A_final_ss);
    
    sys.tmp.T_init_ss = M_init_ss\eye(size(A_init_ss));
    
    sys.tmp.T_final_ss = M_final_ss\eye(size(A_final_ss));
    
    sys.tmp.id_init_ss =  find(real(diag(E_init_ss))<0); % dont take zero eigenvalues
    
    sys.tmp.id_final_ss =  find(real(diag(E_final_ss))>0);
    
    solinit.y = interp1([bvp_start tstart tfinal bvp_final],[sys.tmp.y_s_start ...
                    sys.tmp.y_s_start ...
                    sys.tmp.y_s_end ...
                    sys.tmp.y_s_end; ...
                    zeros(sys.counters.genCoord,4)]',solinit.x)';
    
end
%%

%%%%% TEST OLD BVP Iinital Guess - Replace Initial Guess Above
load('3D_Semicircle55_4s_10-5N_Redefined_FINAL')
solinit.y(1,:) = interp1(feedbackFF.x,feedbackFF.y(6,:),solinit.x);
solinit.y(2,:) = interp1(feedbackFF.x,feedbackFF.y(7,:),solinit.x);
solinit.y(3,:) = interp1(feedbackFF.x,feedbackFF.y(13,:),solinit.x);
solinit.y(4,:) = interp1(feedbackFF.x,feedbackFF.y(14,:),solinit.x);


disp('Starting BVP solver ...')
tic;
%standard for best & fast results (also worked for 3 Arm if initial guess
%is useful!!) - Vectorized on is only used for function handed to solver
%not for FJac (tested!!)..so this does not need to be vectorizable
% besser wohl 'NMax',1000000 rechnet aber sehr lange, ca. 10000 war zu
% wenig 100000 hat geklaptt mit sinnvollen Toleranzen
options = bvpset('NMax',100000,'AbsTol',1e-7,'RelTol',1e-5,'Vectorized','on','FJacobian',@(t,x)FJacFiniteDiff(t,x)); 
bvp_res = bvp5c(@(t,x) SCc3A3D_eqm_nonlin_red(t,x,sys.parameters.data),@bcfun3,solinit,options);

% simplified Jacobian with reducedSystemMatrix (seems to be quite
% inaccurate for non-static points)
%options = bvpset('NMax',100000,'AbsTol',1e-7,'RelTol',1e-5,'FJacobian',@(t,x)SC_reduced_systemMatrix(t,x,sys.parameters.data),'Vectorized','off');
%bvp_res = bvp5c(@(t,x) SC_eqm_nonlin_red(t,x,sys.parameters.data),@bcfun3,solinit,options);


% eqm_nonlin_ss
%options = bvpset('NMax',1000000,'AbsTol',1e-5,'RelTol',1e-5,'Vectorized','off','FJacobian',@(t,x)FJacFiniteDiff_ss(t,x));
%bvp_res = bvp5c(@(t,x) eqm_nonlin_ss(t,x),@bcfun_ss,solinit,options);


%multipoint
%options = bvpset('NMax',100000,'AbsTol',1e-7,'RelTol',1e-5,'FJacobian',@(t,x,region) reduced_systemMatrix(t,x,region),'Vectorized','off');
%bvp_res = bvp5c(@(t,x,region) eqm_nonlin_red(t,x,region),@bcfun3,solinit,options);

% Matlab Function - much slower (as e.g. not usable with Vectorized on)
%bvp_res = bvp5c(@(t,x) eqm_nonlin_red(t,x),@bcfun3,solinit,options);

% bvptwp solver - should be good for sharp changes ==> FJacobian and the
% eqm need to be called without sys.parameters.data, thus an intermediate
% help file might be necessary 
%options =
%bvptwpset('Solver','twpbvpc_m','NMax',1000000,'RelTol',1e-5,'Vectorized','on','FJacobian',@(t,x)FJacFiniteDiff(t,x));
% %@(t,x)callSC_reduced_systemMatrix(t,x));
%
%bvp_res = bvptwp(@(t,x) callSC_eqm_nonlin_red(t,x),@bcfun3,solinit,options);

toc;

%% Help functions for bvptwp solver
% function Dx = callSC_eqm_nonlin_red(t,x)
% global sys
% Dx = SC_eqm_nonlin_red(t,x,sys.parameters.data);

% function A = callSC_reduced_systemMatrix(t,x)
% global sys
% A = SC_reduced_systemMatrix(t,x,sys.parameters.data);