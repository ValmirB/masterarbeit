function res_ = bcfun3(ya,yb)

global sys

%standard
r_init = sys.tmp.T_init*(ya-[sys.tmp.y_s_start(sys.parameters.coordinates.idx_indCoord); zeros(numel(sys.parameters.coordinates.idx_indCoord),1)]);
r_final = sys.tmp.T_final*(yb-[sys.tmp.y_s_end(sys.parameters.coordinates.idx_indCoord); zeros(numel(sys.parameters.coordinates.idx_indCoord),1)]);
res_ = [r_init(sys.tmp.id_init); r_final(sys.tmp.id_final)];

% multipoint
% res_ = [ya(1,1) 
%         norm([ya(1,2)-yb(1,1) ya(2,2)-yb(2,1)])
%         r_init(sys.tmp.id_init)
%         yb(1,2)];
%res_ = [norm([ya(1) ya(2)]); norm([yb(1) yb(2)])];