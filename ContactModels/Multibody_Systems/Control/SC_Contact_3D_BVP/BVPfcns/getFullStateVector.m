function result = getFullStateVector(t_, x_red)
% get the full state from minimal coordinates

global sys

y = zeros(sys.counters.genCoord, length(t_));
Dy = zeros(sys.counters.genCoord, length(t_));

id_dep_coord = sys.parameters.coordinates.idx_depCoord;
id_ind_velo  = sys.parameters.coordinates.idx_indVelo;

% get initial values
[y_, ~, ~, ~] = calcInitConditions(t_(1),[0;0;-0.7;0.7;zeros(2*sys.counters.genCoord-4,1)]); % initial guess necessary
y_ = staticEquilibrium(t_(1),y_);

for i = 1:length(t_)

    t = t_(i);
    x_ = x_red(:,i);
       
    y_ = [y_(id_dep_coord); x_(1:sys.counters.genCoord-sys.counters.posConstraints)];
    y_  = findroot(@positionConstraints, y_,[],'index',id_dep_coord,'preargs',t);
    Dy_ = zeros(sys.counters.genCoord,1);
    Dy_(id_ind_velo) = x_(1+sys.counters.genCoord-sys.counters.posConstraints : 2*(sys.counters.genCoord-sys.counters.posConstraints));
    Dy_ = dependent_velocities(t, y_, Dy_(id_ind_velo));
       
    y(:,i) = y_;
    Dy(:,i) = Dy_;

end

result = [y; Dy];