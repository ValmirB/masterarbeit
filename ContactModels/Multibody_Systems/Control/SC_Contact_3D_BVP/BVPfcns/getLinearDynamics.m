y0 = educatedGuess(0,zeros(sys.counters.genCoord-4,1));

[y_s_start, ~] = calcInitConditions(0,y0,zeros(sys.counters.genCoord,1));
y0 = educatedGuess(2,zeros(sys.counters.genCoord-4,1));
[y_s_end, ~] = calcInitConditions(2,y0,zeros(sys.counters.genCoord,1));

A_init = finiteDifferences(@eqm_nonlin_red, ...
    [y_s_start(sys.parameters.vectors.idx_ind); zeros(numel(sys.parameters.vectors.idx_ind),1)],1e-8,'preargs',0);

A_final = finiteDifferences(@eqm_nonlin_red, ...
    [y_s_end(sys.parameters.vectors.idx_ind); zeros(numel(sys.parameters.vectors.idx_ind),1)],1e-8,'preargs',2);

[M_init, E_init] = eigreal(A_init);

[M_final, E_final] = eigreal(A_final);

sys.tmp.T_init = M_init\A_init;

sys.tmp.T_final = M_final\A_final;

id_stable =  find(real(diag(E_init))<=0);

id_unstable =  find(real(diag(E_final))>0);

y_0_init = sol_real_hd.y(:,1)-[y_s_start(sys.parameters.vectors.idx_ind);zeros(size(y_s_start(sys.parameters.vectors.idx_ind)))];
y_0_final = sol_real_hd.y(:,end)-[y_s_end(sys.parameters.vectors.idx_ind);zeros(size(y_s_end(sys.parameters.vectors.idx_ind)))];

N_init = M_init\eye(size(M_init));
N_final = M_final\eye(size(M_final));

z0_init = N_init(id_unstable,:)*y_0_init;
z0_final = N_final(id_stable,:)*y_0_final;

A_unstable_init = N_init(id_unstable,:)*A_init*M_init(:,id_unstable);
A_stable_final = N_final(id_stable,:)*A_final*M_final(:,id_stable);


options = odeset('RelTol',1e-9,'AbsTol',1e-12);

stableDym = @(t,x) A_stable_final*x;
res_stable = ode45(stableDym, [2 5], z0_final, options);

unstableDym = @(t,x) A_unstable_init*x;
res_unstable = ode45(unstableDym, [0 -1], z0_init, options);


close all;

figure;

subplot(2,1,1);
plot(res_unstable.x,M_init(:,id_unstable)*res_unstable.y);
title('Unstable dynamics');

subplot(2,1,2);
plot(res_stable.x,M_final(:,id_stable)*res_stable.y);
title('Stable dynamics');

y_unstable = M_init(:,id_unstable)*res_unstable.y + ...
    repmat([y_s_start(sys.parameters.vectors.idx_ind);zeros(size(y_s_start(sys.parameters.vectors.idx_ind)))],1, numel(res_unstable.x));

y_stable = M_final(:,id_stable)*res_stable.y + ...
    repmat([y_s_end(sys.parameters.vectors.idx_ind);zeros(size(y_s_end(sys.parameters.vectors.idx_ind)))],1, numel(res_stable.x));

figure;

plot(res_unstable.x,y_unstable);
hold on;
plot(sol_real_hd.x,sol_real_hd.y);
hold on;
plot(res_stable.x, y_stable);

res.x = [res_unstable.x(end:-1:2) sol_real_hd.x res_stable.x(2:end)];
res.y = [y_unstable(:,end:-1:2) sol_real_hd.y y_stable(:,2:1:end)];

feedback= postProcessBVP(res);