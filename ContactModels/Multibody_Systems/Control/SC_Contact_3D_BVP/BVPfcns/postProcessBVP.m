function feedback = postProcessBVP(sol, numSamples, hdf5filename)
% this function is amonts others needed as we calculated only the indep.
% coord. ==> the dependent coord. and the outputs need to be calculated
% aswell
global sys

if isfield(sol, 'solver')
    t_ = linspace(sol.x(1),sol.x(end),numSamples);
    x_min = deval(sol,t_);
else
    t_ = sol.x;
    x_min = sol.y;
end

sys.results.bvpInt = [];
sys.results.bvpInt.x = t_;

% sys.results.bvpInt.y = SC_getFullStateVector(t_,x_min,sys.parameters.data);    % calculation with
%c-Code - not working for Flexor as root search jumps between correct
%solution and rubbish ==> use matlab code here (costs aorund 30s more time)
%..problem could arise in observer again! and tests with a first initial
%guess and giving the last solution as guess for the next one did not help
%(worked in Matlab code but not in CCode..so a closer look needs to be
%done)

sys.results.bvpInt.y = getFullStateVector(t_, x_min);                           % calculation with sys-functions

feedback = calcOutputs(sys.results.bvpInt);

res_ = feedback;
res_.x = linspace(0,sol.x(end)-sol.x(1),numSamples);

if exist('h5create','file')==2
    exportHDF5File(res_,hdf5filename);
end

end

function result = calcOutputs(result)

global sys

    if(isstruct(result))
        result.Dy = zeros(sys.counters.genCoord*2, length(result.x)); 
                          
        for h_ = length(result.x):-1:1
            u_(:,h_) = inputs_all(result.x(h_),result.y(:,h_));
        end
        
        % Evaluate system output values
        outGroups_ = fieldnames(sys.model.output);
        for g_ = 1:length(outGroups_)
            
            if (exist(['eqm_sysout_',outGroups_{g_}],'file') == 2)
                myField_ = [outGroups_{g_}(1),outGroups_{g_}(2:end)];
                myFunc_ = str2func(['eqm_sysout_',outGroups_{g_}]);
                numOut = numel(fieldnames(sys.model.output.(outGroups_{g_})));
                result.outputs.(myField_) = zeros(numOut,numel(result.x));
                for h_ = 1:length(result.x)
                    result.outputs.(myField_)(:,h_) = myFunc_(result.x(h_),result.y(:,h_),u_(:,h_));
                end
            end
        end
        
        if(exist('lagrange_multipliers.m', 'file') == 2)
            myField_ = 'lagrangeMultipliers';
            myFunc_ = str2func('lagrange_multipliers');
            result.(myField_) = zeros(numel(sys.parameters.lagrangeMultiplier),numel(result.x));
            
            for h_ = 1:length(result.x)
                result.(myField_)(:,h_) = myFunc_(result.x(h_),result.y(:,h_),u_(:,h_));
            end
            
        end
        
        result.c_pos = zeros(sys.counters.constraint,numel(result.x));
        result.c_vel = zeros(sys.counters.constraint,numel(result.x));
        result.c_acc = zeros(sys.counters.constraint,numel(result.x));
        
        for h_ = 1:length(result.x)
            [C_, G_, Dc_, D2c_] = constraintDerivatives(result.x(h_),result.y(:,h_));
            dx_ = SCc3A3D_eqm_nonlin_ss(result.x(h_),result.y(:,h_),sys.parameters.data);
            result.Dy(:,h_) = dx_;
            result.c_acc(:,h_) = C_*dx_(end/2+1:end) + D2c_;
            result.c_vel(:,h_) = C_*result.y(end/2+1:end,h_) + Dc_;
            result.c_pos(:,h_) = positionConstraints(result.x(h_),result.y(:,h_));
        end
        
        
    end

end

