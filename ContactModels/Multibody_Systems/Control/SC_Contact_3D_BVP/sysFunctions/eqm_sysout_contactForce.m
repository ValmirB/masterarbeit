function result_ = eqm_sysout_contactForce(t, x_, u_, varargin)
% eqm_sysout_contactForce - Vector of the system Flexor output contactForce
% 
% Entries are as in sys.model.output.contactForce

global sys;

% standardized signals

F_contact = f_F_contact(t);
e_theta = f_e_theta(t);

% System output contactForce vector
Fy = -F_contact;%*cos(e_theta);

result_ = zeros(1,1);

result_(1) = Fy;

% END OF FILE 
