function c_ = positionConstraints(t, x_, u_, varargin)

global sys;

% generalized coordinates, velocities and auxiliary coordinates

p_a = x_(1);
p_b = x_(2);
r_alpha1 = x_(3);
r_beta2 = x_(4);
r_gamma3 = x_(5);
EA2_q001 = x_(6);
EA3_q001 = x_(7);

% constant user-defined variables
d_axes = sys.parameters.data.d_axes;
l2 = sys.parameters.data.l2;
l3 = sys.parameters.data.l3;
phi2_1 = sys.parameters.data.phi2_1;
phi3_1 = sys.parameters.data.phi3_1;
psi2_1 = sys.parameters.data.psi2_1;
r_ec_x = sys.parameters.data.r_ec_x;
r_ec_y = sys.parameters.data.r_ec_y;
radius = sys.parameters.data.radius;
v2_1 = sys.parameters.data.v2_1;
w2_1 = sys.parameters.data.w2_1;
w3_1 = sys.parameters.data.w3_1;
z_0 = sys.parameters.data.z_0;

% standardized signals

e_theta = f_e_theta(t);
tcp_traj3 = f_tcp_traj3(t);

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

elBo_EA1_2_x_ =  SID_EA1_.frame.node(2).origin(1,:) + SID_EA1_.frame.node(2).phi(1,:) * x_(sys.model.body.EA1.data.edof.idx);
elBo_EA2_1_x_ =  SID_EA2_.frame.node(1).origin(1,:) + SID_EA2_.frame.node(1).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_x_ =  SID_EA2_.frame.node(2).origin(1,:) + SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);


% Constraints on position level

c_ = zeros(5,1);

c_(1) = d_axes + p_b - 1.0*elBo_EA1_2_x_*cos(r_alpha1) + elBo_EA2_1_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
c_(2) = - 1.0*p_a - 1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_1_x_ - 1.0*elBo_EA2_2_x_) - 1.0*elBo_EA1_2_x_*sin(r_alpha1);
c_(3) = r_ec_y - 1.0*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + radius*sin(e_theta) - 1.0*l3*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) - 1.0*EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + EA3_q001*phi3_1*w3_1*sin(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
c_(4) = r_ec_x - 1.0*p_b - 1.0*d_axes - 1.0*l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + radius*cos(e_theta) + elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*l3*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*cos(r_gamma3) - 1.0*EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + EA3_q001*phi3_1*w3_1*cos(r_beta2 - 1.0*elBo_EA2_2_rot_z_ + EA2_q001*psi2_1*v2_1)*sin(r_gamma3);
c_(5) = tcp_traj3 - 1.0*z_0 - 1.0*l3*sin(r_gamma3) - 1.0*EA3_q001*phi3_1*w3_1*cos(r_gamma3);


% END OF FILE

