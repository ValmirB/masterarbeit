function result_ = eqm_sysout_desiredOut(t, x_, u_, varargin)
% eqm_sysout_desiredOut - Vector of the system Flexor output desiredOut
% 
% Entries are as in sys.model.output.desiredOut

global sys;

% constant user-defined variables
r_ec_x = sys.parameters.data.r_ec_x;
r_ec_y = sys.parameters.data.r_ec_y;
radius = sys.parameters.data.radius;

% standardized signals

De_theta = f_De_theta(t);
e_theta = f_e_theta(t);

% System output desiredOut vector
des_x = r_ec_x + radius*cos(e_theta);
des_y = r_ec_y + radius*sin(e_theta);
des_Dx = -De_theta*radius*sin(e_theta);
des_Dy = De_theta*radius*cos(e_theta);

result_ = zeros(4,1);

result_(1) = des_x;
result_(2) = des_y;
result_(3) = des_Dx;
result_(4) = des_Dy;

% END OF FILE 
