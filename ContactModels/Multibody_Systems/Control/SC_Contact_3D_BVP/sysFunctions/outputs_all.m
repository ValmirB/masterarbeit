function res_ = outputs_all(t, x_, u_, varargin)
% res_ = outputs_all(t, x_, varargin)
% outputs_all - Function to compose the complete output vector
% --------------------- Automatically generated file! ---------------------
%
% Initialization
res_ = zeros(21,1);
res_(1:4) = eqm_sysout_desiredOut(t, x_, u_);
res_(5:10) = eqm_sysout_exactOut(t, x_, u_);
res_(11:16) = eqm_sysout_approxOut(t, x_, u_);
res_(17:20) = eqm_sysout_Curvature(t, x_, u_);
res_(21:21) = eqm_sysout_contactForce(t, x_, u_);

% END OF FILE

