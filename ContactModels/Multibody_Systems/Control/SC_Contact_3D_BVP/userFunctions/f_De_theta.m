function De_theta = f_De_theta(t, varargin)
% f_De_theta - definition of smooth transition De_theta

global sys

% constant user-defined variables
final_angle = sys.parameters.data.final_angle;
start_angle = sys.parameters.data.start_angle;
traj_end = sys.parameters.data.traj_end;
traj_start = sys.parameters.data.traj_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = start_angle;
endValue(1) = final_angle;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = sys.parameters.data.De_theta.poly;

De_theta = 0.5*(endValue-startValue)*(2/(endTime-startTime))*polyval(transitionPoly_,max(min(scaledTime_,1),-1));

% END OF FILE
