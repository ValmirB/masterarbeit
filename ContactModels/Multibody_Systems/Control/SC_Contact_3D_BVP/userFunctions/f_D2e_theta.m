function D2e_theta = f_D2e_theta(t, varargin)
% f_D2e_theta - definition of smooth transition D2e_theta

global sys

% constant user-defined variables
final_angle = sys.parameters.data.final_angle;
start_angle = sys.parameters.data.start_angle;
traj_end = sys.parameters.data.traj_end;
traj_start = sys.parameters.data.traj_start;
startTime(1) = traj_start;
endTime(1) = traj_end;
startValue(1) = start_angle;
endValue(1) = final_angle;

% Scaled time
scaledTime_ = (startTime + endTime -2*t)/(startTime - endTime);

% Coefficients of the polynominal
transitionPoly_ = sys.parameters.data.D2e_theta.poly;

D2e_theta = 0.5*(endValue-startValue)*(2/(endTime-startTime))^2*polyval(transitionPoly_,max(min(scaledTime_,1),-1));

% END OF FILE
