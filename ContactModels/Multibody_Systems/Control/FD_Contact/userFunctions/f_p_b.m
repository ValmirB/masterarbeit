function p_b = f_p_b(t, x_, varargin)
% f_p_b - definition of time-depending user-defined variable p_b

global sys;


% constant user-defined variables
p_b_d = sys.parameters.data.p_b_d;
p_b = zeros(1,1);

p_b(1) = p_b_d;

% END OF FILE

