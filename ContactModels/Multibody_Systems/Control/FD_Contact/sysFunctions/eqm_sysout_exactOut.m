function result_ = eqm_sysout_exactOut(t, x_, u_, varargin)
% eqm_sysout_exactOut - Vector of the system Flexor output exactOut
% 
% Entries are as in sys.model.output.exactOut

global sys;

% generalized coordinates, velocities and auxiliary coordinates

r_beta2 = x_(2);
EA2_q001 = x_(3);
EA3_q001 = x_(4);
Dr_beta2 = x_(6);
% system inputs
p_b = u_(4);
Dp_b = u_(5);
r_gamma3 = u_(7);
Dr_gamma3 = u_(8);

% constant user-defined variables
d_axes = sys.parameters.data.d_axes;

% Automatically introduced abbreviations

SID_EA1_ = sys.model.sid(sys.model.body.EA1.data.sidIdx);
SID_EA2_ = sys.model.sid(sys.model.body.EA2.data.sidIdx);
SID_EA3_ = sys.model.sid(sys.model.body.EA3.data.sidIdx);

DelBo_EA2_2_rot_z_ = SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA2_2_x_ = SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA2_3_rot_z_ = SID_EA2_.frame.node(3).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA2_3_x_ = SID_EA2_.frame.node(3).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA2_3_y_ = SID_EA2_.frame.node(3).phi(2,:) * x_(sys.model.body.EA2.data.edof.idx+4);
DelBo_EA3_2_rot_z_ = SID_EA3_.frame.node(2).psi(3,:) * x_(sys.model.body.EA3.data.edof.idx+4);
DelBo_EA3_2_x_ = SID_EA3_.frame.node(2).phi(1,:) * x_(sys.model.body.EA3.data.edof.idx+4);
DelBo_EA3_2_y_ = SID_EA3_.frame.node(2).phi(2,:) * x_(sys.model.body.EA3.data.edof.idx+4);
elBo_EA2_2_rot_z_ =  SID_EA2_.frame.node(2).orientation(3) + SID_EA2_.frame.node(2).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_2_x_ =  SID_EA2_.frame.node(2).origin(1,:) + SID_EA2_.frame.node(2).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_rot_z_ =  SID_EA2_.frame.node(3).orientation(3) + SID_EA2_.frame.node(3).psi(3,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_x_ =  SID_EA2_.frame.node(3).origin(1,:) + SID_EA2_.frame.node(3).phi(1,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA2_3_y_ =  SID_EA2_.frame.node(3).origin(2,:) + SID_EA2_.frame.node(3).phi(2,:) * x_(sys.model.body.EA2.data.edof.idx);
elBo_EA3_2_x_ =  SID_EA3_.frame.node(2).origin(1,:) + SID_EA3_.frame.node(2).phi(1,:) * x_(sys.model.body.EA3.data.edof.idx);
elBo_EA3_2_y_ =  SID_EA3_.frame.node(2).origin(2,:) + SID_EA3_.frame.node(2).phi(2,:) * x_(sys.model.body.EA3.data.edof.idx);

% System output exactOut vector
Pos_x = d_axes + p_b + elBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - elBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - r_beta2);
Pos_y = elBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + elBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - r_beta2) + elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) - elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - r_beta2);
Rot_angle_theta = (127583615252853*EA2_q001)/36028797018963968 + (3766281193921321*EA3_q001)/9007199254740992 + r_beta2 + r_gamma3;
Vel_x = Dp_b + DelBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - DelBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + (DelBo_EA2_2_rot_z_ - Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - r_beta2) - elBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - r_beta2)) - DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + DelBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + DelBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - r_beta2) - (elBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + elBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3))*(DelBo_EA2_3_rot_z_ - DelBo_EA2_2_rot_z_ + Dr_beta2 + Dr_gamma3) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) - Dr_beta2*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2);
Vel_y = DelBo_EA3_2_y_*cos(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) + DelBo_EA3_2_x_*sin(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - (DelBo_EA2_2_rot_z_ - Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) + elBo_EA2_3_y_*sin(elBo_EA2_2_rot_z_ - r_beta2)) + DelBo_EA2_3_y_*cos(elBo_EA2_2_rot_z_ - r_beta2) + DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) - DelBo_EA2_3_x_*sin(elBo_EA2_2_rot_z_ - r_beta2) + (elBo_EA3_2_x_*cos(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3) - elBo_EA3_2_y_*sin(elBo_EA2_3_rot_z_ - elBo_EA2_2_rot_z_ + r_beta2 + r_gamma3))*(DelBo_EA2_3_rot_z_ - DelBo_EA2_2_rot_z_ + Dr_beta2 + Dr_gamma3) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2) - Dr_beta2*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - r_beta2);
Rot_velocity = DelBo_EA2_3_rot_z_ - DelBo_EA2_2_rot_z_ + DelBo_EA3_2_rot_z_ + Dr_beta2 + Dr_gamma3;

result_ = zeros(6,1);

result_(1) = Pos_x;
result_(2) = Pos_y;
result_(3) = Rot_angle_theta;
result_(4) = Vel_x;
result_(5) = Vel_y;
result_(6) = Rot_velocity;

% END OF FILE 
