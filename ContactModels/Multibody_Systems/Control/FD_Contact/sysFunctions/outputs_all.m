function res_ = outputs_all(t, x_, u_, varargin)
% res_ = outputs_all(t, x_, varargin)
% outputs_all - Function to compose the complete output vector
% --------------------- Automatically generated file! ---------------------
%
% Initialization
res_ = zeros(16,1);
res_(1:6) = eqm_sysout_exactOut(t, x_, u_);
res_(7:12) = eqm_sysout_approxOut(t, x_, u_);
res_(13:16) = eqm_sysout_Curvature(t, x_, u_);

% END OF FILE

