#ifndef FDc3A2D_pd_matlab_H
#define FDc3A2D_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "FDc3A2D_paraStruct.h"

double FDc3A2D_getScalar(const void *context, const char *name, double defaultValue);

int FDc3A2D_getSystemStruct(void *dataPtr, const void *context);

void FDc3A2D_educatedGuess(double t, double *y_, void *dataPtr);
void FDc3A2D_educatedGuess(double t, double *y_, void *dataPtr);

#endif
