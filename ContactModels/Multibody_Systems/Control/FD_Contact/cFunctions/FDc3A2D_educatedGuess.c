#include <math.h>
#include "FDc3A2D_paraStruct.h"
#include "FDc3A2D_pi_code.h"
#include "FDc3A2D_pd_matlab.h"
#include "FDc3A2D_userDefined.h"
#include "neweul.h"

void FDc3A2D_educatedGuess(double t, double *y_, void *dataPtr);

void FDc3A2D_educatedGuess(double t, double *y_, void *dataPtr){

	struct FDc3A2D_paraStruct *data  = (struct FDc3A2D_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
