#ifndef FDc3A2D_constraintEquations_H
#define FDc3A2D_constraintEquations_H

#include "FDc3A2D_paraStruct.h"

void FDc3A2D_constraintEquations(double t, double *x_, double *u_, void *dataPtr);

void FDc3A2D_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr);

void FDc3A2D_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr);

void FDc3A2D_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr);

void FDc3A2D_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr);

void FDc3A2D_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr);

#endif
