#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "FDc3A2D_pi_code.h"
#include "FDc3A2D_userDefined.h"
#include "FDc3A2D_pd_matlab.h"
#include "neweul.h"
#include "FDc3A2D_paraStruct.h"
#include "FDc3A2D_Flexor.h"

#include "FDc3A2D_constraintEquations.h"
void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr = NULL;

	double t      = mxGetScalar( prhs[0] );
	double *x     = mxGetPr( prhs[1] );
	double *f     = NULL;
	double *u_    = NULL;

#if defined FDC3A2D_NUM_ALL_INPUTS && FDC3A2D_NUM_ALL_INPUTS > 0
	u_ = calloc( FDC3A2D_NUM_ALL_INPUTS, sizeof(double));
#endif

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		free(u_);
		return;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		free(u_);
		return;
	}

	if ((mxGetM(prhs[1])!=(2*FDC3A2D_SYS_DOF+FDC3A2D_AUX_DOF)) || (mxGetN(prhs[1])!=1)){
		mexPrintf("The second input argument has to be a 8x1 vector!\n");
		free(u_);
		return;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*FDC3A2D_SYS_DOF+FDC3A2D_AUX_DOF, 1, mxREAL);
	f = mxGetPr(plhs[0]);

	/* Initialize System Struct */
	dataPtr = FDc3A2D_initializeSystemStruct();

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		FDc3A2D_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	/* Compute all inputs */
	FDc3A2D_f_control_inputs(t, x, &(u_[0]), dataPtr);

	/* Evaluate System Dynamics */
	FDc3A2D_equations_of_motion(t, x, u_, f, dataPtr);

	/* Evaluate Auxiliary Dynamics */
	FDc3A2D_auxiliary_dynamics(t, x, u_, f, dataPtr);

	/* Free System Struct */
	FDc3A2D_freeStructure(dataPtr);

#if defined FDC3A2D_NUM_ALL_INPUTS && FDC3A2D_NUM_ALL_INPUTS > 0
	free(u_);
#endif

}

