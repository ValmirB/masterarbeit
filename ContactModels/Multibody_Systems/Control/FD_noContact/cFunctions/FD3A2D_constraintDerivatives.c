#include "FD3A2D_userDefined.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "neweul.h"
#include "FD3A2D_paraStruct.h"
#include <string.h>
#include "FD3A2D_constraintDerivatives.h"

void FD3A2D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr){

	struct FD3A2D_paraStruct *data  = (struct FD3A2D_paraStruct *) dataPtr;


	/* Automatically introduced abbreviations */

	double DelBo_EA1_2_x_ = FD3A2D_f_DelBo_EA1_2_x_(x_);
	double DelBo_EA2_1_x_ = FD3A2D_f_DelBo_EA2_1_x_(x_);
	double DelBo_EA2_2_rot_z_ = FD3A2D_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = FD3A2D_f_DelBo_EA2_2_x_(x_);
	double elBo_EA1_2_x_ = FD3A2D_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = FD3A2D_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FD3A2D_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FD3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FD3A2D_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];
	double Dr_alpha1 = x_[9];
	double Dr_beta2 = x_[10];

	int i_ = 0;

	double **C_  = data->con->C_;
	double *Dc_  = data->con->Dc_;
	double *D2c_ = data->con->D2c_;


	C_[0][0] = 0.0;
	C_[1][0] = - 1.0*pow(cos(r_alpha1),2.0)- 1.0*pow(sin(r_alpha1),2.0);
	C_[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_[1][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_[0][2] = elBo_EA1_2_x_*sin(r_alpha1);
	C_[1][2] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	C_[0][3] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_[1][3] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_[0][4] = 0.0;
	C_[1][4] = 0.0;
	C_[0][5] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));
	C_[1][5] = cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*sin(elBo_EA2_1_rot_z_) - 0.12720812859558897067735472319328*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000078774485292178524877876637249719*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.12720812859558897067735472319328*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_[0][6] = 0.0;
	C_[1][6] = 0.0;

	Dc_[0] = 0.0;
	Dc_[1] = 0.0;

	D2c_[0] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*((2.0*DelBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + sin(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*((2.0*DelBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_) + elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) - 1.0*cos(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + sin(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) +pow( Dr_alpha1,2.0)*elBo_EA1_2_x_*cos(r_alpha1) + 2.0*DelBo_EA1_2_x_*Dr_alpha1*sin(r_alpha1);
	D2c_[1] = pow(Dr_alpha1,2.0)*elBo_EA1_2_x_*sin(r_alpha1) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*((2.0*DelBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_) + elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) - 1.0*cos(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + sin(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*((2.0*DelBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2))*(DelBo_EA2_2_rot_z_ - 1.0*Dr_beta2) + sin(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) - 2.0*DelBo_EA1_2_x_*Dr_alpha1*cos(r_alpha1);

}
