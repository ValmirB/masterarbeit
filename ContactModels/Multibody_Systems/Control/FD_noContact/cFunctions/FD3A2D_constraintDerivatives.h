#ifndef FD3A2D_constraintDerivatives_H
#define FD3A2D_constraintDerivatives_H

#include "FD3A2D_paraStruct.h"

void FD3A2D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
