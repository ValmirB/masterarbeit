#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "FD3A2D_pi_code.h"
#include "FD3A2D_userDefined.h"
#include "FD3A2D_pd_matlab.h"
#include "neweul.h"
#include "FD3A2D_paraStruct.h"
#include "FD3A2D_Flexor.h"

#include "FD3A2D_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double t             = mxGetScalar( prhs[0] );
	double *x_red        = mxGetPr( prhs[1] );
	double *y_ind        = &(x_red[0]);
	double *Dy_ind       = &(x_red[FD3A2D_NUM_INDEPENDENT_GC]);
	double *x_           = calloc( 2*FD3A2D_SYS_DOF, sizeof(double));
	double **Jr          = callocMatrix( FD3A2D_SYS_DOF,FD3A2D_NUM_INDEPENDENT_GC);
	double **Jl          = callocMatrix( FD3A2D_SYS_DOF,FD3A2D_NUM_INDEPENDENT_GC);
	double *f_           = calloc( FD3A2D_SYS_DOF, sizeof(double));
	double **M_          = callocMatrix( FD3A2D_SYS_DOF, FD3A2D_SYS_DOF);
	double **Mred        = callocMatrix(FD3A2D_NUM_INDEPENDENT_GC, FD3A2D_NUM_INDEPENDENT_GC);
	double **MJr         = callocMatrix( FD3A2D_SYS_DOF, FD3A2D_NUM_INDEPENDENT_GC);
	double *KD           = calloc(2*FD3A2D_SYS_DOF*FD3A2D_NUM_INDEPENDENT_GC, sizeof(double));
	double *matlabReturn = NULL;
	double *u_           = calloc(FD3A2D_NUM_ALL_INPUTS, sizeof(double));
	unsigned short *z    = calloc(FD3A2D_NUM_INDEPENDENT_GC, sizeof(unsigned short));
	int counter_         = 0;
	int i,j;
	double myStep = 1e-8;
	struct FD3A2D_paraStruct *data = NULL;

	dataPtr = FD3A2D_initializeSystemStruct();

	data  = (struct FD3A2D_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		goto clean_up;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		goto clean_up;
	}

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		FD3A2D_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	if ((mxGetM(prhs[1])!=2*FD3A2D_NUM_INDEPENDENT_GC) || (mxGetN(prhs[1])!=1)){
		mexPrintf("The second input argument has to be a 10x1 vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*FD3A2D_NUM_INDEPENDENT_GC, 2*FD3A2D_NUM_INDEPENDENT_GC, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	/* Get dependent generalized coordinates and velocities */
	for ( counter_ = 0 ; counter_ < FD3A2D_NUM_INDEPENDENT_GC ; counter_++ ){
		x_[data->independentIndices[counter_]]  =  y_ind[counter_];
		x_[data->independentIndices[counter_]+FD3A2D_SYS_DOF] = Dy_ind[counter_];
	}

	FD3A2D_educatedGuess(t, x_, dataPtr);

	FD3A2D_getDependentGenCoords(t, x_, u_, dataPtr);

	
	FD3A2D_jacobianPartition(t, x_, u_, Jr, Jl, dataPtr);
	
	/* Loop over all position values */

	for (i=0; i<FD3A2D_NUM_INDEPENDENT_GC; i++){
		clearVector(FD3A2D_SYS_DOF,f_);
		clearVector(FD3A2D_NUM_ALL_INPUTS,u_);
		clearMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,M_);

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		FD3A2D_f_control_inputs(t, x_, &(u_[0]), dataPtr);
		FD3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<FD3A2D_SYS_DOF; j++)
			KD[FD3A2D_SYS_DOF*i+j] = f_[j];

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			x_[j] = x_[j] - 2.0*Jr[j][i]*myStep;

		clearVector(FD3A2D_SYS_DOF,f_);
		clearMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,M_);

		FD3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			KD[FD3A2D_SYS_DOF*i+j] = (KD[FD3A2D_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		clearVector(FD3A2D_SYS_DOF,f_);
		clearVector(FD3A2D_NUM_ALL_INPUTS,u_);
		clearMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,M_);

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			x_[j+FD3A2D_SYS_DOF] = x_[j+FD3A2D_SYS_DOF] + Jr[j][i]*myStep;

		FD3A2D_f_control_inputs(t, x_, &(u_[0]), dataPtr);
		FD3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<FD3A2D_SYS_DOF; j++)
			KD[FD3A2D_NUM_INDEPENDENT_GC*FD3A2D_SYS_DOF+FD3A2D_SYS_DOF*i+j] = f_[j];

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			x_[j+FD3A2D_SYS_DOF] = x_[j+FD3A2D_SYS_DOF] - 2.0*Jr[j][i]*myStep;

		clearVector(FD3A2D_SYS_DOF,f_);
		clearMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,M_);

		FD3A2D_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			KD[FD3A2D_NUM_INDEPENDENT_GC*FD3A2D_SYS_DOF+FD3A2D_SYS_DOF*i+j] = (KD[FD3A2D_NUM_INDEPENDENT_GC*FD3A2D_SYS_DOF+FD3A2D_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<FD3A2D_SYS_DOF; j++)
			x_[j+FD3A2D_SYS_DOF] = x_[j+FD3A2D_SYS_DOF] + Jr[j][i]*myStep;

	}

	MatrixMultiplication( 'N', 'N', FD3A2D_SYS_DOF, FD3A2D_SYS_DOF, FD3A2D_NUM_INDEPENDENT_GC, 1.0, M_, Jr, 0.0, MJr); /* MJr allokieren */

	MatrixMultiplication( 'T', 'N', FD3A2D_NUM_INDEPENDENT_GC, FD3A2D_SYS_DOF, FD3A2D_NUM_INDEPENDENT_GC, 1.0, Jl, MJr, 0.0, Mred); /* Mred allokieren */

	GaussianElimination( FD3A2D_NUM_INDEPENDENT_GC, z, Mred);

	for (i=0; i<FD3A2D_NUM_INDEPENDENT_GC; i++)
		matlabReturn[(2*(FD3A2D_NUM_INDEPENDENT_GC+i))*FD3A2D_NUM_INDEPENDENT_GC+i] = 1.0;

	for (i=0; i<2*FD3A2D_NUM_INDEPENDENT_GC; i++){
		MatrixVectorMultiplication( 'T', FD3A2D_NUM_INDEPENDENT_GC, FD3A2D_SYS_DOF, 1.0, Jl, &(KD[FD3A2D_SYS_DOF*i]), 0.0, &(matlabReturn[2*FD3A2D_NUM_INDEPENDENT_GC*i+FD3A2D_NUM_INDEPENDENT_GC]));
		permuteVector(FD3A2D_NUM_INDEPENDENT_GC, z, &(matlabReturn[2*FD3A2D_NUM_INDEPENDENT_GC*i+FD3A2D_NUM_INDEPENDENT_GC]));
		LUForBack_1dim( FD3A2D_NUM_INDEPENDENT_GC, Mred, &(matlabReturn[2*FD3A2D_NUM_INDEPENDENT_GC*i+FD3A2D_NUM_INDEPENDENT_GC]));
	}

clean_up:

	free(x_);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_NUM_INDEPENDENT_GC,Jr);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_NUM_INDEPENDENT_GC,Jl);
	free(f_);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_SYS_DOF,M_);
	freeMatrix(FD3A2D_NUM_INDEPENDENT_GC,FD3A2D_NUM_INDEPENDENT_GC,Mred);
	freeMatrix(FD3A2D_SYS_DOF,FD3A2D_NUM_INDEPENDENT_GC,MJr);
	free(KD);
	free(u_);
	free(z);

	/* Free System Struct */
	FD3A2D_freeStructure(dataPtr);

}
