% Sensitivitätsanalyse


%% init relocated
clear all

currentFolder = pwd;
cd(currentFolder)
% addpath(genpath([currentFolder '/ServoConstraints']))
addpath(genpath([currentFolder '/cFunctions']))
addpath(genpath([currentFolder '/sysFunctions']))
addpath(genpath([currentFolder '/userFunctions']))
% addpath(genpath([currentFolder '/neweulm2_20151213']))
load('sys.mat')

% rigid
w2_1 = 0;
w3_1 = 0;
v2_1 = 0;
v3_1 = 0;

%Faktoren für relocated output
w2_1 = 0.8;
w3_1 = 0.8;
v2_1 = 1.07;
v3_1 = 0.5;


sys.parameters.data.w2_1 = w2_1;
sys.parameters.data.w3_1 = w3_1;
sys.parameters.data.v2_1 = v2_1;
sys.parameters.data.v3_1 = v3_1;


%error in states

a_error     = 0;
b_error     = 0;
alpha_error = 0;
beta_error  = 0;
gam_error   = 0;
q1_error    = 0;
q2_error    = 0;

y_error = [a_error; b_error; alpha_error; beta_error; gam_error; q1_error; q2_error];

[y0, dy0] = calcInitConditions(0,[0,0,-0.7,0.7,0,0,0,zeros(1,7)]'); %-1 und 1 sind initial guesses der Winkel, zeros(3,1) sind Geschwindigkeit

% y0 = calcInitConditions(0,zeros(sys.counters.genCoord,1),zeros(sys.counters.genCoord,1)); %-1 und 1 sind initial guesses der Winkel, zeros(3,1) sind Geschwindigkeit
pos_EE = eqm_sysout_exactOut(0,[y0+y_error; dy0],zeros(5,1));

x_EE0 = pos_EE(1,1);
y_EE0 = pos_EE(2,1);

sampleTime = 1e-3;




%% sensitivity

t = 0;
x_ = [y0; dy0];
u_ = [y0(1,1),0,0,y0(2,1),0,0,y0(3,1),0,0]';

eqm_new = @(x_, varargin)eqm_sysout_exactOut(t, x_, u_, varargin);

sens = finiteDifferences(eqm_new,[y0,dy0],1e-3);
sens = sens(1:3,1:7);

id_dep_coord = sys.parameters.coordinates.idx_depCoord;
% 
x_red = y0;

u_ = [x_EE0;0;0;y_EE0;0;0;pos_EE(3,1);0;0];

y_  = educatedGuess(t, x_red(1 : sys.counters.genCoord-sys.counters.posConstraints));

y_  = findroot(@positionConstraints, y_,[],'index',id_dep_coord,'preargs',t, 'postargs', u_);

%c_ = positionConstraints(t, x_, u_, varargin)

