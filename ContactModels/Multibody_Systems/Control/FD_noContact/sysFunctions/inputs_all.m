function u_ = inputs_all(t, x_)
% u_ = inputs_all(t, x_)
% inputs_all - Function to compose the complete input vector

% Vector of the system inputs

u_ = zeros(3,1);
u_(1:3) = control_inputs(t, x_);

% END OF FILE

