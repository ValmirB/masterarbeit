%% 
clear all; close all; clc

load('sys.mat');
addpath('cFunctions/')
addpath('sysFunctions/')
addpath('userFunctions/')

%% output redefinition
sys.parameters.data.w2_1 = 0.8;
sys.parameters.data.w3_1 = 0.8;
sys.parameters.data.v2_1 = 1.07;
sys.parameters.data.v3_1 = 0.5;

%% startTimeInt
load('Test2_exactContact_3to5N_BVP.mat');


% actuators
sys.parameters.data.p_a_d = feedbackFF.y(1,1);
sys.parameters.data.p_b_d = feedbackFF.y(2,1);
sys.parameters.data.r_gamma3_d = feedbackFF.y(5,1);

actInit = feedbackFF.y([1,2,5],1);

%actual ICs need to be calculated 
[y0, Dy0, ~, ~] = calcInitConditions(0,[-1;1;0;0;zeros(2*sys.counters.genCoord-4,1)]);   % initial guess necessary
y0 = staticEquilibrium(0,y0);

disp('Start simulink simulation!')
