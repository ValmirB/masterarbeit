function [f_, M_, D2y_] = system_components(t, x_)
% [f_, M_, D2y_] = system_components(t, x_)
% system_components - Components of equations of motion for MB-System
% with closed kinematic loops which can be written as M*D2y = f.
% The forces and the velocities are updated within the function in order to
% satisfy the constraints.
%
% With one output argument only the force vector f_ is returned.
% With two output arguments, the mass matrix M_ and the forces f_ are returned.
% With three output arguments only the acceleratoin D2y is returned. The
% other components are zero.
%
% Input arguments:
% t .... Time
% x_ ... State vector

global sys;

% Evaluate all inputs
u_ = inputs_all(t, x_);

% Jacobian, velocities and accelerations due to the constraints
[Jr_a, Jl_a, Jr_v, theta_, gamma_] = constraintEquations(t, x_, u_);
if nargout < 3
	% Calculate the force vector and the mass matrix
	[M_, f_] = system_dynamics(t, x_, u_);
	% Determine the force vector in accordance with the constraints
	f_ = M_*full(Jr_a*((Jl_a*M_*Jr_a)\Jl_a)*(f_ - M_*gamma_)  + gamma_);
else % elseif nargout == 3
	% Calculate the acceleration from the state space form
	Dx_ = eqm_nonlin_ss(t, x_);
	D2y_ = Dx_(end/2+1:end);

	% Ignore the other output arguments
	M_ = 0;
	f_ = 0;
end

% END OF FILE

