function lambda_ = lagrange_multipliers(t, x_, u_, varargin)
% copied file

% Constraint derivatives
[C_, G_, Dc_, D2c_] = constraintDerivatives(t, x_, u_, varargin);

if isempty(G_)
    G_ = transpose(C_);
end

% Dynamics of the unconstraint system
[M_, f_] = system_dynamics(t, x_, u_, varargin);

% Compute: lambda_ = -(C_*(M_\G_))\(D2c_ + C_*(M_\f_));

% Step 1: compute the "inverse" of M_, multiply with C_
concatMat = C_*(M_\[G_ f_]);

% Step 2: compute inverse of (C_*(M_\G_))
lambda_ = -concatMat(:,1:end-1)\(D2c_ + concatMat(:,end));

% END OF FILE
