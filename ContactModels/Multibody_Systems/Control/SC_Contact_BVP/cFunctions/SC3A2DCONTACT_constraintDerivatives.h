#ifndef SC3A2DCONTACT_constraintDerivatives_H
#define SC3A2DCONTACT_constraintDerivatives_H

#include "SC3A2DCONTACT_paraStruct.h"

void SC3A2DCONTACT_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
