#ifndef SC3A2DCONTACT_Flexor_H
#define SC3A2DCONTACT_Flexor_H
#define SC3A2DCONTACT_SYS_DOF (7)
#define SC3A2DCONTACT_AUX_DOF (0)
#define SC3A2DCONTACT_LOOPS_EXIST (1)
#define SC3A2DCONTACT_NUM_LOOPS (5)
#define SC3A2DCONTACT_NUM_POSCONSTRAINTS (5)
#define SC3A2DCONTACT_NUM_VELCONSTRAINTS (0)
#define SC3A2DCONTACT_NUM_INDEPENDENT_GC (2)
#define SC3A2DCONTACT_NUM_DEPENDENT_GC (5)
#define SC3A2DCONTACT_NUM_INDEPENDENT_GV (2)
#define SC3A2DCONTACT_NUM_DEPENDENT_GV (5)
#define SC3A2DCONTACT_NUM_FELEM (1)

#define SC3A2DCONTACT_NUM_INPUT_GROUPS (0)
#define SC3A2DCONTACT_NUM_INPUTS {}
#define SC3A2DCONTACT_NUM_ALL_INPUTS (0)
#define SC3A2DCONTACT_NUM_OUTPUT_GROUPS (5)
#define SC3A2DCONTACT_NUM_OUTPUTS_DESIREDOUT (4)
#define SC3A2DCONTACT_NUM_OUTPUTS_EXACTOUT (6)
#define SC3A2DCONTACT_NUM_OUTPUTS_APPROXOUT (6)
#define SC3A2DCONTACT_NUM_OUTPUTS_CURVATURE (4)
#define SC3A2DCONTACT_NUM_OUTPUTS_CONTACTFORCE (1)
#define SC3A2DCONTACT_NUM_OUTPUTS {4, 6, 6, 4, 1}
#define SC3A2DCONTACT_NUM_ALL_OUTPUTS (21)
#endif
