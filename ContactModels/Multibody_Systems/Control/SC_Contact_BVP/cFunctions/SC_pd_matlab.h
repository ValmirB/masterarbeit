#ifndef SC_pd_matlab_H
#define SC_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "SC_paraStruct.h"

double SC_getScalar(const void *context, const char *name, double defaultValue);

int SC_getSystemStruct(void *dataPtr, const void *context);

void SC_educatedGuess(double t, double *y_, void *dataPtr);
void SC_educatedGuess(double t, double *y_, void *dataPtr);

#endif
