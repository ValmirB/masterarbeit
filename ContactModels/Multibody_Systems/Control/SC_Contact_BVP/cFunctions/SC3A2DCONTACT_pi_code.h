#ifndef SC3A2DCONTACT_pi_ind_H
#define SC3A2DCONTACT_pi_ind_H

#include "SC3A2DCONTACT_paraStruct.h"

/* System dynamics */
void SC3A2DCONTACT_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr);

/* Auxiliary dynamics */
void SC3A2DCONTACT_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

/* Equations of motion */
void SC3A2DCONTACT_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

#endif
