#include <string.h>
#include <stdlib.h>
#include "neweul.h"
#include "SC_paraStruct.h"
#include "SC_userDefined.h"
#include <stdio.h>

/* Initialize structure with system parameters */
void *SC_initializeSystemStruct(void){

	struct SC_paraStruct *data = calloc(1, sizeof(struct SC_paraStruct));

	/* User defined constant parameters */
	data->I_ee = 0.000000e+00;
	data->I_mb = 0.000000e+00;
	data->K_L = 8.000000e+01;
	data->K_R = 1.500000e+01;
	data->d_axes = 6.100000e-01;
	data->d_mb = 0.000000e+00;
	data->force_end = 2.000000e+00;
	data->force_start = 1.000000e+00;
	data->g = 9.810000e+00;
	data->l2 = 1.000000e+00;
	data->l3 = 4.643000e-01;
	data->m_C1 = 6.000000e+00;
	data->m_C2 = 6.600000e+00;
	data->m_ee = 0.000000e+00;
	data->m_mb = 0.000000e+00;
	data->phi2_1 = 5.177806e-02;
	data->phi3_1 = 1.108656e-01;
	data->psi2_1 = 1.307493e-01;
	data->psi3_1 = 4.181412e-01;
	data->traj_end = 3.000000e+00;
	data->traj_start = 1.000000e+00;
	data->traj_theta_end = 5.235988e-01;
	data->traj_theta_start = 5.235988e-01;
	data->traj_x_end = 1.600000e+00;
	data->traj_x_start = 1.200000e+00;
	data->traj_y_end = 2.000000e-01;
	data->traj_y_start = 5.000000e-01;
	data->v2_1 = 9.250000e-01;
	data->v3_1 = 9.250000e-01;
	data->w2_1 = 9.250000e-01;
	data->w3_1 = 9.250000e-01;
	data->z_0 = 1.570000e-01;
	data->p_a_s = 0.000000e+00;
	data->Dp_a_s = 0.000000e+00;
	data->D2p_a_s = 0.000000e+00;
	data->p_b_s = 0.000000e+00;
	data->Dp_b_s = 0.000000e+00;
	data->D2p_b_s = 0.000000e+00;
	data->r_alpha1_s = 0.000000e+00;
	data->Dr_alpha1_s = 0.000000e+00;
	data->D2r_alpha1_s = 0.000000e+00;
	data->r_beta2_s = 0.000000e+00;
	data->Dr_beta2_s = 0.000000e+00;
	data->D2r_beta2_s = 0.000000e+00;
	data->r_gamma3_s = 0.000000e+00;
	data->Dr_gamma3_s = 0.000000e+00;
	data->D2r_gamma3_s = 0.000000e+00;
	data->EA2_q001_s = 0.000000e+00;
	data->DEA2_q001_s = 0.000000e+00;
	data->D2EA2_q001_s = 0.000000e+00;
	data->EA3_q001_s = 0.000000e+00;
	data->DEA3_q001_s = 0.000000e+00;
	data->D2EA3_q001_s = 0.000000e+00;

	/* Rudimental system information */
	data->sysDof = 7;
	data->numBodies = 7;
	data->externalForces = NULL;
	data->externalTorques = NULL;

	/* Independent generalized coordinates */
	data->independentIndices[0] = 5;
	data->independentIndices[1] = 6;

	/* Dependent generalized coordinates */
	data->dependentIndices[0] = 0;
	data->dependentIndices[1] = 1;
	data->dependentIndices[2] = 2;
	data->dependentIndices[3] = 3;
	data->dependentIndices[4] = 4;

	SC_initializeConstraintStructure(data);

	return (void *)data;

}


void SC_initializeConstraintStructure(struct SC_paraStruct *data){

	data->con = malloc(sizeof(struct SC_constraintStructure));

	data->con->C_      = callocMatrix((SC_NUM_POSCONSTRAINTS+SC_NUM_VELCONSTRAINTS),SC_SYS_DOF);
	data->con->Dc_     = calloc((SC_NUM_POSCONSTRAINTS+SC_NUM_VELCONSTRAINTS),sizeof(double));
	data->con->D2c_    = calloc((SC_NUM_POSCONSTRAINTS+SC_NUM_VELCONSTRAINTS),sizeof(double));
	data->con->G_      = callocMatrix(SC_SYS_DOF,(SC_NUM_POSCONSTRAINTS+SC_NUM_VELCONSTRAINTS));

	data->con->Jr_v    = callocMatrix(SC_SYS_DOF,SC_NUM_INDEPENDENT_GC);
	data->con->Jr_a    = callocMatrix(SC_SYS_DOF,SC_NUM_INDEPENDENT_GV);
	data->con->Jl_a    = callocMatrix(SC_NUM_INDEPENDENT_GV,SC_SYS_DOF);
	data->con->theta_  = calloc(SC_SYS_DOF,sizeof(double));
	data->con->gamma_  = calloc(SC_SYS_DOF,sizeof(double));

	data->con->lambda_ = calloc((SC_NUM_POSCONSTRAINTS+SC_NUM_VELCONSTRAINTS),sizeof(double));

}

void SC_freeConstraintStructure(struct SC_paraStruct *data) {

	freeMatrix((SC_NUM_POSCONSTRAINTS+SC_NUM_VELCONSTRAINTS),SC_SYS_DOF,data->con->C_);
	free(data->con->Dc_);
	free(data->con->D2c_);
	freeMatrix(SC_SYS_DOF,(SC_NUM_POSCONSTRAINTS+SC_NUM_VELCONSTRAINTS),data->con->G_);

	freeMatrix(SC_SYS_DOF,SC_NUM_INDEPENDENT_GC,data->con->Jr_v);
	freeMatrix(SC_SYS_DOF,SC_NUM_INDEPENDENT_GV,data->con->Jr_a);
	freeMatrix(SC_NUM_INDEPENDENT_GV,SC_SYS_DOF,data->con->Jl_a);
	free(data->con->theta_);
	free(data->con->gamma_);

	free(data->con->lambda_);

	free(data->con);

}

void SC_freeStructure(void *dataPtr){

	struct SC_paraStruct *data  = (struct SC_paraStruct *) dataPtr;

	SC_freeConstraintStructure(data);
	free(data);

}
