function EE_force_y = f_EE_force_y(t, x_, varargin)
% f_EE_force_y - definition of time-depending user-defined variable EE_force_y

global sys;


% constant user-defined variables
EE_force_y_d = sys.parameters.data.EE_force_y_d;
EE_force_y = zeros(1,1);

EE_force_y(1) = EE_force_y_d;

% END OF FILE

