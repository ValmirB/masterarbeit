#ifndef SC_paraStruct_H
#define SC_paraStruct_H

#include <stdlib.h>
#include <math.h>
#include "SC_Flexor.h"

struct SC_constraintStructure {

	double **G_;

	double **C_;
	double *Dc_;
	double *D2c_;

	double **Jr_v;
	double **Jr_a;
	double **Jl_a;
	double *theta_;
	double *gamma_;

	double *lambda_;

};

struct SC_paraStruct {

	/* User defined constant parameters */
	double I_ee;
	double I_mb;
	double K_L;
	double K_R;
	double d_axes;
	double d_mb;
	double force_end;
	double force_start;
	double g;
	double l2;
	double l3;
	double m_C1;
	double m_C2;
	double m_ee;
	double m_mb;
	double phi2_1;
	double phi3_1;
	double psi2_1;
	double psi3_1;
	double traj_end;
	double traj_start;
	double traj_theta_end;
	double traj_theta_start;
	double traj_x_end;
	double traj_x_start;
	double traj_y_end;
	double traj_y_start;
	double v2_1;
	double v3_1;
	double w2_1;
	double w3_1;
	double z_0;
	double p_a_s;
	double Dp_a_s;
	double D2p_a_s;
	double p_b_s;
	double Dp_b_s;
	double D2p_b_s;
	double r_alpha1_s;
	double Dr_alpha1_s;
	double D2r_alpha1_s;
	double r_beta2_s;
	double Dr_beta2_s;
	double D2r_beta2_s;
	double r_gamma3_s;
	double Dr_gamma3_s;
	double D2r_gamma3_s;
	double EA2_q001_s;
	double DEA2_q001_s;
	double D2EA2_q001_s;
	double EA3_q001_s;
	double DEA3_q001_s;
	double D2EA3_q001_s;

	/* Rudimental system information */
	int sysDof;
	int numBodies;
	double *externalForces;
	double *externalTorques;

	/* Abbreviation Force Elements */

	/* Independent generalized coordinates */
	int independentIndices[2];

	/* Dependent generalized coordinates */
	int dependentIndices[5];

	struct SC_constraintStructure *con;

};

void *SC_initializeSystemStruct(void);

void SC_initializeConstraintStructure(struct SC_paraStruct *data);

void SC_freeConstraintStructure(struct SC_paraStruct *data);

void SC_freeStructure(void *dataPtr);

#endif

