#ifndef SC3A2DCONTACT_linearizeMBS_H
#define SC3A2DCONTACT_linearizeMBS_H
#include "SC3A2DCONTACT_paraStruct.h"

/* Function prototypes */
void SC3A2DCONTACT_createSystemMatrix(double t_WP, double *x_WP, double *u_WP,
    double **A, int dim, void *dataPtr);

void SC3A2DCONTACT_createInputMatrix(double t_WP, double *x_WP, double *u_WP,
    double **B, int dim, int numIn, void *dataPtr);

void SC3A2DCONTACT_createOutputMatrix(double t_WP, double *x_WP,
    double *u_WP, double **C,
    int dim, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));

void SC3A2DCONTACT_createFeedthroughMatrix(double t_WP, double *x_WP,
    double *u_WP, double **D,
    int numIn, int numOut, void *dataPtr,
    void (*outputFunction)(double, double *, double *, double *, void *));
#endif
