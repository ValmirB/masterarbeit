#include <math.h>
#include "SC3A2DCONTACT_paraStruct.h"
#include "SC3A2DCONTACT_pi_code.h"
#include "SC3A2DCONTACT_pd_matlab.h"
#include "SC3A2DCONTACT_userDefined.h"
#include "neweul.h"

void SC3A2DCONTACT_educatedGuess(double t, double *y_, void *dataPtr);

void SC3A2DCONTACT_educatedGuess(double t, double *y_, void *dataPtr){

	struct SC3A2DCONTACT_paraStruct *data  = (struct SC3A2DCONTACT_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
