#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "SC3A2DCONTACT_pi_code.h"
#include "SC3A2DCONTACT_userDefined.h"
#include "SC3A2DCONTACT_pd_matlab.h"
#include "neweul.h"
#include "SC3A2DCONTACT_paraStruct.h"
#include "SC3A2DCONTACT_Flexor.h"

#include "SC3A2DCONTACT_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double t             = mxGetScalar( prhs[0] );
	double *x_red        = mxGetPr( prhs[1] );
	double *y_ind        = &(x_red[0]);
	double *Dy_ind       = &(x_red[SC3A2DCONTACT_NUM_INDEPENDENT_GC]);
	double *x_           = calloc( 2*SC3A2DCONTACT_SYS_DOF, sizeof(double));
	double **Jr          = callocMatrix( SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_NUM_INDEPENDENT_GC);
	double **Jl          = callocMatrix( SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_NUM_INDEPENDENT_GC);
	double *f_           = calloc( SC3A2DCONTACT_SYS_DOF, sizeof(double));
	double **M_          = callocMatrix( SC3A2DCONTACT_SYS_DOF, SC3A2DCONTACT_SYS_DOF);
	double **Mred        = callocMatrix(SC3A2DCONTACT_NUM_INDEPENDENT_GC, SC3A2DCONTACT_NUM_INDEPENDENT_GC);
	double **MJr         = callocMatrix( SC3A2DCONTACT_SYS_DOF, SC3A2DCONTACT_NUM_INDEPENDENT_GC);
	double *KD           = calloc(2*SC3A2DCONTACT_SYS_DOF*SC3A2DCONTACT_NUM_INDEPENDENT_GC, sizeof(double));
	double *matlabReturn = NULL;
	double *u_           = calloc(SC3A2DCONTACT_NUM_ALL_INPUTS, sizeof(double));
	unsigned short *z    = calloc(SC3A2DCONTACT_NUM_INDEPENDENT_GC, sizeof(unsigned short));
	int counter_         = 0;
	int i,j;
	double myStep = 1e-8;
	struct SC3A2DCONTACT_paraStruct *data = NULL;

	dataPtr = SC3A2DCONTACT_initializeSystemStruct();

	data  = (struct SC3A2DCONTACT_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<2){
		mexPrintf("Please provide two or three input arguments!\n");
		goto clean_up;
	}

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		goto clean_up;
	}

	if ((nrhs==3) && (mxIsStruct(prhs[2]))){
		SC3A2DCONTACT_getSystemStruct(dataPtr, (const void *) prhs[2]);
	}

	if ((mxGetM(prhs[1])!=2*SC3A2DCONTACT_NUM_INDEPENDENT_GC) || (mxGetN(prhs[1])!=1)){
		mexPrintf("The second input argument has to be a 4x1 vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*SC3A2DCONTACT_NUM_INDEPENDENT_GC, 2*SC3A2DCONTACT_NUM_INDEPENDENT_GC, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	/* Get dependent generalized coordinates and velocities */
	for ( counter_ = 0 ; counter_ < SC3A2DCONTACT_NUM_INDEPENDENT_GC ; counter_++ ){
		x_[data->independentIndices[counter_]]  =  y_ind[counter_];
		x_[data->independentIndices[counter_]+SC3A2DCONTACT_SYS_DOF] = Dy_ind[counter_];
	}

	SC3A2DCONTACT_educatedGuess(t, x_, dataPtr);

	SC3A2DCONTACT_getDependentGenCoords(t, x_, u_, dataPtr);

	
	SC3A2DCONTACT_jacobianPartition(t, x_, u_, Jr, Jl, dataPtr);
	
	/* Loop over all position values */

	for (i=0; i<SC3A2DCONTACT_NUM_INDEPENDENT_GC; i++){
		clearVector(SC3A2DCONTACT_SYS_DOF,f_);
		clearVector(SC3A2DCONTACT_NUM_ALL_INPUTS,u_);
		clearMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_SYS_DOF,M_);

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		SC3A2DCONTACT_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			KD[SC3A2DCONTACT_SYS_DOF*i+j] = f_[j];

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			x_[j] = x_[j] - 2.0*Jr[j][i]*myStep;

		clearVector(SC3A2DCONTACT_SYS_DOF,f_);
		clearMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_SYS_DOF,M_);

		SC3A2DCONTACT_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			KD[SC3A2DCONTACT_SYS_DOF*i+j] = (KD[SC3A2DCONTACT_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			x_[j] = x_[j] + Jr[j][i]*myStep;

		clearVector(SC3A2DCONTACT_SYS_DOF,f_);
		clearVector(SC3A2DCONTACT_NUM_ALL_INPUTS,u_);
		clearMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_SYS_DOF,M_);

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			x_[j+SC3A2DCONTACT_SYS_DOF] = x_[j+SC3A2DCONTACT_SYS_DOF] + Jr[j][i]*myStep;

		SC3A2DCONTACT_system_dynamics(t, x_, u_, f_, M_, dataPtr);
		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			KD[SC3A2DCONTACT_NUM_INDEPENDENT_GC*SC3A2DCONTACT_SYS_DOF+SC3A2DCONTACT_SYS_DOF*i+j] = f_[j];

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			x_[j+SC3A2DCONTACT_SYS_DOF] = x_[j+SC3A2DCONTACT_SYS_DOF] - 2.0*Jr[j][i]*myStep;

		clearVector(SC3A2DCONTACT_SYS_DOF,f_);
		clearMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_SYS_DOF,M_);

		SC3A2DCONTACT_system_dynamics(t, x_, u_, f_, M_, dataPtr);

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			KD[SC3A2DCONTACT_NUM_INDEPENDENT_GC*SC3A2DCONTACT_SYS_DOF+SC3A2DCONTACT_SYS_DOF*i+j] = (KD[SC3A2DCONTACT_NUM_INDEPENDENT_GC*SC3A2DCONTACT_SYS_DOF+SC3A2DCONTACT_SYS_DOF*i+j] - f_[j])/(2.0*myStep);

		for (j=0; j<SC3A2DCONTACT_SYS_DOF; j++)
			x_[j+SC3A2DCONTACT_SYS_DOF] = x_[j+SC3A2DCONTACT_SYS_DOF] + Jr[j][i]*myStep;

	}

	MatrixMultiplication( 'N', 'N', SC3A2DCONTACT_SYS_DOF, SC3A2DCONTACT_SYS_DOF, SC3A2DCONTACT_NUM_INDEPENDENT_GC, 1.0, M_, Jr, 0.0, MJr); /* MJr allokieren */

	MatrixMultiplication( 'T', 'N', SC3A2DCONTACT_NUM_INDEPENDENT_GC, SC3A2DCONTACT_SYS_DOF, SC3A2DCONTACT_NUM_INDEPENDENT_GC, 1.0, Jl, MJr, 0.0, Mred); /* Mred allokieren */

	GaussianElimination( SC3A2DCONTACT_NUM_INDEPENDENT_GC, z, Mred);

	for (i=0; i<SC3A2DCONTACT_NUM_INDEPENDENT_GC; i++)
		matlabReturn[(2*(SC3A2DCONTACT_NUM_INDEPENDENT_GC+i))*SC3A2DCONTACT_NUM_INDEPENDENT_GC+i] = 1.0;

	for (i=0; i<2*SC3A2DCONTACT_NUM_INDEPENDENT_GC; i++){
		MatrixVectorMultiplication( 'T', SC3A2DCONTACT_NUM_INDEPENDENT_GC, SC3A2DCONTACT_SYS_DOF, 1.0, Jl, &(KD[SC3A2DCONTACT_SYS_DOF*i]), 0.0, &(matlabReturn[2*SC3A2DCONTACT_NUM_INDEPENDENT_GC*i+SC3A2DCONTACT_NUM_INDEPENDENT_GC]));
		permuteVector(SC3A2DCONTACT_NUM_INDEPENDENT_GC, z, &(matlabReturn[2*SC3A2DCONTACT_NUM_INDEPENDENT_GC*i+SC3A2DCONTACT_NUM_INDEPENDENT_GC]));
		LUForBack_1dim( SC3A2DCONTACT_NUM_INDEPENDENT_GC, Mred, &(matlabReturn[2*SC3A2DCONTACT_NUM_INDEPENDENT_GC*i+SC3A2DCONTACT_NUM_INDEPENDENT_GC]));
	}

clean_up:

	free(x_);
	freeMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_NUM_INDEPENDENT_GC,Jr);
	freeMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_NUM_INDEPENDENT_GC,Jl);
	free(f_);
	freeMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_SYS_DOF,M_);
	freeMatrix(SC3A2DCONTACT_NUM_INDEPENDENT_GC,SC3A2DCONTACT_NUM_INDEPENDENT_GC,Mred);
	freeMatrix(SC3A2DCONTACT_SYS_DOF,SC3A2DCONTACT_NUM_INDEPENDENT_GC,MJr);
	free(KD);
	free(u_);
	free(z);

	/* Free System Struct */
	SC3A2DCONTACT_freeStructure(dataPtr);

}
