function q_ = eqm_nonlin_q(t, x_, u_, varargin)
% EQM_NONLIN_Q - Vector of generalized applied forces of the system Flexor
% For flexible bodies, the elastic forces are contained in this vector.
% Vector of applied forces

qa_ = eqm_nonlin_qa(t, x_, u_);
% Vector of inner elastic forces
h_e_ = eqm_nonlin_h_e(t, x_, u_);

% Jacobian matrix
Jg_ = eqm_nonlin_Jg(t, x_, u_);

% Force vector
q_ = transpose(Jg_)*(qa_+h_e_);


% END OF FILE

