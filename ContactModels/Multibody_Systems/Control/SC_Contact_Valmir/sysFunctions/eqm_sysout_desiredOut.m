function result_ = eqm_sysout_desiredOut(t, x_, u_, varargin)
% eqm_sysout_desiredOut - Vector of the system Flexor output desiredOut
% 
% Entries are as in sys.model.output.desiredOut

global sys;

% standardized signals

Dtcp_traj1 = f_Dtcp_traj1(t);
Dtcp_traj2 = f_Dtcp_traj2(t);
tcp_traj1 = f_tcp_traj1(t);
tcp_traj2 = f_tcp_traj2(t);

% System output desiredOut vector
des_x = tcp_traj1;
des_y = tcp_traj2;
des_Dx = Dtcp_traj1;
des_Dy = Dtcp_traj2;

result_ = zeros(4,1);

result_(1) = des_x;
result_(2) = des_y;
result_(3) = des_Dx;
result_(4) = des_Dy;

% END OF FILE 
