%% Initialize
% Clean up
clear all; close all; clc;
load('sys.mat')
global sys EEexport

%sys.parameters.data.w2_1 =0.95% 0.95%0.947;
% Trajectory definition
trajectoryTime_ = 2; % in seconds

sys.parameters.data.force_start = 7;
sys.parameters.data.force_end = 3;


sys.parameters.data.traj_x_start = 1.31;
sys.parameters.data.traj_x_end = 1.31;

sys.parameters.data.traj_y_start = 0.55;
sys.parameters.data.traj_y_end = 0.30;

sys.parameters.data.traj_theta_start = pi/6;
sys.parameters.data.traj_theta_end = pi/6;




% Add folder of the sysDef_basic & add Neweul_M2
if ~(exist('sysDef_basic','file')==2)
    currentDir_ = pwd;
    folderName_ = 'Multibody_Systems';
    strEnd_ = strfind(currentDir_,[filesep,folderName_,filesep]) + length(folderName_);
    addpath(currentDir_(1:strEnd_));
end

currentFolder = pwd;
addpath(genpath(currentFolder));

%% output redefinition - VB ursprünglich

%VB - geht bei 4s Traj Time
sys.parameters.data.w2_1 = 0.8;
sys.parameters.data.w3_1 = 0.8;
sys.parameters.data.v2_1 = 1.07;
sys.parameters.data.v3_1 = 0.5;

%NM - geht bei 4s Traj Time
% sys.parameters.data.w2_1 = 0.78;
% sys.parameters.data.w3_1 = 0.86;
% sys.parameters.data.v2_1 = 0.92;
% sys.parameters.data.v3_1 = 0.5;



%% Start Time Int

sys.parameters.data.traj_end = sys.parameters.data.traj_start + trajectoryTime_;
tstart = sys.parameters.data.traj_start;

[y0, Dy0, ~, ~] = calcInitConditions(tstart,[0;0;-0.7;0.7;zeros(2*sys.counters.genCoord-4,1)]);   % initial guess necessary
y0 = staticEquilibrium(0,y0);

%%
disp('Start TimeInt ...')
feedbackFF = timeInt('waitbar',true,'function','SCc3A2D_eqm_nonlin_ss','x0', [y0;Dy0], 'time', 0:0.5e-3:(tstart + trajectoryTime_ + 1), 'integrator', @ode4);

%% get accelerations
disp('Start Postprocessing ...')
feedbackFF.Dy = zeros(sys.counters.genCoord*2, length(feedbackFF.x));
for ii = 1:length(feedbackFF.x)
    dx_ = eqm_nonlin_ss(feedbackFF.x(ii),feedbackFF.y(:,ii));
    feedbackFF.Dy(:,ii) = dx_;
end

%%
dateString = strrep(strrep(datestr(now),':','_'),' ','_');

if ~isempty(sys.parameters.coordinates.elastic)
    EEexport.output = 'desOut'; % this exports the written output to the hdf5file as desired EE
    hdf5filename = (['FF_',sys.info.systemSetup.trajectoryType,'_',num2str(sys.parameters.data.traj_end-sys.parameters.data.traj_start) ,'s_traj_',num2str(numel(sys.parameters.coordinates.elastic)),'edof_EETP',sys.info.systemSetup.EE_Tracking_Point,'_',EEexport.output,'_',dateString,'.h5']);
else
    hdf5filename = (['FF_',sys.info.systemSetup.trajectoryType,'_',num2str(sys.parameters.data.traj_end-sys.parameters.data.traj_start) ,'s_traj_',num2str(numel(sys.parameters.coordinates.elastic)),'edof_',dateString,'.h5']);
end

if exist('h5create','file')==2
    exportHDF5File(feedbackFF,hdf5filename);
end
clearvars -global EEexport


disp('Calculation Complete')