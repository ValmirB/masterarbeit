#ifndef SC_constraintDerivatives_H
#define SC_constraintDerivatives_H

#include "SC_paraStruct.h"

void SC_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
