#include "SCc3A2D_userDefined.h"
#include <stdio.h>
#include <math.h>
#include "SCc3A2D_pi_code.h"
#include <stdlib.h>
#include "neweul.h"
#include <string.h>
#include "SCc3A2D_Flexor.h"
#include "SCc3A2D_constraintEquations.h"

/* System dynamics */
void SCc3A2D_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double I_ee = data->I_ee;
	double g = data->g;
	double m_C1 = data->m_C1;
	double m_C2 = data->m_C2;
	double m_ee = data->m_ee;


	/* user-defined signals */

	double F_contact = SCc3A2D_f_F_contact(t, data);


	/* Automatically introduced abbreviations */

	double DelBo_EA2_2_rot_z_ = SCc3A2D_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = SCc3A2D_f_DelBo_EA2_2_x_(x_);
	double DelBo_EA2_3_rot_z_ = SCc3A2D_f_DelBo_EA2_3_rot_z_(x_);
	double DelBo_EA2_3_x_ = SCc3A2D_f_DelBo_EA2_3_x_(x_);
	double DelBo_EA2_3_y_ = SCc3A2D_f_DelBo_EA2_3_y_(x_);
	double DelBo_EA3_2_rot_x_ = SCc3A2D_f_DelBo_EA3_2_rot_x_(x_);
	double DelBo_EA3_2_rot_z_ = SCc3A2D_f_DelBo_EA3_2_rot_z_(x_);
	double DelBo_EA3_2_x_ = SCc3A2D_f_DelBo_EA3_2_x_(x_);
	double DelBo_EA3_2_y_ = SCc3A2D_f_DelBo_EA3_2_y_(x_);
	double elBo_EA2_2_rot_z_ = SCc3A2D_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = SCc3A2D_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = SCc3A2D_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = SCc3A2D_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = SCc3A2D_f_elBo_EA2_3_y_(x_);
	double elBo_EA3_2_rot_x_ = SCc3A2D_f_elBo_EA3_2_rot_x_(x_);
	double elBo_EA3_2_rot_z_ = SCc3A2D_f_elBo_EA3_2_rot_z_(x_);
	double elBo_EA3_2_x_ = SCc3A2D_f_elBo_EA3_2_x_(x_);
	double elBo_EA3_2_y_ = SCc3A2D_f_elBo_EA3_2_y_(x_);

	/* generalized coordinates */

	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];
	double r_gamma3 = x_[4];
	double EA2_q001 = x_[5];
	double EA3_q001 = x_[6];
	double Dr_alpha1 = x_[9];
	double Dr_beta2 = x_[10];
	double Dr_gamma3 = x_[11];
	double DEA2_q001 = x_[12];
	double DEA3_q001 = x_[13];

#if defined SCC3A2D_SYS_DOF && SCC3A2D_SYS_DOF > 0
	double Jg[6][SCC3A2D_SYS_DOF]  = {{0.0}};
	double MJ[7][SCC3A2D_SYS_DOF]  = {{0.0}};
	double Mq[6][6]  = {{0.0}};
	double Mq_coup[6][1] = {{0.0}};
	double accelq[7] = {0.0};
	double qa[7]     = {0.0};
	int i_,j_,k_;

	clearVector(SCC3A2D_SYS_DOF,f);
#endif


	/* Body C1 */

	/* Jacobian matrix */
	Jg[1][0] = 1.0;

	/* Mass matrix */
	Mq[0][0] = m_C1;
	Mq[1][1] = m_C1;
	Mq[2][2] = m_C1;
	Mq[3][3] = 1.0;
	Mq[4][4] = 1.0;
	Mq[5][5] = 1.0;

	/* Vector of local accelerations */

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	1.0*g*m_C1;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */

	/* Mass matrix calculations */
	MJ[1][0] = Mq[1][1]*Jg[1][0];

	M[0][0] = M[0][0]+Jg[1][0]*MJ[1][0];

	/* Force vector calculations */



	/* Body C2 */

	/* Jacobian matrix */
	Jg[0][1] = 1.0;

	/* Mass matrix */
	Mq[0][0] = m_C2;
	Mq[1][1] = m_C2;
	Mq[2][2] = m_C2;
	Mq[3][3] = 1.0;
	Mq[4][4] = 1.0;
	Mq[5][5] = 1.0;

	/* Vector of local accelerations */

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	1.0*g*m_C2;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1];

	/* Force vector calculations */



	/* Body EA1 */

	/* Jacobian matrix */
	Jg[0][0] = sin(r_alpha1);
	Jg[1][0] = cos(r_alpha1);
	Jg[5][2] = 1.0;

	/* Mass matrix */
	Mq[0][0] = 2.158;
	Mq[1][1] = 2.158;
	Mq[5][1] = 0.41626;
	Mq[2][2] = 2.158;
	Mq[4][2] = - 
	0.41626;
	Mq[3][3] = 0.0019316386666666661604818688857677;
	Mq[2][4] = - 
	0.41626;
	Mq[4][4] = 0.13202505266666664240915451955516;
	Mq[1][5] = 0.41626;
	Mq[5][5] = 0.13079362466666669129189415343717;


	/* Vector of local accelerations */

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	2.158*g;
	qa[4] = 0.41626*g;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */
	qa[0] = qa[0] + 
	0.41626*pow(Dr_alpha1,2.0);

	/* Mass matrix calculations */
	MJ[0][0] = Mq[0][0]*Jg[0][0];
	MJ[1][0] = Mq[1][1]*Jg[1][0];
	MJ[1][2] = Mq[1][5]*Jg[5][2];
	MJ[5][0] = Mq[5][1]*Jg[1][0];
	MJ[5][2] = Mq[5][5]*Jg[5][2];

	M[0][0] = M[0][0]+Jg[0][0]*MJ[0][0]+Jg[1][0]*MJ[1][0];
	M[0][2] = M[0][2]+Jg[1][0]*MJ[1][2];
	M[2][0] = M[2][0]+Jg[5][2]*MJ[5][0];
	M[2][2] = M[2][2]+Jg[5][2]*MJ[5][2];

	/* Force vector calculations */


	f[0] = f[0]+Jg[0][0]*qa[0];


	/* Body EA2 */

	/* Jacobian matrix */
	Jg[0][1] = sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2);
	Jg[1][1] = cos(r_beta2)*sin(elBo_EA2_2_rot_z_) - 
	 1.0*cos(elBo_EA2_2_rot_z_)*sin(r_beta2);
	Jg[1][3] = - 
	 1.0*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 
	 1.0*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0);
	Jg[5][3] = 1.0;
	Jg[1][5] = 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0);
	Jg[5][5] = - 
	0.12720812859558897067735472319328;

	/* Mass matrix */
	Mq[0][0] = 5.582176000000005799961400043685;
	Mq[5][0] = - 
	0.10280645791469449068511465839038*EA2_q001;
	Mq[1][1] = 5.582176000000005799961400043685;
	Mq[5][1] = 3.5562046480000013559674698626623 - 
	 0.0000000000019022355826173726226087097839751*EA2_q001;
	Mq[2][2] = 5.582176000000005799961400043685;
	Mq[3][2] = 0.10280645791469449068511465839038*EA2_q001;
	Mq[4][2] = 0.0000000000019022355826173726226087097839751*EA2_q001 - 
	 3.5562046480000013559674698626623;
	Mq[2][3] = 0.10280645791469449068511465839038*EA2_q001;
	Mq[3][3] = 1542.0024056266133811732288450003;
	Mq[4][3] = 100.70291968414107941498514264822*EA2_q001;
	Mq[2][4] = 0.0000000000019022355826173726226087097839751*EA2_q001 - 
	 3.5562046480000013559674698626623;
	Mq[3][4] = 100.70291968414107941498514264822*EA2_q001;
	Mq[4][4] = 2.9100075646213303670606364903506 - 
	 0.00000000000343084166564062676893844993155*EA2_q001;
	Mq[0][5] = - 
	0.10280645791469449068511465839038*EA2_q001;
	Mq[1][5] = 3.5562046480000013559674698626623 - 
	 0.0000000000019022355826173726226087097839751*EA2_q001;
	Mq[5][5] = 2.9083586035013273551896872959333 - 
	 0.00000000000343084166564062676893844993155*EA2_q001;
	Mq_coup[0][0] = -0.0000000000019022355826173726226087097839751;
	Mq_coup[1][0] = 0.10280645791469449068511465839038;
	Mq_coup[5][0] = 0.10408578881898107593162450257296;

	M[5][5] += 5.209986e-03;

	/* Vector of local accelerations */
	accelq[0] = - 
	 1.0*Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 
	 1.0*cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_));
	accelq[1] = Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_));

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	5.582176000000005799961400043685*g;
	qa[3] = - 
	0.10280645791469449068511465839038*EA2_q001*g;
	qa[4] = - 
	0.00000000000000000000000000040389678347315804437080502542479*g*(4709707183750797.0*EA2_q001 - 
	 8804736243303947221300412416.0);

	/* Elastic forces h_e */
	qa[6] = qa[6] - 
	 0.0024999999961810774433412785100472*DEA2_q001 - 
	 0.99999999847243092876425407666829*EA2_q001;

	/* Forces due to choice of frame of reference h_omega */
	qa[0] = qa[0] - 
	 1.0*DEA2_q001*(0.20561291582938898137022931678075*DelBo_EA2_2_rot_z_ - 
	 0.20561291582938898137022931678075*Dr_beta2) - 
	 1.0*pow(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2,2.0)*(0.0000000000019022355826173726226087097839751*EA2_q001 - 
	 3.5562046480000013559674698626623);
	qa[1] = qa[1] + 
	0.10280645791469449068511465839038*EA2_q001*pow(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2,2.0)- 
	 1.0*DEA2_q001*(0.0000000000038044711652347452452174195679502*DelBo_EA2_2_rot_z_ - 
	 0.0000000000038044711652347452452174195679502*Dr_beta2);
	qa[5] = qa[5] - 
	0.00000000000343084166564062676893844993155*DEA2_q001*(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2);
	qa[6] = qa[6] + 
	pow(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2,2.0)*(0.0056547461988264999602216143159694*EA2_q001 - 
	 0.000000000001715420832820313384469224965775);

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];
	MJ[0][3] = Mq[0][5]*Jg[5][3];
	MJ[0][5] = Mq[0][5]*Jg[5][5];
	MJ[1][1] = Mq[1][1]*Jg[1][1];
	MJ[1][3] = Mq[1][1]*Jg[1][3]+Mq[1][5]*Jg[5][3];
	MJ[1][5] = Mq[1][1]*Jg[1][5]+Mq[1][5]*Jg[5][5];
	MJ[5][1] = Mq[5][0]*Jg[0][1]+Mq[5][1]*Jg[1][1];
	MJ[5][3] = Mq[5][1]*Jg[1][3]+Mq[5][5]*Jg[5][3];
	MJ[5][5] = Mq[5][1]*Jg[1][5]+Mq[5][5]*Jg[5][5];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1]+Jg[1][1]*MJ[1][1];
	M[1][3] = M[1][3]+Jg[0][1]*MJ[0][3]+Jg[1][1]*MJ[1][3];
	M[1][5] = M[1][5]+Jg[0][1]*MJ[0][5]+Jg[1][1]*MJ[1][5];
	M[3][1] = M[3][1]+Jg[1][3]*MJ[1][1]+Jg[5][3]*MJ[5][1];
	M[3][3] = M[3][3]+Jg[1][3]*MJ[1][3]+Jg[5][3]*MJ[5][3];
	M[3][5] = M[3][5]+Jg[1][3]*MJ[1][5]+Jg[5][3]*MJ[5][5];
	M[5][1] = M[5][1]+Jg[1][5]*MJ[1][1]+Jg[5][5]*MJ[5][1];
	M[5][3] = M[5][3]+Jg[1][5]*MJ[1][3]+Jg[5][5]*MJ[5][3];
	M[5][5] = M[5][5]+Jg[1][5]*MJ[1][5]+Jg[5][5]*MJ[5][5];
	M[1][5] = M[1][5]+Jg[0][1]*Mq_coup[0][0]+Jg[1][1]*Mq_coup[1][0];
	M[3][5] = M[3][5]+Jg[1][3]*Mq_coup[1][0]+Jg[5][3]*Mq_coup[5][0];
	M[5][5] = M[5][5]+Jg[1][5]*Mq_coup[1][0]+Jg[5][5]*Mq_coup[5][0];
	M[5][1] = M[5][1]+Mq_coup[0][0]*Jg[0][1]+Mq_coup[1][0]*Jg[1][1];
	M[5][3] = M[5][3]+Mq_coup[1][0]*Jg[1][3]+Mq_coup[5][0]*Jg[5][3];
	M[5][5] = M[5][5]+Mq_coup[1][0]*Jg[1][5]+Mq_coup[5][0]*Jg[5][5];

	/* Force vector calculations */
	qa[0] = qa[0]+Mq[0][0]*accelq[0];
	qa[1] = qa[1]+Mq[1][1]*accelq[1];
	qa[5] = qa[5]+Mq[5][0]*accelq[0]+Mq[5][1]*accelq[1];
	qa[6] = qa[6]+Mq_coup[0][0]*accelq[0]+Mq_coup[1][0]*accelq[1];


	f[1] = f[1]+Jg[0][1]*qa[0]+Jg[1][1]*qa[1];
	f[3] = f[3]+Jg[1][3]*qa[1]+Jg[5][3]*qa[5];
	f[5] = f[5]+Jg[1][5]*qa[1]+Jg[5][5]*qa[5];
	for(i_=0; i_<1; i_++)
		f[i_+5] += qa[i_+6];


	/* Body EA3 */

	/* Jacobian matrix */
	Jg[0][1] = cos(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 
	 1.0*sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	Jg[1][1] = - 
	 1.0*cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 
	 1.0*sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	Jg[0][3] = sin(r_gamma3)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_));
	Jg[1][3] = cos(r_gamma3)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) + 
	 sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_));
	Jg[5][3] = 1.0;
	Jg[5][4] = 1.0;
	Jg[0][5] = cos(r_gamma3)*(0.05177806232203822911497326231256*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000075504122110177805935058228092758*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) + 
	 sin(r_gamma3)*(0.05177806232203822911497326231256*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000075504122110177805935058228092758*sin(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)));
	Jg[1][5] = cos(r_gamma3)*(0.05177806232203822911497326231256*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000075504122110177805935058228092758*sin(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 
	 1.0*sin(r_gamma3)*(0.05177806232203822911497326231256*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000075504122110177805935058228092758*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_));
	Jg[5][5] = 0.0035411566804658678453421316589811;

	/* Mass matrix */
	Mq[0][0] = 1.953632;
	Mq[5][0] = - 
	0.082637824577984173024525205164537*EA3_q001;
	Mq[1][1] = 1.953632;
	Mq[5][1] = 0.46025217599999990181203202155302 - 
	 0.0000000000083259892263668554836784628430295*EA3_q001;
	Mq[2][2] = 1.953632;
	Mq[3][2] = 0.082637824577984173024525205164537*EA3_q001;
	Mq[4][2] = 0.0000000000083259892263668554836784628430295*EA3_q001 - 
	 0.46025217599999990181203202155302;
	Mq[2][3] = 0.082637824577984173024525205164537*EA3_q001;
	Mq[3][3] = 0.0018106169526666670427372807239408;
	Mq[4][3] = - 
	0.035011687062733401476499750515359*EA3_q001;
	Mq[2][4] = 0.0000000000083259892263668554836784628430295*EA3_q001 - 
	 0.46025217599999990181203202155302;
	Mq[3][4] = - 
	0.035011687062733401476499750515359*EA3_q001;
	Mq[4][4] = 0.16441743652066659331367759477871 - 
	 0.0000000000069423446906757523202870395849084*EA3_q001;
	Mq[5][4] = - 
	0.00000000015844330795472250592579362832302*EA3_q001;
	Mq[0][5] = - 
	0.082637824577984173024525205164537*EA3_q001;
	Mq[1][5] = 0.46025217599999990181203202155302 - 
	 0.0000000000083259892263668554836784628430295*EA3_q001;
	Mq[4][5] = - 
	0.00000000015844330795472250592579362832302*EA3_q001;
	Mq[5][5] = 0.16401884600666660762868787060142 - 
	 0.0000000000069423446906757523202870395849084*EA3_q001;
	Mq_coup[0][0] = -0.0000000000083259892263668554836784628430295;
	Mq_coup[1][0] = 0.082637824577984173024525205164537;
	Mq_coup[3][0] = 0.0000000017265879372822199862790674352773;
	Mq_coup[5][0] = 0.03270122901544592064482230853173;

	M[6][6] += 6.802983e-03;

	/* Vector of local accelerations */
	accelq[0] = cos(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) + 
	 sin(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_))));
	accelq[1] = cos(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) - 
	 1.0*sin(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_))));

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	1.953632*g;
	qa[3] = - 
	0.082637824577984173024525205164537*EA3_q001*g;
	qa[4] = - 
	0.0000000000000000000000000016155871338926321774832201016991*g*(5153537714989119.0*EA3_q001 - 
	 284882298419310817479491584.0);

	/* Elastic forces h_e */
	qa[6] = qa[6] - 
	 0.0025000000016936123163613370223857*DEA3_q001 - 
	 1.0000000006774449889945799441193*EA3_q001;

	/* Forces due to choice of frame of reference h_omega */
	qa[0] = qa[0] + 
	DEA3_q001*(0.16527564915596834604905041032907*DelBo_EA2_3_rot_z_ - 
	 0.16527564915596834604905041032907*DelBo_EA2_2_rot_z_ + 
	 0.16527564915596834604905041032907*Dr_beta2 + 
	 0.16527564915596834604905041032907*Dr_gamma3) - 
	 1.0*(0.0000000000083259892263668554836784628430295*EA3_q001 - 
	 0.46025217599999990181203202155302)*pow(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3,2.0);
	qa[1] = qa[1] + 
	0.082637824577984173024525205164537*EA3_q001*pow(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3,2.0)+ 
	 DEA3_q001*(0.000000000016651978452733710967356925686059*DelBo_EA2_3_rot_z_ - 
	 0.000000000016651978452733710967356925686059*DelBo_EA2_2_rot_z_ + 
	 0.000000000016651978452733710967356925686059*Dr_beta2 + 
	 0.000000000016651978452733710967356925686059*Dr_gamma3);
	qa[3] = qa[3] - 
	0.00000000015844330795472250592579362832302*EA3_q001*pow(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3,2.0);
	qa[4] = qa[4] + 
	0.000000055944900149750829493980081464494*DEA3_q001*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3);
	qa[5] = qa[5] + 
	0.0000000000069423446906757523202870395849084*DEA3_q001*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3);
	qa[6] = qa[6] + 
	(0.012145419883758388698846708564361*EA3_q001 - 
	 0.0000000000034711723453378761601435197924542)*pow(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3,2.0);

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];
	MJ[0][3] = Mq[0][0]*Jg[0][3]+Mq[0][5]*Jg[5][3];
	MJ[0][4] = Mq[0][5]*Jg[5][4];
	MJ[0][5] = Mq[0][0]*Jg[0][5]+Mq[0][5]*Jg[5][5];
	MJ[1][1] = Mq[1][1]*Jg[1][1];
	MJ[1][3] = Mq[1][1]*Jg[1][3]+Mq[1][5]*Jg[5][3];
	MJ[1][4] = Mq[1][5]*Jg[5][4];
	MJ[1][5] = Mq[1][1]*Jg[1][5]+Mq[1][5]*Jg[5][5];
	MJ[4][3] = Mq[4][5]*Jg[5][3];
	MJ[4][4] = Mq[4][5]*Jg[5][4];
	MJ[4][5] = Mq[4][5]*Jg[5][5];
	MJ[5][1] = Mq[5][0]*Jg[0][1]+Mq[5][1]*Jg[1][1];
	MJ[5][3] = Mq[5][0]*Jg[0][3]+Mq[5][1]*Jg[1][3]+Mq[5][5]*Jg[5][3];
	MJ[5][4] = Mq[5][5]*Jg[5][4];
	MJ[5][5] = Mq[5][0]*Jg[0][5]+Mq[5][1]*Jg[1][5]+Mq[5][5]*Jg[5][5];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1]+Jg[1][1]*MJ[1][1];
	M[1][3] = M[1][3]+Jg[0][1]*MJ[0][3]+Jg[1][1]*MJ[1][3];
	M[1][4] = M[1][4]+Jg[0][1]*MJ[0][4]+Jg[1][1]*MJ[1][4];
	M[1][5] = M[1][5]+Jg[0][1]*MJ[0][5]+Jg[1][1]*MJ[1][5];
	M[3][1] = M[3][1]+Jg[0][3]*MJ[0][1]+Jg[1][3]*MJ[1][1]+Jg[5][3]*MJ[5][1];
	M[3][3] = M[3][3]+Jg[0][3]*MJ[0][3]+Jg[1][3]*MJ[1][3]+Jg[5][3]*MJ[5][3];
	M[3][4] = M[3][4]+Jg[0][3]*MJ[0][4]+Jg[1][3]*MJ[1][4]+Jg[5][3]*MJ[5][4];
	M[3][5] = M[3][5]+Jg[0][3]*MJ[0][5]+Jg[1][3]*MJ[1][5]+Jg[5][3]*MJ[5][5];
	M[4][1] = M[4][1]+Jg[5][4]*MJ[5][1];
	M[4][3] = M[4][3]+Jg[5][4]*MJ[5][3];
	M[4][4] = M[4][4]+Jg[5][4]*MJ[5][4];
	M[4][5] = M[4][5]+Jg[5][4]*MJ[5][5];
	M[5][1] = M[5][1]+Jg[0][5]*MJ[0][1]+Jg[1][5]*MJ[1][1]+Jg[5][5]*MJ[5][1];
	M[5][3] = M[5][3]+Jg[0][5]*MJ[0][3]+Jg[1][5]*MJ[1][3]+Jg[5][5]*MJ[5][3];
	M[5][4] = M[5][4]+Jg[0][5]*MJ[0][4]+Jg[1][5]*MJ[1][4]+Jg[5][5]*MJ[5][4];
	M[5][5] = M[5][5]+Jg[0][5]*MJ[0][5]+Jg[1][5]*MJ[1][5]+Jg[5][5]*MJ[5][5];
	M[1][6] = M[1][6]+Jg[0][1]*Mq_coup[0][0]+Jg[1][1]*Mq_coup[1][0];
	M[3][6] = M[3][6]+Jg[0][3]*Mq_coup[0][0]+Jg[1][3]*Mq_coup[1][0]+Jg[5][3]*Mq_coup[5][0];
	M[4][6] = M[4][6]+Jg[5][4]*Mq_coup[5][0];
	M[5][6] = M[5][6]+Jg[0][5]*Mq_coup[0][0]+Jg[1][5]*Mq_coup[1][0]+Jg[5][5]*Mq_coup[5][0];
	M[6][1] = M[6][1]+Mq_coup[0][0]*Jg[0][1]+Mq_coup[1][0]*Jg[1][1];
	M[6][3] = M[6][3]+Mq_coup[0][0]*Jg[0][3]+Mq_coup[1][0]*Jg[1][3]+Mq_coup[5][0]*Jg[5][3];
	M[6][4] = M[6][4]+Mq_coup[5][0]*Jg[5][4];
	M[6][5] = M[6][5]+Mq_coup[0][0]*Jg[0][5]+Mq_coup[1][0]*Jg[1][5]+Mq_coup[5][0]*Jg[5][5];

	/* Force vector calculations */
	qa[0] = qa[0]+Mq[0][0]*accelq[0];
	qa[1] = qa[1]+Mq[1][1]*accelq[1];
	qa[5] = qa[5]+Mq[5][0]*accelq[0]+Mq[5][1]*accelq[1];
	qa[6] = qa[6]+Mq_coup[0][0]*accelq[0]+Mq_coup[1][0]*accelq[1];


	f[1] = f[1]+Jg[0][1]*qa[0]+Jg[1][1]*qa[1];
	f[3] = f[3]+Jg[0][3]*qa[0]+Jg[1][3]*qa[1]+Jg[5][3]*qa[5];
	f[4] = f[4]+Jg[5][4]*qa[5];
	f[5] = f[5]+Jg[0][5]*qa[0]+Jg[1][5]*qa[1]+Jg[5][5]*qa[5];
	for(i_=0; i_<1; i_++)
		f[i_+6] += qa[i_+6];


	/* Body EE */

	/* Jacobian matrix */
	Jg[0][1] = - 
	 cos(elBo_EA3_2_rot_z_)*(1.0*sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 
	 cos(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) - 
	 1.0*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + 
	 sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))));
	Jg[1][1] = 1.0*sin(elBo_EA3_2_rot_z_)*(1.0*sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 
	 cos(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) - 
	 1.0*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + 
	 sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))));
	Jg[2][1] = sin(elBo_EA3_2_rot_x_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + 
	 sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))));
	Jg[0][3] = cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)) - 
	 1.0*cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*sin(r_gamma3)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_))) + 
	 cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) + 
	 sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_z_)*pow(sin(elBo_EA3_2_rot_x_),2.0);
	Jg[1][3] = cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)) + 
	 sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*sin(r_gamma3)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_))) + 
	 cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) + 
	 sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 elBo_EA3_2_y_*pow(sin(elBo_EA3_2_rot_x_),2.0)*sin(elBo_EA3_2_rot_z_);
	Jg[2][3] = - 
	 1.0*sin(elBo_EA3_2_rot_x_)*(cos(r_gamma3)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) + 
	 sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) - 
	 1.0*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)) - 
	 1.0*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_));
	Jg[3][3] = sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
	Jg[4][3] = cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_);
	Jg[5][3] = cos(elBo_EA3_2_rot_x_);
	Jg[0][4] = cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_z_)*pow(sin(elBo_EA3_2_rot_x_),2.0);
	Jg[1][4] = cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)) + 
	 elBo_EA3_2_y_*pow(sin(elBo_EA3_2_rot_x_),2.0)*sin(elBo_EA3_2_rot_z_);
	Jg[2][4] = - 
	 1.0*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)) - 
	 1.0*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_));
	Jg[3][4] = sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
	Jg[4][4] = cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_);
	Jg[5][4] = cos(elBo_EA3_2_rot_x_);
	Jg[0][5] = cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(0.05177806232203822911497326231256*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000075504122110177805935058228092758*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) + 
	 sin(r_gamma3)*(0.05177806232203822911497326231256*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000075504122110177805935058228092758*sin(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)))) + 
	 0.0035411566804658678453421316589811*cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)) + 
	 cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(0.05177806232203822911497326231256*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000075504122110177805935058228092758*sin(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 
	 1.0*sin(r_gamma3)*(0.05177806232203822911497326231256*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000075504122110177805935058228092758*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) - 
	 0.0035411566804658678453421316589811*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_z_)*pow(sin(elBo_EA3_2_rot_x_),2.0);
	Jg[1][5] = 0.0035411566804658678453421316589811*cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)) - 
	 1.0*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(0.05177806232203822911497326231256*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000075504122110177805935058228092758*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) + 
	 sin(r_gamma3)*(0.05177806232203822911497326231256*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000075504122110177805935058228092758*sin(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)))) + 
	 cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(0.05177806232203822911497326231256*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000075504122110177805935058228092758*sin(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 
	 1.0*sin(r_gamma3)*(0.05177806232203822911497326231256*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000075504122110177805935058228092758*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 0.0035411566804658678453421316589811*elBo_EA3_2_y_*pow(sin(elBo_EA3_2_rot_x_),2.0)*sin(elBo_EA3_2_rot_z_);
	Jg[2][5] = - 
	 1.0*sin(elBo_EA3_2_rot_x_)*(cos(r_gamma3)*(0.05177806232203822911497326231256*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000075504122110177805935058228092758*sin(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 
	 1.0*sin(r_gamma3)*(0.05177806232203822911497326231256*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000075504122110177805935058228092758*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.12720812859558897067735472319328*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.12720812859558897067735472319328*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.12720812859558897067735472319328*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) - 
	 0.0035411566804658678453421316589811*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)) - 
	 0.0035411566804658678453421316589811*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_));
	Jg[3][5] = 0.0035411566804658678453421316589811*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
	Jg[4][5] = 0.0035411566804658678453421316589811*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_);
	Jg[5][5] = 0.0035411566804658678453421316589811*cos(elBo_EA3_2_rot_x_);
	Jg[0][6] = 0.11086564964710307612527628862154*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_) - 
	 0.0000000000079072648755528962870184095894835*cos(elBo_EA3_2_rot_z_);
	Jg[1][6] = 0.0000000000079072648755528962870184095894835*sin(elBo_EA3_2_rot_z_) + 
	 0.11086564964710307612527628862154*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_);
	Jg[2][6] = - 
	0.11086564964710307612527628862154*sin(elBo_EA3_2_rot_x_);
	Jg[3][6] = 0.000002211520596332927660824640286874*cos(elBo_EA3_2_rot_z_) + 
	 0.41814120987041747401491420532693*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
	Jg[4][6] = 0.41814120987041747401491420532693*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_) - 
	 0.000002211520596332927660824640286874*sin(elBo_EA3_2_rot_z_);
	Jg[5][6] = 0.41814120987041747401491420532693*cos(elBo_EA3_2_rot_x_);

	/* Mass matrix */
	Mq[0][0] = m_ee;
	Mq[1][1] = m_ee;
	Mq[2][2] = m_ee;
	Mq[5][5] = I_ee;

	/* Vector of local accelerations */
	accelq[0] = cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) + 
	 sin(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_))))) + 
	 cos(elBo_EA3_2_rot_x_)*(2.0*DelBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_) - 
	 2.0*DelBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) + 
	 cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 elBo_EA3_2_y_*pow(sin(elBo_EA3_2_rot_x_),2.0)*sin(elBo_EA3_2_rot_z_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) - 
	 1.0*sin(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_))))) + 
	 cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(2.0*DelBo_EA3_2_y_*sin(elBo_EA3_2_rot_x_) + 
	 cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3);
	accelq[1] = cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) - 
	 1.0*sin(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_))))) - 
	 1.0*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) + 
	 sin(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_))))) - 
	 1.0*cos(elBo_EA3_2_rot_x_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3)*(2.0*DelBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 2.0*DelBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_) + 
	 cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_z_)*pow(sin(elBo_EA3_2_rot_x_),2.0)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3)) - 
	 1.0*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(2.0*DelBo_EA3_2_y_*sin(elBo_EA3_2_rot_x_) + 
	 cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3);
	accelq[2] = cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3)*(2.0*DelBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 2.0*DelBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_) + 
	 cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) - 
	 1.0*elBo_EA3_2_y_*cos(elBo_EA3_2_rot_z_)*pow(sin(elBo_EA3_2_rot_x_),2.0)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3)) - 
	 1.0*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(2.0*DelBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_) - 
	 2.0*DelBo_EA3_2_x_*sin(elBo_EA3_2_rot_z_) + 
	 cos(elBo_EA3_2_rot_x_)*(elBo_EA3_2_x_*cos(elBo_EA3_2_rot_z_) + 
	 elBo_EA3_2_y_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 elBo_EA3_2_y_*pow(sin(elBo_EA3_2_rot_x_),2.0)*sin(elBo_EA3_2_rot_z_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) - 
	 1.0*sin(elBo_EA3_2_rot_x_)*(cos(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))) - 
	 1.0*sin(r_gamma3)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)))));
	accelq[3] = - 
	 1.0*cos(elBo_EA3_2_rot_x_)*(DelBo_EA3_2_rot_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*DelBo_EA3_2_rot_z_*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) - 
	 1.0*DelBo_EA3_2_rot_z_*cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3);
	accelq[4] = DelBo_EA3_2_rot_z_*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) - 
	 1.0*cos(elBo_EA3_2_rot_x_)*(DelBo_EA3_2_rot_x_*cos(elBo_EA3_2_rot_z_) + 
	 DelBo_EA3_2_rot_z_*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3);
	accelq[5] = cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(DelBo_EA3_2_rot_x_*cos(elBo_EA3_2_rot_z_) + 
	 DelBo_EA3_2_rot_z_*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(DelBo_EA3_2_rot_x_*sin(elBo_EA3_2_rot_z_) - 
	 1.0*DelBo_EA3_2_rot_z_*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_))*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3);

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[0] = 1.0*F_contact*(cos(elBo_EA3_2_rot_z_)*(1.0*sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 
	 cos(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) + 
	 1.0*cos(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + 
	 sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))))) - 
	 1.0*g*m_ee*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_);
	qa[1] = - 
	 F_contact*(sin(elBo_EA3_2_rot_z_)*(1.0*sin(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 
	 cos(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) - 
	 cos(elBo_EA3_2_rot_x_)*cos(elBo_EA3_2_rot_z_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + 
	 sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))))) - 
	 1.0*g*m_ee*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_);
	qa[2] = - 
	 1.0*F_contact*sin(elBo_EA3_2_rot_x_)*(cos(r_gamma3)*(sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + 
	 cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + 
	 sin(r_gamma3)*(cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)))) - 
	 1.0*g*m_ee*cos(elBo_EA3_2_rot_x_);

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */
	qa[3] = qa[3] - 
	1.0*I_ee*(cos(elBo_EA3_2_rot_x_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 DelBo_EA3_2_rot_z_*cos(elBo_EA3_2_rot_x_))*(cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) - 
	 1.0*DelBo_EA3_2_rot_x_*sin(elBo_EA3_2_rot_z_) + 
	 DelBo_EA3_2_rot_z_*cos(elBo_EA3_2_rot_z_)*sin(elBo_EA3_2_rot_x_));
	qa[4] = qa[4] + 
	I_ee*(cos(elBo_EA3_2_rot_x_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 DelBo_EA3_2_rot_z_*cos(elBo_EA3_2_rot_x_))*(DelBo_EA3_2_rot_x_*cos(elBo_EA3_2_rot_z_) + 
	 sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_)*(DelBo_EA2_3_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2 + 
	 Dr_gamma3) + 
	 DelBo_EA3_2_rot_z_*sin(elBo_EA3_2_rot_x_)*sin(elBo_EA3_2_rot_z_));

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];
	MJ[0][3] = Mq[0][0]*Jg[0][3];
	MJ[0][4] = Mq[0][0]*Jg[0][4];
	MJ[0][5] = Mq[0][0]*Jg[0][5];
	MJ[0][6] = Mq[0][0]*Jg[0][6];
	MJ[1][1] = Mq[1][1]*Jg[1][1];
	MJ[1][3] = Mq[1][1]*Jg[1][3];
	MJ[1][4] = Mq[1][1]*Jg[1][4];
	MJ[1][5] = Mq[1][1]*Jg[1][5];
	MJ[1][6] = Mq[1][1]*Jg[1][6];
	MJ[2][1] = Mq[2][2]*Jg[2][1];
	MJ[2][3] = Mq[2][2]*Jg[2][3];
	MJ[2][4] = Mq[2][2]*Jg[2][4];
	MJ[2][5] = Mq[2][2]*Jg[2][5];
	MJ[2][6] = Mq[2][2]*Jg[2][6];
	MJ[5][3] = Mq[5][5]*Jg[5][3];
	MJ[5][4] = Mq[5][5]*Jg[5][4];
	MJ[5][5] = Mq[5][5]*Jg[5][5];
	MJ[5][6] = Mq[5][5]*Jg[5][6];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1]+Jg[1][1]*MJ[1][1]+Jg[2][1]*MJ[2][1];
	M[1][3] = M[1][3]+Jg[0][1]*MJ[0][3]+Jg[1][1]*MJ[1][3]+Jg[2][1]*MJ[2][3];
	M[1][4] = M[1][4]+Jg[0][1]*MJ[0][4]+Jg[1][1]*MJ[1][4]+Jg[2][1]*MJ[2][4];
	M[1][5] = M[1][5]+Jg[0][1]*MJ[0][5]+Jg[1][1]*MJ[1][5]+Jg[2][1]*MJ[2][5];
	M[1][6] = M[1][6]+Jg[0][1]*MJ[0][6]+Jg[1][1]*MJ[1][6]+Jg[2][1]*MJ[2][6];
	M[3][1] = M[3][1]+Jg[0][3]*MJ[0][1]+Jg[1][3]*MJ[1][1]+Jg[2][3]*MJ[2][1];
	M[3][3] = M[3][3]+Jg[0][3]*MJ[0][3]+Jg[1][3]*MJ[1][3]+Jg[2][3]*MJ[2][3]+Jg[5][3]*MJ[5][3];
	M[3][4] = M[3][4]+Jg[0][3]*MJ[0][4]+Jg[1][3]*MJ[1][4]+Jg[2][3]*MJ[2][4]+Jg[5][3]*MJ[5][4];
	M[3][5] = M[3][5]+Jg[0][3]*MJ[0][5]+Jg[1][3]*MJ[1][5]+Jg[2][3]*MJ[2][5]+Jg[5][3]*MJ[5][5];
	M[3][6] = M[3][6]+Jg[0][3]*MJ[0][6]+Jg[1][3]*MJ[1][6]+Jg[2][3]*MJ[2][6]+Jg[5][3]*MJ[5][6];
	M[4][1] = M[4][1]+Jg[0][4]*MJ[0][1]+Jg[1][4]*MJ[1][1]+Jg[2][4]*MJ[2][1];
	M[4][3] = M[4][3]+Jg[0][4]*MJ[0][3]+Jg[1][4]*MJ[1][3]+Jg[2][4]*MJ[2][3]+Jg[5][4]*MJ[5][3];
	M[4][4] = M[4][4]+Jg[0][4]*MJ[0][4]+Jg[1][4]*MJ[1][4]+Jg[2][4]*MJ[2][4]+Jg[5][4]*MJ[5][4];
	M[4][5] = M[4][5]+Jg[0][4]*MJ[0][5]+Jg[1][4]*MJ[1][5]+Jg[2][4]*MJ[2][5]+Jg[5][4]*MJ[5][5];
	M[4][6] = M[4][6]+Jg[0][4]*MJ[0][6]+Jg[1][4]*MJ[1][6]+Jg[2][4]*MJ[2][6]+Jg[5][4]*MJ[5][6];
	M[5][1] = M[5][1]+Jg[0][5]*MJ[0][1]+Jg[1][5]*MJ[1][1]+Jg[2][5]*MJ[2][1];
	M[5][3] = M[5][3]+Jg[0][5]*MJ[0][3]+Jg[1][5]*MJ[1][3]+Jg[2][5]*MJ[2][3]+Jg[5][5]*MJ[5][3];
	M[5][4] = M[5][4]+Jg[0][5]*MJ[0][4]+Jg[1][5]*MJ[1][4]+Jg[2][5]*MJ[2][4]+Jg[5][5]*MJ[5][4];
	M[5][5] = M[5][5]+Jg[0][5]*MJ[0][5]+Jg[1][5]*MJ[1][5]+Jg[2][5]*MJ[2][5]+Jg[5][5]*MJ[5][5];
	M[5][6] = M[5][6]+Jg[0][5]*MJ[0][6]+Jg[1][5]*MJ[1][6]+Jg[2][5]*MJ[2][6]+Jg[5][5]*MJ[5][6];
	M[6][1] = M[6][1]+Jg[0][6]*MJ[0][1]+Jg[1][6]*MJ[1][1]+Jg[2][6]*MJ[2][1];
	M[6][3] = M[6][3]+Jg[0][6]*MJ[0][3]+Jg[1][6]*MJ[1][3]+Jg[2][6]*MJ[2][3]+Jg[5][6]*MJ[5][3];
	M[6][4] = M[6][4]+Jg[0][6]*MJ[0][4]+Jg[1][6]*MJ[1][4]+Jg[2][6]*MJ[2][4]+Jg[5][6]*MJ[5][4];
	M[6][5] = M[6][5]+Jg[0][6]*MJ[0][5]+Jg[1][6]*MJ[1][5]+Jg[2][6]*MJ[2][5]+Jg[5][6]*MJ[5][5];
	M[6][6] = M[6][6]+Jg[0][6]*MJ[0][6]+Jg[1][6]*MJ[1][6]+Jg[2][6]*MJ[2][6]+Jg[5][6]*MJ[5][6];

	/* Force vector calculations */
	qa[0] = qa[0]+Mq[0][0]*accelq[0];
	qa[1] = qa[1]+Mq[1][1]*accelq[1];
	qa[2] = qa[2]+Mq[2][2]*accelq[2];
	qa[5] = qa[5]+Mq[5][5]*accelq[5];

	f[1] = f[1]+Jg[0][1]*qa[0]+Jg[1][1]*qa[1]+Jg[2][1]*qa[2];
	f[3] = f[3]+Jg[0][3]*qa[0]+Jg[1][3]*qa[1]+Jg[2][3]*qa[2]+Jg[3][3]*qa[3]+Jg[4][3]*qa[4]+Jg[5][3]*qa[5];
	f[4] = f[4]+Jg[0][4]*qa[0]+Jg[1][4]*qa[1]+Jg[2][4]*qa[2]+Jg[3][4]*qa[3]+Jg[4][4]*qa[4]+Jg[5][4]*qa[5];
	f[5] = f[5]+Jg[0][5]*qa[0]+Jg[1][5]*qa[1]+Jg[2][5]*qa[2]+Jg[3][5]*qa[3]+Jg[4][5]*qa[4]+Jg[5][5]*qa[5];
	f[6] = f[6]+Jg[0][6]*qa[0]+Jg[1][6]*qa[1]+Jg[2][6]*qa[2]+Jg[3][6]*qa[3]+Jg[4][6]*qa[4]+Jg[5][6]*qa[5];

}

/* Auxiliary dynamics */
void SCc3A2D_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr){


}


/* Equations of motion */
void SCc3A2D_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;

	/* Allocate memory */
	int i_ = 0;
	double **M     = callocMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF);
	double *temp   = calloc(SCC3A2D_NUM_INDEPENDENT_GC,sizeof(double));

	double **Jr_v  = NULL;
	double **Jr_a  = NULL;
	double **Jl_a  = NULL;
	double *theta_ = NULL;
	double *gamma_ = NULL;

	clearVector((SCC3A2D_SYS_DOF),&(dx[SCC3A2D_SYS_DOF]));

	SCc3A2D_system_dynamics(t, x_, u_, &(dx[SCC3A2D_SYS_DOF]), M, dataPtr);

	memcpy(dx, &(x_[SCC3A2D_SYS_DOF]), SCC3A2D_SYS_DOF*sizeof(double));

	/* Jacobian, velocities and accelerations due to the constraints */
	SCc3A2D_constraintEquations(t, x_, u_, dataPtr);

	Jr_v  = data->con->Jr_v;
	Jr_a  = data->con->Jr_a;
	Jl_a  = data->con->Jl_a;
	theta_ = data->con->theta_;
	gamma_ = data->con->gamma_;

	/* Projection of the velocities */
	for (i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++)
		dx[i_] = dx[i_] - theta_[i_];
	MatrixVectorMultiplication( 'T', SCC3A2D_NUM_INDEPENDENT_GC, SCC3A2D_SYS_DOF, 1.0, Jr_v, dx, 0.0, temp);
	for (i_ = 0; i_ < SCC3A2D_SYS_DOF; i_++)
		dx[i_] = theta_[i_];
	MatrixVectorMultiplication( 'N', SCC3A2D_SYS_DOF, SCC3A2D_NUM_INDEPENDENT_GC, 1.0, Jr_v, temp, 1.0, dx);

	/* Projection of the accelerations */
	projectSystemDynamics( 1, SCC3A2D_SYS_DOF, SCC3A2D_NUM_INDEPENDENT_GV, Jr_a, Jl_a, M, &(dx[SCC3A2D_SYS_DOF]), gamma_);

	/* Free allocated memory */
	free(temp);
	freeMatrix(SCC3A2D_SYS_DOF,SCC3A2D_SYS_DOF,M);

}
