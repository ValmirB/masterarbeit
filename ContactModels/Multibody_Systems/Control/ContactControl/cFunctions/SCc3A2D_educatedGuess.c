#include <math.h>
#include "SCc3A2D_paraStruct.h"
#include "SCc3A2D_pi_code.h"
#include "SCc3A2D_pd_matlab.h"
#include "SCc3A2D_userDefined.h"
#include "neweul.h"

void SCc3A2D_educatedGuess(double t, double *y_, void *dataPtr);

void SCc3A2D_educatedGuess(double t, double *y_, void *dataPtr){

	struct SCc3A2D_paraStruct *data  = (struct SCc3A2D_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
