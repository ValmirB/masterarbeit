function EE_x = f_EE_x(t, x_, varargin)
% f_EE_x - definition of time-depending user-defined variable EE_x

global sys;


% constant user-defined variables
EE_x_d = sys.parameters.data.EE_x_d;
EE_x = zeros(1,1);

EE_x(1) = EE_x_d;

% END OF FILE

