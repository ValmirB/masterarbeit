%% Start

% Clean up
restoredefaultpath
clear all; close all; clc;

% always change to the folder where this file is located at
path_2_file = mfilename('fullpath');
path_2_file = path_2_file(1:end-length(mfilename));
cd(path_2_file);
addpath(path_2_file);

blue = [0, 0.39, 0.87];
red = [0.86, 0.13, 0.3];
green = [0, 0.55, 0];
violet = [0.4 0 0.4];
orange = [1 0.4 0];

for i = 1:5
    if i == 1
        cd T_Kinematic_ex_h/Scripts/Input_Data_/Flexible/Line_const
    elseif i == 2
        cd T_Kinematic_ex_h/Scripts/Input_Data_/Flexible/Line_turn
    elseif i ==3
        cd T_Kinematic_ex_h/Scripts/Input_Data_/Flexible/Circle
    elseif i == 4
        cd T_Kinematic_ex_v/Scripts/Input_Data_/Flexible/Line_1
    elseif i == 5
        cd T_Kinematic_ex_v/Scripts/Input_Data_/Flexible/Line_2
    end
    
    load('Input_Data.mat');

    t = Input_Data.time;
    U = Input_Data.signals.values;
    U = squeeze(U);
    if length(U(:,1)) < length(U(1,:))
        U = U';
    end
    
    n = 250;
    l = length(U);
    x = 1:l;
    xq = linspace(1, l, n);
    U = interp1(x,U,xq);
    t = interp1(x,t,xq);
    
    eval(['U_' num2str(i) '= U']);
    eval(['t_' num2str(i) '= t']);
    
    cd ../../../../..

end

t = linspace(0, 100, n);


%% Forces of Linear Drive 1

figure(1)
subplot(4,2,1)
plot(t, U_1(:,4), 'Color', blue, 'LineWidth', 1)
hold on
plot(t, U_2(:,4), 'Color', red, 'LineWidth', 1)
plot(t, U_3(:,4), 'Color', green, 'LineWidth', 1)
plot(t, U_4(:,4), 'Color', violet, 'LineWidth', 1)
plot(t, U_5(:,4), 'Color', orange, 'LineWidth', 1)
grid on
ylabel('velocity [m/s]')
ylim([-1.5 1])


subplot(4,2,2)
plot(t, U_1(:,6), 'Color', blue, 'LineWidth', 1)
hold on
plot(t, U_2(:,6), 'Color', red, 'LineWidth', 1)
plot(t, U_3(:,6), 'Color', green, 'LineWidth', 1)
plot(t, U_4(:,6), 'Color', violet, 'LineWidth', 1)
plot(t, U_5(:,6), 'Color', orange, 'LineWidth', 1)
grid on
set(gca,'yaxislocation','right');
ylabel('acceleration [\ms]')
ylim([-25 40])
 

subplot(4,2,3)
plot(t, U_1(:,1), 'Color', blue, 'LineWidth', 1)
hold on
plot(t, U_2(:,1), 'Color', red, 'LineWidth', 1)
plot(t, U_3(:,1), 'Color', green, 'LineWidth', 1)
plot(t, U_4(:,1), 'Color', violet, 'LineWidth', 1)
plot(t, U_5(:,1), 'Color', orange, 'LineWidth', 1)
grid on
ylabel('input force [N]')
ylim([-200 300])

subplot(4,2,4)
plot(t, U_1(:,1).*U_1(:,4), 'Color', blue, 'LineWidth', 1)
hold on
plot(t, U_2(:,1).*U_2(:,4), 'Color', red, 'LineWidth', 1)
plot(t, U_3(:,1).*U_3(:,4), 'Color', green, 'LineWidth', 1)
plot(t, U_4(:,1).*U_4(:,4), 'Color', violet, 'LineWidth', 1)
plot(t, U_5(:,1).*U_5(:,4), 'Color', orange, 'LineWidth', 1)
grid on
set(gca,'yaxislocation','right');
ylabel('power [W]')
ylim([-300 250])


%% Forces of Linear Drive 2

subplot(4,2,5)
plot(t, U_1(:,5), 'Color', blue, 'LineWidth', 1')
hold on
plot(t, U_2(:,5), 'Color', red, 'LineWidth', 1')
plot(t, U_3(:,5), 'Color', green, 'LineWidth', 1')
plot(t, U_4(:,5), 'Color', violet, 'LineWidth', 1')
plot(t, U_5(:,5), 'Color', orange, 'LineWidth', 1')
grid on
ylabel('velocity [m/s]')
ylim([-1.5 1.5])


subplot(4,2,6)
plot(t, U_1(:,7), 'Color', blue, 'LineWidth', 1)
hold on
plot(t, U_2(:,7), 'Color', red, 'LineWidth', 1)
plot(t, U_3(:,7), 'Color', green, 'LineWidth', 1)
plot(t, U_4(:,7), 'Color', violet, 'LineWidth', 1)
plot(t, U_5(:,7), 'Color', orange, 'LineWidth', 1)
grid on
set(gca,'yaxislocation','right');
ylabel('acceleration [\ms]')
 

subplot(4,2,7)
plot(t, U_1(:,2), 'Color', blue, 'LineWidth', 1)
hold on
plot(t, U_2(:,2), 'Color', red, 'LineWidth', 1)
plot(t, U_3(:,2), 'Color', green, 'LineWidth', 1)
plot(t, U_4(:,2), 'Color', violet, 'LineWidth', 1)
plot(t, U_5(:,2), 'Color', orange, 'LineWidth', 1)
grid on
xlabel('time [%]')
ylabel('input force [N]')
ylim([-150 150])

subplot(4,2,8)
plot(t, U_1(:,2).*U_1(:,5), 'Color', blue, 'LineWidth', 1)
hold on
plot(t, U_2(:,2).*U_2(:,5), 'Color', red, 'LineWidth', 1)
plot(t, U_3(:,2).*U_3(:,5), 'Color', green, 'LineWidth', 1)
plot(t, U_4(:,2).*U_4(:,5), 'Color', violet, 'LineWidth', 1)
plot(t, U_5(:,2).*U_5(:,5), 'Color', orange, 'LineWidth', 1)
grid on
xlabel('time [%]')
set(gca,'yaxislocation','right');
ylabel('power [W]')



%% % Speichern f�r Latex
posRMS = get(gcf, 'Position');
set(gcf, 'Position', [posRMS(1) posRMS(2) 500, 800]);                                % set ratio of the figure
path_2_file = mfilename('fullpath');                                                 % Pfad der aktuellen Datei   
strEnd_ = strfind(path_2_file, [filesep, 'Masterthesis', filesep]) + 13 ;            % finde den Ordner
addpath(genpath([path_2_file(1:strEnd_) 'Latex\Documents\Diagrams\Matlab2Latex']));  % f�ge Matlab2Latex zum Pfad hinzu
cd([path_2_file(1:strEnd_) 'Latex\Documents\Diagrams\Development'])                  % wechsel in den Speicherordner
matlab2tikz('Input_Data_Flexible_LR.tikz', 'width', '\figurewidth', 'parseStringsAsMath', true); % Speichern ('floatFormat', '%.8g')
cd(path_2_file(1:end-length(mfilename)))