function exportHDF5File(extrapolatedResult, varargin)

global sys;
global EEexport;

numSamples = numel(extrapolatedResult.x);

if nargin==1
    dateString = strrep(strrep(datestr(now),':','_'),' ','_');
    fileName = (['FF_',num2str(sys.parameters.data.traj_end-sys.parameters.data.traj_start) ,'s_traj_',num2str(numel(sys.parameters.vectors.elastic)),'edof_',dateString,'.h5']);
else
    fileName = varargin{1};
end

%% Ensure correct format
% The velocities, accelerations and control inputs have to start and end with 0

% Da
extrapolatedResult.y(sys.counters.genCoord+1,1)   = 0;
extrapolatedResult.y(sys.counters.genCoord+1,end) = 0;

% D2a
extrapolatedResult.Dy(sys.counters.genCoord+1,1)   = 0;
extrapolatedResult.Dy(sys.counters.genCoord+1,end) = 0;

% Db
extrapolatedResult.y(sys.counters.genCoord+2,1)   = 0;
extrapolatedResult.y(sys.counters.genCoord+2,end) = 0;

% D2b
extrapolatedResult.Dy(sys.counters.genCoord+2,1)   = 0;
extrapolatedResult.Dy(sys.counters.genCoord+2,end) = 0;

% ua
extrapolatedResult.lagrangeMultipliers(3,1)   = 0;
extrapolatedResult.lagrangeMultipliers(3,end) = 0;

% ub
extrapolatedResult.lagrangeMultipliers(4,1)   = 0;
extrapolatedResult.lagrangeMultipliers(4,end) = 0;

if numel(sys.model.sid) == 3
    % Dgamma
    extrapolatedResult.y(sys.counters.genCoord+5,1)   = 0;
    extrapolatedResult.y(sys.counters.genCoord+5,end) = 0;
    
    % D2gamma
    extrapolatedResult.Dy(sys.counters.genCoord+5,1)   = 0;
    extrapolatedResult.Dy(sys.counters.genCoord+5,end) = 0;
    
    % ugamma
    extrapolatedResult.lagrangeMultipliers(5,1)   = 0;
    extrapolatedResult.lagrangeMultipliers(5,end) = 0;
end

%% Create h5 file
try
    % Angular accelerations 
    if numel(sys.model.sid) == 3
        h5create(fileName,'/desired/D2gamma',numSamples);h5write(fileName,'/desired/D2gamma',extrapolatedResult.Dy(sys.counters.genCoord+5,:));
    end
    
    % Angular velocities
    h5create(fileName,'/desired/Dalpha',numSamples);h5write(fileName,'/desired/Dalpha',extrapolatedResult.y(sys.counters.genCoord+3,:));
    h5create(fileName,'/desired/Dbeta',numSamples);h5write(fileName,'/desired/Dbeta',extrapolatedResult.y(sys.counters.genCoord+4,:));
    if numel(sys.model.sid) == 3
        h5create(fileName,'/desired/Dgamma',numSamples);h5write(fileName,'/desired/Dgamma',extrapolatedResult.y(sys.counters.genCoord+5,:));
    end
    
    % Link angles
    h5create(fileName,'/desired/alpha',numSamples);h5write(fileName,'/desired/alpha',extrapolatedResult.y(3,:));
    h5create(fileName,'/desired/beta',numSamples);h5write(fileName,'/desired/beta',extrapolatedResult.y(4,:));
    if numel(sys.model.sid) == 3
        h5create(fileName,'/desired/gamma',numSamples);h5write(fileName,'/desired/gamma',extrapolatedResult.y(5,:));
    end
    
    % Car accelerations
    h5create(fileName,'/desired/D2a',numSamples);h5write(fileName,'/desired/D2a',extrapolatedResult.Dy(sys.counters.genCoord+1,:));
    h5create(fileName,'/desired/D2b',numSamples);h5write(fileName,'/desired/D2b',extrapolatedResult.Dy(sys.counters.genCoord+2,:));
    
    % Car velocities
    h5create(fileName,'/desired/Da',numSamples);h5write(fileName,'/desired/Da',extrapolatedResult.y(sys.counters.genCoord+1,:));
    h5create(fileName,'/desired/Db',numSamples);h5write(fileName,'/desired/Db',extrapolatedResult.y(sys.counters.genCoord+2,:));
    
    % Car positions
    h5create(fileName,'/desired/a',numSamples);h5write(fileName,'/desired/a',extrapolatedResult.y(1,:));
    h5create(fileName,'/desired/b',numSamples);h5write(fileName,'/desired/b',extrapolatedResult.y(2,:));
    
    % Time
    h5create(fileName,'/desired/time',numSamples);h5write(fileName,'/desired/time',extrapolatedResult.x);
    
    % Curvature
    if isfield(extrapolatedResult.outputs,'Curvature')
        h5create(fileName,'/desired/dms1',numSamples);h5write(fileName,'/desired/dms1',extrapolatedResult.outputs.Curvature(1,:));
        h5create(fileName,'/desired/dms2',numSamples);h5write(fileName,'/desired/dms2',extrapolatedResult.outputs.Curvature(2,:));
        h5create(fileName,'/desired/dms3',numSamples);h5write(fileName,'/desired/dms3',extrapolatedResult.outputs.Curvature(3,:));
    end
    if isfield(extrapolatedResult.outputs,'DCurvature')
        h5create(fileName,'/desired/Ddms1',numSamples);h5write(fileName,'/desired/Ddms1',extrapolatedResult.outputs.DCurvature(1,:));
        h5create(fileName,'/desired/Ddms2',numSamples);h5write(fileName,'/desired/Ddms2',extrapolatedResult.outputs.DCurvature(2,:));
        h5create(fileName,'/desired/Ddms3',numSamples);h5write(fileName,'/desired/Ddms3',extrapolatedResult.outputs.DCurvature(3,:));
    end
    
    % Control currents
    h5create(fileName,'/desired/ua',numSamples);h5write(fileName,'/desired/ua',extrapolatedResult.lagrangeMultipliers(3,:));
    h5create(fileName,'/desired/ub',numSamples);h5write(fileName,'/desired/ub',extrapolatedResult.lagrangeMultipliers(4,:));
    if numel(sys.model.sid) == 3
        h5create(fileName,'/desired/ugamma',numSamples);h5write(fileName,'/desired/ugamma',extrapolatedResult.lagrangeMultipliers(5,:));
    end

    % End-effector positions
    if isfield(EEexport,'output') && strcmp(EEexport.output,'NLGEOM') 
        h5create(fileName,'/desired/EEx',numSamples);h5write(fileName,'/desired/EEx',extrapolatedResult.outputs.NLGEOMOutput(1,:));
        h5create(fileName,'/desired/EEy',numSamples);h5write(fileName,'/desired/EEy',extrapolatedResult.outputs.NLGEOMOutput(2,:));
    elseif isfield(EEexport,'output') && strcmp(EEexport.output,'rlcNLG')
        h5create(fileName,'/desired/EEx',numSamples);h5write(fileName,'/desired/EEx',extrapolatedResult.outputs.RlcNLGOutput(1,:));
        h5create(fileName,'/desired/EEy',numSamples);h5write(fileName,'/desired/EEy',extrapolatedResult.outputs.RlcNLGOutput(2,:));
    elseif isfield(extrapolatedResult.outputs,'desiredOut') % standard case
        h5create(fileName,'/desired/EEx',numSamples);h5write(fileName,'/desired/EEx',extrapolatedResult.outputs.desiredOut(1,:));
        h5create(fileName,'/desired/EEy',numSamples);h5write(fileName,'/desired/EEy',extrapolatedResult.outputs.desiredOut(2,:));
        h5create(fileName,'/desired/DEEx',numSamples);h5write(fileName,'/desired/DEEx',extrapolatedResult.outputs.desiredOut(3,:));
        h5create(fileName,'/desired/DEEy',numSamples);h5write(fileName,'/desired/DEEy',extrapolatedResult.outputs.desiredOut(4,:));
    end  
    currentFolder_ = pwd;
    if exist([currentFolder_,'\feedforwardTrajectories\'],'dir') ~= 7
        mkdir([currentFolder_,'\feedforwardTrajectories\'])
    end
    movefile(fileName,[currentFolder_,'\feedforwardTrajectories\', fileName]);
       
    display(['Saving result to ', fileName,'!']);
    
catch myError
    
    delete(fileName);
    rethrow(myError);
    
end
    
end