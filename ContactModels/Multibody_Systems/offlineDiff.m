function dy = offlineDiff(t,y)

samples = numel(t);

y = y(:)';

dy = zeros(size(y,1),samples);

for i=1:samples
    
    if (i==1)
    
        dy(:,i) = (y(:,i+1) - y(:,i))/(2*(t(i+1)-t(i)));
        
    elseif (i==samples(end))
        
        dy(:,i) = (y(:,i-1) - y(:,i))/(2*(t(i)-t(i-1)));
        
    else
        
        dy(:,i) = (y(:,i+1) - y(:,i-1))/((t(i+1)-t(i-1)));
    
    end
    
end