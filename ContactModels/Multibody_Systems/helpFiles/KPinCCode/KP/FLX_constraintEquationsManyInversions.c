#include "FLX_userDefined.h"
#include <stdio.h>
#include <math.h>
#include "FLX_pi_code.h"
#include <stdlib.h>
#include "neweul.h"
#include "FLX_paraStruct.h"
#include <string.h>
#include "FLX_Flexor.h"
#include "FLX_constraintEquations.h"
#include "FLX_constraintDerivatives.h"

void FLX_constraintEquations(double t, double *x_, double *u_, void *dataPtr){
/* FLX_CONSTRAINT_EQUATIONS - Assembly of the Jacobian matrix,         */
/* the velocity and accelerations due to the constraints.            */
/* Usage of the qr decomposition to remove the Lagrange multipliers. */

	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;

	int i_ = 0;
	int j_ = 0;
	int k_ = 0;

	double **C_     = data->con->C_;
	double *Dc_     = data->con->Dc_;
	double *D2c_    = data->con->D2c_;
	double **G_     = data->con->G_;

	double **Jr_v  = data->con->Jr_v;
	double **Jr_a  = data->con->Jr_a;
	double **Jl_a  = data->con->Jl_a;
	double *theta_ = data->con->theta_;
	double *gamma_ = data->con->gamma_;
	
	//double **C_depInv  = callocMatrix(4,4);

// 	double **Q_c_  = callocMatrix(FLX_SYS_DOF,FLX_SYS_DOF);
// 	double **R_c_  = callocMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS));
// 	double **Q_g_  = callocMatrix(FLX_SYS_DOF,FLX_SYS_DOF);
// 	double **R_g_  = callocMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS));
// 	double **Q_acc = callocMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS));
// 	double **Q_vel = callocMatrix(FLX_SYS_DOF,FLX_NUM_POSCONSTRAINTS);
// 	double **R_acc = callocMatrix((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS));
// 	double **R_vel = callocMatrix((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),FLX_NUM_POSCONSTRAINTS);
	double *temp_  = calloc((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),sizeof(double));
	double **tempC_dep  = callocMatrix((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS));
	unsigned short *z = calloc((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),sizeof(unsigned short));

    // jacobianPartition Adaption Start*****************************

	/* constant user-defined variables */ 

	double K_L = data->K_L;


	/* Automatically introduced abbreviations */

	double elBo_EA1_2_x_ = FLX_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = FLX_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FLX_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FLX_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FLX_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];

	/* Help vars */
	double **GT_ind      = callocMatrix(4,1);
	double **GT_dep      = callocMatrix(4,4);
	double **C_dep       = callocMatrix(4,4);
	double **C_ind       = callocMatrix(4,1);
	
	FLX_constraint_derivatives(t, x_, u_, dataPtr);

	/* Independent constraint matrix C (sign changed) */
	C_ind[0][0] = -C_[0][4];
	C_ind[1][0] = -C_[1][4];
	C_ind[2][0] = -C_[2][4];
	C_ind[3][0] = -C_[3][4];

	/* Dependent constraint matrix C */
	C_dep[0][0] = C_[0][0];
	C_dep[0][1] = C_[0][1];
	C_dep[0][2] = C_[0][2];
	C_dep[0][3] = C_[0][3];
	C_dep[1][0] = C_[1][0];
	C_dep[1][1] = C_[1][1];
	C_dep[1][2] = C_[1][2];
	C_dep[1][3] = C_[1][3];
	C_dep[2][0] = C_[2][0];
	C_dep[2][1] = C_[2][1];
	C_dep[2][2] = C_[2][2];
	C_dep[2][3] = C_[2][3];
	C_dep[3][0] = C_[3][0];
	C_dep[3][1] = C_[3][1];
	C_dep[3][2] = C_[3][2];
	C_dep[3][3] = C_[3][3];

	/* Transposed independent input matrix G (sign changed)*/
	GT_ind[0][0] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*sin(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 0.11469314458245681576098462528535*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.11469314458245681576098462528535*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	GT_ind[1][0] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.11469314458245681576098462528535*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*sin(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 0.11469314458245681576098462528535*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_));

	/* Transposed dependent input matrix G */
	GT_dep[1][0] = - 1.0*pow(cos(r_alpha1),2.0)- 1.0*pow(sin(r_alpha1),2.0);
	GT_dep[2][0] = K_L;
	GT_dep[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_dep[1][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_dep[3][1] = K_L;
	GT_dep[0][2] = elBo_EA1_2_x_*sin(r_alpha1);
	GT_dep[1][2] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	GT_dep[0][3] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	GT_dep[1][3] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));

	/* Fill Identity matrix */
	// for ( i_ = 0 ; i_ < 1 ; i_++ ){
		// C_depInv[i_][i_] = 1.0;
	// }
	
	for ( i_ = 0 ; i_ < 4 ; i_++ ){
		for ( j_ = 0 ; j_ < 4 ; j_++ ){
			tempC_dep[j_][i_]  =  C_dep[j_][i_];
		}
	}	
	InversionByLU(4, 1, z, tempC_dep, C_ind);
	
	/* Calculate Identity = C_dep^-1 */
	//InversionByLU(4, 4, z, tempC_dep, C_depInv);
	/* C_ind = -C_dep^-1*C_ind wobei C_ind schon negativ ist und mCxmC wohl umbenennen */
	//MatrixMultiplication('N', 'N', (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), FLX_SYS_DOF, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), 1.0, C_depInv, C_ind, 0.0, C_ind);

	InversionByLU(4, 1, z, GT_dep, GT_ind);

	for ( i_ = 0 ; i_ < 1 ; i_++ ){
		Jr_a[data->independentIndices[i_]][i_]  =  1.0;
		Jl_a[i_][data->independentIndices[i_]] = 1.0;
		for ( j_ = 0 ; j_ < 4 ; j_++ ){
			Jr_a[data->dependentIndices[j_]][i_]  =  C_ind[j_][i_];
			Jl_a[i_][data->dependentIndices[j_]] = GT_ind[j_][i_];
		}
	}
    
    
    /* Assume that Jr_v = Jr_a so no non-holonomic constraints */
    for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
		for(j_ = 0; j_ < (FLX_SYS_DOF-FLX_NUM_POSCONSTRAINTS-FLX_NUM_VELCONSTRAINTS); j_++){
			Jr_v[i_][j_] = Jr_a[i_][j_];
		}
	}
    


    //******************************* jacobianPartition Adaption End
    
    
    
    
// 	FLX_constraint_derivatives(t, x_, u_, data);

// 	for(i_ = 0; i_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); i_++){
// 		for(j_ = 0; j_ < FLX_SYS_DOF; j_++){
// 			R_c_[j_][i_] = C_[i_][j_];
// 		}
// 	}

// 	/* Perform a qr decomposition */
// 	householderQR(FLX_SYS_DOF, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), R_c_, Q_c_);

// 	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
// 		for(j_ = 0; j_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); j_++){
// 			R_g_[i_][j_] = G_[i_][j_];
// 		}
// 	}

// 	householderQR(FLX_SYS_DOF, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), R_g_, Q_g_);

// 	/* Get subspace */
// 	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
// 		for(j_ = 0; j_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); j_++){
// 			Q_acc[i_][j_] = Q_c_[i_][j_];
// 		}
// 	}

// 	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
// 		for(j_ = 0; j_ < FLX_NUM_POSCONSTRAINTS; j_++){
// 			Q_vel[i_][j_] = Q_c_[i_][j_];
// 		}
// 	}

// 	/* Jacobian mapping independent coordinates to all genCoords: */
// 	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
// 		for(j_ = (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); j_ < FLX_SYS_DOF; j_++){
// 			Jr_a[i_][j_-(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS)] = Q_c_[i_][j_];
// 		}
// 	}

// 	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
// 		for(j_ = FLX_NUM_POSCONSTRAINTS; j_ < FLX_SYS_DOF; j_++){
// 			Jr_v[i_][j_-FLX_NUM_POSCONSTRAINTS] = Q_c_[i_][j_];
// 		}
// 	}

// 	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
// 		for(j_ = (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); j_ < FLX_SYS_DOF; j_++){
// 			Jl_a[j_-(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS)][i_] = Q_g_[i_][j_];
// 		}
// 	}

// 	for(i_ = 0; i_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); i_++){
// 		for(j_ = 0; j_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); j_++){
// 			R_acc[j_][i_] = R_c_[i_][j_];
// 		}
// 	}

// 	for(i_ = 0; i_ < FLX_NUM_POSCONSTRAINTS; i_++){
// 		for(j_ = 0; j_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); j_++){
// 			R_vel[j_][i_] = R_c_[i_][j_];
// 		}
// 	}

// 	/* Local velocities */
// 	memcpy(temp_, Dc_, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS)*sizeof(double));
// 	InversionByLU1dim((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), z, R_vel, temp_);
// 	MatrixVectorMultiplication('N', FLX_SYS_DOF, FLX_NUM_POSCONSTRAINTS, -1.0, Q_vel, temp_, 0.0, theta_);
    
    memcpy(temp_, Dc_, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS)*sizeof(double));
    for(i_ = 0; i_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); i_++){
			temp_[i_] = -temp_[i_];
		}
	/* C_dep is already "inverted" */    
    //permuteVector(4, z, temp_);
	//LUForBack_1dim(4, C_dep, temp_);
	
	for ( i_ = 0 ; i_ < 4 ; i_++ ){
		 for ( j_ = 0 ; j_ < 4 ; j_++ ){
			 tempC_dep[j_][i_]  =  C_dep[j_][i_];
		 }
	}	
	InversionByLU1dim((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), z, tempC_dep, temp_);
	
	/* temp_ = -C_dep^-1*temp_ wobei C_ind schon negativ ist und mCxmC wohl umbenennen */
	//MatrixVectorMultiplication( 'N', (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), 1.0, C_depInv, temp_, 0.0, temp_);
	
	
	/* distribute temp values to theta */
	for ( i_ = 0 ; i_ < 1 ; i_++ ){
		theta_[data->independentIndices[i_]]  =  0.0;
	}
	for ( j_ = 0 ; j_ < 4 ; j_++ ){
		theta_[data->dependentIndices[j_]] = temp_[j_];
	}
		
	
    
	//MatrixVectorMultiplication('N', FLX_SYS_DOF, FLX_NUM_POSCONSTRAINTS, -1.0, Q_vel, temp_, 0.0, theta_);

	/* Local accelarations */
// 	memcpy(temp_, D2c_, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS)*sizeof(double));
// 	InversionByLU1dim((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), z, R_acc, temp_);
// 	MatrixVectorMultiplication('N', FLX_SYS_DOF, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), -1.0, Q_acc, temp_, 0.0, gamma_);
    memcpy(temp_, D2c_, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS)*sizeof(double));
    for(i_ = 0; i_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); i_++){
			temp_[i_] = -temp_[i_];
		}
	/* C_dep is already inverted */    
    //permuteVector(4, z, temp_);
	//LUForBack_1dim(4, C_dep, temp_);
	for ( i_ = 0 ; i_ < 4 ; i_++ ){
		for ( j_ = 0 ; j_ < 4 ; j_++ ){
			tempC_dep[j_][i_]  =  C_dep[j_][i_];
		}
	}	
	InversionByLU1dim((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), z, tempC_dep, temp_);
	
	/* distribute temp values to gamma */
	for ( i_ = 0 ; i_ < 1 ; i_++ ){
		gamma_[data->independentIndices[i_]]  =  0.0;
	}
	for ( j_ = 0 ; j_ < 4 ; j_++ ){
		gamma_[data->dependentIndices[j_]] = temp_[j_];
	}
	

	
    freeMatrix(4,1,GT_ind);
	freeMatrix(4,4,GT_dep);
	freeMatrix(4,4,C_dep);
	//freeMatrix(4,4,Identity);
	freeMatrix(4,1,C_ind);
	free(z);
	free(temp_);
// 	freeMatrix(FLX_SYS_DOF,FLX_SYS_DOF,Q_c_);
// 	freeMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),R_c_);
// 	freeMatrix(FLX_SYS_DOF,FLX_SYS_DOF,Q_g_);
// 	freeMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),R_g_);
// 	freeMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),Q_acc);
// 	freeMatrix(FLX_SYS_DOF,FLX_NUM_POSCONSTRAINTS,Q_vel);
// 	freeMatrix((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),R_acc);
// 	freeMatrix((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),FLX_NUM_POSCONSTRAINTS,R_vel);

}

void FLX_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr){

	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;

	double *deltaY       = calloc(4,sizeof(double));
	double *Dy_ind       = calloc(1,sizeof(double));
	double **C_dep       = callocMatrix(4,4);
	double **C_ind       = callocMatrix(4,1);

	double normRes_      = 1.0;
	double myTol_        = 1e-8;

	unsigned short *z    = calloc(4,sizeof(unsigned short));
	int counter_         = 0;
	int numIter_         = 0;
	int maxIter_         = 15;
	int k_ = 0;

	/* y_ contains the educated guess */
	while ( (normRes_ > myTol_) && (numIter_ < maxIter_) ){

		/* get constraintEq */
		FLX_positionConstraints(t, x_, u_, deltaY, C_dep, dataPtr);
		normRes_ = taxicabNorm_vec(4,deltaY);

		/* get deltaY */
		InversionByLU1dim(4, z, C_dep, deltaY);

		normRes_ = maxDouble(taxicabNorm_vec(4,deltaY),normRes_);

		for ( counter_ = 0 ; counter_ < 4 ; counter_++ ){
			x_[data->dependentIndices[counter_]] = x_[data->dependentIndices[counter_]] - deltaY[counter_];
		}

		numIter_ += 1;

	}

	for ( counter_ = 0 ; counter_ < 1 ; counter_++ ){
		Dy_ind[counter_] = x_[data->independentIndices[counter_] + FLX_SYS_DOF];
	}

	for ( counter_ = 0 ; counter_ < 4 ; counter_++ ){
		deltaY[counter_] = 0.0;
	}

	FLX_localVelocityConstraints( t, x_, u_, deltaY, C_ind, dataPtr);

	MatrixVectorMultiplication('N', 4, 1, -1.0,  C_ind, Dy_ind, -1.0, deltaY);

	permuteVector(4, z, deltaY);

	LUForBack_1dim(4, C_dep, deltaY);

	for ( counter_ = 0 ; counter_ < 4 ; counter_++ ){
		x_[data->dependentIndices[counter_] + FLX_SYS_DOF] = deltaY[counter_];
	}

	/* Freeing allocated memory */
	free(deltaY);
	free(Dy_ind);
	free(z);
	freeMatrix(4,4,C_dep);
	freeMatrix(4,1,C_ind);

}

void FLX_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr){

	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;

	int i_ = 0;
	int j_ = 0;

	double **C_     = data->con->C_;
	double *Dc_     = data->con->Dc_;
	double *D2c_    = data->con->D2c_;
	double **G_     = data->con->G_;
	double *lambda_ = data->con->lambda_;

	double **M     = callocMatrix(FLX_SYS_DOF,FLX_SYS_DOF);
	double **MTemp = callocMatrix(FLX_SYS_DOF,FLX_SYS_DOF);
	double **mCxmC = callocMatrix((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS));
	double **GTemp = callocMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS));
	double *f      = calloc(FLX_SYS_DOF,sizeof(double));
	unsigned short *z = calloc((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),sizeof(unsigned short));

	FLX_constraint_derivatives(t, x_, u_, dataPtr);


	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
		for(j_ = 0; j_ < (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS); j_++){
			GTemp[i_][j_] = G_[i_][j_];
		}
	}

	/* Evaluate system dynamics */
	FLX_system_dynamics(t, x_, u_, f, M, dataPtr);

	for(i_ = 0; i_ < FLX_SYS_DOF; i_++){
		for(j_ = 0; j_ < FLX_SYS_DOF; j_++){
			MTemp[i_][j_] = M[i_][j_];
		}
	}

	/* f = M^(-1)*f */
	cholDecomposition(FLX_SYS_DOF, M, f);

	/* lambda_ =  C*M^(-1)*f + D2c_ */
	MatrixVectorMultiplicationMod('N', (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), FLX_SYS_DOF, 1.0, C_, f, 1.0, D2c_, lambda_);

	/* GTemp = M^(-1)*G */
	cholDecomposition2dim(FLX_SYS_DOF, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), MTemp, GTemp);

	/* mCxmC = -C*M^(-1)*G */
	MatrixMultiplication('N', 'N', (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), FLX_SYS_DOF, (FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), -1.0, C_, GTemp, 0.0, mCxmC);

	/* lambda_ = mCxmC^(-1) * lambda_ ; please note that cholesky is not possible here */
	InversionByLU1dim((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS), z, mCxmC, lambda_);

	free(z);
	freeMatrix(FLX_SYS_DOF,FLX_SYS_DOF,M);
	freeMatrix(FLX_SYS_DOF,FLX_SYS_DOF,MTemp);
	freeMatrix((FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),mCxmC);
	freeMatrix(FLX_SYS_DOF,(FLX_NUM_POSCONSTRAINTS+FLX_NUM_VELCONSTRAINTS),GTemp);
	free(f);

}

void FLX_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr){

	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double d_axes = data->d_axes;
	double l2 = data->l2;
	double phi2_1 = data->phi2_1;
	double w2_1 = data->w2_1;
	/* system inputs */
	double EE_x_d = u_[0];
	double EE_y_d = u_[3];


	/* Automatically introduced abbreviations */

	double elBo_EA1_2_x_ = FLX_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = FLX_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FLX_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FLX_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FLX_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double p_a = x_[0];
	double p_b = x_[1];
	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];
	double EA2_q001 = x_[4];



	/* Dependent constraint matrix C */
	C_dep[0][0] = 0.0;
	C_dep[1][0] = - 1.0*pow(cos(r_alpha1),2.0)- 1.0*pow(sin(r_alpha1),2.0);
	C_dep[2][0] = 0.0;
	C_dep[3][0] = 0.0;
	C_dep[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_dep[1][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	C_dep[2][1] = sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_));
	C_dep[3][1] = sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)) - 1.0*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2));
	C_dep[0][2] = elBo_EA1_2_x_*sin(r_alpha1);
	C_dep[1][2] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	C_dep[2][2] = 0.0;
	C_dep[3][2] = 0.0;
	C_dep[0][3] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_dep[1][3] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_dep[2][3] = cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	C_dep[3][3] = sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 1.0*l2 + elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	/* Constraint equations on position level */
	con_eq[0] = d_axes + p_b - 1.0*elBo_EA1_2_x_*cos(r_alpha1) + elBo_EA2_1_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	con_eq[1] = - 1.0*p_a - 1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(elBo_EA2_1_x_ - 1.0*elBo_EA2_2_x_) - 1.0*elBo_EA1_2_x_*sin(r_alpha1);
	con_eq[2] = EE_y_d - 1.0*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + l2*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);
	con_eq[3] = EE_x_d - 1.0*d_axes - 1.0*p_b - 1.0*l2*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) + elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

}

void FLX_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr){

	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double l2 = data->l2;
	double phi2_1 = data->phi2_1;
	double w2_1 = data->w2_1;
	/* system inputs */
	double DEE_x_d = u_[1];
	double DEE_y_d = u_[4];


	/* Automatically introduced abbreviations */

	double elBo_EA2_1_rot_z_ = FLX_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FLX_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FLX_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FLX_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_beta2 = x_[3];
	double EA2_q001 = x_[4];


	/* Independent constraint matrix C */
	C_ind[0][0] = - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*sin(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 0.11469314458245681576098462528535*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.11469314458245681576098462528535*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_ind[1][0] = cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*sin(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 0.11469314458245681576098462528535*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.11469314458245681576098462528535*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	C_ind[2][0] = 0.11469314458245681576098462528535*EA2_q001*phi2_1*w2_1*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2) - 1.0*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(phi2_1*w2_1 - 0.11469314458245681576098462528535*l2 + 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0));
	C_ind[3][0] = - 1.0*sin(elBo_EA2_2_rot_z_ - 1.0*r_beta2)*(phi2_1*w2_1 - 0.11469314458245681576098462528535*l2 + 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 0.11469314458245681576098462528535*EA2_q001*phi2_1*w2_1*cos(elBo_EA2_2_rot_z_ - 1.0*r_beta2);

	/* Local constraint equations on velocity level */
	Dc_[0] = 0.0;
	Dc_[1] = 0.0;
	Dc_[2] = DEE_y_d;
	Dc_[3] = DEE_x_d;

}

void FLX_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr){

	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double K_L = data->K_L;


	/* Automatically introduced abbreviations */

	double elBo_EA1_2_x_ = FLX_f_elBo_EA1_2_x_(x_);
	double elBo_EA2_1_rot_z_ = FLX_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FLX_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FLX_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FLX_f_elBo_EA2_2_x_(x_);

	/* generalized coordinates */

	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];

	/* Help vars */
	double **GT_ind      = callocMatrix(4,1);
	double **GT_dep      = callocMatrix(4,4);
	double **C_dep       = callocMatrix(4,4);
	double **C_ind       = callocMatrix(4,1);
	unsigned short *z    = calloc(4,sizeof(unsigned short));
	int i_               = 0;
	int j_               = 0;

	double **C_    = data->con->C_;

	FLX_constraint_derivatives(t, x_, u_, dataPtr);

	/* Independent constraint matrix C (sign changed) */
	C_ind[0][0] = -C_[0][4];
	C_ind[1][0] = -C_[1][4];
	C_ind[2][0] = -C_[2][4];
	C_ind[3][0] = -C_[3][4];

	/* Dependent constraint matrix C */
	C_dep[0][0] = C_[0][0];
	C_dep[0][1] = C_[0][1];
	C_dep[0][2] = C_[0][2];
	C_dep[0][3] = C_[0][3];
	C_dep[1][0] = C_[1][0];
	C_dep[1][1] = C_[1][1];
	C_dep[1][2] = C_[1][2];
	C_dep[1][3] = C_[1][3];
	C_dep[2][0] = C_[2][0];
	C_dep[2][1] = C_[2][1];
	C_dep[2][2] = C_[2][2];
	C_dep[2][3] = C_[2][3];
	C_dep[3][0] = C_[3][0];
	C_dep[3][1] = C_[3][1];
	C_dep[3][2] = C_[3][2];
	C_dep[3][3] = C_[3][3];

	/* Transposed independent input matrix G (sign changed)*/
	GT_ind[0][0] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*sin(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 0.11469314458245681576098462528535*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.11469314458245681576098462528535*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	GT_ind[1][0] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*cos(elBo_EA2_1_rot_z_) - 1.0*sin(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 0.11469314458245681576098462528535*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(0.000000000000038066427647350752645032930885902*sin(elBo_EA2_1_rot_z_) + cos(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 0.11469314458245681576098462528535*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_));

	/* Transposed dependent input matrix G */
	GT_dep[1][0] = - 1.0*pow(cos(r_alpha1),2.0)- 1.0*pow(sin(r_alpha1),2.0);
	GT_dep[2][0] = K_L;
	GT_dep[0][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) + cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_dep[1][1] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_))) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) + cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_)));
	GT_dep[3][1] = K_L;
	GT_dep[0][2] = elBo_EA1_2_x_*sin(r_alpha1);
	GT_dep[1][2] = -1.0*elBo_EA1_2_x_*cos(r_alpha1);
	GT_dep[0][3] = sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));
	GT_dep[1][3] = - 1.0*cos(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)) - 1.0*sin(elBo_EA2_1_rot_z_ - 1.0*elBo_EA2_2_rot_z_ + r_beta2)*(sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_));

	InversionByLU(4, 1, z, C_dep, C_ind);

	InversionByLU(4, 1, z, GT_dep, GT_ind);

	for ( i_ = 0 ; i_ < 1 ; i_++ ){
		Jr[data->independentIndices[i_]][i_]  =  1.0;
		Jl[data->independentIndices[i_]][i_]  =  1.0;
		for ( j_ = 0 ; j_ < 4 ; j_++ ){
			Jr[data->dependentIndices[j_]][i_]  =  C_ind[j_][i_];
			Jl[data->dependentIndices[j_]][i_]  =  GT_ind[j_][i_];
		}
	}

	freeMatrix(4,1,GT_ind);
	freeMatrix(4,4,GT_dep);
	freeMatrix(4,4,C_dep);
	freeMatrix(4,1,C_ind);
	free(z);

}
