#include "FLX_userDefined.h"
#include <stdio.h>
#include <math.h>
#include "FLX_pi_code.h"
#include <stdlib.h>
#include "neweul.h"
#include <string.h>
#include "FLX_Flexor.h"
#include "FLX_constraintEquations.h"

/* System dynamics */
void FLX_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr){


	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;


	/* constant user-defined variables */ 

	double I_ee = data->I_ee;
	double I_mb = data->I_mb;
	double d_mb = data->d_mb;
	double g = data->g;
	double m_C1 = data->m_C1;
	double m_C2 = data->m_C2;
	double m_ee = data->m_ee;
	double m_mb = data->m_mb;


	/* Automatically introduced abbreviations */

	double DelBo_EA2_1_rot_z_ = FLX_f_DelBo_EA2_1_rot_z_(x_);
	double DelBo_EA2_1_x_ = FLX_f_DelBo_EA2_1_x_(x_);
	double DelBo_EA2_2_rot_z_ = FLX_f_DelBo_EA2_2_rot_z_(x_);
	double DelBo_EA2_2_x_ = FLX_f_DelBo_EA2_2_x_(x_);
	double DelBo_EA2_3_x_ = FLX_f_DelBo_EA2_3_x_(x_);
	double DelBo_EA2_3_y_ = FLX_f_DelBo_EA2_3_y_(x_);
	double elBo_EA2_1_rot_z_ = FLX_f_elBo_EA2_1_rot_z_(x_);
	double elBo_EA2_1_x_ = FLX_f_elBo_EA2_1_x_(x_);
	double elBo_EA2_2_rot_z_ = FLX_f_elBo_EA2_2_rot_z_(x_);
	double elBo_EA2_2_x_ = FLX_f_elBo_EA2_2_x_(x_);
	double elBo_EA2_3_rot_z_ = FLX_f_elBo_EA2_3_rot_z_(x_);
	double elBo_EA2_3_x_ = FLX_f_elBo_EA2_3_x_(x_);
	double elBo_EA2_3_y_ = FLX_f_elBo_EA2_3_y_(x_);

	/* Force elements */

	double f_SDC1_vec[6] = {0.0};
	double f_SDC1 = forces_SDC1(t, x_, u_, &(f_SDC1_vec[0]), data);
	double f_SDC1_x_ = f_SDC1_vec[0];
	double f_SDC1_y_ = f_SDC1_vec[1];
	double f_SDC1_z_ = f_SDC1_vec[2];
	double l_SDC1_x_ = f_SDC1_vec[3];
	double l_SDC1_y_ = f_SDC1_vec[4];
	double l_SDC1_z_ = f_SDC1_vec[5];

	double f_SDC2_vec[6] = {0.0};
	double f_SDC2 = forces_SDC2(t, x_, u_, &(f_SDC2_vec[0]), data);
	double f_SDC2_x_ = f_SDC2_vec[0];
	double f_SDC2_y_ = f_SDC2_vec[1];
	double f_SDC2_z_ = f_SDC2_vec[2];
	double l_SDC2_x_ = f_SDC2_vec[3];
	double l_SDC2_y_ = f_SDC2_vec[4];
	double l_SDC2_z_ = f_SDC2_vec[5];


	/* generalized coordinates */

	double p_a = x_[0];
	double p_b = x_[1];
	double r_alpha1 = x_[2];
	double r_beta2 = x_[3];
	double EA2_q001 = x_[4];
	double Dr_alpha1 = x_[7];
	double Dr_beta2 = x_[8];
	double DEA2_q001 = x_[9];

#if defined FLX_SYS_DOF && FLX_SYS_DOF > 0
	double Jg[6][FLX_SYS_DOF]  = {{0.0}};
	double MJ[7][FLX_SYS_DOF]  = {{0.0}};
	double Mq[6][6]  = {{0.0}};
	double Mq_coup[6][1] = {{0.0}};
	double accelq[7] = {0.0};
	double qa[7]     = {0.0};
	int i_,j_,k_;

	clearVector(FLX_SYS_DOF,f);
#endif


	/* Body C1 */

	/* Jacobian matrix */
	Jg[1][0] = 1.0;

	/* Mass matrix */
	Mq[0][0] = m_C1;
	Mq[1][1] = m_C1;
	Mq[2][2] = m_C1;
	Mq[3][3] = 1.0;
	Mq[4][4] = 1.0;
	Mq[5][5] = 1.0;

	/* Vector of local accelerations */

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[0] = - 
	1.0*f_SDC1_x_;
	qa[1] = - 
	1.0*f_SDC1_y_;
	qa[2] = - 
	 1.0*f_SDC1_z_ - 
	 1.0*g*m_C1;
	qa[3] = f_SDC1_z_*p_a - 
	 1.0*l_SDC1_x_;
	qa[4] = - 
	1.0*l_SDC1_y_;
	qa[5] = - 
	 1.0*l_SDC1_z_ - 
	 1.0*f_SDC1_x_*p_a;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */

	/* Mass matrix calculations */
	MJ[1][0] = Mq[1][1]*Jg[1][0];

	M[0][0] = M[0][0]+Jg[1][0]*MJ[1][0];

	/* Force vector calculations */

	f[0] = f[0]+Jg[1][0]*qa[1];


	/* Body C2 */

	/* Jacobian matrix */
	Jg[0][1] = 1.0;

	/* Mass matrix */
	Mq[0][0] = m_C2;
	Mq[1][1] = m_C2;
	Mq[2][2] = m_C2;
	Mq[3][3] = 1.0;
	Mq[4][4] = 1.0;
	Mq[5][5] = 1.0;

	/* Vector of local accelerations */

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[0] = - 
	1.0*f_SDC2_x_;
	qa[1] = - 
	1.0*f_SDC2_y_;
	qa[2] = - 
	 1.0*f_SDC2_z_ - 
	 1.0*g*m_C2;
	qa[3] = - 
	1.0*l_SDC2_x_;
	qa[4] = - 
	 1.0*l_SDC2_y_ - 
	 1.0*f_SDC2_z_*p_b;
	qa[5] = f_SDC2_y_*p_b - 
	 1.0*l_SDC2_z_;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1];

	/* Force vector calculations */

	f[1] = f[1]+Jg[0][1]*qa[0];


	/* Body EA1 */

	/* Jacobian matrix */
	Jg[0][0] = sin(r_alpha1);
	Jg[1][0] = cos(r_alpha1);
	Jg[5][2] = 1.0;

	/* Mass matrix */
	Mq[0][0] = 2.158;
	Mq[1][1] = 2.158;
	Mq[5][1] = 0.41626;
	Mq[2][2] = 2.158;
	Mq[4][2] = - 
	0.41626;
	Mq[3][3] = 0.0019316386666666661604818688857677;
	Mq[2][4] = - 
	0.41626;
	Mq[4][4] = 0.13202505266666664240915451955516;
	Mq[1][5] = 0.41626;
	Mq[5][5] = 0.13079362466666669129189415343717;


	/* Vector of local accelerations */

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	2.158*g;
	qa[4] = 0.41626*g;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */
	qa[0] = qa[0] + 
	0.41626*pow(Dr_alpha1,2.0);

	/* Mass matrix calculations */
	MJ[0][0] = Mq[0][0]*Jg[0][0];
	MJ[1][0] = Mq[1][1]*Jg[1][0];
	MJ[1][2] = Mq[1][5]*Jg[5][2];
	MJ[5][0] = Mq[5][1]*Jg[1][0];
	MJ[5][2] = Mq[5][5]*Jg[5][2];

	M[0][0] = M[0][0]+Jg[0][0]*MJ[0][0]+Jg[1][0]*MJ[1][0];
	M[0][2] = M[0][2]+Jg[1][0]*MJ[1][2];
	M[2][0] = M[2][0]+Jg[5][2]*MJ[5][0];
	M[2][2] = M[2][2]+Jg[5][2]*MJ[5][2];

	/* Force vector calculations */


	f[0] = f[0]+Jg[0][0]*qa[0];


	/* Body EA2 */

	/* Jacobian matrix */
	Jg[0][1] = sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2);
	Jg[1][1] = cos(r_beta2)*sin(elBo_EA2_2_rot_z_) - 
	 1.0*cos(elBo_EA2_2_rot_z_)*sin(r_beta2);
	Jg[1][3] = - 
	 1.0*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)- 
	 1.0*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0);
	Jg[5][3] = 1.0;
	Jg[1][4] = 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0);
	Jg[5][4] = - 
	0.11469314458245681576098462528535;

	/* Mass matrix */
	Mq[0][0] = 4.6429200000000045989168029336724;
	Mq[5][0] = - 
	0.11045100556867568086794051396282*EA2_q001;
	Mq[1][1] = 4.6429200000000045989168029336724;
	Mq[5][1] = 2.9600122000000004263142727722879 - 
	 0.00000000000039443597255866384795160914272132*EA2_q001;
	Mq[2][2] = 4.6429200000000045989168029336724;
	Mq[3][2] = 0.11045100556867568086794051396282*EA2_q001;
	Mq[4][2] = 0.00000000000039443597255866384795160914272132*EA2_q001 - 
	 2.9600122000000004263142727722879;
	Mq[2][3] = 0.11045100556867568086794051396282*EA2_q001;
	Mq[3][3] = 0.0030378344533333332191937792288172;
	Mq[4][3] = - 
	0.1314406826441340547795277871046*EA2_q001;
	Mq[2][4] = 0.00000000000039443597255866384795160914272132*EA2_q001 - 
	 2.9600122000000004263142727722879;
	Mq[3][4] = - 
	0.1314406826441340547795277871046*EA2_q001;
	Mq[4][4] = 2.6598436873733297680644227511948 - 
	 0.0000000000008228352014592571789423162234799*EA2_q001;
	Mq[0][5] = - 
	0.11045100556867568086794051396282*EA2_q001;
	Mq[1][5] = 2.9600122000000004263142727722879 - 
	 0.00000000000039443597255866384795160914272132*EA2_q001;
	Mq[5][5] = 2.6579737404933352351577013905626 - 
	 0.0000000000008228352014592571789423162234799*EA2_q001;
	Mq_coup[0][0] = -0.00000000000039443597255866384795160914272132;
	Mq_coup[1][0] = 0.11045100556867568086794051396282;
	Mq_coup[5][0] = 0.13093661483784849175471265425585;

	M[4][4] += 9.001186e-03;

	/* Vector of local accelerations */
	accelq[0] = - 
	 1.0*Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) - 
	 1.0*cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_));
	accelq[1] = Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_));

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	4.6429200000000045989168029336724*g;
	qa[3] = - 
	0.11045100556867568086794051396282*EA2_q001*g;
	qa[4] = - 
	0.00000000000000000000000000040389678347315804437080502542479*g*(976576166729679.0*EA2_q001 - 
	 7328635238306410846184538112.0);

	/* Elastic forces h_e */
	qa[6] = qa[6] - 
	 0.000022499999632467841789891516146582*DEA2_q001 - 
	 0.99999998366523734638633413851494*EA2_q001;

	/* Forces due to choice of frame of reference h_omega */
	qa[0] = qa[0] - 
	 1.0*pow(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2,2.0)*(0.00000000000039443597255866384795160914272132*EA2_q001 - 
	 2.9600122000000004263142727722879) - 
	 1.0*DEA2_q001*(0.22090201113735136173588102792564*DelBo_EA2_2_rot_z_ - 
	 0.22090201113735136173588102792564*Dr_beta2);
	qa[1] = qa[1] + 
	0.11045100556867568086794051396282*EA2_q001*pow(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2,2.0)- 
	 1.0*DEA2_q001*(0.00000000000078887194511732769590321828544263*DelBo_EA2_2_rot_z_ - 
	 0.00000000000078887194511732769590321828544263*Dr_beta2);
	qa[5] = qa[5] - 
	0.0000000000008228352014592571789423162234799*DEA2_q001*(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2);
	qa[6] = qa[6] + 
	pow(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2,2.0)*(0.0093940596209376274472457879483045*EA2_q001 - 
	 0.00000000000041141760072962858947115811173995);

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];
	MJ[0][3] = Mq[0][5]*Jg[5][3];
	MJ[0][4] = Mq[0][5]*Jg[5][4];
	MJ[1][1] = Mq[1][1]*Jg[1][1];
	MJ[1][3] = Mq[1][1]*Jg[1][3]+Mq[1][5]*Jg[5][3];
	MJ[1][4] = Mq[1][1]*Jg[1][4]+Mq[1][5]*Jg[5][4];
	MJ[5][1] = Mq[5][0]*Jg[0][1]+Mq[5][1]*Jg[1][1];
	MJ[5][3] = Mq[5][1]*Jg[1][3]+Mq[5][5]*Jg[5][3];
	MJ[5][4] = Mq[5][1]*Jg[1][4]+Mq[5][5]*Jg[5][4];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1]+Jg[1][1]*MJ[1][1];
	M[1][3] = M[1][3]+Jg[0][1]*MJ[0][3]+Jg[1][1]*MJ[1][3];
	M[1][4] = M[1][4]+Jg[0][1]*MJ[0][4]+Jg[1][1]*MJ[1][4];
	M[3][1] = M[3][1]+Jg[1][3]*MJ[1][1]+Jg[5][3]*MJ[5][1];
	M[3][3] = M[3][3]+Jg[1][3]*MJ[1][3]+Jg[5][3]*MJ[5][3];
	M[3][4] = M[3][4]+Jg[1][3]*MJ[1][4]+Jg[5][3]*MJ[5][4];
	M[4][1] = M[4][1]+Jg[1][4]*MJ[1][1]+Jg[5][4]*MJ[5][1];
	M[4][3] = M[4][3]+Jg[1][4]*MJ[1][3]+Jg[5][4]*MJ[5][3];
	M[4][4] = M[4][4]+Jg[1][4]*MJ[1][4]+Jg[5][4]*MJ[5][4];
	M[1][4] = M[1][4]+Jg[0][1]*Mq_coup[0][0]+Jg[1][1]*Mq_coup[1][0];
	M[3][4] = M[3][4]+Jg[1][3]*Mq_coup[1][0]+Jg[5][3]*Mq_coup[5][0];
	M[4][4] = M[4][4]+Jg[1][4]*Mq_coup[1][0]+Jg[5][4]*Mq_coup[5][0];
	M[4][1] = M[4][1]+Mq_coup[0][0]*Jg[0][1]+Mq_coup[1][0]*Jg[1][1];
	M[4][3] = M[4][3]+Mq_coup[1][0]*Jg[1][3]+Mq_coup[5][0]*Jg[5][3];
	M[4][4] = M[4][4]+Mq_coup[1][0]*Jg[1][4]+Mq_coup[5][0]*Jg[5][4];

	/* Force vector calculations */
	qa[0] = qa[0]+Mq[0][0]*accelq[0];
	qa[1] = qa[1]+Mq[1][1]*accelq[1];
	qa[5] = qa[5]+Mq[5][0]*accelq[0]+Mq[5][1]*accelq[1];
	qa[6] = qa[6]+Mq_coup[0][0]*accelq[0]+Mq_coup[1][0]*accelq[1];


	f[1] = f[1]+Jg[0][1]*qa[0]+Jg[1][1]*qa[1];
	f[3] = f[3]+Jg[1][3]*qa[1]+Jg[5][3]*qa[5];
	f[4] = f[4]+Jg[1][4]*qa[1]+Jg[5][4]*qa[5];
	for(i_=0; i_<1; i_++)
		f[i_+4] += qa[i_+6];


	/* Body EE */

	/* Jacobian matrix */
	Jg[0][1] = cos(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_));
	Jg[1][1] = - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_));
	Jg[0][3] = elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*sin(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0));
	Jg[1][3] = elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_);
	Jg[5][3] = 1.0;
	Jg[0][4] = 0.087623632180840738126192945856019*sin(elBo_EA2_3_rot_z_) - 
	 0.00000000000031368516045582045365715977748205*cos(elBo_EA2_3_rot_z_) + 
	 sin(elBo_EA2_3_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 0.11469314458245681576098462528535*elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 0.11469314458245681576098462528535*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_);
	Jg[1][4] = 0.087623632180840738126192945856019*cos(elBo_EA2_3_rot_z_) + 
	 0.00000000000031368516045582045365715977748205*sin(elBo_EA2_3_rot_z_) + 
	 cos(elBo_EA2_3_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 
	 0.11469314458245681576098462528535*elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) - 
	 0.11469314458245681576098462528535*elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_);
	Jg[5][4] = 0.048941537984115948689556319095573;

	/* Mass matrix */
	Mq[0][0] = m_ee;
	Mq[1][1] = m_ee;
	Mq[2][2] = m_ee;
	Mq[5][5] = I_ee;

	/* Vector of local accelerations */
	accelq[0] = (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*((DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 elBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_)) - 
	 2.0*DelBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_)) - 
	 1.0*cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)));
	accelq[1] = (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(2.0*DelBo_EA2_3_x_*cos(elBo_EA2_3_rot_z_) + 
	 2.0*DelBo_EA2_3_y_*sin(elBo_EA2_3_rot_z_) + 
	 (DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2)*(elBo_EA2_3_y_*cos(elBo_EA2_3_rot_z_) - 
	 1.0*elBo_EA2_3_x_*sin(elBo_EA2_3_rot_z_))) + 
	 sin(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_3_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)));

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	1.0*g*m_ee;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];
	MJ[0][3] = Mq[0][0]*Jg[0][3];
	MJ[0][4] = Mq[0][0]*Jg[0][4];
	MJ[1][1] = Mq[1][1]*Jg[1][1];
	MJ[1][3] = Mq[1][1]*Jg[1][3];
	MJ[1][4] = Mq[1][1]*Jg[1][4];
	MJ[5][3] = Mq[5][5]*Jg[5][3];
	MJ[5][4] = Mq[5][5]*Jg[5][4];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1]+Jg[1][1]*MJ[1][1];
	M[1][3] = M[1][3]+Jg[0][1]*MJ[0][3]+Jg[1][1]*MJ[1][3];
	M[1][4] = M[1][4]+Jg[0][1]*MJ[0][4]+Jg[1][1]*MJ[1][4];
	M[3][1] = M[3][1]+Jg[0][3]*MJ[0][1]+Jg[1][3]*MJ[1][1];
	M[3][3] = M[3][3]+Jg[0][3]*MJ[0][3]+Jg[1][3]*MJ[1][3]+Jg[5][3]*MJ[5][3];
	M[3][4] = M[3][4]+Jg[0][3]*MJ[0][4]+Jg[1][3]*MJ[1][4]+Jg[5][3]*MJ[5][4];
	M[4][1] = M[4][1]+Jg[0][4]*MJ[0][1]+Jg[1][4]*MJ[1][1];
	M[4][3] = M[4][3]+Jg[0][4]*MJ[0][3]+Jg[1][4]*MJ[1][3]+Jg[5][4]*MJ[5][3];
	M[4][4] = M[4][4]+Jg[0][4]*MJ[0][4]+Jg[1][4]*MJ[1][4]+Jg[5][4]*MJ[5][4];

	/* Force vector calculations */
	qa[0] = qa[0]+Mq[0][0]*accelq[0];
	qa[1] = qa[1]+Mq[1][1]*accelq[1];

	f[1] = f[1]+Jg[0][1]*qa[0]+Jg[1][1]*qa[1];
	f[3] = f[3]+Jg[0][3]*qa[0]+Jg[1][3]*qa[1];
	f[4] = f[4]+Jg[0][4]*qa[0]+Jg[1][4]*qa[1];


	/* Body MB */

	/* Jacobian matrix */
	Jg[0][1] = cos(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*sin(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_));
	Jg[1][1] = - 
	 1.0*sin(elBo_EA2_1_rot_z_)*(sin(elBo_EA2_2_rot_z_)*sin(r_beta2) + 
	 cos(elBo_EA2_2_rot_z_)*cos(r_beta2)) - 
	 1.0*cos(elBo_EA2_1_rot_z_)*(cos(elBo_EA2_2_rot_z_)*sin(r_beta2) - 
	 1.0*cos(r_beta2)*sin(elBo_EA2_2_rot_z_));
	Jg[0][3] = elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_) - 
	 1.0*sin(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0));
	Jg[1][3] = d_mb - 
	 1.0*cos(elBo_EA2_1_rot_z_)*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) + 
	 elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_);
	Jg[5][3] = 1.0;
	Jg[0][4] = sin(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 
	 0.000000000000038066427647350752645032930885902*cos(elBo_EA2_1_rot_z_) - 
	 0.11469314458245681576098462528535*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_);
	Jg[1][4] = 0.000000000000038066427647350752645032930885902*sin(elBo_EA2_1_rot_z_) - 
	 0.18524088272676737798594359674098*d_mb + 
	 cos(elBo_EA2_1_rot_z_)*(0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 0.11469314458245681576098462528535*elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0)) - 
	 0.11469314458245681576098462528535*elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_);
	Jg[5][4] = - 
	0.18524088272676737798594359674098;

	/* Mass matrix */
	Mq[0][0] = m_mb;
	Mq[1][1] = m_mb;
	Mq[2][2] = m_mb;
	Mq[5][5] = I_mb;

	/* Vector of local accelerations */
	accelq[0] = (2.0*DelBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_) + 
	 elBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2))*(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2) - 
	 1.0*cos(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 sin(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_))) + 
	 d_mb*pow(DelBo_EA2_1_rot_z_ - 
	 1.0*DelBo_EA2_2_rot_z_ + 
	 Dr_beta2,2.0);
	accelq[1] = (2.0*DelBo_EA2_1_x_*cos(elBo_EA2_1_rot_z_) - 
	 1.0*elBo_EA2_1_x_*sin(elBo_EA2_1_rot_z_)*(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2))*(DelBo_EA2_2_rot_z_ - 
	 1.0*Dr_beta2) + 
	 sin(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) - 
	 2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_)) + 
	 Dr_beta2*(elBo_EA2_2_x_*pow(cos(elBo_EA2_2_rot_z_),2.0)+ 
	 elBo_EA2_2_x_*pow(sin(elBo_EA2_2_rot_z_),2.0))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)) + 
	 sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_1_rot_z_)*(Dr_beta2*(2.0*cos(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) - 
	 1.0*DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_)) + 
	 2.0*sin(elBo_EA2_2_rot_z_)*(DelBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) + 
	 DelBo_EA2_2_rot_z_*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_))) + 
	 cos(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*sin(elBo_EA2_2_rot_z_) - 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*cos(elBo_EA2_2_rot_z_)) - 
	 1.0*sin(elBo_EA2_2_rot_z_)*(pow(DelBo_EA2_2_rot_z_,2.0)*elBo_EA2_2_x_*cos(elBo_EA2_2_rot_z_) + 
	 2.0*DelBo_EA2_2_x_*DelBo_EA2_2_rot_z_*sin(elBo_EA2_2_rot_z_)));

	/* Vector of applied forces qa */
	clearVector(7,qa);
	qa[2] = - 
	1.0*g*m_mb;

	/* Elastic forces h_e */

	/* Forces due to choice of frame of reference h_omega */

	/* Mass matrix calculations */
	MJ[0][1] = Mq[0][0]*Jg[0][1];
	MJ[0][3] = Mq[0][0]*Jg[0][3];
	MJ[0][4] = Mq[0][0]*Jg[0][4];
	MJ[1][1] = Mq[1][1]*Jg[1][1];
	MJ[1][3] = Mq[1][1]*Jg[1][3];
	MJ[1][4] = Mq[1][1]*Jg[1][4];
	MJ[5][3] = Mq[5][5]*Jg[5][3];
	MJ[5][4] = Mq[5][5]*Jg[5][4];

	M[1][1] = M[1][1]+Jg[0][1]*MJ[0][1]+Jg[1][1]*MJ[1][1];
	M[1][3] = M[1][3]+Jg[0][1]*MJ[0][3]+Jg[1][1]*MJ[1][3];
	M[1][4] = M[1][4]+Jg[0][1]*MJ[0][4]+Jg[1][1]*MJ[1][4];
	M[3][1] = M[3][1]+Jg[0][3]*MJ[0][1]+Jg[1][3]*MJ[1][1];
	M[3][3] = M[3][3]+Jg[0][3]*MJ[0][3]+Jg[1][3]*MJ[1][3]+Jg[5][3]*MJ[5][3];
	M[3][4] = M[3][4]+Jg[0][3]*MJ[0][4]+Jg[1][3]*MJ[1][4]+Jg[5][3]*MJ[5][4];
	M[4][1] = M[4][1]+Jg[0][4]*MJ[0][1]+Jg[1][4]*MJ[1][1];
	M[4][3] = M[4][3]+Jg[0][4]*MJ[0][3]+Jg[1][4]*MJ[1][3]+Jg[5][4]*MJ[5][3];
	M[4][4] = M[4][4]+Jg[0][4]*MJ[0][4]+Jg[1][4]*MJ[1][4]+Jg[5][4]*MJ[5][4];

	/* Force vector calculations */
	qa[0] = qa[0]+Mq[0][0]*accelq[0];
	qa[1] = qa[1]+Mq[1][1]*accelq[1];

	f[1] = f[1]+Jg[0][1]*qa[0]+Jg[1][1]*qa[1];
	f[3] = f[3]+Jg[0][3]*qa[0]+Jg[1][3]*qa[1];
	f[4] = f[4]+Jg[0][4]*qa[0]+Jg[1][4]*qa[1];

}

/* Auxiliary dynamics */
void FLX_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr){


}


/* Equations of motion */
void FLX_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr){

	struct FLX_paraStruct *data  = (struct FLX_paraStruct *) dataPtr;

	/* Allocate memory */
	int i_ = 0;
	double **M     = callocMatrix(FLX_SYS_DOF,FLX_SYS_DOF);
	double *temp   = calloc(FLX_NUM_INDEPENDENT_GC,sizeof(double));

	double **Jr_v  = NULL;
	double **Jr_a  = NULL;
	double **Jl_a  = NULL;
	double *theta_ = NULL;
	double *gamma_ = NULL;

	clearVector((FLX_SYS_DOF),&(dx[FLX_SYS_DOF]));

	FLX_system_dynamics(t, x_, u_, &(dx[FLX_SYS_DOF]), M, dataPtr);

	memcpy(dx, &(x_[FLX_SYS_DOF]), FLX_SYS_DOF*sizeof(double));

	/* Jacobian, velocities and accelerations due to the constraints */
	FLX_constraintEquations(t, x_, u_, dataPtr);

	Jr_v  = data->con->Jr_v;
	Jr_a  = data->con->Jr_a;
	Jl_a  = data->con->Jl_a;
	theta_ = data->con->theta_;
	gamma_ = data->con->gamma_;

	/* Projection of the velocities */
	for (i_ = 0; i_ < FLX_SYS_DOF; i_++)
		dx[i_] = dx[i_] - theta_[i_];
	MatrixVectorMultiplication( 'T', FLX_NUM_INDEPENDENT_GC, FLX_SYS_DOF, 1.0, Jr_v, dx, 0.0, temp);
	for (i_ = 0; i_ < FLX_SYS_DOF; i_++)
		dx[i_] = theta_[i_];
	MatrixVectorMultiplication( 'N', FLX_SYS_DOF, FLX_NUM_INDEPENDENT_GC, 1.0, Jr_v, temp, 1.0, dx);

	/* Projection of the accelerations */
	projectSystemDynamics( 1, FLX_SYS_DOF, FLX_NUM_INDEPENDENT_GV, Jr_a, Jl_a, M, &(dx[FLX_SYS_DOF]), gamma_);

	/* Free allocated memory */
	free(temp);
	freeMatrix(FLX_SYS_DOF,FLX_SYS_DOF,M);

}
