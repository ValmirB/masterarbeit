% use this after feedback = timeInt(...)

cpos = 100*ones(sys.counters.constraint,length(feedbackFF.x));
for i=1:length(feedbackFF.x)
    cpos(:,i) = positionConstraints(feedbackFF.x(1,i),feedbackFF.y(:,i),[]);
end

Dy = zeros(sys.counters.genCoord*2,length(feedbackFF.x));
for ii=1:sys.counters.genCoord*2
   Dy(ii,:) = offlineDiff(feedbackFF.x,feedbackFF.y(ii,:)); 
end

cvel = 100*ones(sys.counters.constraint,length(feedbackFF.x));
cacc = 100*ones(sys.counters.constraint,length(feedbackFF.x));

for i=1:length(feedbackFF.x)
    [C_, G_, dc_, d2c_] = constraintDerivatives(feedbackFF.x(1,i),feedbackFF.y(:,i),[]);
    cvel(:,i) = C_ * feedbackFF.y(sys.counters.genCoord+1:end,i) + dc_;
    cacc(:,i) = C_ *Dy(sys.counters.genCoord+1:end,i) + d2c_;
end