#include <math.h>
#include <mex.h>
#include <matrix.h>
#include "SC_pi_code.h"
#include "SC_userDefined.h"
#include "SC_pd_matlab.h"
#include "neweul.h"
#include "SC_paraStruct.h"
#include "SC_Flexor.h"

#include "SC_constraintEquations.h"

void mexFunction ( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ){

	void *dataPtr        = NULL;

	double *t_vec        = NULL;
	double t             = 0;
	double *x_red        = NULL;
	double *y_ind        = NULL;
	double *Dy_ind       = NULL;
	double *x_           = calloc(2*SC_SYS_DOF, sizeof(double));
	double *dx_          = calloc(2*SC_SYS_DOF, sizeof(double));
	double *matlabReturn = NULL;
	double *u_vec        = NULL;
    double *u_help       = NULL;
    double *u_           = calloc( SC_NUM_ALL_INPUTS, sizeof(double));
	size_t numStates     = 1;
	int counter_         = 0;
	int vec_loop         = 0;
	struct SC_paraStruct *data = NULL;

//#if defined SC_NUM_ALL_INPUTS && SC_NUM_ALL_INPUTS > 0
//	u_ = calloc( SC_NUM_ALL_INPUTS, sizeof(double));
//#endif

	/* Initialize System Struct */
	dataPtr = SC_initializeSystemStruct();

	data  = (struct SC_paraStruct *) dataPtr;

	/* Error handling*/
	if (nrhs<3){
		mexPrintf("Please provide four input arguments!\n");
		goto clean_up;
	}

	t_vec = mxGetPr( prhs[0] );
	x_red = mxGetPr( prhs[1] );
    u_vec = mxGetPr( prhs[2] );

	if (nlhs>1){
		mexPrintf("Only one output is allowed!\n");
		goto clean_up;
	}

	if ((nrhs==4) && (mxIsStruct(prhs[3]))){
		SC_getSystemStruct(dataPtr, (const void *) prhs[3]);
	}

	/* Get number of right-hand sides */
	numStates = mxGetN(prhs[1]);

	if (((mxGetM(prhs[0]) * mxGetN(prhs[0])) != numStates)){
		mexPrintf("The dimensions of the passed arguments don not match!\n");
		goto clean_up;
	}

	if ((mxGetM(prhs[1])!=2*SC_NUM_INDEPENDENT_GC)){
		mexPrintf("The second input argument has to be a 6xnumStates vector!\n");
		goto clean_up;
	}

	/* Allocate function output */
	plhs[0] = mxCreateDoubleMatrix(2*SC_NUM_INDEPENDENT_GC, numStates, mxREAL);
	matlabReturn = mxGetPr(plhs[0]);

	for ( vec_loop = 0; vec_loop < (int) numStates; vec_loop++){

		t = t_vec[vec_loop];
        u_help = &(u_vec[SC_NUM_ALL_INPUTS*vec_loop]);
		y_ind = &(x_red[2*SC_NUM_INDEPENDENT_GC*vec_loop]);
		Dy_ind = &(x_red[2*SC_NUM_INDEPENDENT_GC*vec_loop+SC_NUM_INDEPENDENT_GC]);
        
        /* Get inputs u */
        for ( counter_ = 0 ; counter_ < SC_NUM_ALL_INPUTS ; counter_++ ){
			u_[counter_] = u_help[counter_];
		}
        
		/* Get dependent generalized coordinates and velocities */
		for ( counter_ = 0 ; counter_ < SC_NUM_INDEPENDENT_GC ; counter_++ ){
			x_[data->independentIndices[counter_]] = y_ind[counter_];
			x_[data->independentIndices[counter_]+SC_SYS_DOF] = Dy_ind[counter_];
		}

		SC_educatedGuess(t, x_, dataPtr);

		SC_getDependentGenCoords(t, x_, u_, dataPtr);

		/* Compute all inputs */
		//SC_f_control_inputs(t, x_, &(u_[0]), dataPtr);

		/* Evaluate system dynamics */
		SC_equations_of_motion(t, x_, u_, dx_, dataPtr);

		/* Move temporary memory to Matlab output pointer */
		for ( counter_ = 0 ; counter_ < SC_NUM_INDEPENDENT_GC ; counter_++ ){
			matlabReturn[vec_loop*2*SC_NUM_INDEPENDENT_GC+counter_] = dx_[data->independentIndices[counter_]];
			matlabReturn[vec_loop*2*SC_NUM_INDEPENDENT_GC+counter_+SC_NUM_INDEPENDENT_GC] = dx_[data->independentIndices[counter_]+SC_SYS_DOF];
		}

	}

	/* Free System Struct */
clean_up:
	SC_freeStructure(dataPtr);
//#if defined SC_NUM_ALL_INPUTS && SC_NUM_ALL_INPUTS > 0
	free(u_);
//#endif
	free(x_);
	free(dx_);

}

