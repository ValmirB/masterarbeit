% quick and dirty Ccode correction for rigid systems
function correctRigidCcode
global sys
cd cFunctions
file = [sys.info.systemSetup.systemExportPrefix,'_pi_code.c'];

fid = fopen(file, 'rt');
X = fread(fid);
fclose(fid);

X = char(X.');
Y = strrep(X, 'double Mq_coup[6][0] = {{0.0}};', '//double Mq_coup[6][0] = {{0.0}};');

fid2 = fopen(file, 'wt');
fwrite(fid2,Y);
fclose(fid2);

filesToMex = {'Flexor',...
            'constraintEquations',...
            'constraintDerivatives',...
            'pi_code',...
            'paraStruct',...
            'userDefined',...
            'pd_matlab',...
            'linearizeMBS'};
        
strToMex = '';
for ii = 1:length(filesToMex)
    strToMex = [strToMex, sys.info.systemSetup.systemExportPrefix,'_', filesToMex{ii}, '.c '];
end
%linkfilelist = [linkfilelist, ' ', filelist{k}(1:end-2), '.obj'];
eval(['mex ', strToMex, 'neweul.c']);

cd ..\
end