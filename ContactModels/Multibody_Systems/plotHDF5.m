clear all; close all; clc

data = h5load('Modal_1_line_1.0s.h5');

t = data.desired.time;
u1 = data.desired.u1;
u2 = data.desired.u2;
s1 = data.desired.s1;
s2 = data.desired.s2;

% Plot
blue = [0, 0.39, 0.87];
red = [0.86, 0.13, 0.3];
green = [0, 0.55, 0];
violet = [0.4 0 0.4];
mum_tuerkis = [0.1765, 0.7765, 0.8392];
mum_blue = [0.0941, 0.2314, 0.3961];

figure(1)
plot(t, u1, 'Color', blue, 'LineWidth', 2,'DisplayName','u_1')
hold on
plot(t, u2, 'Color', red, 'LineWidth', 2,'DisplayName','u_2')
grid on
legend('toggle')
ylabel('Control [A]')
xlabel('time [s]')

figure(2)
plot(t, s1, 'Color', blue, 'LineWidth', 2,'DisplayName','s_1')
hold on
plot(t, s2, 'Color', red, 'LineWidth', 2,'DisplayName','s_2')
grid on
legend('toggle')
ylabel('Carposition [m]')
xlabel('time [s]')