function defineGraphics_basic(varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - create graphic visualization of modeling elements - %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global sys

% inital settings
use_STL_ = false;
use_third_Arm_ = false;
orientation_third_Arm_ = 'horizontal';

% changing inital settings
for n_ = 2 : 2 : length(varargin)
    switch varargin{n_ - 1}
        case 'use_STL'
            use_STL_ = varargin{n_};
        case 'use_third_Arm'
            use_third_Arm_ = varargin{n_};
        case 'orientation_third_Arm'
            orientation_third_Arm_ = varargin{n_};
        otherwise
            disp('---------- Attention: Input does not match any option! ----------');
            keyboard;
    end
end


d_axes = sys.parameters.data.d_axes;
sys.graphics.surfaces = [];


% Colors
mum_tuerkis = [0.1765, 0.7765, 0.8392];
mum_blue = [0.0941, 0.2314, 0.3961];
black = [0.3 0.3 0.3];
dark_grey = [0.5 0.5 0.5];
silver = [0.75 0.75 0.75;]; 
middle_grey = [0.85 0.85 0.85];
light_grey = [0.99 0.99 0.99];



% Führungsplatte
h(1) = drawCube('c', [0.0 0.0 -0.005], 'd', [0.2, 1.12, 0.01], 'FaceColor', light_grey);
h(2) = drawCube('c', [d_axes + 0.1 0.0 -0.005], 'd', [1.12, 0.2, 0.01], 'FaceColor', light_grey);
addGraphics('ISYS',h);

% Elastic Arms
drawMesh('Id', 'EA1', 'FaceColor', middle_grey);
drawMesh('Id', 'EA2', 'FaceColor', middle_grey);
if use_third_Arm_
   drawMesh('Id', 'EA3', 'FaceColor', middle_grey); 
end


% Components (created out of CAD-Files or not)
if use_STL_
    % get Path of Files & add them
    %currentDir_ = pwd
    fullpath = '../../../CAD/STLs';
    
    bodenplatte = fullfile(fullpath, 'Bodenplatte.stl');
    achse = fullfile(fullpath, 'Achse.stl');
    schlitten = fullfile(fullpath, 'Schlitten.stl');
    schlittenklemm = fullfile(fullpath, 'SchlittenKlemmung.stl');
    adapterplatte = fullfile(fullpath, 'Adapterplatte.stl');
    schlittenlager_y = fullfile(fullpath, 'Schlittenlager_y.stl');
    schlittenlager_x = fullfile(fullpath, 'Schlittenlager_x.stl');
    gabel = fullfile(fullpath, 'Gabel.stl');
    gabellager = fullfile(fullpath, 'Gabellager.stl');
    endeffector = fullfile(fullpath, 'Endeffector.stl'); 
    if use_third_Arm_
        emotor = fullfile(fullpath, 'E_Motor.stl');
        motorgabel = fullfile(fullpath, 'E_Motor_Gabel.stl');
    end

    
    % Bodenplatte
    h = drawSTL('FileName', bodenplatte, 'd', [1e-2, 1e-2, 1e-2], 'FaceColor', dark_grey);
    transformGraphics(h,'Translation',[0.55 0.0 -0.06]);
    addGraphics('ISYS',h);
    
    % Linearaxle 1
    h(1) = drawSTL('FileName', achse, 'd', [1e-2, 1e-2, 1e-2], 'FaceColor', light_grey);
    transformGraphics(h(1),'RotationAngles',[pi/2 pi/2 0]);

    % Linearaxle 2
    h(2) = drawSTL('FileName', achse, 'd', [1e-2, 1e-2, 1e-2], 'FaceColor', light_grey);
    transformGraphics(h(2),'RotationAngles',[pi/2 0 0]);
    transformGraphics(h(2),'Translation', [d_axes+0.1 0 0]);
    addGraphics('ISYS',h);

    % Slider 1
    h = drawSTL('FileName', schlitten, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', black);
    transformGraphics(h,'RotationAngles',[pi/2 -pi/2 0]);
    addGraphics('C1',h);
    
    % Slider 2
    h = drawSTL('FileName', schlittenklemm, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', black);
    transformGraphics(h,'RotationAngles',[pi/2 0 0],'Translation',[0.029,0,0]);
    addGraphics('C2',h);
    
    % Adapterplatte 1 & 2
    h = drawSTL('FileName', adapterplatte, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
    transformGraphics(h,'Translation', [0 0 0.088]);
    transformGraphics(h,'RotationAngles',[pi 0 0]);
    addGraphics('C1',h);
    h = drawSTL('FileName', adapterplatte, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
    transformGraphics(h,'Translation', [0 0 0.088]);
    transformGraphics(h,'RotationAngles',[pi 0 0]);
    addGraphics('C2',h);
    
    % Schlittenlager_y
    h = drawSTL('FileName', schlittenlager_y, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
    addGraphics('EA1_1',h);
    
    % Schlittenlager_x
    h = drawSTL('FileName', schlittenlager_x, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
    addGraphics('EA2_2',h);  
    
    % Kinematic Loop
    h = drawSTL('FileName', gabel, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
    transformGraphics(h,'RotationAngles',[0 pi 0]);
    transformGraphics(h,'Translation', [-0.082 0 0]);
    addGraphics('EA1_2',h);  
    h = drawSTL('FileName', gabellager, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
    transformGraphics(h,'RotationAngles',[0 0 -pi/2]);
    addGraphics('EA2_1',h);

    % Endeffector
    h = drawSTL('FileName', endeffector, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
    transformGraphics(h,'RotationAngles',[0 pi 0]); 
    addGraphics('EE',h);
    
    % Third Arm
    if use_third_Arm_
    	h = drawSTL('FileName', emotor, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
        if strcmp(orientation_third_Arm_, 'vertical')  
            transformGraphics(h,'RotationAngles',[-pi/2 0 0]);
        end
        addGraphics('EA2_3',h);
    	h = drawSTL('FileName', motorgabel, 'd', [1e-2, 1e-2, 1e-2],'FaceColor', silver);
        if strcmp(orientation_third_Arm_, 'vertical')  
            transformGraphics(h,'RotationAngles',[pi 0 0]);
        end
        addGraphics('EA3_1',h);        
    end
    
else
    % Bodenplatte
    h = drawCube('c', [0.55 0.0 -0.06], 'd', [2, 1.25, 0.1], 'FaceColor',  dark_grey);
    addGraphics('ISYS',h);
    
    % Linearaxle 1 & 2
    h(1) = drawCube('c', [0.0 0.0 0.03], 'd', [0.115, 1.12, 0.06], 'FaceColor', light_grey);
	h(2) = drawCube('c', [d_axes+0.1 0.0 0.03], 'd', [1.12, 0.115, 0.06], 'FaceColor', light_grey);
    addGraphics('ISYS',h);
    
    % Slide 1 & 2
    h = drawCube('c', [0 0 0.06], 'd', [0.165, 0.25, 0.08], 'FaceColor', black);
    addGraphics('C1',h);    
    h = drawCube('c', [0 0 0.06], 'd', [0.25, 0.165, 0.08], 'FaceColor', black);
    addGraphics('C2',h);
    
    % Adapterplatten 1 & 2
    h = drawCube('c', [0 0 0.1085], 'd', [0.16, 0.14, 0.017], 'FaceColor', silver);
    addGraphics('C1',h);    
    h = drawCube('c', [0 0 0.1085], 'd', [0.14, 0.16, 0.017], 'FaceColor', silver);
    addGraphics('C2',h);
    
    % Schlittenlager_y
    h = drawCube('c', [0.014 0 0], 'd', [0.06, 0.04, 0.08], 'FaceColor', silver);
    addGraphics('EA1_1',h);
    
    % Schlittenlager_x
    h = drawCube('c', [0 0 0], 'd', [0.088, 0.04, 0.08], 'FaceColor', silver);
    addGraphics('EA2_2',h);
    
    % Kinematic Loop
    h = drawCube('c', [-0.115 0 0], 'd', [0.02, 0.04, 0.08], 'FaceColor',  silver);
    addGraphics('EA1_2',h);
    h = drawCube('c', [-0.053 0 0.045], 'd', [0.126, 0.04, 0.01], 'FaceColor',  silver);
    addGraphics('EA1_2',h);
    h = drawCube('c', [-0.053 0 -0.045], 'd', [0.126, 0.04, 0.01], 'FaceColor',  silver);
    addGraphics('EA1_2',h);
    h = drawCube('c', [0.014 0 0], 'd', [0.06, 0.04, 0.08], 'FaceColor', silver);
    addGraphics('EA2_1',h);
    
    % Endeffektor
    h = drawCube('c', [-0.02 0 0], 'd', [0.06, 0.04, 0.08], 'FaceColor',  silver);
    addGraphics('EE',h);
    
    % Third Arm
    if use_third_Arm_
        h(1) = drawCube('c', [-0.077 0 0], 'd', [0.01, 0.08, 0.08], 'FaceColor', silver);
        h(2) = drawCube('c', [-0.026 0 0.035], 'd', [0.092, 0.08, 0.01], 'FaceColor', silver);
    	h(3) = drawCube('c', [0 0 -0.005], 'd', [0.06, 0.06, 0.07], 'FaceColor',  silver);
        if strcmp(orientation_third_Arm_, 'vertical')  
            transformGraphics(h([2 3]),'RotationAngles',[-pi/2 0 0]);
        end
        addGraphics('EA2_3',h);
        h(1) = drawCube('c', [0.1 0 0], 'd', [0.01, 0.08, 0.08], 'FaceColor', silver);
        h(2) = drawCube('c', [0.0425 0 0.045], 'd', [0.125, 0.08, 0.01], 'FaceColor', silver);
        if strcmp(orientation_third_Arm_, 'vertical')
            transformGraphics(h([1 2]),'RotationAngles',[pi 0 0]);
        end
        addGraphics('EA3_1',h);
    end
    
end