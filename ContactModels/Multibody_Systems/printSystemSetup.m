%%  print the used settings
function printSystemSetup
    global sys
   
    %%Table style
    fprintf('\nThe following system has been created:\n');
    fprintf('\t- system type...\t\t\t %s\n',sys.info.systemSetup.systype);
    fprintf('\t- equation type...\t\t\t %s\n',sys.info.systemSetup.Equation_Type);
    fprintf('\t- actuators as...\t\t\t %s\n',sys.info.systemSetup.carpos);
    fprintf('\t- geometric loop...\t\t\t %s\n',sys.info.systemSetup.geometric_loop);
    fprintf('\t- tracking point...\t\t\t %s\n',sys.info.systemSetup.EE_Tracking_Point);
    fprintf('\t- trajectory...\t\t\t\t %s\n',sys.info.systemSetup.trajectoryType);
    fprintf('\t- SID-file first Arm... \t %s\n',sys.info.systemSetup.sid_first_Arm);
    fprintf('\t- SID-file second Arm...\t %s\n',sys.info.systemSetup.sid_second_Arm);

    if sys.info.systemSetup.use_third_Arm
        fprintf('\t  Third am is used!\n');
        fprintf('\t- SID-file third Arm...\t\t %s\n',sys.info.systemSetup.sid_third_Arm);
        fprintf('\t- orientation third Arm...\t %s\n',sys.info.systemSetup.orientation_third_Arm);
    else
        fprintf('\t- Third am is NOT used!\n');
    end
    
    
end