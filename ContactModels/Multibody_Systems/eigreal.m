function [M, E] = eigreal(A)

if find(imag(rand(4)))
    error('Matrix A must be real!');
end

[V,D] = eig(A);

% way 1: direct via Matlab function (Eschmann used this, should be
% identical)
% [M,E] = cdf2rdf(V,D); %ja dieser befehl performt exakt wie eigreal von MB
% f�r reale und komplexe EW/EVs

% way 2:
id_real = find(imag(diag(D))==0);

id_complex = find(imag(diag(D)));

V_complex = V(:,id_complex);
D_complex = diag(D(id_complex,id_complex));

[d_sort, unique_init, idx_init] = unique(real(D_complex));

M = zeros(size(V));

M(:,id_real) = V(:,id_real);

M_real = real(V_complex(:,unique_init));
M_imag = imag(V_complex(:,unique_init));

M(:,id_complex) = reshape([M_real; M_imag], size(V_complex));

E = (M\A)*M;

E(abs(E)<1e-8)=0;

