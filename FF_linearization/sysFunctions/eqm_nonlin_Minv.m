function Minv_ = eqm_nonlin_Minv(t, x_, u_, varargin)
% EQM_NONLIN_MINV - Inverse mass matrix of the system Flexor

% Inverse mass matrix

Minv_ = eqm_nonlin_M(t, x_, u_)\eye(sys.counters.genCoord);


% END OF FILE

