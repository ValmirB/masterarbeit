function result_ = eqm_nonlin_h_e(t, x_, u_, varargin)
% EQM_NONLIN_H_E of the system Flexor
% Vector of the inner elastic forces
% See eqm_nonlin_q.m for more information

global sys;

% generalized coordinates, velocities and auxiliary coordinates

EA2_q001 = x_(6);
EA3_q001 = x_(7);
DEA2_q001 = x_(13);
DEA3_q001 = x_(14);


% Force vector

h_e_ = cell(6,1);


h_e_{1} = zeros(6,1);


h_e_{2} = zeros(6,1);


h_e_{3} = zeros(6,1);


h_e_{4} = zeros(7,1);

h_e_{4}(7) = - ...
	 (5764607514228399*DEA2_q001)/2305843009213693952 - ...
	 (9007199240981873*EA2_q001)/9007199254740992;

h_e_{5} = zeros(7,1);

h_e_{5}(7) = - ...
	 (5764607507962807*DEA3_q001)/2305843009213693952 - ...
	 (4503599615595943*EA3_q001)/4503599627370496;

h_e_{6} = zeros(6,1);

result_ = vertcat(h_e_{:});


% END OF FILE

