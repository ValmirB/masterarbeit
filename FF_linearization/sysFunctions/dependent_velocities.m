function Dy_ = dependent_velocities(t, y_, Dy_i, varargin)
% copied file
% dependent_velocities - Compute all velocities in dependence of the independent velocities

global sys;

idxDep_ = sys.parameters.coordinates.idx_depVelo;
idxInd_ = sys.parameters.coordinates.idx_indVelo;

% Generalized velocities
Dy_ = zeros(sys.counters.genCoord,1);
Dy_(idxInd_) = Dy_i;

x_ = [y_; Dy_];

% Evaluate all inputs
u_ = inputs_all(t, y_);

% Evaluation of relative kinematics and calculation absolute kinematics
if ~isempty(varargin)&&isstruct(varargin{1})
	kin = varargin{1};
else
	kin = evalAbsoluteKinematics(t, x_, u_, 1);
end

% Constraint derivatives
[C_, G_, Dc_, D2c_] = constraintDerivatives(t, x_, u_, kin);

% Compute dependent velocities
Dy_(idxDep_) = -C_(:,idxDep_)\(C_(:,idxInd_)*Dy_i+Dc_);

% END OF FILE
