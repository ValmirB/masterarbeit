function k_ = eqm_nonlin_k(t, x_, u_, varargin)
% EQM_NONLIN_K of the system Flexor
% Vector of generalized Coriolis, centrifugal and gyroscopic forces
% For flexible bodies, the elastic forces are NOT included.
% The elastic forces are a part of the q-vector.

% Global jacobian matrix
Jg_ = eqm_nonlin_Jg(t, x_, u_);

% Global generalized mass matrix
Mq_ = eqm_nonlin_Mq(t, x_, u_);

% Vector of local accelerations, of the frame of reference
accelq_ = eqm_nonlin_accelq(t, x_, u_);

% Vector of forces of local accelerations, 
% caused by the choice of the frames of reference,
% being not in the centers of gravity
h_omega_ = eqm_nonlin_h_omega(t, x_, u_);

% Vector of generalized Coriolis, centrifugal and gyroscopic forces
k_ = transpose(Jg_)*(Mq_*accelq_+h_omega_);


% END OF FILE

