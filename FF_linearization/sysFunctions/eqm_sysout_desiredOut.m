function result_ = eqm_sysout_desiredOut(t, x_, u_, varargin)
% eqm_sysout_desiredOut - Vector of the system Flexor output desiredOut
% 
% Entries are as in sys.model.output.desiredOut

global sys;
% system inputs
EE_x = u_(1);
DEE_x = u_(2);
EE_y = u_(4);
DEE_y = u_(5);

% System output desiredOut vector
des_x = EE_x;
des_y = EE_y;
des_Dx = DEE_x;
des_Dy = DEE_y;

result_ = zeros(4,1);

result_(1) = des_x;
result_(2) = des_y;
result_(3) = des_Dx;
result_(4) = des_Dy;

% END OF FILE 
