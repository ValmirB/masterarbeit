e0 = -0.002;

P = 5;
D = 3;

e1 = -P/2+sqrt((P^2/4)-D);
e2 = -P/2-sqrt((P^2/4)-D);


C2 = (-e0*e1)/(e1+e2);
C1 = e0-C2;

% e = C1*e^(e1*t)+C2*e^(e2*t);
error = zeros(8000,1);
i=1;
for t=0:1e-3:8
    
    error(i,1) = C1*exp(e1*t)+C2*exp(e2*t);
    i=i+1;
end

plot(0:1e-3:8,error)