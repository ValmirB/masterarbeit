#ifndef SCg3A2D_constraintEquations_H
#define SCg3A2D_constraintEquations_H

#include "SCg3A2D_paraStruct.h"

void SCg3A2D_constraintEquations(double t, double *x_, double *u_, void *dataPtr);

void SCg3A2D_getDependentGenCoords(double t, double *x_, double *u_, void *dataPtr);

void SCg3A2D_lagrangeMultipliers(double t, double *x_, double *u_, void *dataPtr);

void SCg3A2D_positionConstraints(double t, double *x_, double *u_, double *con_eq, double **C_dep, void *dataPtr);

void SCg3A2D_localVelocityConstraints(double t, double *x_, double *u_, double *Dc_, double **C_ind, void *dataPtr);

void SCg3A2D_jacobianPartition(double t, double *x_, double *u_, double **Jr, double **Jl, void *dataPtr);

#endif
