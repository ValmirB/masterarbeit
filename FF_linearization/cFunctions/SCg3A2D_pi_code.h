#ifndef SCg3A2D_pi_ind_H
#define SCg3A2D_pi_ind_H

#include "SCg3A2D_paraStruct.h"

/* System dynamics */
void SCg3A2D_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr);

/* Auxiliary dynamics */
void SCg3A2D_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

/* Equations of motion */
void SCg3A2D_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

#endif
