#include <math.h>
#include "SCg3A2D_paraStruct.h"
#include "SCg3A2D_pi_code.h"
#include "SCg3A2D_pd_matlab.h"
#include "SCg3A2D_userDefined.h"
#include "neweul.h"

void SCg3A2D_educatedGuess(double t, double *y_, void *dataPtr);

void SCg3A2D_educatedGuess(double t, double *y_, void *dataPtr){

	struct SCg3A2D_paraStruct *data  = (struct SCg3A2D_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
