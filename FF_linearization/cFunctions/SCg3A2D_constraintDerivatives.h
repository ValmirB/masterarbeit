#ifndef SCg3A2D_constraintDerivatives_H
#define SCg3A2D_constraintDerivatives_H

#include "SCg3A2D_paraStruct.h"

void SCg3A2D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
