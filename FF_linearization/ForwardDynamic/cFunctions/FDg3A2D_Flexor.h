#ifndef FDg3A2D_Flexor_H
#define FDg3A2D_Flexor_H
#define FDG3A2D_SYS_DOF (4)
#define FDG3A2D_AUX_DOF (0)
#define FDG3A2D_LOOPS_EXIST (1)
#define FDG3A2D_NUM_LOOPS (2)
#define FDG3A2D_NUM_POSCONSTRAINTS (2)
#define FDG3A2D_NUM_VELCONSTRAINTS (0)
#define FDG3A2D_NUM_INDEPENDENT_GC (2)
#define FDG3A2D_NUM_DEPENDENT_GC (2)
#define FDG3A2D_NUM_INDEPENDENT_GV (2)
#define FDG3A2D_NUM_DEPENDENT_GV (2)
#define FDG3A2D_NUM_FELEM (0)

#define FDG3A2D_NUM_INPUT_GROUPS (1)
#define FDG3A2D_NUM_INPUTS_CONTROL (9)
#define FDG3A2D_NUM_INPUTS {9}
#define FDG3A2D_NUM_ALL_INPUTS (9)
#define FDG3A2D_NUM_OUTPUT_GROUPS (3)
#define FDG3A2D_NUM_OUTPUTS_EXACTOUT (6)
#define FDG3A2D_NUM_OUTPUTS_APPROXOUT (6)
#define FDG3A2D_NUM_OUTPUTS_CURVATURE (4)
#define FDG3A2D_NUM_OUTPUTS {6, 6, 4}
#define FDG3A2D_NUM_ALL_OUTPUTS (16)
#endif
