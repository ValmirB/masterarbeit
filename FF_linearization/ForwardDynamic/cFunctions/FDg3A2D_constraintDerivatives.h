#ifndef FDg3A2D_constraintDerivatives_H
#define FDg3A2D_constraintDerivatives_H

#include "FDg3A2D_paraStruct.h"

void FDg3A2D_constraint_derivatives(double t, double *x_, double *u_, void *dataPtr);

#endif
