#ifndef FDg3A2D_pd_matlab_H
#define FDg3A2D_pd_matlab_H
#include <mex.h>
#include <stdio.h>
#include "FDg3A2D_paraStruct.h"

double FDg3A2D_getScalar(const void *context, const char *name, double defaultValue);

int FDg3A2D_getSystemStruct(void *dataPtr, const void *context);

void FDg3A2D_educatedGuess(double t, double *y_, void *dataPtr);
void FDg3A2D_educatedGuess(double t, double *y_, void *dataPtr);

#endif
