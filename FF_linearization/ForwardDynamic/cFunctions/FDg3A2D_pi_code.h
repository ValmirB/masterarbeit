#ifndef FDg3A2D_pi_ind_H
#define FDg3A2D_pi_ind_H

#include "FDg3A2D_paraStruct.h"

/* System dynamics */
void FDg3A2D_system_dynamics(double t, double *x_, double *u_,
		double *f, double **M, void *dataPtr);

/* Auxiliary dynamics */
void FDg3A2D_auxiliary_dynamics(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

/* Equations of motion */
void FDg3A2D_equations_of_motion(double t, double *x_, double *u_,
		double *dx, void *dataPtr);

#endif
