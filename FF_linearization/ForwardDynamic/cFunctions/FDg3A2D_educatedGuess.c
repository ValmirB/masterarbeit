#include <math.h>
#include "FDg3A2D_paraStruct.h"
#include "FDg3A2D_pi_code.h"
#include "FDg3A2D_pd_matlab.h"
#include "FDg3A2D_userDefined.h"
#include "neweul.h"

void FDg3A2D_educatedGuess(double t, double *y_, void *dataPtr);

void FDg3A2D_educatedGuess(double t, double *y_, void *dataPtr){

	struct FDg3A2D_paraStruct *data  = (struct FDg3A2D_paraStruct *) dataPtr;

	/* THIS FILE IS SUPPOSED TO BE EDITED BY THE USER! 
	 *        ALL MODIFICATIONS WILL BE KEPT         */

}
