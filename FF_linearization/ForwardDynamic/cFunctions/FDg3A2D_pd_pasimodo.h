#ifndef FDg3A2D_PD_PASIMODO_H
#define FDg3A2D_PD_PASIMODO_H
#define FDg3A2D_SYSSTRUCT (1)
#include <string.h>
#include "FDg3A2D_paraStruct.h"
#include "RadauParameters.h"

typedef double (*GetScalarFunctionType)(const void *, char *, double);
typedef int (*GetBoolFunctionType)(const void *, char *, int);
typedef int (*GetIntegerFunctionType)(const void *, char *, int);
typedef void (*LogFunctionType)(char *);

typedef char* (*GetSystemStructFunction)(int, const void*);

extern GetScalarFunctionType getScalar;
extern GetBoolFunctionType getBool;
extern GetIntegerFunctionType getInteger;
extern LogFunctionType logOutput;
extern LogFunctionType debugOutput;
extern LogFunctionType warning;
extern LogFunctionType error;


#define DEBUG(...)\
	{\
		char buffer[1000];\
		snprintf(buffer, 1000, __VA_ARGS__);\
		(*debugOutput)(buffer);\
	}


#define LOG(...)\
	{\
		char buffer[1000];\
		snprintf(buffer, 1000, __VA_ARGS__);\
		(*logOutput)(buffer);\
	}

void FDg3A2D_equationsOfMotion(int *n, double *t,
	double *x, double *f, struct FDg3A2D_paraStruct *data, int *ipar);

void FDg3A2D_minimalToGlobal(double t, double *minimal, double *global, struct FDg3A2D_paraStruct *data);

void FDg3A2D_massMatrix( int *n, double *am,
	int *lmas, struct FDg3A2D_paraStruct *data, int *ipar );

void FDg3A2D_jacobianMatrix ( int *n, double *t,
	double *x, double *dfy, int *ldfy, struct FDg3A2D_paraStruct *data, int *ipar );

typedef void (*RadauRightSide)(int *n, double *t,
  double *x, double *f, struct FDg3A2D_paraStruct *data, int *ipar);

typedef void (*RadauMAS)(int *n, double *am,
  int *lmas, struct FDg3A2D_paraStruct *data, int *ipar);

typedef void (*RadauJAC)(int *n, double *t,
  double *x, double *dfy, int *ldfy, struct FDg3A2D_paraStruct *data, int *ipar);


int FDg3A2D_getSystemStruct(struct FDg3A2D_paraStruct *data, const void *context);

void FDg3A2D_initMBS(int *dim, int *numbodies, GetSystemStructFunction FDg3A2D_getSystemStruct, const void *context);

void FDg3A2D_integrateMBS(double fromT, double toT, double *rhs, double *states, double *globalStates, GetSystemStructFunction FDg3A2D_getSystemStruct, radauParas *radauStruct, void *context);

void FDg3A2D_calcInitialStates(double t, double *states, double *globalStates, GetSystemStructFunction FDg3A2D_getSystemStruct__, const void *context);

void FDg3A2D_solOut(int *nr, double *told,
	double *t, double *x, double *cont, int *lrc,
	int *n, struct FDg3A2D_paraStruct *data, int *ipar, int *irtrn);


#ifdef FORTRANNOUNDER
/* Fotran functions without underscore */
#ifdef FORTRANUPP
/* Fotran functions without underscore  & UPPERCASE letters */
#define RADAU5_ RADAU5
#define CONTR5_ CONTR5
#else
/* Fotran functions without underscore  & lowercase letters */
#define RADAU5_ radau5
#define CONTR5_ contr5
#endif
#else
/* Fortran functions with underscore */
#ifdef FORTRANUPP
/* Fortran functions with underscore & UPPERCASE letters */
#else
/* Fortran functions with underscore & lowercase letters */
#define RADAU5_ radau5_
#define CONTR5_ contr5_
#endif
#endif

extern double CONTR5_ (int *i,
  double *x, double *cont, int *lrc);

extern void RADAU5_ (int *n, RadauRightSide fcn,
	double *t, double *x, double *tend,
	double *h, double *rtol, double *atol,
	int *itol, RadauJAC jac, int *ijac,
	int *mljac, int *mujac,
	RadauMAS mas, int *imas,
	int *mlmas, int *mumas,
	void *solOut, int *iout,
	double *work, int *lwork,
	int *iwork, int *liwork,
	struct FDg3A2D_paraStruct *data, int *ipar, int* idid);


#endif

